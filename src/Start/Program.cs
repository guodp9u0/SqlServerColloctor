﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Start {
    static class Program {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main() {
            bool flag = true;
            Mutex mutex = new Mutex(true, "ZhuanCloud", out flag);
            if (!flag) {
                MessageBox.Show("SQL专家云已经在运行了，可在任务栏查看", "SQL专家云");
                return;
            }
            WindowsIdentity current = WindowsIdentity.GetCurrent();
            WindowsPrincipal windowsPrincipal = new WindowsPrincipal(current);
            if (windowsPrincipal.IsInRole(WindowsBuiltInRole.Administrator)) {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new FrmMain());
                mutex.ReleaseMutex();
                return;
            }
            MessageBox.Show("请以管理员身份运行程序", "SQL专家云");
        }
    }
}
