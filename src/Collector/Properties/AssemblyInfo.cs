using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: AssemblyCompany("北京格瑞趋势科技有限公司")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("Copyright © 北京格瑞趋势科技有限公司  2016")]
[assembly: AssemblyDescription("SQL专家云收集工具")]
[assembly: AssemblyFileVersion("1.0.0")]
[assembly: AssemblyProduct("ZhuanCloud.Collector")]
[assembly: AssemblyTitle("ZhuanCloud.Collector")]
[assembly: AssemblyTrademark("")]
[assembly: CompilationRelaxations(8)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: ComVisible(false)]
[assembly: Guid("95358543-7fc5-491d-b55d-c795faa3ab3c")]
