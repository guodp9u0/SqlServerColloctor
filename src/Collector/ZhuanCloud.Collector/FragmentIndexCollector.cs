using System;
using System.Data;
using System.Data.OleDb;

namespace ZhuanCloud.Collector
{
	public class FragmentIndexCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, int timeOut)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(sqlString, timeOut);
				if (data.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblFragmentIndex\r\n        ( DBName ,\r\n          SchemaName ,\r\n          TableName ,\r\n          IndexName ,\r\n          AvgFragmentationInPercent ,\r\n          FragmentCount ,\r\n          PageCount ,\r\n          RowCount ,\r\n          UserSeeks ,\r\n          UserScans ,\r\n          UserLookups ,\r\n          LastUserSeek ,\r\n          LastUserScan ,\r\n          LastUserLookup\r\n        )\r\nVALUES  ( @DBName ,\r\n          @SchemaName ,\r\n          @TableName ,\r\n          @IndexName ,\r\n          @AvgFragmentationInPercent ,\r\n          @FragmentCount ,\r\n          @PageCount ,\r\n          @RowCount ,\r\n          @UserSeeks ,\r\n          @UserScans ,\r\n          @UserLookups ,\r\n          @LastUserSeek ,\r\n          @LastUserScan ,\r\n          @LastUserLookup\r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@DBName", OleDbType.VarChar)
						{
							SourceColumn = "db_name"
						},
						new OleDbParameter("@SchemaName", OleDbType.VarChar)
						{
							SourceColumn = "schema_name"
						},
						new OleDbParameter("@TableName", OleDbType.VarChar)
						{
							SourceColumn = "table_name"
						},
						new OleDbParameter("@IndexName", OleDbType.VarChar)
						{
							SourceColumn = "index_name"
						},
						new OleDbParameter("@AvgFragmentationInPercent", OleDbType.Integer)
						{
							SourceColumn = "avg_fragmentation_in_percent"
						},
						new OleDbParameter("@FragmentCount", OleDbType.Integer)
						{
							SourceColumn = "fragment_count"
						},
						new OleDbParameter("@PageCount", OleDbType.Integer)
						{
							SourceColumn = "page_count"
						},
						new OleDbParameter("@RowCount", OleDbType.Integer)
						{
							SourceColumn = "row_count"
						},
						new OleDbParameter("@UserSeeks", OleDbType.Integer)
						{
							SourceColumn = "user_seeks"
						},
						new OleDbParameter("@UserScans", OleDbType.Integer)
						{
							SourceColumn = "user_scans"
						},
						new OleDbParameter("@UserLookups", OleDbType.Integer)
						{
							SourceColumn = "user_lookups"
						},
						new OleDbParameter("@LastUserSeek", OleDbType.Date)
						{
							SourceColumn = "last_user_seek"
						},
						new OleDbParameter("@LastUserScan", OleDbType.Date)
						{
							SourceColumn = "last_user_scan"
						},
						new OleDbParameter("@LastUserLookup", OleDbType.Date)
						{
							SourceColumn = "last_user_lookup"
						}
					};
					result = base.SaveToAccess(oleDBString, data, cmdText, parameters);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, int timeOut)
		{
			string cmdText = "\r\nSET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED\r\nSELECT DISTINCT\r\n        DB_NAME(DB_ID()) [db_name] ,\r\n        dbschemas.name AS [schema_name] ,\r\n        dbtables.name AS table_name ,\r\n        dbindexes.name AS index_name ,\r\n        avg_fragmentation_in_percent ,\r\n        indexestats.fragment_count ,\r\n        indexestats.page_count ,\r\n        dps.row_count ,\r\n        di.user_seeks ,\r\n        di.user_scans ,\r\n        di.user_lookups ,\r\n        di.last_user_seek ,\r\n        di.last_user_scan ,\r\n        last_user_lookup\r\nFROM    sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, NULL) AS indexestats\r\n        INNER JOIN sys.tables dbtables ON dbtables.[object_id] = indexestats.[object_id]\r\n        INNER JOIN sys.schemas dbschemas ON dbtables.[schema_id] = dbschemas.[schema_id]\r\n        INNER JOIN sys.indexes AS dbindexes ON dbindexes.[object_id] = indexestats.[object_id]\r\n                                               AND indexestats.index_id = dbindexes.index_id\r\n        INNER JOIN ( SELECT   object_id ,\r\n                        SUM(row_count) AS row_count\r\n               FROM     sys.dm_db_partition_stats\r\n               WHERE    index_id < 2\r\n               GROUP BY object_id ,\r\n                        index_id\r\n             ) dps ON dbindexes.[object_id] = dps.[object_id]\r\n        LEFT JOIN sys.dm_db_index_usage_stats di ON dbindexes.[object_id] = di.[object_id]\r\n                                                    AND di.[database_id] = DB_ID()\r\nWHERE   avg_fragmentation_in_percent > 30\r\n        AND indexestats.page_count > 0\r\n        AND indexestats.index_type_desc <> 'HEAP'\r\n        AND dbindexes.name IS NOT NULL\r\nORDER BY dps.row_count DESC ,\r\n        avg_fragmentation_in_percent DESC\r\nOPTION (MAXDOP 2)\r\n";
			return base.FillDataTable(sqlString, cmdText, timeOut);
		}
	}
}
