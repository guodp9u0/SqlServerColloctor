using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;

namespace ZhuanCloud.Collector
{
	public class CollectConfig
	{
		[Browsable(false)]
		public List<Perf> AllPerfList
		{
			get;
			set;
		}

		[Browsable(false)]
		public List<string> DBList
		{
			get;
			set;
		}

		[Browsable(false)]
		public string OleDbString
		{
			get;
			set;
		}

		[Browsable(false)]
		public string SqlConString
		{
			get;
			set;
		}

		[Browsable(false)]
		public Version SZCVersion
		{
			get;
			set;
		}

		[Browsable(false)]
		public string DBInstanceName
		{
			get;
			set;
		}

		[Browsable(false)]
		public string TempDirectory
		{
			get;
			set;
		}

		[Browsable(false)]
		public string MoebiusDirectory
		{
			get;
			set;
		}

		[Category("\t\t\t\t\t\t\t性能计数器"), DefaultValue(true), Description("是否收集性能计数器相关信息"), DisplayName("\t是否收集"), ReadOnly(false)]
		public bool HasPerfCounter
		{
			get;
			set;
		}

		[Category("\t\t\t\t\t\t\t性能计数器"), DefaultValue(10), Description("收集性能计数器相关信息的频率(单位：秒)"), DisplayName("间隔时间(秒)"), ReadOnly(false)]
		public int PerfElapsed
		{
			get;
			set;
		}

		[Category("\t\t\t\t\t\t\t性能计数器"), Description("需要收集的性能计数器列表"), DisplayName("性能计数器列表"), Editor(typeof(PerfConvertor), typeof(UITypeEditor)), ReadOnly(false)]
		public List<Perf> PerfList
		{
			get;
			set;
		}

		[Category("\t\t\t\t\t\tTempdb信息"), DefaultValue(true), Description("是否收集Tempdb相关信息"), DisplayName("\t是否收集"), ReadOnly(false)]
		public bool HasTempdbInfo
		{
			get;
			set;
		}

		[Category("\t\t\t\t\t\tTempdb信息"), DefaultValue(5), Description("收集Tempdb的频率(单位：分钟)"), DisplayName("间隔时间(分钟)"), ReadOnly(false)]
		public int TempdbElapsed
		{
			get;
			set;
		}

		[Category("\t\t\t\t\t对象定义"), DefaultValue(true), Description("是否收集对象定义相关信息"), DisplayName("\t\t是否收集"), ReadOnly(false)]
		public bool HasDBDefine
		{
			get;
			set;
		}

		[Browsable(false), Category("\t\t\t\t\t对象定义"), DefaultValue(false), Description("是否检查表一致性信息"), DisplayName("\t是否检查表一致性"), EditorBrowsable(EditorBrowsableState.Never), ReadOnly(false)]
		public bool HasCheckTable
		{
			get;
			set;
		}

		[Category("\t\t\t\t\t对象定义"), Description("是否收集对象定义相关信息"), DisplayName("收集类别"), ReadOnly(false), TypeConverter(typeof(DBItemConverter))]
		public DBItemType DBItemType
		{
			get;
			set;
		}

		[Category("\t\t\t\t\t对象定义"), DefaultValue(false), Description("是否解密已加密的数据库对象"), DisplayName("是否解密已加密对象"), ReadOnly(false)]
		public bool IsDecrypt
		{
			get;
			set;
		}

		[Category("\t\t\t\t\t对象定义"), Description("配置收集对象定义时的过滤条件"), DisplayName("过滤条件"), Editor(typeof(DBFilterEditor), typeof(UITypeEditor)), ReadOnly(false)]
		public List<DBFilter> DBFilterList
		{
			get;
			set;
		}

		[Category("\t\t\t\t数据库文件"), DefaultValue(true), Description("是否收集数据库文件相关信息"), DisplayName("\t是否收集"), ReadOnly(false)]
		public bool HasDBFile
		{
			get;
			set;
		}

		[Category("\t\t\t\t数据库文件"), DefaultValue(5), Description("收集数据库文件相关信息的频率(单位：分钟)"), DisplayName("间隔时间(分钟)"), ReadOnly(false)]
		public int DBFileElapsed
		{
			get;
			set;
		}

		[Category("\t\t\t会话"), DefaultValue(true), Description("是否收集会话相关信息"), DisplayName("\t\t\t\t是否收集"), ReadOnly(false)]
		public bool HasSession
		{
			get;
			set;
		}

		[Category("\t\t\t会话"), DefaultValue(3), Description("收集等待相关信息的频率(单位：秒)"), DisplayName("\t\t\t等待间隔时间(秒)"), ReadOnly(false)]
		public int WaitElapsed
		{
			get;
			set;
		}

		[Category("\t\t\t会话"), DefaultValue(10), Description("仅收集空闲时间大于或等于阈值的会话(单位：分钟)"), DisplayName("\t\t空闲会话阈值(分钟)"), ReadOnly(false)]
		public int UnusedSessionValue
		{
			get;
			set;
		}

		[Category("\t\t\t会话"), DefaultValue(5), Description("收集空闲会话相关信息的频率(单位：分钟)"), DisplayName("\t空闲会话间隔时间(分钟)"), ReadOnly(false)]
		public int UnusedSessionElapsed
		{
			get;
			set;
		}

		[Category("\t\t查询语句"), DefaultValue(true), Description("是否收集查询语句"), DisplayName("\t是否收集"), ReadOnly(false)]
		public bool HasSqlText
		{
			get;
			set;
		}

		[Category("\t\t查询语句"), DefaultValue(3000), Description("仅收集运行时间大于或等于阈值的查询语句(单位：毫秒)"), DisplayName("运行时间阈值(毫秒)"), ReadOnly(false)]
		public int SqlTextValue
		{
			get;
			set;
		}

		[Category("\t执行计划"), DefaultValue(true), Description("是否收集执行计划相关信息"), DisplayName("\t是否收集"), ReadOnly(false)]
		public bool HasSqlPlan
		{
			get;
			set;
		}

		[Category("\t执行计划"), DefaultValue(1), Description("收集执行计划的频率(单位：小时)"), DisplayName("间隔时间(小时)"), ReadOnly(false)]
		public int SqlPlanElapsed
		{
			get;
			set;
		}

		[Category("\t执行计划"), DefaultValue(500), Description("仅收集执行时间大于或等于阈值的执行计划(单位：毫秒)"), DisplayName("执行时间阈值(毫秒)"), ReadOnly(false)]
		public int SqlPlanValue
		{
			get;
			set;
		}

		[Category("错误日志"), DefaultValue(true), Description("是否收集错误日志相关信息"), DisplayName("\t是否收集"), ReadOnly(false)]
		public bool HasErrorLog
		{
			get;
			set;
		}

		[Category("错误日志"), DefaultValue(1), Description("收集错误日志的频率(单位：小时)"), DisplayName("间隔时间(小时)"), ReadOnly(false)]
		public int ErrorLogElapsed
		{
			get;
			set;
		}

		[Category("错误日志"), Description("配置收集错误日志的关键字列表"), DisplayName("关键字"), ReadOnly(false)]
		public string[] ErrorLogKeys
		{
			get;
			set;
		}

		[Category("Moebius"), DefaultValue(false), Description("是否收集Moebius相关信息"), DisplayName("\t是否收集"), ReadOnly(false)]
		public bool HasMoebius
		{
			get;
			set;
		}

		[Category("其它"), DefaultValue(300), Description("设置执行命令的超时时间(单位：秒)"), DisplayName("超时时间(秒)"), ReadOnly(false)]
		public int TimeOut
		{
			get;
			set;
		}
	}
}
