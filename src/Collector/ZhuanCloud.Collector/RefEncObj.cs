using System;

namespace ZhuanCloud.Collector
{
	internal class RefEncObj
	{
		public string ObjectName
		{
			get;
			set;
		}

		public string Type
		{
			get;
			set;
		}

		public RefEncObj(string objName, string type)
		{
			this.ObjectName = objName;
			this.Type = type;
		}
	}
}
