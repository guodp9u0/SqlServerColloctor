using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text;

namespace ZhuanCloud.Collector
{
	public class UnMoebiusCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, int timeOut)
		{
			DataSet data = this.GetData(sqlString, timeOut);
			if (data.Tables.Contains("tblLinkServer"))
			{
				DataTable dataTable = data.Tables["tblLinkServer"];
				if (dataTable.Rows.Count > 0)
				{
					string text = "\r\nINSERT  INTO tblLinkServer\r\n        ( InstanceName ,\r\n          Product ,\r\n          Provider ,\r\n          [DataSource] ,\r\n          [Location] ,\r\n          ProviderString ,\r\n          [Catalog]\r\n        )\r\nVALUES  ( @InstanceName ,\r\n          @Product ,\r\n          @Provider ,\r\n          @DataSource ,\r\n          @Location ,\r\n          @ProviderString ,\r\n          @Catalog\r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@InstanceName", OleDbType.VarChar)
						{
							SourceColumn = "name"
						},
						new OleDbParameter("@Product", OleDbType.VarChar)
						{
							SourceColumn = "product"
						},
						new OleDbParameter("@Provider", OleDbType.VarChar)
						{
							SourceColumn = "provider"
						},
						new OleDbParameter("@DataSource", OleDbType.LongVarWChar)
						{
							SourceColumn = "data_source"
						},
						new OleDbParameter("@Location", OleDbType.LongVarWChar)
						{
							SourceColumn = "location"
						},
						new OleDbParameter("@ProviderString", OleDbType.LongVarWChar)
						{
							SourceColumn = "provider_string"
						},
						new OleDbParameter("@Catalog", OleDbType.VarChar)
						{
							SourceColumn = "catalog"
						}
					};
					try
					{
						if (!base.SaveToAccess(oleDBString, dataTable, text, parameters))
						{
							bool result = false;
							return result;
						}
					}
					catch (Exception ex)
					{
						ErrorLog.Write(new Exception(text));
						ErrorLog.Write(ex);
						bool result = false;
						return result;
					}
				}
			}
			if (data.Tables.Contains("tblWindowsSession"))
			{
				DataTable dataTable2 = data.Tables["tblWindowsSession"];
				if (dataTable2.Rows.Count > 0)
				{
					string text2 = "\r\nINSERT  INTO tblWindowsSession\r\n        ( [HostName] ,\r\n          [ProgramName] ,\r\n          HostProcessId ,\r\n          ClientInterfaceName ,\r\n          [LoginName] ,\r\n          NTDomain ,\r\n          LastRequestStartTime ,\r\n          LastRequestEndTime\r\n        )\r\nVALUES  ( @HostName ,\r\n          @ProgramName ,\r\n          @HostProcessId ,\r\n          @ClientInterfaceName ,\r\n          @LoginName ,\r\n          @NTDomain ,\r\n          @LastRequestStartTime ,\r\n          @LastRequestEndTime\r\n        )\r\n";
					OleDbParameter[] parameters2 = new OleDbParameter[]
					{
						new OleDbParameter("@HostName", OleDbType.VarChar)
						{
							SourceColumn = "host_name"
						},
						new OleDbParameter("@ProgramName", OleDbType.VarChar)
						{
							SourceColumn = "program_name"
						},
						new OleDbParameter("@HostProcessId", OleDbType.Integer)
						{
							SourceColumn = "host_process_id"
						},
						new OleDbParameter("@ClientInterfaceName", OleDbType.VarChar)
						{
							SourceColumn = "client_interface_name"
						},
						new OleDbParameter("@LoginName", OleDbType.VarChar)
						{
							SourceColumn = "login_name"
						},
						new OleDbParameter("@NTDomain", OleDbType.VarChar)
						{
							SourceColumn = "nt_domain"
						},
						new OleDbParameter("@LastRequestStartTime", OleDbType.Date)
						{
							SourceColumn = "last_request_start_time"
						},
						new OleDbParameter("@LastRequestStartTime", OleDbType.Date)
						{
							SourceColumn = "last_request_start_time"
						}
					};
					try
					{
						if (!base.SaveToAccess(oleDBString, dataTable2, text2, parameters2))
						{
							bool result = false;
							return result;
						}
					}
					catch (Exception ex2)
					{
						ErrorLog.Write(new Exception(text2));
						ErrorLog.Write(ex2);
						bool result = false;
						return result;
					}
				}
			}
			if (data.Tables.Contains("tblSnapshotTrans"))
			{
				DataTable dataTable3 = data.Tables["tblSnapshotTrans"];
				if (dataTable3.Rows.Count > 0)
				{
					string text3 = "\r\nINSERT  INTO tblSnapshotTrans\r\n        ( [HostName] ,\r\n          [ProgramName] ,\r\n          HostProcessId ,\r\n          ClientInterfaceName ,\r\n          [LoginName] ,\r\n          NTDomain ,\r\n          LastRequestStartTime ,\r\n          LastRequestEndTime\r\n        )\r\nVALUES  ( @HostName ,\r\n          @ProgramName ,\r\n          @HostProcessId ,\r\n          @ClientInterfaceName ,\r\n          @LoginName ,\r\n          @NTDomain ,\r\n          @LastRequestStartTime ,\r\n          @LastRequestEndTime\r\n        )\r\n";
					OleDbParameter[] parameters3 = new OleDbParameter[]
					{
						new OleDbParameter("@HostName", OleDbType.VarChar)
						{
							SourceColumn = "host_name"
						},
						new OleDbParameter("@ProgramName", OleDbType.VarChar)
						{
							SourceColumn = "program_name"
						},
						new OleDbParameter("@HostProcessId", OleDbType.Integer)
						{
							SourceColumn = "host_process_id"
						},
						new OleDbParameter("@ClientInterfaceName", OleDbType.VarChar)
						{
							SourceColumn = "client_interface_name"
						},
						new OleDbParameter("@LoginName", OleDbType.VarChar)
						{
							SourceColumn = "login_name"
						},
						new OleDbParameter("@NTDomain", OleDbType.VarChar)
						{
							SourceColumn = "nt_domain"
						},
						new OleDbParameter("@LastRequestStartTime", OleDbType.Date)
						{
							SourceColumn = "last_request_start_time"
						},
						new OleDbParameter("@LastRequestStartTime", OleDbType.Date)
						{
							SourceColumn = "last_request_start_time"
						}
					};
					try
					{
						if (!base.SaveToAccess(oleDBString, dataTable3, text3, parameters3))
						{
							bool result = false;
							return result;
						}
					}
					catch (Exception ex3)
					{
						ErrorLog.Write(new Exception(text3));
						ErrorLog.Write(ex3);
						bool result = false;
						return result;
					}
				}
			}
			return true;
		}

		public bool SaveDBData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut, string condition)
		{
			SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder(sqlString);
			string initialCatalog = sqlConnectionStringBuilder.InitialCatalog;
			DataSet dBData = this.GetDBData(sqlString, timeOut, sqlVersion, condition);
			if (dBData.Tables.Contains("tblUniqueKeyTables"))
			{
				DataTable dataTable = dBData.Tables["tblUniqueKeyTables"];
				if (dataTable.Rows.Count > 0)
				{
					string text = "\r\nINSERT  INTO tblUniqueKeyTables\r\n        ( DBName ,\r\n          SchemaName ,\r\n          TableName ,\r\n          [RowCount] ,\r\n          CreateDate ,\r\n          ModifyDate \r\n        )\r\nVALUES  ( @DBName ,\r\n          @SchemaName ,\r\n          @TableName ,\r\n          @RowCount ,\r\n          @CreateDate ,\r\n          @ModifyDate \r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@DBName", OleDbType.VarChar)
						{
							Value = initialCatalog
						},
						new OleDbParameter("@SchemaName", OleDbType.VarChar)
						{
							SourceColumn = "schema_name"
						},
						new OleDbParameter("@TableName", OleDbType.VarChar)
						{
							SourceColumn = "table_name"
						},
						new OleDbParameter("@RowCount", OleDbType.Double)
						{
							SourceColumn = "row_count"
						},
						new OleDbParameter("@CreateDate", OleDbType.Date)
						{
							SourceColumn = "create_date"
						},
						new OleDbParameter("@ModifyDate", OleDbType.Date)
						{
							SourceColumn = "modify_date"
						}
					};
					try
					{
						if (!base.SaveToAccess(oleDBString, dataTable, text, parameters))
						{
							bool result = false;
							return result;
						}
					}
					catch (Exception ex)
					{
						ErrorLog.Write(new Exception(text));
						ErrorLog.Write(ex);
						bool result = false;
						return result;
					}
				}
			}
			if (dBData.Tables.Contains("tblServiceQueue"))
			{
				DataTable dataTable2 = dBData.Tables["tblServiceQueue"];
				if (dataTable2.Rows.Count > 0)
				{
					string text2 = "\r\nINSERT  INTO tblServiceQueue\r\n        ( DBName ,\r\n          QueueName ,\r\n          ActivationProcedure ,\r\n          IsActivationEnabled ,\r\n          IsReceiveEnabled ,\r\n          IsEnqueueEnabled ,\r\n          IsRetentionEnabled ,\r\n          CreateDate ,\r\n          ModifyDate \r\n        )\r\nVALUES  ( @DBName ,\r\n          @QueueName ,\r\n          @ActivationProcedure ,\r\n          @IsActivationEnabled ,\r\n          @IsReceiveEnabled ,\r\n          @IsEnqueueEnabled ,\r\n          @IsRetentionEnabled ,\r\n          @CreateDate ,\r\n          @ModifyDate \r\n        )\r\n";
					OleDbParameter[] parameters2 = new OleDbParameter[]
					{
						new OleDbParameter("@DBName", OleDbType.VarChar)
						{
							Value = initialCatalog
						},
						new OleDbParameter("@QueueName", OleDbType.VarChar)
						{
							SourceColumn = "name"
						},
						new OleDbParameter("@ActivationProcedure", OleDbType.LongVarWChar)
						{
							SourceColumn = "activation_procedure"
						},
						new OleDbParameter("@IsActivationEnabled", OleDbType.Boolean)
						{
							SourceColumn = "is_activation_enabled"
						},
						new OleDbParameter("@IsReceiveEnabled", OleDbType.Boolean)
						{
							SourceColumn = "is_receive_enabled"
						},
						new OleDbParameter("@IsEnqueueEnabled", OleDbType.Boolean)
						{
							SourceColumn = "is_enqueue_enabled"
						},
						new OleDbParameter("@IsRetentionEnabled", OleDbType.Boolean)
						{
							SourceColumn = "is_retention_enabled"
						},
						new OleDbParameter("@CreateDate", OleDbType.Date)
						{
							SourceColumn = "create_date"
						},
						new OleDbParameter("@ModifyDate", OleDbType.Date)
						{
							SourceColumn = "modify_date"
						}
					};
					try
					{
						if (!base.SaveToAccess(oleDBString, dataTable2, text2, parameters2))
						{
							bool result = false;
							return result;
						}
					}
					catch (Exception ex2)
					{
						ErrorLog.Write(new Exception(text2));
						ErrorLog.Write(ex2);
						bool result = false;
						return result;
					}
				}
			}
			if (dBData.Tables.Contains("tblCDCTables"))
			{
				DataTable dataTable3 = dBData.Tables["tblCDCTables"];
				if (dataTable3.Rows.Count > 0)
				{
					string text3 = "\r\nINSERT  INTO tblCDCTables\r\n        ( DBName ,\r\n          SchemaName ,\r\n          TableName ,\r\n          RowCount ,\r\n          CreateDate ,\r\n          ModifyDate\r\n        )\r\nVALUES  ( @DBName ,\r\n          @SchemaName ,\r\n          @TableName ,\r\n          @RowCount ,\r\n          @CreateDate ,\r\n          @ModifyDate\r\n        )\r\n";
					OleDbParameter[] parameters3 = new OleDbParameter[]
					{
						new OleDbParameter("@DBName", OleDbType.VarChar)
						{
							Value = initialCatalog
						},
						new OleDbParameter("@SchemaName", OleDbType.VarChar)
						{
							SourceColumn = "schema_name"
						},
						new OleDbParameter("@TableName", OleDbType.VarChar)
						{
							SourceColumn = "table_name"
						},
						new OleDbParameter("@RowCount", OleDbType.Double)
						{
							SourceColumn = "row_count"
						},
						new OleDbParameter("@CreateDate", OleDbType.Date)
						{
							SourceColumn = "create_date"
						},
						new OleDbParameter("@ModifyDate", OleDbType.Date)
						{
							SourceColumn = "modify_date"
						}
					};
					try
					{
						if (!base.SaveToAccess(oleDBString, dataTable3, text3, parameters3))
						{
							bool result = false;
							return result;
						}
					}
					catch (Exception ex3)
					{
						ErrorLog.Write(new Exception(text3));
						ErrorLog.Write(ex3);
						bool result = false;
						return result;
					}
				}
			}
			if (dBData.Tables.Contains("tblOldTypeTables"))
			{
				DataTable dataTable4 = dBData.Tables["tblOldTypeTables"];
				if (dataTable4.Rows.Count > 0)
				{
					string text4 = "\r\nINSERT  INTO tblOldTypeTables\r\n        ( DBName ,\r\n          SchemaName ,\r\n          TableName ,\r\n          TypeName \r\n        )\r\nVALUES  ( @DBName ,\r\n          @SchemaName ,\r\n          @TableName ,\r\n          @TypeName \r\n        )\r\n";
					OleDbParameter[] parameters4 = new OleDbParameter[]
					{
						new OleDbParameter("@DBName", OleDbType.VarChar)
						{
							Value = initialCatalog
						},
						new OleDbParameter("@SchemaName", OleDbType.VarChar)
						{
							SourceColumn = "schema_name"
						},
						new OleDbParameter("@TableName", OleDbType.VarChar)
						{
							SourceColumn = "table_name"
						},
						new OleDbParameter("@TypeName", OleDbType.VarChar)
						{
							SourceColumn = "type_name"
						}
					};
					try
					{
						if (!base.SaveToAccess(oleDBString, dataTable4, text4, parameters4))
						{
							bool result = false;
							return result;
						}
					}
					catch (Exception ex4)
					{
						ErrorLog.Write(new Exception(text4));
						ErrorLog.Write(ex4);
						bool result = false;
						return result;
					}
				}
			}
			return true;
		}

		private DataSet GetData(string sqlString, int timeOut)
		{
			DataSet dataSet = new DataSet();
			string cmdText = "\r\nSELECT  name ,\r\n        product ,\r\n        provider ,\r\n        ISNULL(data_source, '') AS data_source ,\r\n        ISNULL(location, '') AS location ,\r\n        ISNULL(provider_string, '') AS provider_string ,\r\n        ISNULL(catalog, '') AS catalog\r\nFROM    sys.servers\r\nWHERE   is_linked = 1\r\n";
			DataTable dataTable = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable.TableName = "tblLinkServer";
			dataSet.Tables.Add(dataTable);
			cmdText = "\r\nSELECT  ISNULL(host_name, '') AS [host_name] ,\r\n        ISNULL(program_name, '') AS [program_name] ,\r\n        ISNULL(host_process_id, 0) AS host_process_id ,\r\n        ISNULL(client_interface_name, '') AS client_interface_name ,\r\n        login_name ,\r\n        ISNULL(nt_domain, '') AS nt_domain ,\r\n        last_request_start_time ,\r\n        last_request_end_time\r\nFROM    sys.dm_exec_sessions\r\nWHERE   session_id > 50\r\n        AND nt_user_name <> N''\r\n        AND ISNULL(program_name, '') NOT LIKE 'Microsoft SQL Server Management Studio%'\r\n        AND ISNULL(program_name, '') NOT LIKE 'SQLAgent%'\r\n        AND ISNULL(program_name, '') <> 'ZhuanCloud'\r\n";
			DataTable dataTable2 = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable2.TableName = "tblWindowsSession";
			dataSet.Tables.Add(dataTable2);
			cmdText = "\r\nSELECT  ISNULL(host_name, '') AS [host_name] ,\r\n        ISNULL(program_name, '') AS [program_name] ,\r\n        ISNULL(host_process_id, 0) AS host_process_id ,\r\n        ISNULL(client_interface_name, '') AS client_interface_name ,\r\n        login_name ,\r\n        ISNULL(nt_domain, '') AS nt_domain ,\r\n        last_request_start_time ,\r\n        last_request_end_time\r\nFROM    sys.dm_exec_sessions\r\nWHERE   session_id > 50\r\n        AND transaction_isolation_level = 5\r\n        AND program_name <> 'ZhuanCloud'\r\n";
			DataTable dataTable3 = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable3.TableName = "tblSnapshotTrans";
			dataSet.Tables.Add(dataTable3);
			return dataSet;
		}

		private DataSet GetDBData(string sqlString, int timeOut, SQLServerVersion sqlVersion, string condition)
		{
			DataSet dataSet = new DataSet();
			string[] array = condition.Split(new string[]
			{
				";"
			}, StringSplitOptions.RemoveEmptyEntries);
			if (array.Length > 0)
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(" AND ");
				int num = 1;
				string[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					string text = array2[i];
					string[] array3 = text.Split(new string[]
					{
						"."
					}, StringSplitOptions.RemoveEmptyEntries);
					if (array3.Length == 1)
					{
						stringBuilder.AppendFormat("  t.name NOT LIKE '{0}' ", array3[0]);
					}
					else if (array3.Length == 2)
					{
						stringBuilder.AppendFormat(" s.name NOT LIKE '{0}' OR t.name NOT LIKE '{1}' ", array3[0], array3[1]);
					}
					if (num < array.Length)
					{
						stringBuilder.Append(" AND ");
					}
					num++;
				}
				condition = stringBuilder.ToString();
			}
			string cmdText = string.Format("\r\nSELECT  s.name AS [schema_name] ,\r\n        t.name AS table_name ,\r\n        dps.row_count ,\r\n        t.create_date ,\r\n        t.modify_date\r\nFROM    sys.tables t\r\n        JOIN sys.schemas s ON t.schema_id = s.schema_id\r\n        JOIN ( SELECT   object_id ,\r\n                        SUM(row_count) AS row_count\r\n               FROM     sys.dm_db_partition_stats\r\n               WHERE    index_id < 2\r\n               GROUP BY object_id ,\r\n                        index_id\r\n             ) dps ON t.[object_id] = dps.[object_id]\r\nWHERE   t.object_id NOT IN ( SELECT object_id\r\n                             FROM   sys.indexes i\r\n                             WHERE  i.is_primary_key = 1\r\n                                    OR i.is_unique = 1\r\n                                    OR i.is_unique_constraint = 1 )\r\n        AND OBJECTPROPERTY(t.object_id, 'IsMSShipped') = 0\r\n        {0}\r\n", condition);
			DataTable dataTable = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable.TableName = "tblUniqueKeyTables";
			dataSet.Tables.Add(dataTable);
			cmdText = "\r\nSELECT  name ,\r\n        activation_procedure ,\r\n        is_activation_enabled ,\r\n        is_receive_enabled ,\r\n        is_enqueue_enabled ,\r\n        is_retention_enabled ,\r\n        create_date ,\r\n        modify_date\r\nFROM    sys.service_queues\r\nWHERE   is_ms_shipped = 0\r\n";
			DataTable dataTable2 = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable2.TableName = "tblServiceQueue";
			dataSet.Tables.Add(dataTable2);
			if (sqlVersion >= SQLServerVersion.SQLServer2008)
			{
				cmdText = "\r\nSELECT  s.name AS [schema_name] ,\r\n        t.name AS table_name ,\r\n        dps.row_count ,\r\n        t.create_date ,\r\n        t.modify_date\r\nFROM    sys.tables t\r\n        JOIN sys.schemas s ON t.schema_id = s.schema_id\r\n        JOIN ( SELECT   object_id ,\r\n                        SUM(row_count) AS row_count\r\n               FROM     sys.dm_db_partition_stats\r\n               WHERE    index_id < 2\r\n               GROUP BY object_id ,\r\n                        index_id\r\n             ) dps ON t.[object_id] = dps.[object_id]\r\nWHERE   type = 'U'\r\n        AND is_tracked_by_cdc = 1\r\n";
				DataTable dataTable3 = base.FillDataTable(sqlString, cmdText, timeOut);
				dataTable3.TableName = "tblCDCTables";
				dataSet.Tables.Add(dataTable3);
			}
			cmdText = "\r\nSELECT DISTINCT\r\n        s.name AS [schema_name] ,\r\n        o.name AS table_name ,\r\n        CASE t.system_type_id\r\n          WHEN 34 THEN 'image'\r\n          WHEN 35 THEN 'text'\r\n          WHEN 99 THEN 'ntext'\r\n          ELSE NULL\r\n        END AS [type_name]\r\nFROM    sys.columns c ,\r\n        sys.objects o ,\r\n        sys.schemas s ,\r\n        sys.types t\r\nWHERE   c.object_id = o.object_id\r\n        AND o.schema_id = s.schema_id\r\n        AND c.system_type_id = t.system_type_id\r\n        AND c.user_type_id = t.user_type_id\r\n        AND c.system_type_id IN ( 34, 35, 99 )\r\n        AND is_ms_shipped = 0\r\n";
			DataTable dataTable4 = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable4.TableName = "tblOldTypeTables";
			dataSet.Tables.Add(dataTable4);
			return dataSet;
		}
	}
}
