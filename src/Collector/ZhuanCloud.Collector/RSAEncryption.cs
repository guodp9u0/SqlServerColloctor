using System;
using System.Security;
using System.Security.Cryptography;

namespace ZhuanCloud.Collector
{
	public class RSAEncryption : IDisposable
	{
		private BigInteger D;

		private BigInteger Exponent;

		private BigInteger Modulus;

		private RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();

		private bool isPrivateKeyLoaded;

		private bool isPublicKeyLoaded;

		public bool IsPrivateKeyLoaded
		{
			get
			{
				return this.isPrivateKeyLoaded;
			}
		}

		public bool IsPublicKeyLoaded
		{
			get
			{
				return this.isPublicKeyLoaded;
			}
		}

		public void LoadPublicFromXml(string publicXmlString)
		{
			try
			{
				this.rsa.FromXmlString(publicXmlString);
				RSAParameters rSAParameters = this.rsa.ExportParameters(false);
				this.Modulus = new BigInteger(rSAParameters.Modulus);
				this.Exponent = new BigInteger(rSAParameters.Exponent);
				this.isPublicKeyLoaded = true;
				this.isPrivateKeyLoaded = false;
			}
			catch (XmlSyntaxException innerException)
			{
				string message = "Exception occurred at LoadPublicFromXml(), Selected file is not a valid xml file.";
				throw new Exception(message, innerException);
			}
			catch (CryptographicException innerException2)
			{
				string message2 = "Exception occurred at LoadPublicFromXml(), Selected xml file is not a public key file.";
				throw new Exception(message2, innerException2);
			}
			catch (Exception innerException3)
			{
				string message3 = "General Exception occurred at LoadPublicFromXml().";
				throw new Exception(message3, innerException3);
			}
		}

		public void LoadPrivateFromXml(string privateXmlString)
		{
			try
			{
				this.rsa.FromXmlString(privateXmlString);
				RSAParameters rSAParameters = this.rsa.ExportParameters(true);
				this.D = new BigInteger(rSAParameters.D);
				this.Exponent = new BigInteger(rSAParameters.Exponent);
				this.Modulus = new BigInteger(rSAParameters.Modulus);
				this.isPrivateKeyLoaded = true;
				this.isPublicKeyLoaded = true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public byte[] PrivateEncryption(byte[] data)
		{
			if (!this.IsPrivateKeyLoaded)
			{
				throw new CryptographicException("Private Key must be loaded before using the Private Encryption method!");
			}
			BigInteger bigInteger = new BigInteger(data);
			BigInteger bigInteger2 = bigInteger.modPow(this.D, this.Modulus);
			return bigInteger2.getBytes();
		}

		public byte[] PublicEncryption(byte[] data)
		{
			if (!this.IsPublicKeyLoaded)
			{
				throw new CryptographicException("Public Key must be loaded before using the Public Encryption method!");
			}
			BigInteger bigInteger = new BigInteger(data);
			BigInteger bigInteger2 = bigInteger.modPow(this.Exponent, this.Modulus);
			return bigInteger2.getBytes();
		}

		public byte[] PrivateDecryption(byte[] encryptedData)
		{
			if (!this.IsPrivateKeyLoaded)
			{
				throw new CryptographicException("Private Key must be loaded before using the Private Decryption method!");
			}
			BigInteger bigInteger = new BigInteger(encryptedData);
			BigInteger bigInteger2 = bigInteger.modPow(this.D, this.Modulus);
			return bigInteger2.getBytes();
		}

		public byte[] PublicDecryption(byte[] encryptedData)
		{
			if (!this.IsPublicKeyLoaded)
			{
				throw new CryptographicException("Public Key must be loaded before using the Public Deccryption method!");
			}
			BigInteger bigInteger = new BigInteger(encryptedData);
			BigInteger bigInteger2 = bigInteger.modPow(this.Exponent, this.Modulus);
			return bigInteger2.getBytes();
		}

		public void Dispose()
		{
			this.rsa.Clear();
		}
	}
}
