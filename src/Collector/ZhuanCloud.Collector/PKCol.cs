using System;

namespace ZhuanCloud.Collector
{
	internal class PKCol
	{
		public string Type
		{
			get;
			set;
		}

		public string KeyName
		{
			get;
			set;
		}

		public string TypeDesc
		{
			get;
			set;
		}

		public string ColName
		{
			get;
			set;
		}

		public bool IsDesc
		{
			get;
			set;
		}

		public bool HasPadIndex
		{
			get;
			set;
		}

		public bool IsRecompute
		{
			get;
			set;
		}

		public bool IsIgnorekey
		{
			get;
			set;
		}

		public bool HasRowLock
		{
			get;
			set;
		}

		public bool HasPageLock
		{
			get;
			set;
		}

		public string FileGroupName
		{
			get;
			set;
		}

		public bool HasANSINULL
		{
			get;
			set;
		}
	}
}
