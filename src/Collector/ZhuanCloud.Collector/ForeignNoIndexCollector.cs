using System;
using System.Data;
using System.Data.OleDb;

namespace ZhuanCloud.Collector
{
	public class ForeignNoIndexCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, int timeOut)
		{
			bool result;
			try
			{
				DataSet data = this.GetData(sqlString, timeOut);
				if (data.Tables.Contains("tblForeignNoIndex"))
				{
					DataTable dataTable = data.Tables["tblForeignNoIndex"];
					if (dataTable.Rows.Count > 0)
					{
						string cmdText = "\r\nINSERT  INTO tblForeignNoIndex\r\n        ( DBName ,\r\n          ParentObjectId ,\r\n          ParentColumnId ,\r\n          ConstraintColumnId ,\r\n          SchemaName ,\r\n          TableName ,\r\n          ForeignKeyName ,\r\n          ColumnName ,\r\n          CreateDate ,\r\n          ModifyDate ,\r\n          ForeignRowCount\r\n        )\r\nVALUES  ( @DBName ,\r\n          @ParentObjectId ,\r\n          @ParentColumnId ,\r\n          @ConstraintColumnId ,\r\n          @SchemaName ,\r\n          @TableName ,\r\n          @ForeignKeyName ,\r\n          @ColumnName ,\r\n          @CreateDate ,\r\n          @ModifyDate ,\r\n          @ForeignRowCount\r\n        )\r\n";
						OleDbParameter[] parameters = new OleDbParameter[]
						{
							new OleDbParameter("@DBName", OleDbType.VarChar)
							{
								SourceColumn = "db_name"
							},
							new OleDbParameter("@ParentObjectId", OleDbType.Integer)
							{
								SourceColumn = "parent_object_id"
							},
							new OleDbParameter("@ParentColumnId", OleDbType.Integer)
							{
								SourceColumn = "parent_column_id"
							},
							new OleDbParameter("@ConstraintColumnId", OleDbType.Integer)
							{
								SourceColumn = "constraint_column_id"
							},
							new OleDbParameter("@SchemaName", OleDbType.VarChar)
							{
								SourceColumn = "schema_name"
							},
							new OleDbParameter("@TableName", OleDbType.VarChar)
							{
								SourceColumn = "table_name"
							},
							new OleDbParameter("@ForeignKeyName", OleDbType.VarChar)
							{
								SourceColumn = "foreign_key_name"
							},
							new OleDbParameter("@ColumnName", OleDbType.VarChar)
							{
								SourceColumn = "column_name"
							},
							new OleDbParameter("@CreateDate", OleDbType.Date)
							{
								SourceColumn = "create_date"
							},
							new OleDbParameter("@ModifyDate", OleDbType.Date)
							{
								SourceColumn = "modify_date"
							},
							new OleDbParameter("@ForeignRowCount", OleDbType.Double)
							{
								SourceColumn = "foreign_row_count"
							}
						};
						if (!base.SaveToAccess(oleDBString, dataTable, cmdText, parameters))
						{
							result = false;
							return result;
						}
					}
				}
				if (data.Tables.Contains("tblForeignNoIndexColumns"))
				{
					DataTable dataTable2 = data.Tables["tblForeignNoIndexColumns"];
					if (dataTable2.Rows.Count > 0)
					{
						string cmdText2 = "\r\nINSERT  INTO tblForeignNoIndexColumns\r\n        ( DBName ,\r\n          ObjectId ,\r\n          IndexId ,\r\n          KeyOrdinal ,\r\n          ColumnId\r\n        )\r\nVALUES  ( @DBName ,\r\n          @ObjectId ,\r\n          @IndexId ,\r\n          @KeyOrdinal ,\r\n          @ColumnId\r\n        )\r\n";
						OleDbParameter[] parameters2 = new OleDbParameter[]
						{
							new OleDbParameter("@DBName", OleDbType.VarChar)
							{
								SourceColumn = "db_name"
							},
							new OleDbParameter("@ObjectId", OleDbType.Integer)
							{
								SourceColumn = "object_id"
							},
							new OleDbParameter("@IndexId", OleDbType.Integer)
							{
								SourceColumn = "index_id"
							},
							new OleDbParameter("@KeyOrdinal", OleDbType.Integer)
							{
								SourceColumn = "key_ordinal"
							},
							new OleDbParameter("@ColumnId", OleDbType.Integer)
							{
								SourceColumn = "column_id"
							}
						};
						if (!base.SaveToAccess(oleDBString, dataTable2, cmdText2, parameters2))
						{
							result = false;
							return result;
						}
					}
				}
				result = true;
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataSet GetData(string sqlString, int timeOut)
		{
			DataSet dataSet = new DataSet();
			string cmdText = "\r\nSET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED\r\nSELECT  DB_NAME(DB_ID()) [db_name] ,\r\n        fk.parent_object_id ,\r\n        fc.parent_column_id ,\r\n        fc.constraint_column_id ,\r\n        s1.name AS [schema_name] ,\r\n        t1.name AS table_name ,\r\n        fk.name AS foreign_key_name ,\r\n        c1.name AS column_name ,\r\n        fk.create_date ,\r\n        fk.modify_date ,\r\n        dps.row_count AS foreign_row_count\r\nFROM    sys.foreign_keys fk\r\n        JOIN sys.foreign_key_columns fc ON fk.[object_id] = fc.constraint_object_id\r\n        JOIN sys.tables t1 ON fk.parent_object_id = t1.[object_id]\r\n        JOIN sys.schemas s1 ON t1.[schema_id] = s1.[schema_id]\r\n        JOIN sys.columns c1 ON t1.[object_id] = c1.[object_id]\r\n                               AND fc.parent_column_id = c1.column_id\r\n        JOIN ( SELECT   object_id ,\r\n                        SUM(row_count) AS row_count\r\n               FROM     sys.dm_db_partition_stats\r\n               WHERE    index_id < 2\r\n               GROUP BY object_id ,\r\n                        index_id\r\n             ) dps ON t1.[object_id] = dps.[object_id]\r\nWHERE   OBJECTPROPERTY(t1.[object_id], 'IsMSShipped') = 0\r\nORDER BY dps.row_count DESC\r\nOPTION (MAXDOP 2)\r\n";
			DataTable dataTable = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable.TableName = "tblForeignNoIndex";
			dataSet.Tables.Add(dataTable);
			string cmdText2 = "\r\nSET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED\r\nSELECT  DB_NAME(DB_ID()) [db_name] ,\r\n        [object_id] ,\r\n        index_id ,\r\n        key_ordinal ,\r\n        column_id\r\nFROM    sys.index_columns\r\nORDER BY [object_id] ,\r\n        index_id ,\r\n        key_ordinal\r\nOPTION (MAXDOP 2)\r\n";
			DataTable dataTable2 = base.FillDataTable(sqlString, cmdText2, timeOut);
			dataTable2.TableName = "tblForeignNoIndexColumns";
			dataSet.Tables.Add(dataTable2);
			return dataSet;
		}
	}
}
