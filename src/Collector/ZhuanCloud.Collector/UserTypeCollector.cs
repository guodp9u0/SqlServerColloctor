using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace ZhuanCloud.Collector
{
	public class UserTypeCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(sqlString, sqlVersion, timeOut);
				if (data.Rows.Count > 0)
				{
					DataTable dataTable = new DataTable();
					dataTable.Columns.Add(new DataColumn("db_name"));
					dataTable.Columns.Add(new DataColumn("schema_name"));
					dataTable.Columns.Add(new DataColumn("object_name"));
					dataTable.Columns.Add(new DataColumn("define"));
					dataTable.Columns.Add(new DataColumn("db_type"));
					SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder(sqlString);
					string initialCatalog = sqlConnectionStringBuilder.InitialCatalog;
					foreach (DataRow dataRow in data.Rows)
					{
						string text = dataRow.ItemArray[0].ToString();
						string text2 = dataRow.ItemArray[1].ToString();
						string typeName = dataRow.ItemArray[2].ToString();
						int maxLength = Convert.ToInt32(dataRow.ItemArray[3]);
						int precision = Convert.ToInt32(dataRow.ItemArray[4]);
						int scale = Convert.ToInt32(dataRow.ItemArray[5]);
						string text3 = Convert.ToBoolean(dataRow.ItemArray[6]) ? "NULL" : "NOT NULL";
						string typeText = this.GetTypeText(typeName, maxLength, precision, scale);
						string text4 = TextManager.Compress(string.Format("CREATE TYPE [{0}].[{1}] FROM {2} {3}", new object[]
						{
							text,
							text2,
							typeText,
							text3
						}));
						dataTable.Rows.Add(new object[]
						{
							initialCatalog,
							text,
							text2,
							text4,
							"用户自定义数据类型"
						});
					}
					if (dataTable.Rows.Count > 0)
					{
						if (!Util.startFlag)
						{
							result = false;
							return result;
						}
						string cmdText = "\r\nINSERT  INTO tblDBUserType\r\n        ( DBName ,\r\n          SchemaName ,\r\n          ObjectName ,\r\n          Define ,\r\n          DBType\r\n        )\r\nVALUES  ( @DBName ,\r\n          @SchemaName ,\r\n          @ObjectName ,\r\n          @Define ,\r\n          @DBType\r\n        )\r\n";
						OleDbParameter[] parameters = new OleDbParameter[]
						{
							new OleDbParameter("@DBName", OleDbType.VarChar)
							{
								SourceColumn = "db_name"
							},
							new OleDbParameter("@SchemaName", OleDbType.VarChar)
							{
								SourceColumn = "schema_name"
							},
							new OleDbParameter("@ObjectName", OleDbType.VarChar)
							{
								SourceColumn = "object_name"
							},
							new OleDbParameter("@Define", OleDbType.VarChar)
							{
								SourceColumn = "define"
							},
							new OleDbParameter("@DBType", OleDbType.VarChar)
							{
								SourceColumn = "db_type"
							}
						};
						result = base.SaveToAccess(oleDBString, dataTable, cmdText, parameters);
						return result;
					}
				}
				result = true;
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			string cmdText = string.Empty;
			if (sqlVersion <= SQLServerVersion.SQLServer2005)
			{
				cmdText = "\r\nSELECT  s.name AS [schema_name] ,\r\n        ut.name AS user_type ,\r\n        st.name AS sys_type ,\r\n        ut.max_length ,\r\n        ut.precision ,\r\n        ut.scale ,\r\n        ut.is_nullable\r\nFROM    sys.types ut ,\r\n        sys.types st ,\r\n        sys.schemas s\r\nWHERE   ut.system_type_id = st.user_type_id\r\n        AND ut.[schema_id] = s.[schema_id]\r\n        AND ut.is_user_defined = 1\r\n        AND ut.is_assembly_type = 0\r\n";
			}
			else
			{
				cmdText = "\r\nSELECT  s.name AS [schema_name] ,\r\n        ut.name AS user_type ,\r\n        st.name AS sys_type ,\r\n        ut.max_length ,\r\n        ut.precision ,\r\n        ut.scale ,\r\n        ut.is_nullable\r\nFROM    sys.types ut ,\r\n        sys.types st ,\r\n        sys.schemas s\r\nWHERE   ut.system_type_id = st.user_type_id\r\n        AND ut.[schema_id] = s.[schema_id]\r\n        AND ut.is_user_defined = 1\r\n        AND ut.is_assembly_type = 0\r\n        AND ut.is_table_type = 0\r\n";
			}
			return base.FillDataTable(sqlString, cmdText, timeOut);
		}

		public string GetTypeText(string typeName, int maxLength, int precision, int scale)
		{
			string arg_05_0 = string.Empty;
			string key;
			switch (key = typeName.ToLower())
			{
			case "nvarchar":
				if (maxLength == -1)
				{
					return "nvarchar(MAX)";
				}
				return string.Format("nvarchar({0})", maxLength / 2);
			case "nchar":
				return string.Format("nchar({0})", maxLength / 2);
			case "binary":
			case "char":
			case "varchar":
			case "varbinary":
				return string.Format("{0}({1})", typeName, (maxLength == -1) ? "MAX" : maxLength.ToString());
			case "time":
			case "datetime2":
			case "datetimeoffset":
				return string.Format("{0}({1})", typeName, scale);
			case "decimal":
			case "numeric":
				return string.Format("{0}({1},{2})", typeName, precision, scale);
			}
			return typeName;
		}
	}
}
