using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;

namespace ZhuanCloud.Collector
{
	public class FlagCollector : OleDbCollector
	{
		public bool SaveFlagName(CollectFlag flag, string oleDBString, int timeOut)
		{
			bool result;
			try
			{
				if (flag != null)
				{
					string cmdText = "\r\nINSERT  INTO tblFlagDescription\r\n        ( FlagName ,\r\n          StartTime ,\r\n          EndTime ,\r\n          Description \r\n        )\r\nVALUES  ( @FlagName ,\r\n          @StartTime ,\r\n          @EndTime ,\r\n          @Description\r\n        )";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@FlagName", OleDbType.VarChar)
						{
							Value = flag.Name
						},
						new OleDbParameter("@StartTime", OleDbType.Date)
						{
							Value = flag.StartTime
						},
						new OleDbParameter("@EndTime", OleDbType.Date)
						{
							Value = flag.EndTime
						},
						new OleDbParameter("@Description", OleDbType.LongVarWChar)
						{
							Value = flag.Description
						}
					};
					result = base.SaveToAccess(oleDBString, cmdText, timeOut, parameters);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		public bool SaveFlag(string flagName, string filePath, string oleDBString, string sqlString, int timeOut)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(flagName, filePath, sqlString, timeOut);
				if (data.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblFlag\r\n        ( FlagName ,\r\n          EventClass ,\r\n          TextData ,\r\n          NTUserName ,\r\n          HostName ,\r\n          ClientProcessID ,\r\n          ApplicationName ,\r\n          LoginName ,\r\n          SPID ,\r\n          Duration ,\r\n          StartTime ,\r\n          EndTime ,\r\n          Reads ,\r\n          Writes ,\r\n          CPU ,\r\n          DatabaseName ,\r\n          RowCount\r\n        )\r\nVALUES  ( @FlagName ,\r\n          @EventClass ,\r\n          @TextData ,\r\n          @NTUserName ,\r\n          @HostName ,\r\n          @ClientProcessID ,\r\n          @ApplicationName ,\r\n          @LoginName ,\r\n          @SPID ,\r\n          @Duration ,\r\n          @StartTime ,\r\n          @EndTime ,\r\n          @Reads ,\r\n          @Writes ,\r\n          @CPU ,\r\n          @DatabaseName ,\r\n          @RowCount\r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@FlagName", OleDbType.VarChar)
						{
							SourceColumn = "FlagName"
						},
						new OleDbParameter("@EventClass", OleDbType.Integer)
						{
							SourceColumn = "EventClass"
						},
						new OleDbParameter("@TextData", OleDbType.LongVarWChar)
						{
							SourceColumn = "TextData"
						},
						new OleDbParameter("@NTUserName", OleDbType.VarChar)
						{
							SourceColumn = "NTUserName"
						},
						new OleDbParameter("@HostName", OleDbType.VarChar)
						{
							SourceColumn = "HostName"
						},
						new OleDbParameter("@ClientProcessID", OleDbType.Integer)
						{
							SourceColumn = "ClientProcessID"
						},
						new OleDbParameter("@ApplicationName", OleDbType.VarChar)
						{
							SourceColumn = "ApplicationName"
						},
						new OleDbParameter("@LoginName", OleDbType.VarChar)
						{
							SourceColumn = "LoginName"
						},
						new OleDbParameter("@SPID", OleDbType.VarChar)
						{
							SourceColumn = "SPID"
						},
						new OleDbParameter("@Duration", OleDbType.Double)
						{
							SourceColumn = "Duration"
						},
						new OleDbParameter("@StartTime", OleDbType.Date)
						{
							SourceColumn = "StartTime"
						},
						new OleDbParameter("@EndTime", OleDbType.Date)
						{
							SourceColumn = "EndTime"
						},
						new OleDbParameter("@Reads", OleDbType.Double)
						{
							SourceColumn = "Reads"
						},
						new OleDbParameter("@Writes", OleDbType.Double)
						{
							SourceColumn = "Writes"
						},
						new OleDbParameter("@CPU", OleDbType.Double)
						{
							SourceColumn = "CPU"
						},
						new OleDbParameter("@DatabaseName", OleDbType.VarChar)
						{
							SourceColumn = "DatabaseName"
						},
						new OleDbParameter("@RowCount", OleDbType.Double)
						{
							SourceColumn = "RowCounts"
						}
					};
					result = base.SaveToAccess(oleDBString, data, cmdText, parameters);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string flagName, string filePath, string sqlString, int timeOut)
		{
			string cmdText = string.Format("\r\nSELECT  '{0}' FlagName ,\r\n        EventClass ,\r\n        TextData ,\r\n        NTUserName ,\r\n        HostName ,\r\n        ClientProcessID ,\r\n        ApplicationName ,\r\n        LoginName ,\r\n        SPID ,\r\n        Duration ,\r\n        StartTime ,\r\n        EndTime ,\r\n        Reads ,\r\n        Writes ,\r\n        CPU ,\r\n        DatabaseName ,\r\n        RowCounts\r\nFROM    fn_trace_gettable('{1}', DEFAULT)\r\nWHERE   TextData IS NOT NULL\r\n        AND ApplicationName <> 'ZhuanCloud'\r\n", flagName, filePath);
			return base.FillDataTable(sqlString, cmdText, timeOut);
		}

		public object Start(string sqlString, int timeOut, string filePath)
		{
			string cmdText = string.Format("\r\ndeclare @rc int\r\ndeclare @TraceID int\r\ndeclare @FileName nvarchar(520)\r\ndeclare @maxfilesize bigint\r\n\r\nSELECT @FileName = '{0}'\r\nset @maxfilesize = 10 \r\n\r\nexec @rc = sp_trace_create @TraceID output, 2, @FileName, @maxfilesize, NULL \r\nif (@rc != 0) goto error\r\n\r\ndeclare @on bit\r\nset @on = 1\r\nexec sp_trace_setevent @TraceID, 10, 15, @on\r\nexec sp_trace_setevent @TraceID, 10, 16, @on\r\nexec sp_trace_setevent @TraceID, 10, 1, @on\r\nexec sp_trace_setevent @TraceID, 10, 9, @on\r\nexec sp_trace_setevent @TraceID, 10, 17, @on\r\nexec sp_trace_setevent @TraceID, 10, 10, @on\r\nexec sp_trace_setevent @TraceID, 10, 18, @on\r\nexec sp_trace_setevent @TraceID, 10, 11, @on\r\nexec sp_trace_setevent @TraceID, 10, 8, @on\r\nexec sp_trace_setevent @TraceID, 10, 35, @on\r\nexec sp_trace_setevent @TraceID, 10, 12, @on\r\nexec sp_trace_setevent @TraceID, 10, 13, @on\r\nexec sp_trace_setevent @TraceID, 10, 6, @on\r\nexec sp_trace_setevent @TraceID, 10, 14, @on\r\nexec sp_trace_setevent @TraceID, 10, 48, @on\r\n\r\nexec sp_trace_setevent @TraceID, 45, 16, @on\r\nexec sp_trace_setevent @TraceID, 45, 1, @on\r\nexec sp_trace_setevent @TraceID, 45, 9, @on\r\nexec sp_trace_setevent @TraceID, 45, 17, @on\r\nexec sp_trace_setevent @TraceID, 45, 10, @on\r\nexec sp_trace_setevent @TraceID, 45, 18, @on\r\nexec sp_trace_setevent @TraceID, 45, 11, @on\r\nexec sp_trace_setevent @TraceID, 45, 8, @on\r\nexec sp_trace_setevent @TraceID, 45, 35, @on\r\nexec sp_trace_setevent @TraceID, 45, 12, @on\r\nexec sp_trace_setevent @TraceID, 45, 13, @on\r\nexec sp_trace_setevent @TraceID, 45, 6, @on\r\nexec sp_trace_setevent @TraceID, 45, 14, @on\r\nexec sp_trace_setevent @TraceID, 45, 15, @on\r\nexec sp_trace_setevent @TraceID, 45, 48, @on\r\n\r\nexec sp_trace_setevent @TraceID, 12, 15, @on\r\nexec sp_trace_setevent @TraceID, 12, 16, @on\r\nexec sp_trace_setevent @TraceID, 12, 1, @on\r\nexec sp_trace_setevent @TraceID, 12, 9, @on\r\nexec sp_trace_setevent @TraceID, 12, 17, @on\r\nexec sp_trace_setevent @TraceID, 12, 6, @on\r\nexec sp_trace_setevent @TraceID, 12, 10, @on\r\nexec sp_trace_setevent @TraceID, 12, 14, @on\r\nexec sp_trace_setevent @TraceID, 12, 18, @on\r\nexec sp_trace_setevent @TraceID, 12, 11, @on\r\nexec sp_trace_setevent @TraceID, 12, 8, @on\r\nexec sp_trace_setevent @TraceID, 12, 35, @on\r\nexec sp_trace_setevent @TraceID, 12, 12, @on\r\nexec sp_trace_setevent @TraceID, 12, 13, @on\r\nexec sp_trace_setevent @TraceID, 12, 48, @on\r\n\r\nexec sp_trace_setevent @TraceID, 41, 15, @on\r\nexec sp_trace_setevent @TraceID, 41, 16, @on\r\nexec sp_trace_setevent @TraceID, 41, 1, @on\r\nexec sp_trace_setevent @TraceID, 41, 9, @on\r\nexec sp_trace_setevent @TraceID, 41, 17, @on\r\nexec sp_trace_setevent @TraceID, 41, 10, @on\r\nexec sp_trace_setevent @TraceID, 41, 18, @on\r\nexec sp_trace_setevent @TraceID, 41, 11, @on\r\nexec sp_trace_setevent @TraceID, 41, 35, @on\r\nexec sp_trace_setevent @TraceID, 41, 12, @on\r\nexec sp_trace_setevent @TraceID, 41, 13, @on\r\nexec sp_trace_setevent @TraceID, 41, 6, @on\r\nexec sp_trace_setevent @TraceID, 41, 14, @on\r\nexec sp_trace_setevent @TraceID, 41, 48, @on\r\n\r\nexec sp_trace_setstatus @TraceID, 1\r\n\r\nselect TraceID=@TraceID\r\ngoto finish\r\n\r\nerror: \r\nselect ErrorCode=@rc\r\n\r\nfinish:\r\n", filePath);
			return base.SQLExecuteScalar(sqlString, cmdText, timeOut);
		}

		public bool Stop(int traceId, string sqlString, int timeOut)
		{
			string cmdText = string.Format("\r\nexec sp_trace_setstatus {0}, 0\r\nexec sp_trace_setstatus {0}, 2", traceId);
			return base.ExecuteNonQuery(sqlString, cmdText, timeOut);
		}

		public string GetTracePath(int traceId, string sqlString, int timeOut)
		{
			string cmdText = string.Format("SELECT [path] FROM sys.traces WHERE id = {0}", traceId);
			object obj = base.SQLExecuteScalar(sqlString, cmdText, timeOut);
			if (obj != null)
			{
				return obj.ToString();
			}
			return string.Empty;
		}

		public List<CollectFlag> GetFlagList(string oleDBString, int timeOut)
		{
			List<CollectFlag> list = new List<CollectFlag>();
			OleDbConnection oleDbConnection = null;
			if (!Util.TryConnect(oleDBString, out oleDbConnection))
			{
				return list;
			}
			try
			{
				OleDbCommand oleDbCommand = oleDbConnection.CreateCommand();
				oleDbCommand.CommandTimeout = timeOut;
				string commandText = "SELECT FlagName,StartTime,EndTime,Description from tblFlagDescription";
				oleDbCommand.CommandText = commandText;
				oleDbConnection.Open();
				OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader(CommandBehavior.CloseConnection);
				while (oleDbDataReader.Read())
				{
					list.Add(new CollectFlag
					{
						Name = oleDbDataReader["FlagName"].ToString(),
						StartTime = (DateTime)oleDbDataReader["StartTime"],
						EndTime = (DateTime)oleDbDataReader["EndTime"],
						Description = oleDbDataReader["Description"].ToString()
					});
				}
				oleDbConnection.Close();
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
			}
			finally
			{
				if (oleDbConnection.State != ConnectionState.Closed)
				{
					oleDbConnection.Close();
				}
				oleDbConnection.Dispose();
			}
			return list;
		}
	}
}
