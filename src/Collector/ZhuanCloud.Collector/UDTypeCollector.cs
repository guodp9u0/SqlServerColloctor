using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text;

namespace ZhuanCloud.Collector
{
	public class UDTypeCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(sqlString, sqlVersion, timeOut);
				if (data.Rows.Count > 0)
				{
					DataTable dataTable = new DataTable();
					dataTable.Columns.Add(new DataColumn("db_name"));
					dataTable.Columns.Add(new DataColumn("schema_name"));
					dataTable.Columns.Add(new DataColumn("object_name"));
					dataTable.Columns.Add(new DataColumn("define"));
					dataTable.Columns.Add(new DataColumn("db_type"));
					dataTable.Columns.Add(new DataColumn("create_date"));
					dataTable.Columns.Add(new DataColumn("modify_date"));
					SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder(sqlString);
					string initialCatalog = sqlConnectionStringBuilder.InitialCatalog;
					foreach (DataRow dataRow in data.Rows)
					{
						string text = dataRow.ItemArray[0].ToString();
						string text2 = dataRow.ItemArray[1].ToString();
						string arg = dataRow.ItemArray[2].ToString();
						string text3 = dataRow.ItemArray[3].ToString();
						DateTime dateTime = DateTime.Parse(dataRow.ItemArray[4].ToString());
						DateTime dateTime2 = DateTime.Parse(dataRow.ItemArray[5].ToString());
						StringBuilder stringBuilder = new StringBuilder();
						stringBuilder.AppendLine("-- CLR 用户定义类型");
						stringBuilder.AppendLine(string.Format("CREATE TYPE [{0}].[{1}]", text, text2));
						stringBuilder.AppendLine(string.Format("EXTERNAL NAME {0}[{1}]", string.IsNullOrEmpty(text3) ? "" : string.Format("[{0}].", text3), arg));
						stringBuilder.AppendLine("GO");
						string text4 = TextManager.Compress(stringBuilder.ToString());
						dataTable.Rows.Add(new object[]
						{
							initialCatalog,
							text,
							text2,
							text4,
							"用户自定义类型",
							dateTime,
							dateTime2
						});
					}
					if (dataTable.Rows.Count > 0)
					{
						if (!Util.startFlag)
						{
							result = false;
							return result;
						}
						string cmdText = "\r\nINSERT  INTO tblDBUserType\r\n        ( DBName ,\r\n          SchemaName ,\r\n          ObjectName ,\r\n          Define ,\r\n          DBType ,\r\n          CreateDate ,\r\n          ModifyDate \r\n        )\r\nVALUES  ( @DBName ,\r\n          @SchemaName ,\r\n          @ObjectName ,\r\n          @Define ,\r\n          @DBType ,\r\n          @CreateDate ,\r\n          @ModifyDate \r\n        )\r\n";
						OleDbParameter[] parameters = new OleDbParameter[]
						{
							new OleDbParameter("@DBName", OleDbType.VarChar)
							{
								SourceColumn = "db_name"
							},
							new OleDbParameter("@SchemaName", OleDbType.VarChar)
							{
								SourceColumn = "schema_name"
							},
							new OleDbParameter("@ObjectName", OleDbType.VarChar)
							{
								SourceColumn = "object_name"
							},
							new OleDbParameter("@Define", OleDbType.VarChar)
							{
								SourceColumn = "define"
							},
							new OleDbParameter("@DBType", OleDbType.VarChar)
							{
								SourceColumn = "db_type"
							},
							new OleDbParameter("@CreateDate", OleDbType.Date)
							{
								SourceColumn = "create_date"
							},
							new OleDbParameter("@ModifyDate", OleDbType.Date)
							{
								SourceColumn = "modify_date"
							}
						};
						result = base.SaveToAccess(oleDBString, dataTable, cmdText, parameters);
						return result;
					}
				}
				result = true;
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			string cmdText = string.Empty;
			if (sqlVersion <= SQLServerVersion.SQLServer2005)
			{
				cmdText = "\r\nSELECT  s.name AS [schema_name] ,\r\n        ut.name AS user_type ,\r\n        asb.name AS assembly_name ,\r\n        ISNULL(ast.assembly_class, N'''') AS class_name ,\r\n        asb.create_date ,\r\n        asb.modify_date\r\nFROM    sys.types ut ,\r\n        sys.schemas s ,\r\n        sys.assemblies asb ,\r\n        sys.assembly_types ast\r\nWHERE   ut.[schema_id] = s.[schema_id]\r\n        AND ut.user_type_id = ast.user_type_id\r\n        AND ast.assembly_id = asb.assembly_id\r\n        AND ut.is_user_defined = 1\r\n        AND ut.is_assembly_type = 1\r\n";
			}
			else
			{
				cmdText = "\r\nSELECT  s.name AS [schema_name] ,\r\n        ut.name AS user_type ,\r\n        asb.name AS assembly_name ,\r\n        ISNULL(ast.assembly_class, N'''') AS class_name ,\r\n        asb.create_date ,\r\n        asb.modify_date\r\nFROM    sys.types ut ,\r\n        sys.schemas s ,\r\n        sys.assemblies asb ,\r\n        sys.assembly_types ast\r\nWHERE   ut.[schema_id] = s.[schema_id]\r\n        AND ut.user_type_id = ast.user_type_id\r\n        AND ast.assembly_id = asb.assembly_id\r\n        AND ut.is_user_defined = 1\r\n        AND ut.is_assembly_type = 1\r\n        AND ut.is_table_type = 0\r\n";
			}
			return base.FillDataTable(sqlString, cmdText, timeOut);
		}
	}
}
