using System;

namespace ZhuanCloud.Collector
{
	internal class UnusedSessionHash
	{
		public string LastIndividualQuery
		{
			get;
			set;
		}

		public string TextHashCode
		{
			get;
			set;
		}
	}
}
