using System;

namespace ZhuanCloud.Collector
{
	public class DBFilter
	{
		public string InstanceName
		{
			get;
			set;
		}

		public string DBName
		{
			get;
			set;
		}

		public int DBType
		{
			get;
			set;
		}

		public string FilterString
		{
			get;
			set;
		}

		public DBFilter()
		{
		}

		public DBFilter(string instanceName, string dbName, int dbType, string filterString)
		{
			this.InstanceName = instanceName;
			this.DBName = dbName;
			this.DBType = dbType;
			this.FilterString = filterString;
		}
	}
}
