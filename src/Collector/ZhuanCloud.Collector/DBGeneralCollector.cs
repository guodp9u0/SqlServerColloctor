using System;
using System.Data;
using System.Data.OleDb;

namespace ZhuanCloud.Collector
{
	public class DBGeneralCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(sqlString, sqlVersion, timeOut);
				if (data.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblDBGeneral\r\n        ( DBName ,\r\n          DatabaseId ,\r\n          DBSize ,\r\n          CollationName ,\r\n          RecoveryModelDesc ,\r\n          CompatibilityLevel ,\r\n          IsBrokerEnabled ,\r\n          PageVerifyOptionDesc ,\r\n          IsParameterizationForced ,\r\n          IsRecursiveTriggersOn ,\r\n          SnapshotIsolationStateDesc ,\r\n          IsReadCommittedSnapshotOn ,\r\n          IsAutoCreateStatsOn ,\r\n          IsAutoUpdateStatsOn ,\r\n          IsAutoCloseOn ,\r\n          IsAutoShrinkOn ,\r\n          IsAutoUpdateStatsAsyncOn ,\r\n          SourceCreateDate ,\r\n          CreateDate ,\r\n          CheckdbDate ,\r\n          CDCEnabled\r\n        )\r\nVALUES  ( @DBName ,\r\n          @DatabaseId ,\r\n          @DBSize ,\r\n          @CollationName ,\r\n          @RecoveryModelDesc ,\r\n          @CompatibilityLevel ,\r\n          @IsBrokerEnabled ,\r\n          @PageVerifyOptionDesc ,\r\n          @IsParameterizationForced ,\r\n          @IsRecursiveTriggersOn ,\r\n          @SnapshotIsolationStateDesc ,\r\n          @IsReadCommittedSnapshotOn ,\r\n          @IsAutoCreateStatsOn ,\r\n          @IsAutoUpdateStatsOn ,\r\n          @IsAutoCloseOn ,\r\n          @IsAutoShrinkOn ,\r\n          @IsAutoUpdateStatsAsyncOn ,\r\n          @SourceCreateDate ,\r\n          @CreateDate ,\r\n          @CheckdbDate ,\r\n          @CDCEnabled\r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@DBName", OleDbType.VarChar)
						{
							SourceColumn = "name"
						},
						new OleDbParameter("@DatabaseId", OleDbType.Integer)
						{
							SourceColumn = "database_id"
						},
						new OleDbParameter("@DBSize", OleDbType.Double)
						{
							SourceColumn = "dbsize_mb"
						},
						new OleDbParameter("@CollationName", OleDbType.VarChar)
						{
							SourceColumn = "collation_name"
						},
						new OleDbParameter("@RecoveryModelDesc", OleDbType.VarChar)
						{
							SourceColumn = "recovery_model_desc"
						},
						new OleDbParameter("@CompatibilityLevel", OleDbType.Integer)
						{
							SourceColumn = "compatibility_level"
						},
						new OleDbParameter("@IsBrokerEnabled", OleDbType.Boolean)
						{
							SourceColumn = "is_broker_enabled"
						},
						new OleDbParameter("@PageVerifyOptionDesc", OleDbType.VarChar)
						{
							SourceColumn = "page_verify_option_desc"
						},
						new OleDbParameter("@IsParameterizationForced", OleDbType.Boolean)
						{
							SourceColumn = "is_parameterization_forced"
						},
						new OleDbParameter("@IsRecursiveTriggersOn", OleDbType.Boolean)
						{
							SourceColumn = "is_recursive_triggers_on"
						},
						new OleDbParameter("@SnapshotIsolationStateDesc", OleDbType.VarChar)
						{
							SourceColumn = "snapshot_isolation_state_desc"
						},
						new OleDbParameter("@IsReadCommittedSnapshotOn", OleDbType.Boolean)
						{
							SourceColumn = "is_read_committed_snapshot_on"
						},
						new OleDbParameter("@IsAutoCreateStatsOn", OleDbType.Boolean)
						{
							SourceColumn = "is_auto_create_stats_on"
						},
						new OleDbParameter("@IsAutoUpdateStatsOn", OleDbType.Boolean)
						{
							SourceColumn = "is_auto_update_stats_on"
						},
						new OleDbParameter("@IsAutoCloseOn", OleDbType.Boolean)
						{
							SourceColumn = "is_auto_close_on"
						},
						new OleDbParameter("@IsAutoShrinkOn", OleDbType.Boolean)
						{
							SourceColumn = "is_auto_shrink_on"
						},
						new OleDbParameter("@IsAutoUpdateStatsAsyncOn", OleDbType.Boolean)
						{
							SourceColumn = "is_auto_update_stats_async_on"
						},
						new OleDbParameter("@SourceCreateDate", OleDbType.Date)
						{
							SourceColumn = "source_create_date"
						},
						new OleDbParameter("@CreateDate", OleDbType.Date)
						{
							SourceColumn = "create_date"
						},
						new OleDbParameter("@CheckdbDate", OleDbType.Date)
						{
							SourceColumn = "checkdb_date"
						},
						new OleDbParameter("@CDCEnabled", OleDbType.Boolean)
						{
							SourceColumn = "is_cdc_enabled"
						}
					};
					result = base.SaveToAccess(oleDBString, data, cmdText, parameters);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			string cmdText = string.Empty;
			if (sqlVersion >= SQLServerVersion.SQLServer2008)
			{
				cmdText = "\r\nCREATE TABLE #expert_dbcc\r\n    (\r\n      ID INT IDENTITY(1, 1)\r\n             PRIMARY KEY ,\r\n      ParentObject VARCHAR(255) ,\r\n      ChildObject VARCHAR(255) ,\r\n      Field VARCHAR(255) ,\r\n      ConfigValue VARCHAR(255)\r\n    ) \r\n\r\nINSERT  INTO #expert_dbcc\r\n        EXEC ( 'DBCC DBInfo() With TableResults, NO_INFOMSGS')\r\nDECLARE @dbcheck_date DATETIME\r\nSELECT DISTINCT  @dbcheck_date = ConfigValue\r\n        FROM    #expert_dbcc\r\n        WHERE   Field = 'dbi_dbccLastKnownGood'\r\nDECLARE @source_create_date DATETIME\r\nSELECT DISTINCT  @source_create_date = ConfigValue\r\n        FROM    #expert_dbcc\r\n        WHERE   Field = 'dbi_crdate'\r\nDROP TABLE #expert_dbcc\r\n\r\nDECLARE @db_size_mb FLOAT\r\nSELECT @db_size_mb=ROUND(( CONVERT(FLOAT, SUM(size)) * ( 8192.0 / 1024.0 ) / 1024 ), 2) FROM sys.database_files\r\n\r\nSELECT  name ,\r\n        database_id ,\r\n        @db_size_mb AS dbsize_mb ,\r\n        collation_name ,\r\n        recovery_model_desc ,\r\n        [compatibility_level] ,\r\n        is_broker_enabled ,\r\n        page_verify_option_desc ,\r\n        is_parameterization_forced ,\r\n        is_recursive_triggers_on ,\r\n        snapshot_isolation_state_desc ,\r\n        is_read_committed_snapshot_on ,\r\n        is_auto_create_stats_on ,\r\n        is_auto_update_stats_on ,\r\n        is_auto_close_on ,\r\n        is_auto_shrink_on ,\r\n        is_auto_update_stats_async_on ,\r\n        @source_create_date AS source_create_date ,\r\n        create_date ,\r\n        @dbcheck_date AS checkdb_date ,\r\n        is_cdc_enabled\r\nFROM    sys.databases db\r\nWHERE   db.database_id = DB_ID()\r\nORDER BY database_id\r\n";
			}
			else
			{
				cmdText = "\r\nCREATE TABLE #expert_dbcc\r\n    (\r\n      ID INT IDENTITY(1, 1)\r\n             PRIMARY KEY ,\r\n      ParentObject VARCHAR(255) ,\r\n      ChildObject VARCHAR(255) ,\r\n      Field VARCHAR(255) ,\r\n      ConfigValue VARCHAR(255)\r\n    ) \r\n\r\nINSERT  INTO #expert_dbcc\r\n        EXEC ( 'DBCC DBInfo() With TableResults, NO_INFOMSGS')\r\nDECLARE @dbcheck_date DATETIME\r\nSELECT DISTINCT  @dbcheck_date = ConfigValue\r\n        FROM    #expert_dbcc\r\n        WHERE   Field = 'dbi_dbccLastKnownGood'\r\nDECLARE @source_create_date DATETIME\r\nSELECT DISTINCT  @source_create_date = ConfigValue\r\n        FROM    #expert_dbcc\r\n        WHERE   Field = 'dbi_crdate'\r\nDROP TABLE #expert_dbcc\r\n\r\nDECLARE @db_size_mb FLOAT\r\nSELECT @db_size_mb=ROUND(( CONVERT(FLOAT, SUM(size)) * ( 8192.0 / 1024.0 ) / 1024 ), 2) FROM sys.database_files\r\n\r\nSELECT  name ,\r\n        database_id ,\r\n        @db_size_mb AS dbsize_mb ,\r\n        collation_name ,\r\n        recovery_model_desc ,\r\n        [compatibility_level] ,\r\n        is_broker_enabled ,\r\n        page_verify_option_desc ,\r\n        is_parameterization_forced ,\r\n        is_recursive_triggers_on ,\r\n        snapshot_isolation_state_desc ,\r\n        is_read_committed_snapshot_on ,\r\n        is_auto_create_stats_on ,\r\n        is_auto_update_stats_on ,\r\n        is_auto_close_on ,\r\n        is_auto_shrink_on ,\r\n        is_auto_update_stats_async_on ,\r\n        @source_create_date AS source_create_date ,\r\n        create_date ,\r\n        @dbcheck_date AS checkdb_date ,\r\n        0 AS is_cdc_enabled\r\nFROM    sys.databases db\r\nWHERE   db.database_id = DB_ID()\r\nORDER BY database_id\r\n";
			}
			return base.FillDataTable(sqlString, cmdText, timeOut);
		}
	}
}
