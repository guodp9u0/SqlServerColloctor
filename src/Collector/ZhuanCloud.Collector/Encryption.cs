using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace ZhuanCloud.Collector
{
	public class Encryption
	{
		public static readonly string clientPubKey = "<RSAKeyValue><Modulus>njTuGPGWAaf0D2oYtRHJWHHnO2RJSLnRFWWNoUvLQyBYXTRDkplfi9/Q48iuDP2VuxKeMOJRqfI3VpIL1Hx7VGstKJ6uoeqIJCuyBTUeDKhq9F1v6QH6uO2X0S5HNxBf7JguVm6Fqb1hvp5uxVPSVmTrTr9tMbsffFxclB5h4k0=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";

		public static readonly string priKey = "<RSAKeyValue><Modulus>ua7tBwvYh5GztLFsM4UTBroXaTzJLrIpC5v6D8UqvXoLK/jHQEsPl8DrpYkklRKkmUQJ+juMeSn+bpa+aN9pR2egHoXDpA2Y9SUjmayt0WLhgZkdcwpw9xPfi1HemG9kzEoLZPF/Ttu2z8b3yNciYl9rm1QFjdiGsWREyZKh5bE=</Modulus><Exponent>AQAB</Exponent><P>7NDFvPGkmmSFKayZO34du37QoM/cyxk1ZysSdBzwRxMu0C0RsmUOon+h9yYvo0NDyObeqfo8+0yp+Erk2P3oCQ==</P><Q>yLm958X8qi7bLJVXK8zMq0C5enud+kImoBx3Ye9950717uhCzQvxe0ETtT/d3xu9X49oge6djGzUwxWfw5pqaQ==</Q><DP>yK1h42siAXONJHOOpkuJBonAYYXDpJgIGRT23k5njtNCqEaI6JOagzmFFtrVI5SsrGJ37bKQWqPspOtSYk9HgQ==</DP><DQ>c7r0DWAPz6d4Yvov6mcIUmzdBAumCBZ/4qzHAEZvFyBll7t+WG7cai4ZSNVr/SknRKOQo+mKGN7exSQAm2y6kQ==</DQ><InverseQ>QgVrcReGrHbKQC4ExSHiHUG17pmsDgEb2Z3lY3guJoSBW3QWzpIdZHGUJ2AYgbC75KNEUqjE+XNlwTORu1uhAg==</InverseQ><D>IGx/T1+uK94xdzgUINr3+5C7TdEjgFfz+ub9v4s6b2PEV6VVhdCVpHY3r+xGyxE6VNkgtOV5Hh9LIpyIG2AHzffO9gCeTE41/0ow9ZH8N0GH3OPHqobFwxFr6bZpeJ9pi0Akv7Jq9S0OsHg7Qm8Gvw8Nv+hROq69Zj8/vRsJiQE=</D></RSAKeyValue>";

		private void CreateKeys()
		{
			string[] array = new string[2];
			RSACryptoServiceProvider rSACryptoServiceProvider = new RSACryptoServiceProvider();
			array[0] = rSACryptoServiceProvider.ToXmlString(false);
			array[1] = rSACryptoServiceProvider.ToXmlString(true);
		}

		public static string EncryptRSA(string sourceString, string pubKey)
		{
			string result = string.Empty;
			try
			{
				byte[] bytes = TextManager.Encoding.GetBytes(sourceString);
				RSACryptoServiceProvider rSACryptoServiceProvider = new RSACryptoServiceProvider();
				rSACryptoServiceProvider.FromXmlString(pubKey);
				int num = rSACryptoServiceProvider.KeySize / 8;
				int num2 = num - 11;
				if (bytes.Length > num2)
				{
					int num3;
					if (bytes.Length % num2 != 0)
					{
						num3 = bytes.Length / num2 + 1;
					}
					else
					{
						num3 = bytes.Length / num2;
					}
					byte[] array = new byte[num3 * num];
					for (int i = 0; i < num3; i++)
					{
						byte[] array2 = new byte[num2];
						if (i == num3 - 1)
						{
							Array.Copy(bytes, i * num2, array2, 0, bytes.Length % num2);
						}
						else
						{
							Array.Copy(bytes, i * num2, array2, 0, num2);
						}
						byte[] sourceArray = rSACryptoServiceProvider.Encrypt(array2, false);
						Array.Copy(sourceArray, 0, array, i * num, num);
					}
					result = Convert.ToBase64String(array);
				}
				else
				{
					byte[] inArray = rSACryptoServiceProvider.Encrypt(bytes, false);
					result = Convert.ToBase64String(inArray);
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
			}
			return result;
		}

		public static string DecryptRSA(string sourceString, string priKey)
		{
			string result = string.Empty;
			try
			{
				byte[] array = Convert.FromBase64String(sourceString);
				RSACryptoServiceProvider rSACryptoServiceProvider = new RSACryptoServiceProvider();
				rSACryptoServiceProvider.FromXmlString(priKey);
				int num = rSACryptoServiceProvider.KeySize / 8;
				int num2 = num - 11;
				if (array.Length == num)
				{
					byte[] bytes = rSACryptoServiceProvider.Decrypt(array, false);
					result = TextManager.Encoding.GetString(bytes);
				}
				else
				{
					int num3 = 0;
					List<byte[]> list = new List<byte[]>();
					int num4 = array.Length / num;
					for (int i = 0; i < num4; i++)
					{
						byte[] array2 = new byte[num];
						Array.Copy(array, i * num, array2, 0, num);
						byte[] array3 = rSACryptoServiceProvider.Decrypt(array2, false);
						num3 += array3.Length;
						list.Add(array3);
					}
					byte[] array4 = new byte[num3];
					for (int j = 0; j < list.Count; j++)
					{
						Array.Copy(list[j], 0, array4, j * num2, num2);
					}
					result = TextManager.Encoding.GetString(array4);
				}
			}
			catch
			{
				return result;
			}
			return result;
		}
	}
}
