using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text;

namespace ZhuanCloud.Collector
{
	public class StoredProcCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut, string condition, bool isDecrypt)
		{
			bool result;
			try
			{
				int majorVersion = base.GetMajorVersion(sqlVersion);
				DataTable data = this.GetData(sqlString, sqlVersion, timeOut, condition);
				if (data.Rows.Count > 0)
				{
					DataTable dataTable = new DataTable();
					dataTable.Columns.Add(new DataColumn("db_name"));
					dataTable.Columns.Add(new DataColumn("schema_name"));
					dataTable.Columns.Add(new DataColumn("object_name"));
					dataTable.Columns.Add(new DataColumn("define"));
					dataTable.Columns.Add(new DataColumn("create_date"));
					dataTable.Columns.Add(new DataColumn("modify_date"));
					dataTable.Columns.Add(new DataColumn("first_exec_date"));
					dataTable.Columns.Add(new DataColumn("last_exec_date"));
					dataTable.Columns.Add(new DataColumn("exec_count"));
					SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder(sqlString);
					string initialCatalog = sqlConnectionStringBuilder.InitialCatalog;
					DateTime dateTime = DateTime.Parse("1900/01/01 00:00:00");
					foreach (DataRow dataRow in data.Rows)
					{
						StringBuilder stringBuilder = new StringBuilder();
						string objectID = dataRow.ItemArray[0].ToString();
						string text = dataRow.ItemArray[1].ToString();
						string text2 = dataRow.ItemArray[2].ToString();
						DateTime dateTime2 = DateTime.Parse(dataRow.ItemArray[3].ToString());
						DateTime dateTime3 = DateTime.Parse(dataRow.ItemArray[4].ToString());
						DateTime dateTime4 = (dataRow.ItemArray[5] == DBNull.Value) ? dateTime : DateTime.Parse(dataRow.ItemArray[5].ToString());
						DateTime dateTime5 = (dataRow.ItemArray[6] == DBNull.Value) ? dateTime : DateTime.Parse(dataRow.ItemArray[6].ToString());
						double num = (dataRow.ItemArray[7] == DBNull.Value) ? 0.0 : Convert.ToDouble(dataRow.ItemArray[7]);
						string a = dataRow.ItemArray[8].ToString().Trim().ToUpper();
						string value = string.Empty;
						if (a == "P")
						{
							stringBuilder.AppendLine(string.Format("USE [{0}]", initialCatalog));
							stringBuilder.AppendLine("GO");
							if (dataRow.ItemArray[9] != DBNull.Value)
							{
								bool flag = Convert.ToBoolean(dataRow.ItemArray[9]);
								stringBuilder.AppendLine(string.Format("SET ANSI_NULLS {0}", flag ? "ON" : "OFF"));
								stringBuilder.AppendLine("GO");
							}
							if (dataRow.ItemArray[10] != DBNull.Value)
							{
								bool flag2 = Convert.ToBoolean(dataRow.ItemArray[10]);
								stringBuilder.AppendLine(string.Format("SET QUOTED_IDENTIFIER {0}", flag2 ? "ON" : "OFF"));
								stringBuilder.AppendLine("GO");
							}
							value = dataRow.ItemArray[11].ToString();
							if (string.IsNullOrEmpty(value))
							{
								if (isDecrypt)
								{
									try
									{
										value = Engine.Decrypt(sqlString, initialCatalog, objectID, majorVersion);
										goto IL_38C;
									}
									catch (Exception innerException)
									{
										value = "-- 对象无法解密";
										ErrorLog.Write(new Exception(string.Format("db_name:{0},stored_proc:{1}.{2}", initialCatalog, text, text2), innerException));
										continue;
									}
								}
								value = "-- 对象不允许解密";
							}
						}
						else if (a == "PC")
						{
							value = "-- CLR 存储过程，无法解密";
						}
						else if (a == "X")
						{
							value = "-- 扩展存储过程，无法解密";
						}
						else
						{
							value = "-- 未知存储过程，无法解密";
						}
						IL_38C:
						stringBuilder.AppendLine(value);
						stringBuilder.AppendLine("GO");
						string text3 = TextManager.Compress(stringBuilder.ToString());
						dataTable.Rows.Add(new object[]
						{
							initialCatalog,
							text,
							text2,
							text3,
							dateTime2,
							dateTime3,
							dateTime4,
							dateTime5,
							num
						});
					}
					if (dataTable.Rows.Count > 0)
					{
						if (!Util.startFlag)
						{
							result = false;
							return result;
						}
						string cmdText = "\r\nINSERT  INTO tblDBStoredProc\r\n        ( DBName ,\r\n          SchemaName ,\r\n          ObjectName ,\r\n          Define ,\r\n          CreateDate ,\r\n          ModifyDate ,\r\n          FirstExecDate ,\r\n          LastExecDate ,\r\n          ExecCount\r\n        )\r\nVALUES  ( @DBName ,\r\n          @SchemaName ,\r\n          @ObjectName ,\r\n          @Define ,\r\n          @CreateDate ,\r\n          @ModifyDate ,\r\n          @FirstExecDate ,\r\n          @LastExecDate ,\r\n          @ExecCount\r\n        )\r\n";
						OleDbParameter[] parameters = new OleDbParameter[]
						{
							new OleDbParameter("@DBName", OleDbType.VarChar)
							{
								SourceColumn = "db_name"
							},
							new OleDbParameter("@SchemaName", OleDbType.VarChar)
							{
								SourceColumn = "schema_name"
							},
							new OleDbParameter("@ObjectName", OleDbType.VarChar)
							{
								SourceColumn = "object_name"
							},
							new OleDbParameter("@Define", OleDbType.VarChar)
							{
								SourceColumn = "define"
							},
							new OleDbParameter("@CreateDate", OleDbType.Date)
							{
								SourceColumn = "create_date"
							},
							new OleDbParameter("@ModifyDate", OleDbType.Date)
							{
								SourceColumn = "modify_date"
							},
							new OleDbParameter("@FirstExecDate", OleDbType.Date)
							{
								SourceColumn = "first_exec_date"
							},
							new OleDbParameter("@LastExecDate", OleDbType.Date)
							{
								SourceColumn = "last_exec_date"
							},
							new OleDbParameter("@ExecCount", OleDbType.Double)
							{
								SourceColumn = "exec_count"
							}
						};
						result = base.SaveToAccess(oleDBString, dataTable, cmdText, parameters);
						return result;
					}
				}
				result = true;
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, SQLServerVersion sqlVersion, int timeOut, string condition)
		{
			string cmdText = string.Empty;
			string arg = string.Empty;
			if (!string.IsNullOrEmpty(condition))
			{
				string[] array = condition.Split(new string[]
				{
					";"
				}, StringSplitOptions.RemoveEmptyEntries);
				if (array.Length > 0)
				{
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.Append(" AND ");
					int num = 1;
					string[] array2 = array;
					for (int i = 0; i < array2.Length; i++)
					{
						string text = array2[i];
						string[] array3 = text.Split(new string[]
						{
							"."
						}, StringSplitOptions.RemoveEmptyEntries);
						if (array3.Length == 1)
						{
							stringBuilder.AppendFormat("  v.name NOT LIKE '{0}' ", array3[0]);
						}
						else if (array3.Length == 2)
						{
							stringBuilder.AppendFormat(" (s.name NOT LIKE '{0}' OR v.name NOT LIKE '{1}') ", array3[0], array3[1]);
						}
						if (num < array.Length)
						{
							stringBuilder.Append(" AND ");
						}
						num++;
					}
					arg = stringBuilder.ToString();
				}
			}
			if (sqlVersion >= SQLServerVersion.SQLServer2008)
			{
				cmdText = string.Format("\r\nSELECT  v.[object_id] AS [objId] ,\r\n        s.name AS [schema_name] ,\r\n        v.name AS proc_name ,\r\n        v.create_date ,\r\n        v.modify_date ,\r\n        pro.first_execution_time ,\r\n        pro.last_execution_time ,\r\n        pro.execution_count ,\r\n        v.[type] ,\r\n        m.uses_ansi_nulls AS ANSI ,\r\n        m.uses_quoted_identifier AS Quoted ,\r\n        m.[definition]\r\nFROM    sys.procedures v\r\n        INNER JOIN sys.schemas s ON v.[schema_id] = s.[schema_id]\r\n                                    AND OBJECTPROPERTY(v.[object_id],'IsMSShipped') = 0 {0} \r\n        LEFT JOIN sys.sql_modules m ON v.[object_id] = m.[object_id]\r\n        LEFT JOIN ( SELECT  database_id ,\r\n                            [object_id] ,\r\n                            MIN(cached_time) AS first_execution_time ,\r\n                            MAX(last_execution_time) AS last_execution_time ,\r\n                            SUM(execution_count) AS execution_count\r\n                    FROM    sys.dm_exec_procedure_stats\r\n                    WHERE   database_id = DB_ID()\r\n                    GROUP BY database_id ,\r\n                            [object_id]\r\n                  ) pro ON v.[object_id] = pro.[object_id]\r\n                           AND m.[object_id] = pro.[object_id]\r\nOPTION (MAXDOP 2)\r\n", arg);
			}
			else
			{
				cmdText = string.Format("\r\nSELECT  v.[object_id] AS [objId] ,\r\n        s.name AS [schema_name] ,\r\n        v.name AS proc_name ,\r\n        v.create_date ,\r\n        v.modify_date ,\r\n        NULL AS first_execution_time ,\r\n        NULL AS last_execution_time ,\r\n        NULL AS execution_count ,\r\n        v.[type] ,\r\n        m.uses_ansi_nulls AS ANSI ,\r\n        m.uses_quoted_identifier AS Quoted ,\r\n        m.[definition]\r\nFROM    sys.procedures v\r\n        INNER JOIN sys.schemas s ON v.[schema_id] = s.[schema_id]\r\n                                    AND OBJECTPROPERTY(v.[object_id], 'IsMSShipped') = 0 {0} \r\n        LEFT JOIN sys.sql_modules m ON v.[object_id] = m.[object_id]\r\nOPTION (MAXDOP 2)\r\n", arg);
			}
			return base.FillDataTable(sqlString, cmdText, timeOut);
		}
	}
}
