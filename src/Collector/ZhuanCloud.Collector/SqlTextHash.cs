using System;

namespace ZhuanCloud.Collector
{
	public class SqlTextHash
	{
		public string TextData
		{
			get;
			set;
		}

		public string HashCode
		{
			get;
			set;
		}
	}
}
