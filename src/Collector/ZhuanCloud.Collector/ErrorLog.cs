using System;
using System.IO;
using System.Text;

namespace ZhuanCloud.Collector
{
	public class ErrorLog
	{
		public static void Write(Exception ex)
		{
			if (string.IsNullOrEmpty(Util.collectDir))
			{
				Util.collectDir = AppDomain.CurrentDomain.BaseDirectory;
			}
			string path = Path.Combine(Util.collectDir, "SZC_ErrorLog.txt");
			try
			{
				using (FileStream fileStream = new FileStream(path, FileMode.Append))
				{
					using (StreamWriter streamWriter = new StreamWriter(fileStream))
					{
						streamWriter.WriteLine("**********************************************");
						StringBuilder stringBuilder = new StringBuilder();
						DateTime now = DateTime.Now;
						stringBuilder.AppendLine(now.ToString("yyyy-MM-dd HH:mm:ss"));
						if (ex.StackTrace != null)
						{
							stringBuilder.AppendLine(ex.StackTrace);
						}
						stringBuilder.AppendLine(ex.Message);
						if (ex.InnerException != null)
						{
							stringBuilder.AppendLine(ex.InnerException.Message);
						}
						streamWriter.WriteLine(stringBuilder.ToString());
						Util.SaveErrorInfo(now, stringBuilder.ToString());
					}
				}
			}
			catch
			{
			}
		}

		public static void Info(string message)
		{
			if (string.IsNullOrEmpty(Util.collectDir))
			{
				Util.collectDir = AppDomain.CurrentDomain.BaseDirectory;
			}
			string path = Path.Combine(Util.collectDir, "SZC_Info.txt");
			try
			{
				using (FileStream fileStream = new FileStream(path, FileMode.Append))
				{
					using (StreamWriter streamWriter = new StreamWriter(fileStream))
					{
						streamWriter.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
						streamWriter.WriteLine(message);
						streamWriter.WriteLine();
					}
				}
			}
			catch
			{
			}
		}

		public static void Debug(string message)
		{
			if (string.IsNullOrEmpty(Util.collectDir))
			{
				Util.collectDir = AppDomain.CurrentDomain.BaseDirectory;
			}
			string path = Path.Combine(Util.collectDir, "SZC_Debug.txt");
			try
			{
				using (FileStream fileStream = new FileStream(path, FileMode.Append))
				{
					using (StreamWriter streamWriter = new StreamWriter(fileStream))
					{
						streamWriter.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
						streamWriter.WriteLine(message);
						streamWriter.WriteLine();
					}
				}
			}
			catch
			{
			}
		}
	}
}
