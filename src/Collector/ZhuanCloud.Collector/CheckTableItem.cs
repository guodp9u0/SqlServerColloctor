using System;

namespace ZhuanCloud.Collector
{
	internal class CheckTableItem
	{
		public int ObjectID
		{
			get;
			set;
		}

		public string SchemeName
		{
			get;
			set;
		}

		public string TableName
		{
			get;
			set;
		}
	}
}
