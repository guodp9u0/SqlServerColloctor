using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace ZhuanCloud.Collector
{
	public class DBCollector : OleDbCollector
	{
		public bool SaveDBList(string oleDBString, List<string> dbList, int timeOut)
		{
			try
			{
				if (dbList.Count > 0)
				{
					foreach (string current in dbList)
					{
						string cmdText = string.Format("INSERT INTO tblCollectDB(DBName) VALUES(?)", current);
						if (!base.SaveToAccess(oleDBString, cmdText, timeOut, new OleDbParameter[]
						{
							new OleDbParameter("@dbName", OleDbType.VarChar)
							{
								Value = current
							}
						}))
						{
							bool result = false;
							return result;
						}
					}
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				bool result = false;
				return result;
			}
			return true;
		}

		public SQLServerVersion GetSQLVersion(string sqlString)
		{
			SQLServerVersion result;
			try
			{
				using (SqlConnection sqlConnection = new SqlConnection(sqlString))
				{
					SqlCommand sqlCommand = sqlConnection.CreateCommand();
					sqlCommand.CommandText = "SELECT SERVERPROPERTY('PRODUCTVERSION') AS [VERSION]";
					sqlConnection.Open();
					object obj = sqlCommand.ExecuteScalar();
					sqlConnection.Close();
					if (obj == null)
					{
						throw new Exception("无法获取实例版本");
					}
					SQLServerVersion sQLServerVersion = SQLServerVersion.SQLServer2005;
					string text = obj.ToString();
					if (text.StartsWith("8."))
					{
						sQLServerVersion = SQLServerVersion.SQLServer2000;
					}
					else if (text.StartsWith("9."))
					{
						sQLServerVersion = SQLServerVersion.SQLServer2005;
					}
					else if (text.StartsWith("10.5"))
					{
						sQLServerVersion = SQLServerVersion.SQLServer2008R2;
					}
					else if (text.StartsWith("10."))
					{
						sQLServerVersion = SQLServerVersion.SQLServer2008;
					}
					else if (text.StartsWith("11."))
					{
						sQLServerVersion = SQLServerVersion.SQLServer2012;
					}
					else if (text.StartsWith("12."))
					{
						sQLServerVersion = SQLServerVersion.SQLServer2014;
					}
					else if (text.StartsWith("13."))
					{
						sQLServerVersion = SQLServerVersion.SQLServer2016;
					}
					result = sQLServerVersion;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				throw new Exception(ex.Message);
			}
			return result;
		}
	}
}
