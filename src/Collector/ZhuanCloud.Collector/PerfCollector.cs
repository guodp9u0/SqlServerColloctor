using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;

namespace ZhuanCloud.Collector
{
	public class PerfCollector : OleDbCollector
	{
		public bool SaveData(string oleDBString, string sqlString, int timeOut, DateTime collectTime, List<Perf> perfList)
		{
			if (perfList != null && perfList.Count > 0)
			{
				DataTable dataTable = new DataTable();
				dataTable.Columns.Add("collect_time", typeof(DateTime));
				dataTable.Columns.Add("perf_id", typeof(int));
				dataTable.Columns.Add("perf_value", typeof(float));
				foreach (Perf current in perfList)
				{
					float num = 0f;
					try
					{
						num = current.PerfCounter.NextValue();
					}
					catch
					{
						num = 0f;
					}
					dataTable.Rows.Add(new object[]
					{
						collectTime,
						current.ID,
						num
					});
				}
				if (dataTable.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblPerfmonValues\r\n        ( CollectTime ,\r\n          PerfID ,\r\n          PerfValue\r\n        )\r\nVALUES  ( @CollectTime ,\r\n          @PerfID ,\r\n          @PerfValue\r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@CollectTime", OleDbType.Date)
						{
							SourceColumn = "collect_time"
						},
						new OleDbParameter("@PerfID", OleDbType.Integer)
						{
							SourceColumn = "perf_id"
						},
						new OleDbParameter("@PerfValue", OleDbType.Single)
						{
							SourceColumn = "perf_value"
						}
					};
					return base.SaveToAccess(oleDBString, dataTable, cmdText, parameters);
				}
			}
			return true;
		}
	}
}
