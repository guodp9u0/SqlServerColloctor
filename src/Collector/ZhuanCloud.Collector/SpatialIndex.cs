using System;

namespace ZhuanCloud.Collector
{
	internal class SpatialIndex
	{
		public string IndexName
		{
			get;
			set;
		}

		public string ColName
		{
			get;
			set;
		}

		public int IndexType
		{
			get;
			set;
		}

		public string TesseSchme
		{
			get;
			set;
		}

		public int Xmin
		{
			get;
			set;
		}

		public int Ymin
		{
			get;
			set;
		}

		public int Xmax
		{
			get;
			set;
		}

		public int Ymax
		{
			get;
			set;
		}

		public string Level_1
		{
			get;
			set;
		}

		public string Level_2
		{
			get;
			set;
		}

		public string Level_3
		{
			get;
			set;
		}

		public string Level_4
		{
			get;
			set;
		}

		public int Cells
		{
			get;
			set;
		}

		public int Factor
		{
			get;
			set;
		}

		public bool HasPadIndex
		{
			get;
			set;
		}

		public bool IsIgnorekey
		{
			get;
			set;
		}

		public bool HasRowLock
		{
			get;
			set;
		}

		public bool HasPageLock
		{
			get;
			set;
		}

		public string FileGroupName
		{
			get;
			set;
		}

		public bool IsDisabled
		{
			get;
			set;
		}
	}
}
