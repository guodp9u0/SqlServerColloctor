using System;

namespace ZhuanCloud.Collector
{
	public class SqlPlan
	{
		public int RowID
		{
			get;
			set;
		}

		public string IndividualQuery
		{
			get;
			set;
		}

		public string ParentQuery
		{
			get;
			set;
		}

		public DateTime CreationTime
		{
			get;
			set;
		}

		public DateTime LastExecutionTime
		{
			get;
			set;
		}

		public double ExecutionCount
		{
			get;
			set;
		}

		public double TotalElapsedTime
		{
			get;
			set;
		}

		public double AvgElapsedTime
		{
			get;
			set;
		}

		public double MinElapsedTime
		{
			get;
			set;
		}

		public double MaxElapsedTime
		{
			get;
			set;
		}

		public double TotalWorkerTime
		{
			get;
			set;
		}

		public double AvgWorkerTime
		{
			get;
			set;
		}

		public double MinWorkerTime
		{
			get;
			set;
		}

		public double MaxWorkerTime
		{
			get;
			set;
		}

		public double TotalLogicalReads
		{
			get;
			set;
		}

		public double AvgLogicalReads
		{
			get;
			set;
		}

		public double MinLogicalReads
		{
			get;
			set;
		}

		public double MaxLogicalReads
		{
			get;
			set;
		}

		public double TotalLogicalWrites
		{
			get;
			set;
		}

		public double AvgLogicalWrites
		{
			get;
			set;
		}

		public double MinLogicalWrites
		{
			get;
			set;
		}

		public double MaxLogicalWrites
		{
			get;
			set;
		}

		public double TotalPhysicalReads
		{
			get;
			set;
		}

		public double AvgPhysicalReads
		{
			get;
			set;
		}

		public double MinPhysicalReads
		{
			get;
			set;
		}

		public double MaxPhysicalReads
		{
			get;
			set;
		}

		public byte[] PlanHandle
		{
			get;
			set;
		}

		public string PlanText
		{
			get;
			set;
		}

		public string TextHashCode
		{
			get;
			set;
		}

		public string ParameterHash
		{
			get;
			set;
		}

		public bool HasMissIndex
		{
			get;
			set;
		}

		public bool HasImplicit
		{
			get;
			set;
		}

		public bool HasSqlPlan
		{
			get;
			set;
		}
	}
}
