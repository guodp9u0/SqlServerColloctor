using System;

namespace ZhuanCloud.Collector
{
	public class CollectFlag
	{
		public string Name
		{
			get;
			set;
		}

		public DateTime StartTime
		{
			get;
			set;
		}

		public DateTime EndTime
		{
			get;
			set;
		}

		public string Description
		{
			get;
			set;
		}

		public CollectFlag()
		{
		}

		public CollectFlag(string name, DateTime time, string description)
		{
			this.Name = name;
			this.StartTime = time;
			this.Description = description;
		}
	}
}
