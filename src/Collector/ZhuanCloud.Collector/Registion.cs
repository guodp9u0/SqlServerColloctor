using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;

namespace ZhuanCloud.Collector
{
	public class Registion
	{
		public string MachineCode
		{
			get
			{
				byte[] bytes = TextManager.Encoding.GetBytes(this.GetMachineCode());
				return Convert.ToBase64String(bytes, Base64FormattingOptions.None);
			}
		}

		public string RegistCode
		{
			get
			{
				return this.GetRegistCode();
			}
		}

		public bool SaveData(string oleDBString, int timeOut)
		{
			OleDbConnection oleDbConnection = null;
			if (!Util.TryConnect(oleDBString, out oleDbConnection))
			{
				return false;
			}
			bool result;
			try
			{
				string machineCode = this.GetMachineCode();
				string value = default(DateTime).ToString("yyyy:MM:dd:HH:mm:ss");
				int value2 = 0;
				int value3 = 0;
				int value4 = 0;
				int value5 = 0;
				int value6 = 0;
				int value7 = 0;
				int value8 = 0;
				int value9 = 0;
				string commandText = "\r\nSELECT CollectionTime ,\r\n\t(SELECT COUNT(*) FROM tblBackup) AS backup_count ,\r\n\t(SELECT COUNT(*) FROM tblCPUInfo) AS cpu_count ,\r\n\t(SELECT COUNT(*) FROM tblDBFile) AS file_count ,\r\n\t(SELECT COUNT(*) FROM tblDBTable) AS object_count ,\r\n\t(SELECT COUNT(*) FROM tblDiskInfo) AS disk_count ,\r\n\t(SELECT COUNT(*) FROM tblSqlPlan) AS plan_count ,\r\n\t(SELECT COUNT(*) FROM tblSqlText) AS query_count ,\r\n\t(SELECT COUNT(*) FROM tblWait) AS wait_count \r\nFROM tblVersion\r\n";
				OleDbCommand oleDbCommand = oleDbConnection.CreateCommand();
				oleDbCommand.CommandTimeout = timeOut;
				oleDbCommand.CommandText = commandText;
				oleDbConnection.Open();
				OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader(CommandBehavior.CloseConnection);
				if (oleDbDataReader.Read())
				{
					value = Convert.ToDateTime(oleDbDataReader["CollectionTime"].ToString()).ToString("yyyy:MM:dd:HH:mm:ss");
					value2 = Convert.ToInt32(oleDbDataReader["backup_count"]);
					value3 = Convert.ToInt32(oleDbDataReader["cpu_count"]);
					value4 = Convert.ToInt32(oleDbDataReader["file_count"]);
					value5 = Convert.ToInt32(oleDbDataReader["object_count"]);
					value6 = Convert.ToInt32(oleDbDataReader["disk_count"]);
					value7 = Convert.ToInt32(oleDbDataReader["plan_count"]);
					value8 = Convert.ToInt32(oleDbDataReader["query_count"]);
					value9 = Convert.ToInt32(oleDbDataReader["wait_count"]);
				}
				oleDbConnection.Close();
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(machineCode);
				stringBuilder.Append("#");
				stringBuilder.Append(value);
				stringBuilder.Append("-");
				stringBuilder.Append(value2);
				stringBuilder.Append("-");
				stringBuilder.Append(value3);
				stringBuilder.Append("-");
				stringBuilder.Append(value4);
				stringBuilder.Append("-");
				stringBuilder.Append(value5);
				stringBuilder.Append("-");
				stringBuilder.Append(value6);
				stringBuilder.Append("-");
				stringBuilder.Append(value7);
				stringBuilder.Append("-");
				stringBuilder.Append(value8);
				stringBuilder.Append("-");
				stringBuilder.Append(value9);
				byte[] bytes = TextManager.Encoding.GetBytes(stringBuilder.ToString());
				string value10 = Convert.ToBase64String(bytes, Base64FormattingOptions.None);
				commandText = "INSERT INTO tblRegistion(SerialNum) VALUES(@SerialNum)";
				OleDbParameter[] values = new OleDbParameter[]
				{
					new OleDbParameter("@SerialNum", OleDbType.LongVarChar)
					{
						Value = value10
					}
				};
				oleDbCommand.CommandText = commandText;
				oleDbCommand.Parameters.Clear();
				oleDbCommand.Parameters.AddRange(values);
				oleDbConnection.Open();
				int num = oleDbCommand.ExecuteNonQuery();
				oleDbConnection.Close();
				result = (num > 0);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			finally
			{
				if (oleDbConnection.State != ConnectionState.Closed)
				{
					oleDbConnection.Close();
				}
				oleDbConnection.Dispose();
			}
			return result;
		}

		private string GetMachineCode()
		{
			StringBuilder stringBuilder = new StringBuilder();
			try
			{
				string value = Assembly.GetEntryAssembly().GetName().Version.ToString();
				stringBuilder.Append(Dns.GetHostName());
				stringBuilder.Append("%");
				stringBuilder.Append(value);
				stringBuilder.Append("#");
				NetworkInterface[] allNetworkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
				if (allNetworkInterfaces.Length > 0)
				{
					NetworkInterface[] array = allNetworkInterfaces;
					for (int i = 0; i < array.Length; i++)
					{
						NetworkInterface networkInterface = array[i];
						if (networkInterface.NetworkInterfaceType == NetworkInterfaceType.Ethernet && (networkInterface.OperationalStatus == OperationalStatus.Up || networkInterface.OperationalStatus == OperationalStatus.Down))
						{
							PhysicalAddress physicalAddress = networkInterface.GetPhysicalAddress();
							if (physicalAddress != null)
							{
								byte[] addressBytes = physicalAddress.GetAddressBytes();
								if (addressBytes != null)
								{
									stringBuilder.Append(BitConverter.ToString(addressBytes));
									stringBuilder.Append(";");
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
			}
			return stringBuilder.ToString();
		}

		private string GetRegistCode()
		{
			string empty = string.Empty;
			string path = Path.Combine(Util.configDir, "RegistCode.txt");
			if (File.Exists(path))
			{
				return File.ReadAllText(path, TextManager.Encoding);
			}
			return empty;
		}

		public bool MachineEnable(string machineCode, string registCode)
		{
			string @string = TextManager.Encoding.GetString(Convert.FromBase64String(machineCode));
			string text = Encryption.DecryptRSA(registCode, Encryption.priKey);
			return !string.IsNullOrEmpty(@string) && !string.IsNullOrEmpty(text) && @string == text;
		}

		public void SaveRegistCode(string registCode)
		{
			try
			{
				if (!Directory.Exists(Util.configDir))
				{
					Directory.CreateDirectory(Util.configDir);
				}
				string path = Path.Combine(Util.configDir, "RegistCode.txt");
				if (File.Exists(path))
				{
					File.Delete(path);
				}
				File.WriteAllText(path, registCode, TextManager.Encoding);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
			}
		}
	}
}
