using System;
using System.Data;
using System.Data.OleDb;

namespace ZhuanCloud.Collector
{
	public class MissIndexCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, int timeOut)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(sqlString, timeOut);
				if (data.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblMissIndex\r\n        ( DBName ,\r\n          SchemaName ,\r\n          TableName ,\r\n          EqualityColumns ,\r\n          InequalityColumns ,\r\n          IncludedColumns ,\r\n          UserSeeks ,\r\n          UserScans ,\r\n          LastUserSeek ,\r\n          LastUserScan ,\r\n          AvgTotalUserCost ,\r\n          AvgUserImpact ,\r\n          [RowCount]\r\n        )\r\nVALUES  ( @DBName ,\r\n          @SchemaName ,\r\n          @TableName ,\r\n          @EqualityColumns ,\r\n          @InequalityColumns ,\r\n          @IncludedColumns ,\r\n          @UserSeeks ,\r\n          @UserScans ,\r\n          @LastUserSeek ,\r\n          @LastUserScan,\r\n          @AvgTotalUserCost ,\r\n          @AvgUserImpact ,\r\n          @RowCount\t\r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@DBName", OleDbType.VarChar)
						{
							SourceColumn = "db_name"
						},
						new OleDbParameter("@SchemaName", OleDbType.VarChar)
						{
							SourceColumn = "schema_name"
						},
						new OleDbParameter("@TableName", OleDbType.VarChar)
						{
							SourceColumn = "table_name"
						},
						new OleDbParameter("@EqualityColumns", OleDbType.LongVarChar)
						{
							SourceColumn = "equality_columns"
						},
						new OleDbParameter("@InequalityColumns", OleDbType.LongVarChar)
						{
							SourceColumn = "inequality_columns"
						},
						new OleDbParameter("@IncludedColumns", OleDbType.LongVarChar)
						{
							SourceColumn = "included_columns"
						},
						new OleDbParameter("@UserSeeks", OleDbType.Double)
						{
							SourceColumn = "user_seeks"
						},
						new OleDbParameter("@UserScans", OleDbType.Double)
						{
							SourceColumn = "user_scans"
						},
						new OleDbParameter("@LastUserSeek", OleDbType.Date)
						{
							SourceColumn = "last_user_seek"
						},
						new OleDbParameter("@LastUserScan", OleDbType.Double)
						{
							SourceColumn = "last_user_scan"
						},
						new OleDbParameter("@AvgTotalUserCost", OleDbType.Double)
						{
							SourceColumn = "avg_total_user_cost"
						},
						new OleDbParameter("@AvgUserImpact", OleDbType.Double)
						{
							SourceColumn = "avg_user_impact"
						},
						new OleDbParameter("@RowCount", OleDbType.Double)
						{
							SourceColumn = "row_count"
						}
					};
					result = base.SaveToAccess(oleDBString, data, cmdText, parameters);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, int timeOut)
		{
			string cmdText = "\r\nSET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED\r\nSELECT  DB_NAME(DB_ID()) [db_name] ,\r\n        s.name AS [schema_name] ,\r\n        o.name AS table_name ,\r\n        equality_columns ,\r\n        inequality_columns ,\r\n        included_columns ,\r\n        user_seeks ,\r\n        user_scans ,\r\n        last_user_seek ,\r\n        last_user_scan ,\r\n        avg_total_user_cost ,\r\n        avg_user_impact ,\r\n        dps.row_count\r\nFROM    sys.dm_db_missing_index_details dd\r\n        JOIN sys.dm_db_missing_index_groups dg ON dg.index_handle = dd.index_handle\r\n        JOIN sys.dm_db_missing_index_group_stats ds ON dg.index_group_handle = ds.group_handle\r\n        JOIN sys.objects o ON dd.[object_id] = o.[object_id]\r\n                              AND o.[type] = 'U'\r\n        JOIN sys.schemas s ON o.[schema_id] = s.[schema_id]\r\n        JOIN ( SELECT   object_id ,\r\n                        SUM(row_count) AS row_count\r\n               FROM     sys.dm_db_partition_stats\r\n               WHERE    index_id < 2\r\n               GROUP BY object_id ,\r\n                        index_id\r\n             ) dps ON dd.[object_id] = dps.[object_id]\r\nWHERE   dd.database_id = DB_ID()\r\n        AND OBJECTPROPERTY(o.[object_id], 'IsMSShipped') = 0\r\nORDER BY dps.row_count DESC ,\r\n        user_seeks DESC\r\nOPTION (MAXDOP 2)\r\n";
			return base.FillDataTable(sqlString, cmdText, timeOut);
		}
	}
}
