using System;

namespace ZhuanCloud.Collector
{
	public class UnusedSession
	{
		public int SessionID
		{
			get;
			set;
		}

		public double TransactionID
		{
			get;
			set;
		}

		public DateTime TransactionBeginTime
		{
			get;
			set;
		}

		public DateTime LoginTime
		{
			get;
			set;
		}

		public DateTime LastRequestStartTime
		{
			get;
			set;
		}

		public DateTime LastRequestEndTime
		{
			get;
			set;
		}

		public DateTime CollectTime
		{
			get;
			set;
		}

		public string TextHashCode
		{
			get;
			set;
		}

		public string LoginName
		{
			get;
			set;
		}

		public string HostName
		{
			get;
			set;
		}

		public string ProgramName
		{
			get;
			set;
		}

		public int HostProcessId
		{
			get;
			set;
		}

		public string ClientInterfaceName
		{
			get;
			set;
		}

		public int TransactionIsolationLevel
		{
			get;
			set;
		}
	}
}
