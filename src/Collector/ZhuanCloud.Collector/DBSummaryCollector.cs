using System;
using System.Data;
using System.Data.OleDb;

namespace ZhuanCloud.Collector
{
	public class DBSummaryCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, int timeOut)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(sqlString, timeOut);
				if (data.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblDBSummary\r\n        ( DatabaseId ,\r\n          DBName ,\r\n          CompatibilityLevel ,\r\n          RecoveryModelDesc ,\r\n          DataSize ,\r\n          LogSize ,\r\n          CreateDate ,\r\n          TableCount ,\r\n          ViewCount ,\r\n          StoredProcCount ,\r\n          FunctionCount           \r\n        )\r\nVALUES  ( @DatabaseId ,\r\n          @DBName ,\r\n          @CompatibilityLevel ,\r\n          @RecoveryModelDesc ,\r\n          @DataSize ,\r\n          @LogSize ,\r\n          @CreateDate ,\r\n          @TableCount ,\r\n          @ViewCount ,\r\n          @StoredProcCount ,\r\n          @FunctionCount     \r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@DatabaseId", OleDbType.Integer)
						{
							SourceColumn = "database_id"
						},
						new OleDbParameter("@DBName", OleDbType.VarChar)
						{
							SourceColumn = "name"
						},
						new OleDbParameter("@CompatibilityLevel", OleDbType.Integer)
						{
							SourceColumn = "compatibility_level"
						},
						new OleDbParameter("@RecoveryModelDesc", OleDbType.VarChar)
						{
							SourceColumn = "recovery_model_desc"
						},
						new OleDbParameter("@DataSize", OleDbType.Double)
						{
							SourceColumn = "data_size_mb"
						},
						new OleDbParameter("@LogSize", OleDbType.Double)
						{
							SourceColumn = "log_size_mb"
						},
						new OleDbParameter("@CreateDate", OleDbType.Date)
						{
							SourceColumn = "create_date"
						},
						new OleDbParameter("@TableCount", OleDbType.Double)
						{
							SourceColumn = "table_count"
						},
						new OleDbParameter("@ViewCount", OleDbType.Double)
						{
							SourceColumn = "view_count"
						},
						new OleDbParameter("@StoredProcCount", OleDbType.Double)
						{
							SourceColumn = "stored_proc_count"
						},
						new OleDbParameter("@FunctionCount", OleDbType.Double)
						{
							SourceColumn = "function_count"
						}
					};
					result = base.SaveToAccess(oleDBString, data, cmdText, parameters);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, int timeOut)
		{
			string cmdText = "\r\nIF OBJECT_ID('tempdb..#DBSummary_Temp', N'U') IS NOT NULL\r\n    DROP TABLE #DBSummary_Temp\r\nSELECT  database_id ,\r\n        name ,\r\n        [compatibility_level] ,\r\n        recovery_model_desc ,\r\n        create_date\r\nINTO    #DBSummary_Temp\r\nFROM    sys.databases\r\nWHERE   name NOT IN ( 'master', 'model', 'msdb', 'tempdb' )\r\n\t\tAND [state] = 0\r\n        AND user_access = 0\r\nORDER BY name\r\n\r\nIF OBJECT_ID('tempdb..#DBItemCount_Temp', N'U') IS NOT NULL\r\n    DROP TABLE #DBItemCount_Temp\r\nCREATE TABLE #DBItemCount_Temp\r\n(\r\n\tdatabase_id INT ,\r\n\tdata_size_mb FLOAT ,\r\n\tlog_size_mb FLOAT ,\r\n\ttable_count BIGINT ,\r\n\tview_count BIGINT ,\r\n\tstored_proc_count BIGINT ,\r\n\tfunction_count BIGINT\t\r\n)\r\n\r\nDECLARE @dbname NVARCHAR(255)\r\nDECLARE RowCur CURSOR STATIC\r\nFOR SELECT name FROM #DBSummary_Temp\r\nOPEN RowCur\r\nFETCH NEXT FROM RowCur INTO @dbname\r\nWHILE @@FETCH_STATUS = 0\r\nBEGIN\r\n\tEXEC('USE [' + @dbname + ']' +\r\n'INSERT INTO #DBItemCount_Temp\r\nSELECT DB_ID() AS database_id ,\r\nROUND(( CONVERT(FLOAT, SUM(size)) * ( 8192.0 / 1024.0 ) / 1024 ), 2) AS data_size_mb,\r\n(\r\n\tSELECT ROUND(( CONVERT(FLOAT, SUM(size)) * ( 8192.0 / 1024.0 ) / 1024 ), 2) FROM sys.database_files WHERE [type] = 1\r\n) AS log_size_mb,\r\n(\r\n\tSELECT COUNT([object_id]) FROM sys.tables\r\n) AS table_count,\r\n(\r\n\tSELECT COUNT([object_id]) FROM sys.views\r\n) AS view_count,\r\n(\r\n\tSELECT COUNT([object_id]) FROM sys.procedures\r\n) AS stored_proc_count,\r\n(\r\n\tSELECT COUNT([object_id]) FROM sys.objects WHERE [type] IN (''AF'',''FN'',''FS'',''FT'',''IF'',''TF'')\r\n) AS function_count\r\nFROM sys.database_files WHERE [type] = 0')\r\nFETCH NEXT FROM RowCur INTO @dbname\r\nEND\r\nCLOSE RowCur\r\nDEALLOCATE RowCur\r\n\r\nSELECT  db.database_id ,\r\n        name ,\r\n        [compatibility_level] ,\r\n        recovery_model_desc ,\r\n        create_date ,\r\n        data_size_mb ,\r\n        log_size_mb ,\r\n        table_count ,\r\n        view_count ,\r\n        stored_proc_count ,\r\n        function_count\r\nFROM    #DBSummary_Temp db\r\nLEFT JOIN #DBItemCount_Temp c\r\nON db.database_id = c.database_id\r\n\r\nDROP TABLE #DBSummary_Temp\r\nDROP TABLE #DBItemCount_Temp\r\n";
			return base.FillDataTable(sqlString, cmdText, timeOut);
		}
	}
}
