using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace ZhuanCloud.Collector
{
	public abstract class OleDbCollector
	{
		public virtual bool SaveData(string oleDBString, int timeOut)
		{
			return true;
		}

		public virtual bool SaveData(string oleDBString, string sqlString, int timeOut)
		{
			return true;
		}

		public virtual bool SaveData(string oleDBString, string sqlString, int timeOut, string condition)
		{
			return true;
		}

		public virtual bool SaveData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			return true;
		}

		public virtual bool SaveData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut, bool isDecrypt)
		{
			return true;
		}

		public virtual bool SaveData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut, string condition)
		{
			return true;
		}

		public virtual bool SaveData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut, string condition, bool isDecrypt)
		{
			return true;
		}

		public int GetMajorVersion(SQLServerVersion sqlVersion)
		{
			switch (sqlVersion)
			{
			case SQLServerVersion.SQLServer2000:
				return 8;
			case SQLServerVersion.SQLServer2005:
				return 9;
			case SQLServerVersion.SQLServer2008:
			case SQLServerVersion.SQLServer2008R2:
				return 10;
			case SQLServerVersion.SQLServer2012:
				return 11;
			case SQLServerVersion.SQLServer2014:
				return 12;
			default:
				return 9;
			}
		}

		public bool ExecuteNonQuery(string sqlString, string cmdText, int timeOut)
		{
			bool result;
			try
			{
				using (SqlConnection sqlConnection = new SqlConnection(sqlString))
				{
					SqlCommand sqlCommand = sqlConnection.CreateCommand();
					sqlCommand.CommandTimeout = timeOut;
					sqlCommand.CommandText = cmdText;
					sqlConnection.Open();
					int num = sqlCommand.ExecuteNonQuery();
					sqlConnection.Close();
					result = (num > 0);
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(new Exception(cmdText));
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		public object SQLExecuteScalar(string sqlString, string cmdText, int timeOut)
		{
			object result;
			try
			{
				using (SqlConnection sqlConnection = new SqlConnection(sqlString))
				{
					SqlCommand sqlCommand = sqlConnection.CreateCommand();
					sqlCommand.CommandTimeout = timeOut;
					sqlCommand.CommandText = cmdText;
					sqlConnection.Open();
					object obj = sqlCommand.ExecuteScalar();
					sqlConnection.Close();
					result = obj;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(new Exception(cmdText));
				ErrorLog.Write(ex);
				result = null;
			}
			return result;
		}

		public DataTable FillDataTable(string sqlString, string cmdText, int timeOut)
		{
			DataTable dataTable = new DataTable();
			try
			{
				SqlConnection sqlConnection = new SqlConnection(sqlString);
				SqlCommand sqlCommand = sqlConnection.CreateCommand();
				sqlCommand.CommandTimeout = timeOut;
				sqlCommand.CommandText = cmdText;
				SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
				sqlDataAdapter.Fill(dataTable);
				if (dataTable.Rows.Count > 0)
				{
					foreach (DataRow dataRow in dataTable.Rows)
					{
						dataRow.SetAdded();
					}
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(new Exception(cmdText));
				ErrorLog.Write(ex);
				throw new Exception(ex.Message);
			}
			return dataTable;
		}

		public bool SaveToAccess(string oleDBString, string cmdText, int timeOut, params OleDbParameter[] parameters)
		{
			OleDbConnection oleDbConnection = null;
			if (!Util.TryConnect(oleDBString, out oleDbConnection))
			{
				return false;
			}
			bool result;
			try
			{
				OleDbCommand oleDbCommand = oleDbConnection.CreateCommand();
				oleDbCommand.CommandTimeout = timeOut;
				oleDbCommand.CommandText = cmdText;
				if (parameters != null && parameters.Length > 0)
				{
					oleDbCommand.Parameters.Clear();
					oleDbCommand.Parameters.AddRange(parameters);
				}
				oleDbConnection.Open();
				int num = oleDbCommand.ExecuteNonQuery();
				oleDbConnection.Close();
				result = (num > 0);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(new Exception(cmdText));
				ErrorLog.Write(ex);
				result = false;
			}
			finally
			{
				if (oleDbConnection.State != ConnectionState.Closed)
				{
					oleDbConnection.Close();
				}
				oleDbConnection.Dispose();
			}
			return result;
		}

		public bool SaveToAccess(string oleDBString, DataTable tblSource, string cmdText, params OleDbParameter[] parameters)
		{
			OleDbConnection oleDbConnection = null;
			if (!Util.TryConnect(oleDBString, out oleDbConnection))
			{
				return false;
			}
			bool result;
			try
			{
				OleDbCommand oleDbCommand = oleDbConnection.CreateCommand();
				oleDbCommand.CommandText = cmdText;
				if (parameters != null && parameters.Length > 0)
				{
					oleDbCommand.Parameters.Clear();
					oleDbCommand.Parameters.AddRange(parameters);
				}
				int num = new OleDbDataAdapter
				{
					InsertCommand = oleDbCommand
				}.Update(tblSource);
				result = (num > 0);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(new Exception(cmdText));
				ErrorLog.Write(ex);
				result = false;
			}
			finally
			{
				if (oleDbConnection.State != ConnectionState.Closed)
				{
					oleDbConnection.Close();
				}
				oleDbConnection.Dispose();
			}
			return result;
		}
	}
}
