using System;
using System.Data;
using System.Data.OleDb;

namespace ZhuanCloud.Collector
{
	public class ConfigCollector : OleDbCollector
	{
		public bool SaveConfig(string oleDBString, CollectConfig config)
		{
			if (config != null)
			{
				string text = string.Format("\r\nINSERT INTO  tblCollectConfig\r\n(\r\n\t\tHasPerfCounter ,\r\n        PerfElapsed ,\r\n        HasTempdbInfo ,\r\n        TempdbElapsed ,\r\n        HasDBDefine ,\r\n        HasCheckTable ,\r\n        HasTable ,\r\n        HasView ,\r\n        HasStoredProc ,\r\n        HasFuction ,\r\n        IsDecrypt ,\r\n        HasDBFile ,\r\n        DBFileElapsed ,\r\n        HasSession ,\r\n        WaitElapsed ,\r\n        UnusedSessionValue ,\r\n        UnusedSessionElapsed ,\r\n        HasSqlText ,\r\n        SqlTextValue ,\r\n        HasSqlPlan ,\r\n        SqlPlanElapsed ,\r\n        SqlPlanValue ,\r\n        HasErrorLog ,\r\n        ErrorLogElapsed ,\r\n        HasMoebius ,\r\n        [TimeOut]\r\n)\r\nVALUES\r\n(\r\n        {0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25}\r\n)\r\n", new object[]
				{
					config.HasPerfCounter,
					config.PerfElapsed,
					config.HasTempdbInfo,
					config.TempdbElapsed,
					config.HasDBDefine,
					config.HasCheckTable,
					config.DBItemType.HasTable,
					config.DBItemType.HasView,
					config.DBItemType.HasStoredProc,
					config.DBItemType.HasFuction,
					config.IsDecrypt,
					config.HasDBFile,
					config.DBFileElapsed,
					config.HasSession,
					config.WaitElapsed,
					config.UnusedSessionValue,
					config.UnusedSessionElapsed,
					config.HasSqlText,
					config.SqlTextValue,
					config.HasSqlPlan,
					config.SqlPlanElapsed,
					config.SqlPlanValue,
					config.HasErrorLog,
					config.ErrorLogElapsed,
					config.HasMoebius,
					config.TimeOut
				});
				try
				{
					if (!base.SaveToAccess(oleDBString, text, config.TimeOut, new OleDbParameter[0]))
					{
						bool result = false;
						return result;
					}
				}
				catch (Exception ex)
				{
					ErrorLog.Write(ex);
					bool result = false;
					return result;
				}
				try
				{
					if (config.ErrorLogKeys != null && config.ErrorLogKeys.Length > 0)
					{
						string[] errorLogKeys = config.ErrorLogKeys;
						for (int i = 0; i < errorLogKeys.Length; i++)
						{
							string value = errorLogKeys[i];
							text = "\r\nINSERT  INTO tblSqlLogStatics\r\n        ( KeyWord, ErrorCount )\r\nVALUES  ( @KeyWord, @ErrorCount )\r\n";
							OleDbParameter[] parameters = new OleDbParameter[]
							{
								new OleDbParameter("@KeyWord", OleDbType.VarChar)
								{
									Value = value
								},
								new OleDbParameter("@ErrorCount", OleDbType.Integer)
								{
									Value = 0
								}
							};
							if (!base.SaveToAccess(oleDBString, text, config.TimeOut, parameters))
							{
								bool result = false;
								return result;
							}
						}
					}
				}
				catch (Exception ex2)
				{
					ErrorLog.Write(ex2);
					bool result = false;
					return result;
				}
				if (config.PerfList == null || config.PerfList.Count <= 0)
				{
					return true;
				}
				text = "\r\nINSERT  INTO tblPerfmon\r\n        ( ID ,\r\n          ObjectName ,\r\n          CounterName ,\r\n          InstanceName \r\n        )\r\nVALUES  ( @ID ,\r\n          @ObjectName ,\r\n          @CounterName ,\r\n          @InstanceName \r\n        )\r\n";
				OleDbConnection oleDbConnection = null;
				if (!Util.TryConnect(oleDBString, out oleDbConnection))
				{
					return false;
				}
				try
				{
					OleDbCommand oleDbCommand = oleDbConnection.CreateCommand();
					oleDbCommand.CommandTimeout = config.TimeOut;
					oleDbConnection.Open();
					foreach (Perf current in config.PerfList)
					{
						OleDbParameter[] values = new OleDbParameter[]
						{
							new OleDbParameter("@ID", OleDbType.VarChar)
							{
								Value = current.ID
							},
							new OleDbParameter("@ObjectName", OleDbType.VarChar)
							{
								Value = current.ObjectName
							},
							new OleDbParameter("@CounterName", OleDbType.VarChar)
							{
								Value = current.CounterName
							},
							new OleDbParameter("@InstanceName", OleDbType.VarChar)
							{
								Value = current.InstanceName
							}
						};
						oleDbCommand.CommandText = text;
						oleDbCommand.Parameters.Clear();
						oleDbCommand.Parameters.AddRange(values);
						oleDbCommand.ExecuteNonQuery();
					}
					oleDbConnection.Close();
					bool result = true;
					return result;
				}
				catch (Exception ex3)
				{
					ErrorLog.Write(ex3);
					bool result = false;
					return result;
				}
				finally
				{
					if (oleDbConnection.State != ConnectionState.Closed)
					{
						oleDbConnection.Close();
					}
					oleDbConnection.Dispose();
				}
				return true;
			}
			return true;
		}
	}
}
