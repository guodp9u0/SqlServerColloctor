using System;
using System.Collections.Generic;

namespace ZhuanCloud.Collector
{
	public class SQLServiceCompare : IComparer<SQLService>
	{
		public int Compare(SQLService x, SQLService y)
		{
			return x.InstanceName.CompareTo(y.InstanceName);
		}
	}
}
