using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Management;

namespace ZhuanCloud.Collector
{
	public class DiskCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, int timeOut)
		{
			Dictionary<string, DiskPartion> dictionary = new Dictionary<string, DiskPartion>();
			try
			{
				using (ManagementClass managementClass = new ManagementClass("Win32_Diskdrive"))
				{
					ManagementObjectCollection instances = managementClass.GetInstances();
					using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = instances.GetEnumerator())
					{
						while (enumerator.MoveNext())
						{
							ManagementObject managementObject = (ManagementObject)enumerator.Current;
							string model = managementObject["Model"].ToString().Trim();
							using (ManagementObjectCollection.ManagementObjectEnumerator enumerator2 = managementObject.GetRelated("Win32_DiskPartition").GetEnumerator())
							{
								while (enumerator2.MoveNext())
								{
									ManagementObject managementObject2 = (ManagementObject)enumerator2.Current;
									string part = managementObject2["Name"].ToString().Trim();
									foreach (ManagementBaseObject current in managementObject2.GetRelated("Win32_LogicalDisk"))
									{
										string text = current["Name"].ToString();
										if (!string.IsNullOrEmpty(text))
										{
											string key = text.Substring(0, 1);
											if (!dictionary.ContainsKey(key))
											{
												DiskPartion value = new DiskPartion(part, model);
												dictionary.Add(key, value);
											}
										}
									}
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
			}
			bool result;
			try
			{
				DataTable dataTable = new DataTable();
				dataTable.TableName = "tblDiskInfo";
				dataTable.Columns.Add(new DataColumn("logic_name"));
				dataTable.Columns.Add(new DataColumn("file_system_type"));
				dataTable.Columns.Add(new DataColumn("total_size_gb"));
				dataTable.Columns.Add(new DataColumn("total_free_space_gb"));
				dataTable.Columns.Add(new DataColumn("partition"));
				dataTable.Columns.Add(new DataColumn("disk_device"));
				DriveInfo[] drives = DriveInfo.GetDrives();
				DriveInfo[] array = drives;
				for (int i = 0; i < array.Length; i++)
				{
					DriveInfo driveInfo = array[i];
					if (driveInfo.DriveType == DriveType.Fixed)
					{
						float num = Convert.ToSingle(((double)driveInfo.TotalSize / 1024.0 / 1024.0 / 1024.0).ToString("F2"));
						float num2 = Convert.ToSingle(((double)driveInfo.TotalFreeSpace / 1024.0 / 1024.0 / 1024.0).ToString("F2"));
						string key2 = driveInfo.Name.Substring(0, 1);
						if (dictionary.ContainsKey(key2))
						{
							dataTable.Rows.Add(new object[]
							{
								driveInfo.Name,
								driveInfo.DriveFormat,
								num,
								num2,
								dictionary[key2].DiskPartition,
								dictionary[key2].Model
							});
						}
						else
						{
							dataTable.Rows.Add(new object[]
							{
								driveInfo.Name,
								driveInfo.DriveFormat,
								num,
								num2,
								string.Empty,
								string.Empty
							});
						}
					}
				}
				string cmdText = "\r\nINSERT  INTO tblDiskInfo\r\n        ( LogicName ,\r\n          FileSystemType ,\r\n          TotalSize_GB ,\r\n          FreeSpace_GB ,\r\n          Partition ,\r\n          DiskDevice \r\n        )\r\nVALUES  ( @LogicName ,\r\n          @FileSystemType ,\r\n          @TotalSize_GB ,\r\n          @FreeSpace_GB ,\r\n          @Partition ,\r\n          @DiskDevice \r\n        )\r\n";
				OleDbParameter[] parameters = new OleDbParameter[]
				{
					new OleDbParameter("@LogicName", OleDbType.VarChar)
					{
						SourceColumn = "logic_name"
					},
					new OleDbParameter("@FileSystemType", OleDbType.VarChar)
					{
						SourceColumn = "file_system_type"
					},
					new OleDbParameter("@TotalSize_GB", OleDbType.Single)
					{
						SourceColumn = "total_size_gb"
					},
					new OleDbParameter("@FreeSpace_GB", OleDbType.Single)
					{
						SourceColumn = "total_free_space_gb"
					},
					new OleDbParameter("@Partition", OleDbType.VarChar)
					{
						SourceColumn = "partition"
					},
					new OleDbParameter("@DiskDevice", OleDbType.VarChar)
					{
						SourceColumn = "disk_device"
					}
				};
				result = base.SaveToAccess(oleDBString, dataTable, cmdText, parameters);
			}
			catch (Exception ex2)
			{
				ErrorLog.Write(ex2);
				result = false;
			}
			return result;
		}
	}
}
