using System;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace ZhuanCloud.Collector
{
	public class MoebiusCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, int timeOut)
		{
			bool result;
			try
			{
				bool data = this.GetData(sqlString, timeOut);
				string cmdText = "\r\nINSERT  INTO tblSinglePoint\r\n        ( DatabaseName, TypeID, Enabled )\r\nVALUES  ( @DatabaseName, @TypeID, @Enabled )\r\n";
				SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder(sqlString);
				OleDbParameter[] parameters = new OleDbParameter[]
				{
					new OleDbParameter("@DatabaseName", OleDbType.VarChar)
					{
						Value = sqlConnectionStringBuilder.InitialCatalog
					},
					new OleDbParameter("@TypeID", OleDbType.Integer)
					{
						Value = 1
					},
					new OleDbParameter("@Enabled", OleDbType.Boolean)
					{
						Value = data
					}
				};
				result = base.SaveToAccess(oleDBString, cmdText, timeOut, parameters);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private bool GetData(string sqlString, int timeOut)
		{
			string cmdText = "\r\nSELECT  COUNT(assembly_id) AS assembly_count\r\nFROM    sys.assemblies\r\nWHERE   name IN ( 'GRQSH.Moebius.Core6', 'GRQSH.Moebius.Core7',\r\n                  'GRQSH.Moebius.Core8', 'GRQSH.Moebius.Core9' )\r\n";
			object obj = base.SQLExecuteScalar(sqlString, cmdText, timeOut);
			return obj != null && Convert.ToInt32(obj) > 0;
		}
	}
}
