using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace ZhuanCloud.Collector
{
	public class MoebiusTrace : OleDbCollector
	{
		public bool SaveData(string filePath, string oleDBString, string sqlString, int timeOut)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(filePath, sqlString, oleDBString, timeOut);
				if (data.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblMoebiusSql\r\n        ( EventClass ,\r\n          TextHashCode ,\r\n          NTUserName ,\r\n          HostName ,\r\n          ClientProcessID ,\r\n          ApplicationName ,\r\n          LoginName ,\r\n          SPID ,\r\n          Duration ,\r\n          StartTime ,\r\n          EndTime ,\r\n          Reads ,\r\n          Writes ,\r\n          CPU ,\r\n          ObjectName ,\r\n          DatabaseName ,\r\n          RowCounts ,\r\n          ParameterHash\r\n        )\r\nVALUES  ( @EventClass ,\r\n          @TextHashCode ,\r\n          @NTUserName ,\r\n          @HostName ,\r\n          @ClientProcessID ,\r\n          @ApplicationName ,\r\n          @LoginName ,\r\n          @SPID ,\r\n          @Duration ,\r\n          @StartTime ,\r\n          @EndTime ,\r\n          @Reads ,\r\n          @Writes ,\r\n          @CPU ,\r\n          @ObjectName ,\r\n          @DatabaseName ,\r\n          @RowCounts ,\r\n          @ParameterHash\r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@EventClass", OleDbType.Integer)
						{
							SourceColumn = "EventClass"
						},
						new OleDbParameter("@TextHashCode", OleDbType.VarChar)
						{
							SourceColumn = "TextData"
						},
						new OleDbParameter("@NTUserName", OleDbType.VarChar)
						{
							SourceColumn = "NTUserName"
						},
						new OleDbParameter("@HostName", OleDbType.VarChar)
						{
							SourceColumn = "HostName"
						},
						new OleDbParameter("@ClientProcessID", OleDbType.Integer)
						{
							SourceColumn = "ClientProcessID"
						},
						new OleDbParameter("@ApplicationName", OleDbType.VarChar)
						{
							SourceColumn = "ApplicationName"
						},
						new OleDbParameter("@LoginName", OleDbType.VarChar)
						{
							SourceColumn = "LoginName"
						},
						new OleDbParameter("@SPID", OleDbType.Integer)
						{
							SourceColumn = "SPID"
						},
						new OleDbParameter("@Duration", OleDbType.Double)
						{
							SourceColumn = "Duration"
						},
						new OleDbParameter("@StartTime", OleDbType.Date)
						{
							SourceColumn = "StartTime"
						},
						new OleDbParameter("@EndTime", OleDbType.Date)
						{
							SourceColumn = "EndTime"
						},
						new OleDbParameter("@Reads", OleDbType.Double)
						{
							SourceColumn = "Reads"
						},
						new OleDbParameter("@Writes", OleDbType.Double)
						{
							SourceColumn = "Writes"
						},
						new OleDbParameter("@CPU", OleDbType.Double)
						{
							SourceColumn = "CPU"
						},
						new OleDbParameter("@ObjectName", OleDbType.VarChar)
						{
							SourceColumn = "ObjectName"
						},
						new OleDbParameter("@DatabaseName", OleDbType.VarChar)
						{
							SourceColumn = "DatabaseName"
						},
						new OleDbParameter("@RowCounts", OleDbType.Double)
						{
							SourceColumn = "RowCounts"
						},
						new OleDbParameter("@ParameterHash", OleDbType.VarChar)
						{
							SourceColumn = "ParameterHash"
						}
					};
					result = base.SaveToAccess(oleDBString, data, cmdText, parameters);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string filePath, string sqlString, string oleDBString, int timeOut)
		{
			string commandText = string.Format("\r\nSELECT  EventClass ,\r\n        TextData ,\r\n        NTUserName ,\r\n        HostName ,\r\n        ClientProcessID ,\r\n        ISNULL(ApplicationName,'') AS ApplicationName ,\r\n        LoginName ,\r\n        SPID ,\r\n        Duration ,\r\n        CONVERT(VARCHAR(100),StartTime,120) StartTime ,\r\n        CONVERT(VARCHAR(100),EndTime,120) EndTime ,\r\n        Reads ,\r\n        Writes ,\r\n        CPU ,\r\n        ISNULL(ObjectName,'') AS ObjectName ,\r\n        DatabaseName ,\r\n        RowCounts\r\nFROM    fn_trace_gettable('{0}', DEFAULT)\r\nWHERE   TextData IS NOT NULL\r\n        AND ISNULL(ApplicationName,'') <> 'ZhuanCloud'\r\nOPTION (MAXDOP 2)\r\n", filePath);
			DataTable dataTable = new DataTable();
			try
			{
				List<SqlTextHash> list = new List<SqlTextHash>();
				SqlConnection sqlConnection = new SqlConnection(sqlString);
				SqlCommand sqlCommand = sqlConnection.CreateCommand();
				sqlCommand.CommandTimeout = timeOut;
				sqlCommand.CommandText = commandText;
				SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
				sqlDataAdapter.Fill(dataTable);
				if (dataTable.Rows.Count > 0)
				{
					dataTable.Columns.Add(new DataColumn("ParameterHash"));
					MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
					foreach (DataRow dataRow in dataTable.Rows)
					{
						string text = dataRow["TextData"].ToString();
						if (!string.IsNullOrEmpty(text))
						{
							byte[] bytes = TextManager.Encoding.GetBytes(text);
							byte[] inArray = mD5CryptoServiceProvider.ComputeHash(bytes);
							string text2 = Convert.ToBase64String(inArray);
							if (!Util.moebiusHashList.Contains(text2))
							{
								list.Add(new SqlTextHash
								{
									TextData = TextManager.Compress(text),
									HashCode = text2
								});
								Util.moebiusHashList.Add(text2);
							}
							dataRow["TextData"] = text2;
							string regexString = TextManager.GetRegexString(text);
							if (!string.IsNullOrEmpty(regexString))
							{
								bytes = TextManager.Encoding.GetBytes(regexString);
								inArray = mD5CryptoServiceProvider.ComputeHash(bytes);
								dataRow["ParameterHash"] = Convert.ToBase64String(inArray);
							}
							dataRow.AcceptChanges();
							dataRow.SetAdded();
						}
					}
					if (list.Count > 0)
					{
						OleDbConnection oleDbConnection = null;
						if (!Util.TryConnect(oleDBString, out oleDbConnection))
						{
							return dataTable;
						}
						try
						{
							OleDbCommand oleDbCommand = oleDbConnection.CreateCommand();
							oleDbCommand.CommandText = "INSERT INTO tblMoebiusSqlHash(TextData,TextHashCode) VALUES(@TextData,@TextHashCode)";
							oleDbConnection.Open();
							foreach (SqlTextHash current in list)
							{
								OleDbParameter[] values = new OleDbParameter[]
								{
									new OleDbParameter("@TextData", OleDbType.LongVarWChar)
									{
										Value = current.TextData
									},
									new OleDbParameter("@TextHashCode", OleDbType.VarChar)
									{
										Value = current.HashCode
									}
								};
								oleDbCommand.Parameters.Clear();
								oleDbCommand.Parameters.AddRange(values);
								oleDbCommand.CommandTimeout = timeOut;
								oleDbCommand.ExecuteNonQuery();
							}
							oleDbConnection.Close();
						}
						catch (Exception ex)
						{
							ErrorLog.Write(ex);
						}
						finally
						{
							if (oleDbConnection.State != ConnectionState.Closed)
							{
								oleDbConnection.Close();
							}
							oleDbConnection.Dispose();
						}
					}
				}
			}
			catch (Exception ex2)
			{
				ErrorLog.Write(ex2);
				throw new Exception(ex2.Message);
			}
			return dataTable;
		}

		public string GetLastTracePath(int traceId, string sqlString, int timeOut)
		{
			string cmdText = string.Format("SELECT [path] FROM sys.traces WHERE id = {0}", traceId);
			object obj = base.SQLExecuteScalar(sqlString, cmdText, timeOut);
			if (obj != null)
			{
				return obj.ToString();
			}
			return string.Empty;
		}

		public int GetLastTraceID(string tracePath, string sqlString, int timeOut)
		{
			string cmdText = string.Format("SELECT id FROM sys.traces WHERE [path] = '{0}'", tracePath);
			object obj = base.SQLExecuteScalar(sqlString, cmdText, timeOut);
			if (obj != null)
			{
				return Convert.ToInt32(obj);
			}
			return -1;
		}

		public object Start(string sqlString, int timeOut, string filePath)
		{
			string cmdText = string.Format("\r\ndeclare @rc int\r\ndeclare @TraceID int\r\ndeclare @FileName nvarchar(520)\r\ndeclare @maxfilesize bigint\r\ndeclare @bigintfilter bigint\r\n\r\nSELECT @FileName = '{0}'\r\nset @maxfilesize = 5 \r\nset @bigintfilter = 1\r\n\r\nexec @rc = sp_trace_create @TraceID output, 2, @FileName, @maxfilesize, NULL \r\nif (@rc != 0) goto error\r\n\r\ndeclare @on bit\r\nset @on = 1\r\n\r\nexec sp_trace_setevent @TraceID, 10, 15, @on\r\nexec sp_trace_setevent @TraceID, 10, 16, @on\r\nexec sp_trace_setevent @TraceID, 10, 1, @on\r\nexec sp_trace_setevent @TraceID, 10, 9, @on\r\nexec sp_trace_setevent @TraceID, 10, 17, @on\r\nexec sp_trace_setevent @TraceID, 10, 6, @on\r\nexec sp_trace_setevent @TraceID, 10, 10, @on\r\nexec sp_trace_setevent @TraceID, 10, 14, @on\r\nexec sp_trace_setevent @TraceID, 10, 18, @on\r\nexec sp_trace_setevent @TraceID, 10, 11, @on\r\nexec sp_trace_setevent @TraceID, 10, 8, @on\r\nexec sp_trace_setevent @TraceID, 10, 35, @on\r\nexec sp_trace_setevent @TraceID, 10, 12, @on\r\nexec sp_trace_setevent @TraceID, 10, 13, @on\r\nexec sp_trace_setevent @TraceID, 10, 48, @on\r\nexec sp_trace_setevent @TraceID, 10, 34, @on\r\n\r\nexec sp_trace_setevent @TraceID, 12, 15, @on\r\nexec sp_trace_setevent @TraceID, 12, 16, @on\r\nexec sp_trace_setevent @TraceID, 12, 1, @on\r\nexec sp_trace_setevent @TraceID, 12, 9, @on\r\nexec sp_trace_setevent @TraceID, 12, 17, @on\r\nexec sp_trace_setevent @TraceID, 12, 6, @on\r\nexec sp_trace_setevent @TraceID, 12, 10, @on\r\nexec sp_trace_setevent @TraceID, 12, 14, @on\r\nexec sp_trace_setevent @TraceID, 12, 18, @on\r\nexec sp_trace_setevent @TraceID, 12, 11, @on\r\nexec sp_trace_setevent @TraceID, 12, 8, @on\r\nexec sp_trace_setevent @TraceID, 12, 35, @on\r\nexec sp_trace_setevent @TraceID, 12, 12, @on\r\nexec sp_trace_setevent @TraceID, 12, 13, @on\r\nexec sp_trace_setevent @TraceID, 12, 48, @on\r\n\r\nexec sp_trace_setevent @TraceID, 45, 15, @on\r\nexec sp_trace_setevent @TraceID, 45, 16, @on\r\nexec sp_trace_setevent @TraceID, 45, 1, @on\r\nexec sp_trace_setevent @TraceID, 45, 9, @on\r\nexec sp_trace_setevent @TraceID, 45, 17, @on\r\nexec sp_trace_setevent @TraceID, 45, 6, @on\r\nexec sp_trace_setevent @TraceID, 45, 10, @on\r\nexec sp_trace_setevent @TraceID, 45, 14, @on\r\nexec sp_trace_setevent @TraceID, 45, 18, @on\r\nexec sp_trace_setevent @TraceID, 45, 11, @on\r\nexec sp_trace_setevent @TraceID, 45, 8, @on\r\nexec sp_trace_setevent @TraceID, 45, 35, @on\r\nexec sp_trace_setevent @TraceID, 45, 12, @on\r\nexec sp_trace_setevent @TraceID, 45, 13, @on\r\nexec sp_trace_setevent @TraceID, 45, 48, @on\r\nexec sp_trace_setevent @TraceID, 45, 34, @on\r\n\r\nexec sp_trace_setfilter @TraceID, 1, 1, 6, N'%truncate%table%'\r\nexec sp_trace_setfilter @TraceID, 1, 1, 6, N'%select%into%from%'\r\nexec sp_trace_setfilter @TraceID, 1, 1, 6, N'%writetext%'\r\nexec sp_trace_setfilter @TraceID, 1, 1, 6, N'%updatetext%'\r\nexec sp_trace_setfilter @TraceID, 1, 1, 6, N'%textptr%'\r\nexec sp_trace_setfilter @TraceID, 1, 1, 6, N'%readtext%'\r\nexec sp_trace_setfilter @TraceID, 1, 1, 6, N'%bulk%insert%'\r\nexec sp_trace_setfilter @TraceID, 1, 1, 6, N'%insert%bulk%'\r\nexec sp_trace_setfilter @TraceID, 1, 1, 6, N'%save%tran%'\r\n\r\nexec sp_trace_setstatus @TraceID, 1\r\n\r\nselect TraceID=@TraceID\r\ngoto finish\r\n\r\nerror: \r\nselect ErrorCode=@rc\r\n\r\nfinish: \r\n", filePath);
			return base.SQLExecuteScalar(sqlString, cmdText, timeOut);
		}

		public bool Stop(int traceId, string sqlString, int timeOut)
		{
			string cmdText = string.Format("\r\nexec sp_trace_setstatus {0}, 0\r\nexec sp_trace_setstatus {0}, 2", traceId);
			return base.ExecuteNonQuery(sqlString, cmdText, timeOut);
		}
	}
}
