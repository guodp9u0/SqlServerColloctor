using System;
using System.Data.OleDb;
using System.Reflection;

namespace ZhuanCloud.Collector
{
	public class VersionCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, int timeOut)
		{
			bool result;
			try
			{
				string value = "ZhuanCloud";
				string value2 = Assembly.GetEntryAssembly().GetName().Version.ToString();
				string cmdText = "INSERT INTO tblVersion(ProgramName,CodePage,CodeName,Version,CollectionTime) VALUES(@ProgramName,@CodePage,@CodeName,@Version,@CollectionTime)";
				OleDbParameter[] parameters = new OleDbParameter[]
				{
					new OleDbParameter("@ProgramName", OleDbType.VarChar)
					{
						Value = value
					},
					new OleDbParameter("@CodePage", OleDbType.Integer)
					{
						Value = TextManager.Encoding.CodePage
					},
					new OleDbParameter("@CodeName", OleDbType.VarChar)
					{
						Value = TextManager.Encoding.BodyName
					},
					new OleDbParameter("@Version", OleDbType.VarChar)
					{
						Value = value2
					},
					new OleDbParameter("@CollectionTime", OleDbType.Date)
					{
						Value = DateTime.Now
					}
				};
				result = base.SaveToAccess(oleDBString, cmdText, timeOut, parameters);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		public bool SaveEndTime(string oleDBString, int timeOut)
		{
			bool result;
			try
			{
				Assembly.GetEntryAssembly().GetName().Version.ToString();
				string cmdText = "UPDATE tblVersion SET EndTime=@EndTime WHERE ID = 1";
				OleDbParameter[] parameters = new OleDbParameter[]
				{
					new OleDbParameter("@ProgramName", OleDbType.Date)
					{
						Value = DateTime.Now
					}
				};
				result = base.SaveToAccess(oleDBString, cmdText, timeOut, parameters);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}
	}
}
