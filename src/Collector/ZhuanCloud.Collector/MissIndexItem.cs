using System;

namespace ZhuanCloud.Collector
{
	public class MissIndexItem
	{
		public int RowID
		{
			get;
			set;
		}

		public string DBName
		{
			get;
			set;
		}

		public string SchemaName
		{
			get;
			set;
		}

		public string TableName
		{
			get;
			set;
		}

		public string EqualityColumns
		{
			get;
			set;
		}

		public string InequalityColumns
		{
			get;
			set;
		}

		public string IncludedColumns
		{
			get;
			set;
		}

		public double Impact
		{
			get;
			set;
		}
	}
}
