using System;
using System.Data.SqlClient;

namespace ZhuanCloud.Collector
{
	public class Engine
	{
		public static string Decrypt(string conn, string dbName, string objectID, int sqlversion)
		{
			SqlDataReader sqlDataReader = SQLHelper.DataReaderRun("select ServerProperty('edition') as Edition", conn);
			sqlDataReader.Read();
			string text = sqlDataReader["Edition"].ToString();
			SQLBits sb = SQLBits.x86;
			if (text.IndexOf("(64-bit)") > -1)
			{
				sb = SQLBits.x64;
			}
			SQLVersion sv = (sqlversion >= 11) ? SQLVersion._2012 : SQLVersion.others;
			SQLObjDecryption sQLObjDecryption = new SQLObjDecryption(conn, dbName, objectID, sv, sb);
			sQLObjDecryption.DecryptedObject();
			return sQLObjDecryption.GetRealDefinition();
		}
	}
}
