using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace ZhuanCloud.Collector
{
	public abstract class SQLHelper
	{
		private static Hashtable parmCache = Hashtable.Synchronized(new Hashtable());

		public static string GetIPAddress(string dataSource)
		{
			int num = dataSource.IndexOf(',');
			string result;
			if (num > 0)
			{
				result = dataSource.Substring(0, num).Trim();
			}
			else
			{
				result = dataSource.Trim();
			}
			return result;
		}

		public static int GetPort(string dataSource, int defaultPort)
		{
			int num = dataSource.IndexOf(',');
			int result;
			if (num > 0)
			{
				result = int.Parse(dataSource.Substring(num + 1).Trim());
			}
			else
			{
				result = defaultPort;
			}
			return result;
		}

		public static int ExecuteNonQuery(string connString, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
		{
			SqlCommand sqlCommand = new SqlCommand();
			int result;
			using (SqlConnection sqlConnection = new SqlConnection(connString))
			{
				SQLHelper.PrepareCommand(sqlCommand, sqlConnection, null, cmdType, cmdText, cmdParms);
				int num = sqlCommand.ExecuteNonQuery();
				sqlCommand.Parameters.Clear();
				result = num;
			}
			return result;
		}

		public static int ExecuteNonQuery(string connString, int commandTimeout, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
		{
			SqlCommand sqlCommand = new SqlCommand();
			sqlCommand.CommandTimeout = commandTimeout;
			int result;
			using (SqlConnection sqlConnection = new SqlConnection(connString))
			{
				SQLHelper.PrepareCommand(sqlCommand, sqlConnection, null, cmdType, cmdText, cmdParms);
				int num = sqlCommand.ExecuteNonQuery();
				sqlCommand.Parameters.Clear();
				result = num;
			}
			return result;
		}

		public static int ExecuteNonQuery(SqlConnection conn, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
		{
			SqlCommand sqlCommand = new SqlCommand();
			SQLHelper.PrepareCommand(sqlCommand, conn, null, cmdType, cmdText, cmdParms);
			int result = sqlCommand.ExecuteNonQuery();
			sqlCommand.Parameters.Clear();
			return result;
		}

		public static int ExecuteNonQuery(SqlConnection conn, int commandTimeout, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
		{
			SqlCommand sqlCommand = new SqlCommand();
			sqlCommand.CommandTimeout = commandTimeout;
			SQLHelper.PrepareCommand(sqlCommand, conn, null, cmdType, cmdText, cmdParms);
			int result = sqlCommand.ExecuteNonQuery();
			sqlCommand.Parameters.Clear();
			return result;
		}

		public static int ExecuteNonQuery(SqlTransaction trans, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
		{
			SqlCommand sqlCommand = new SqlCommand();
			SQLHelper.PrepareCommand(sqlCommand, trans.Connection, trans, cmdType, cmdText, cmdParms);
			int result = sqlCommand.ExecuteNonQuery();
			sqlCommand.Parameters.Clear();
			return result;
		}

		public static int ExecuteNonQuery(SqlTransaction trans, int commandTimeout, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
		{
			SqlCommand sqlCommand = new SqlCommand();
			sqlCommand.CommandTimeout = commandTimeout;
			SQLHelper.PrepareCommand(sqlCommand, trans.Connection, trans, cmdType, cmdText, cmdParms);
			int result = sqlCommand.ExecuteNonQuery();
			sqlCommand.Parameters.Clear();
			return result;
		}

		public static SqlDataReader ExecuteReader(string connString, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
		{
			SqlCommand sqlCommand = new SqlCommand();
			SqlConnection sqlConnection = new SqlConnection(connString);
			SqlDataReader result;
			try
			{
				SQLHelper.PrepareCommand(sqlCommand, sqlConnection, null, cmdType, cmdText, cmdParms);
				SqlDataReader sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.Default);
				sqlCommand.Parameters.Clear();
				result = sqlDataReader;
			}
			catch
			{
				sqlConnection.Close();
				result = null;
			}
			return result;
		}

		public static SqlDataReader ExecuteReader(SqlConnection conn, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
		{
			SqlCommand sqlCommand = new SqlCommand();
			SQLHelper.PrepareCommand(sqlCommand, conn, null, cmdType, cmdText, cmdParms);
			SqlDataReader result = sqlCommand.ExecuteReader();
			sqlCommand.Parameters.Clear();
			return result;
		}

		public static object ExecuteScalar(string connString, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
		{
			SqlCommand sqlCommand = new SqlCommand();
			object result;
			using (SqlConnection sqlConnection = new SqlConnection(connString))
			{
				SQLHelper.PrepareCommand(sqlCommand, sqlConnection, null, cmdType, cmdText, cmdParms);
				object obj = sqlCommand.ExecuteScalar();
				sqlCommand.Parameters.Clear();
				result = obj;
			}
			return result;
		}

		public static object ExecuteScalar(SqlConnection conn, CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
		{
			SqlCommand sqlCommand = new SqlCommand();
			SQLHelper.PrepareCommand(sqlCommand, conn, null, cmdType, cmdText, cmdParms);
			object result = sqlCommand.ExecuteScalar();
			sqlCommand.Parameters.Clear();
			return result;
		}

		public static void CacheParameters(string cacheKey, params SqlParameter[] cmdParms)
		{
			SQLHelper.parmCache[cacheKey] = cmdParms;
		}

		public static void ClearParameterValues(params SqlParameter[] cmdParms)
		{
			for (int i = 0; i < cmdParms.Length; i++)
			{
				SqlParameter sqlParameter = cmdParms[i];
				sqlParameter.Value = DBNull.Value;
			}
		}

		public static SqlParameter CreateReturnParameter(string parameterName)
		{
			return new SqlParameter(parameterName, SqlDbType.Int, 4, ParameterDirection.ReturnValue, false, 0, 0, string.Empty, DataRowVersion.Default, null);
		}

		public static SqlParameter[] GetCachedParameters(string cacheKey)
		{
			SqlParameter[] array = (SqlParameter[])SQLHelper.parmCache[cacheKey];
			if (array == null)
			{
				return null;
			}
			SqlParameter[] array2 = new SqlParameter[array.Length];
			int i = 0;
			int num = array.Length;
			while (i < num)
			{
				array2[i] = (SqlParameter)((ICloneable)array[i]).Clone();
				i++;
			}
			return array2;
		}

		private static void PrepareCommand(SqlCommand cmd, SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, SqlParameter[] cmdParms)
		{
			if (conn.State != ConnectionState.Open)
			{
				conn.Open();
			}
			cmd.Connection = conn;
			cmd.CommandText = cmdText;
			if (trans != null)
			{
				cmd.Transaction = trans;
			}
			cmd.CommandType = cmdType;
			if (cmdParms != null)
			{
				for (int i = 0; i < cmdParms.Length; i++)
				{
					SqlParameter value = cmdParms[i];
					cmd.Parameters.Add(value);
				}
			}
		}

		public static SqlDataReader DataReaderRun(string CommText, string ConnectionStr)
		{
			SqlDataReader result = null;
			SqlConnection sqlConnection = new SqlConnection(ConnectionStr);
			SqlCommand sqlCommand = SQLHelper.CreateCommand(CommText);
			try
			{
				sqlCommand.Connection = sqlConnection;
				sqlConnection.Open();
				result = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
			}
			catch (Exception ex)
			{
				sqlConnection.Close();
				ErrorLog.Write(ex);
			}
			return result;
		}

		public static SqlCommand CreateCommand(string CommText)
		{
			return new SqlCommand
			{
				CommandText = CommText
			};
		}

		public static DataSet ExecuteDataset(string connectionString, CommandType commandType, string commandText)
		{
			return SQLHelper.ExecuteDataset(connectionString, commandType, commandText, null);
		}

		public static DataSet ExecuteDataset(string connectionString, int commandTimeout, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			DataSet result;
			using (SqlConnection sqlConnection = new SqlConnection(connectionString))
			{
				result = SQLHelper.ExecuteDataset(sqlConnection, commandTimeout, commandType, commandText, commandParameters);
			}
			return result;
		}

		public static DataSet ExecuteDataset(string connectionString, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			DataSet result;
			using (SqlConnection sqlConnection = new SqlConnection(connectionString))
			{
				result = SQLHelper.ExecuteDataset(sqlConnection, commandType, commandText, commandParameters);
			}
			return result;
		}

		public static DataSet ExecuteDataset(SqlConnection connection, CommandType commandType, string commandText)
		{
			return SQLHelper.ExecuteDataset(connection, commandType, commandText, null);
		}

		public static DataSet ExecuteDataset(SqlConnection connection, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			return SQLHelper.ExecuteDataset(connection, 0, commandType, commandText, commandParameters);
		}

		public static DataSet ExecuteDataset(SqlConnection connection, int commandTimeout, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			SqlCommand sqlCommand = new SqlCommand();
			if (commandTimeout > 0)
			{
				sqlCommand.CommandTimeout = commandTimeout;
			}
			SQLHelper.PrepareCommand(sqlCommand, connection, null, commandType, commandText, commandParameters);
			SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
			DataSet dataSet = new DataSet();
			sqlDataAdapter.Fill(dataSet);
			sqlCommand.Parameters.Clear();
			return dataSet;
		}

		public static DataSet ExecuteDataset(SqlTransaction transaction, CommandType commandType, string commandText)
		{
			return SQLHelper.ExecuteDataset(transaction, commandType, commandText, null);
		}

		public static DataSet ExecuteDataset(SqlTransaction transaction, CommandType commandType, string commandText, params SqlParameter[] commandParameters)
		{
			SqlCommand sqlCommand = new SqlCommand();
			SQLHelper.PrepareCommand(sqlCommand, transaction.Connection, transaction, commandType, commandText, commandParameters);
			SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
			DataSet dataSet = new DataSet();
			sqlDataAdapter.Fill(dataSet);
			sqlCommand.Parameters.Clear();
			return dataSet;
		}
	}
}
