using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text;

namespace ZhuanCloud.Collector
{
	public class UserTableTypeCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			if (sqlVersion < SQLServerVersion.SQLServer2008)
			{
				return true;
			}
			bool result;
			try
			{
				DataSet userTable = this.GetUserTable(sqlString, timeOut);
				if (userTable.Tables.Contains("UserTableNames"))
				{
					DataRowCollection rows = userTable.Tables["UserTableNames"].Rows;
					if (rows.Count > 0)
					{
						DataTable dataTable = new DataTable();
						dataTable.Columns.Add(new DataColumn("db_name"));
						dataTable.Columns.Add(new DataColumn("schema_name"));
						dataTable.Columns.Add(new DataColumn("object_name"));
						dataTable.Columns.Add(new DataColumn("define"));
						dataTable.Columns.Add(new DataColumn("db_type"));
						dataTable.Columns.Add(new DataColumn("create_date"));
						dataTable.Columns.Add(new DataColumn("modify_date"));
						SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder(sqlString);
						string initialCatalog = sqlConnectionStringBuilder.InitialCatalog;
						foreach (DataRow dataRow in rows)
						{
							int objId = Convert.ToInt32(dataRow.ItemArray[0]);
							string text = dataRow.ItemArray[1].ToString();
							string text2 = dataRow.ItemArray[2].ToString();
							string userTableDefine = this.GetUserTableDefine(userTable, objId, text, text2);
							string text3 = TextManager.Compress(userTableDefine);
							DateTime dateTime = DateTime.Parse(dataRow.ItemArray[3].ToString());
							DateTime dateTime2 = DateTime.Parse(dataRow.ItemArray[4].ToString());
							dataTable.Rows.Add(new object[]
							{
								initialCatalog,
								text,
								text2,
								text3,
								"用户自定义表类型",
								dateTime,
								dateTime2
							});
						}
						if (dataTable.Rows.Count > 0)
						{
							if (!Util.startFlag)
							{
								result = false;
								return result;
							}
							string cmdText = "\r\nINSERT  INTO tblDBUserType\r\n        ( DBName ,\r\n          SchemaName ,\r\n          ObjectName ,\r\n          Define ,\r\n          DBType ,\r\n          CreateDate ,\r\n          ModifyDate \r\n        )\r\nVALUES  ( @DBName ,\r\n          @SchemaName ,\r\n          @ObjectName ,\r\n          @Define ,\r\n          @DBType ,\r\n          @CreateDate ,\r\n          @ModifyDate \r\n        )\r\n";
							OleDbParameter[] parameters = new OleDbParameter[]
							{
								new OleDbParameter("@DBName", OleDbType.VarChar)
								{
									SourceColumn = "db_name"
								},
								new OleDbParameter("@SchemaName", OleDbType.VarChar)
								{
									SourceColumn = "schema_name"
								},
								new OleDbParameter("@ObjectName", OleDbType.VarChar)
								{
									SourceColumn = "object_name"
								},
								new OleDbParameter("@Define", OleDbType.VarChar)
								{
									SourceColumn = "define"
								},
								new OleDbParameter("@DBType", OleDbType.VarChar)
								{
									SourceColumn = "db_type"
								},
								new OleDbParameter("@CreateDate", OleDbType.Date)
								{
									SourceColumn = "create_date"
								},
								new OleDbParameter("@ModifyDate", OleDbType.Date)
								{
									SourceColumn = "modify_date"
								}
							};
							result = base.SaveToAccess(oleDBString, dataTable, cmdText, parameters);
							return result;
						}
					}
				}
				result = true;
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private string GetUserTableDefine(DataSet userTableDS, int objId, string schema_name, string table_name)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (userTableDS.Tables.Contains("UserTableCols"))
			{
				DataRow[] array = userTableDS.Tables["UserTableCols"].Select("objId=" + objId);
				if (array.Length > 0)
				{
					stringBuilder.Append("CREATE TYPE ");
					stringBuilder.Append(string.Concat(new string[]
					{
						"[",
						schema_name,
						"].[",
						table_name,
						"]"
					}));
					stringBuilder.Append(" AS TABLE(" + Environment.NewLine);
					for (int i = 0; i < array.Length; i++)
					{
						string str = array[i].ItemArray[2].ToString();
						stringBuilder.Append(new string(' ', 4));
						stringBuilder.Append("[" + str + "]");
						string text = array[i].ItemArray[3].ToString();
						if (!string.IsNullOrEmpty(text))
						{
							stringBuilder.Append(" AS " + text + " ," + Environment.NewLine);
						}
						else
						{
							bool flag = Convert.ToBoolean(array[i].ItemArray[4]);
							if (flag)
							{
								string text2 = array[i].ItemArray[5].ToString();
								string text3 = array[i].ItemArray[6].ToString();
								stringBuilder.Append(string.Concat(new string[]
								{
									" [",
									text2,
									"].[",
									text3,
									"] "
								}));
							}
							else
							{
								string typeName = array[i].ItemArray[6].ToString();
								int maxLength = Convert.ToInt32(array[i].ItemArray[7]);
								int precision = Convert.ToInt32(array[i].ItemArray[8]);
								int scale = Convert.ToInt32(array[i].ItemArray[9]);
								string typeText = this.GetTypeText(typeName, maxLength, precision, scale);
								stringBuilder.Append(" " + typeText + " ");
							}
							string text4 = array[i].ItemArray[10].ToString();
							if (!string.IsNullOrEmpty(text4))
							{
								stringBuilder.Append(text4 + " ");
							}
							string value = Convert.ToBoolean(array[i].ItemArray[11]) ? "NULL" : "NOT NULL";
							stringBuilder.Append(value);
							int num = Convert.ToInt32(array[i].ItemArray[12]);
							if (num != 0 && userTableDS.Tables.Contains("UserTableDefault"))
							{
								DataRow[] array2 = userTableDS.Tables["UserTableDefault"].Select("objId=" + num);
								if (array2.Length > 0)
								{
									string text5 = array2[0].ItemArray[1].ToString();
									if (!string.IsNullOrEmpty(text5))
									{
										stringBuilder.Append(" DEFAULT " + text5);
									}
								}
							}
							stringBuilder.Append("," + Environment.NewLine);
						}
					}
					if (userTableDS.Tables.Contains("UserTableKey"))
					{
						DataRow[] array3 = userTableDS.Tables["UserTableKey"].Select("prikey=1 and uniquekey=0 and objId=" + objId);
						if (array3.Length > 0)
						{
							stringBuilder.Append(new string(' ', 4));
							stringBuilder.Append("PRIMARY KEY ");
							string str2 = array3[0].ItemArray[1].ToString();
							stringBuilder.Append(str2 + Environment.NewLine);
							stringBuilder.Append(new string(' ', 4));
							stringBuilder.Append("(" + Environment.NewLine);
							DataRow[] array4 = array3;
							for (int j = 0; j < array4.Length; j++)
							{
								DataRow dataRow = array4[j];
								string arg = dataRow.ItemArray[2].ToString();
								string arg2 = Convert.ToBoolean(dataRow.ItemArray[3]) ? "DESC" : "ASC";
								stringBuilder.Append(new string(' ', 8));
								stringBuilder.AppendLine(string.Format("[{0}] {1},", arg, arg2));
							}
							stringBuilder.Remove(stringBuilder.Length - Environment.NewLine.Length - 1, 1);
							stringBuilder.Append(new string(' ', 4));
							stringBuilder.AppendLine(")WITH (IGNORE_DUP_KEY = OFF),");
						}
						DataRow[] array5 = userTableDS.Tables["UserTableKey"].Select("prikey=0 and uniquekey=1 and objId=" + objId);
						if (array5.Length > 0)
						{
							stringBuilder.Append(new string(' ', 4));
							stringBuilder.Append("UNIQUE ");
							string str3 = array5[0].ItemArray[1].ToString();
							stringBuilder.Append(str3 + Environment.NewLine);
							stringBuilder.Append(new string(' ', 4));
							stringBuilder.Append("(" + Environment.NewLine);
							DataRow[] array6 = array5;
							for (int k = 0; k < array6.Length; k++)
							{
								DataRow dataRow2 = array6[k];
								string arg3 = dataRow2.ItemArray[2].ToString();
								string arg4 = Convert.ToBoolean(dataRow2.ItemArray[3]) ? "DESC" : "ASC";
								stringBuilder.Append(new string(' ', 8));
								stringBuilder.AppendLine(string.Format("[{0}] {1},", arg3, arg4));
							}
							stringBuilder.Remove(stringBuilder.Length - Environment.NewLine.Length - 1, 1);
							stringBuilder.Append(new string(' ', 4));
							stringBuilder.AppendLine(")WITH (IGNORE_DUP_KEY = OFF),");
						}
					}
					stringBuilder.Remove(stringBuilder.Length - Environment.NewLine.Length - 1, 1);
					stringBuilder.AppendLine(")");
					stringBuilder.AppendLine("GO");
				}
			}
			return stringBuilder.ToString();
		}

		public string GetTypeText(string typeName, int maxLength, int precision, int scale)
		{
			string arg_05_0 = string.Empty;
			string key;
			switch (key = typeName.ToLower())
			{
			case "nvarchar":
				if (maxLength == -1)
				{
					return "nvarchar(MAX)";
				}
				return string.Format("nvarchar({0})", maxLength / 2);
			case "nchar":
				return string.Format("nchar({0})", maxLength / 2);
			case "binary":
			case "char":
			case "varchar":
			case "varbinary":
				return string.Format("{0}({1})", typeName, (maxLength == -1) ? "MAX" : maxLength.ToString());
			case "time":
			case "datetime2":
			case "datetimeoffset":
				return string.Format("{0}({1})", typeName, scale);
			case "decimal":
			case "numeric":
				return string.Format("{0}({1},{2})", typeName, precision, scale);
			}
			return typeName;
		}

		private DataSet GetUserTable(string sqlString, int timeOut)
		{
			DataSet dataSet = new DataSet();
			string cmdText = "\r\nSELECT  t.type_table_object_id AS [objId] ,\r\n        s.name AS [schema_name] ,\r\n        t.name AS [type_name] ,\r\n        o.create_date ,\r\n        o.modify_date\r\nFROM    sys.table_types t ,\r\n        sys.schemas s ,\r\n        sys.objects o\r\nWHERE   t.[schema_id] = s.[schema_id]\r\n        AND o.object_id = t.type_table_object_id\r\n        AND t.is_user_defined = 1\r\n        AND t.is_table_type = 1\r\n";
			DataTable dataTable = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable.TableName = "UserTableNames";
			dataSet.Tables.Add(dataTable);
			string cmdText2 = "\r\nSELECT DISTINCT\r\n        c.[object_id] AS [objId] ,\r\n        c.column_id ,\r\n        c.name ,\r\n        CASE c.is_computed\r\n          WHEN 1 THEN e.[text]\r\n          ELSE NULL\r\n        END AS express ,\r\n        CASE c.system_type_id\r\n          WHEN c.user_type_id THEN 0\r\n          ELSE 1\r\n        END AS is_usertype ,\r\n        s.name 'UserTypeSchema' ,\r\n        t.name 'TypeName' ,\r\n        c.max_length ,\r\n        c.[precision] ,\r\n        c.scale ,\r\n        CASE c.is_identity\r\n          WHEN 1\r\n          THEN 'identity(' + CONVERT(NVARCHAR(20), i.seed_value) + ','\r\n               + CONVERT(NVARCHAR(20), i.increment_value) + ')'\r\n          ELSE NULL\r\n        END AS colidentity ,\r\n        c.is_nullable ,\r\n        c.default_object_id\r\nFROM    sys.table_types tt\r\n        JOIN sys.columns AS c ON tt.type_table_object_id = c.[object_id]\r\n        JOIN sys.types AS t ON c.user_type_id = t.user_type_id\r\n        JOIN sys.schemas AS s ON t.schema_id = s.schema_id\r\n        LEFT JOIN syscomments AS e ON c.object_id = e.id\r\n        LEFT JOIN sys.identity_columns AS i ON c.object_id = i.object_id\r\nORDER BY c.[object_id] ,\r\n        c.column_id\r\n";
			DataTable dataTable2 = base.FillDataTable(sqlString, cmdText2, timeOut);
			dataTable2.TableName = "UserTableCols";
			dataSet.Tables.Add(dataTable2);
			string cmdText3 = "\r\nSELECT  dc.[object_id] AS [objId] ,\r\n        dc.[definition]\r\nFROM    sys.default_constraints dc ,\r\n        sys.table_types t\r\nWHERE   dc.parent_object_id = t.type_table_object_id\r\n";
			DataTable dataTable3 = base.FillDataTable(sqlString, cmdText3, timeOut);
			dataTable3.TableName = "UserTableDefault";
			dataSet.Tables.Add(dataTable3);
			string cmdText4 = "\r\nSELECT  i.[object_id] AS [objId] ,\r\n        i.type_desc AS TypeDesc ,\r\n        c.name AS ColName ,\r\n        ic.is_descending_key AS IsDescCol ,\r\n        i.is_primary_key AS prikey ,\r\n        i.is_unique_constraint AS uniquekey\r\nFROM    sys.indexes i ,\r\n        sys.index_columns ic ,\r\n        sys.columns c ,\r\n        sys.table_types t\r\nWHERE   i.[object_id] = ic.[object_id]\r\n        AND i.[object_id] = c.[object_id]\r\n        AND ic.column_id = c.column_id\r\n        AND i.[object_id] = t.type_table_object_id\r\n        AND i.index_id = ic.index_id\r\nORDER BY [objId] ,\r\n        ic.key_ordinal\r\n";
			DataTable dataTable4 = base.FillDataTable(sqlString, cmdText4, timeOut);
			dataTable4.TableName = "UserTableKey";
			dataSet.Tables.Add(dataTable4);
			return dataSet;
		}
	}
}
