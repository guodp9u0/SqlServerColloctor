using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;

namespace ZhuanCloud.Collector
{
	public class ConfigManager
	{
		public CollectConfig GetDefaultConfig()
		{
			CollectConfig collectConfig = new CollectConfig();
			collectConfig.HasSqlText = true;
			collectConfig.SqlTextValue = 3000;
			collectConfig.HasSession = true;
			collectConfig.WaitElapsed = 3;
			collectConfig.UnusedSessionValue = 10;
			collectConfig.UnusedSessionElapsed = 5;
			collectConfig.HasPerfCounter = true;
			collectConfig.PerfElapsed = 10;
			collectConfig.HasTempdbInfo = true;
			collectConfig.TempdbElapsed = 5;
			collectConfig.HasDBDefine = true;
			collectConfig.HasCheckTable = false;
			collectConfig.DBItemType = new DBItemType
			{
				HasTable = true,
				HasView = true,
				HasFuction = true,
				HasStoredProc = true
			};
			collectConfig.IsDecrypt = false;
			List<DBFilter> dBFilterList = new List<DBFilter>();
			collectConfig.DBFilterList = dBFilterList;
			collectConfig.HasDBFile = true;
			collectConfig.DBFileElapsed = 5;
			collectConfig.HasSqlPlan = true;
			collectConfig.SqlPlanElapsed = 1;
			collectConfig.SqlPlanValue = 500;
			collectConfig.HasErrorLog = true;
			collectConfig.ErrorLogElapsed = 1;
			collectConfig.ErrorLogKeys = new string[]
			{
				"Autogrow",
				"Login failed",
				"Operating system error",
				"I/O requests",
				"Deadlock encountered"
			};
			collectConfig.HasMoebius = false;
			collectConfig.TimeOut = 300;
			return collectConfig;
		}

		public static List<Perf> LoadPerfCounter(string dbInstanceName, out string errMsg)
		{
			string empty = string.Empty;
			List<Perf> list = ConfigManager.GetPerfList(dbInstanceName, out empty);
			if (list == null)
			{
				list = ConfigManager.GetDefaultList(dbInstanceName, out empty);
			}
			errMsg = empty;
			return list;
		}

		private static List<Perf> GetDefaultList(string dbInstanceName, out string errMsg)
		{
			StringBuilder stringBuilder = new StringBuilder();
			List<Perf> list = new List<Perf>();
			try
			{
				string empty = string.Empty;
				List<Perf> routinePerf = ConfigManager.GetRoutinePerf(out empty);
				if (routinePerf.Count > 0)
				{
					list.AddRange(routinePerf);
				}
				if (!string.IsNullOrEmpty(empty))
				{
					stringBuilder.Append(empty);
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
			}
			try
			{
				string empty2 = string.Empty;
				List<Perf> dBPerf = ConfigManager.GetDBPerf(dbInstanceName, out empty2);
				if (dBPerf.Count > 0)
				{
					list.AddRange(dBPerf);
				}
				if (!string.IsNullOrEmpty(empty2))
				{
					stringBuilder.Append(empty2);
				}
			}
			catch (Exception ex2)
			{
				ErrorLog.Write(ex2);
			}
			try
			{
				string empty3 = string.Empty;
				List<Perf> networkPerf = ConfigManager.GetNetworkPerf(out empty3);
				if (networkPerf.Count > 0)
				{
					list.AddRange(networkPerf);
				}
				if (!string.IsNullOrEmpty(empty3))
				{
					stringBuilder.Append(empty3);
				}
			}
			catch (Exception ex3)
			{
				ErrorLog.Write(ex3);
			}
			errMsg = stringBuilder.ToString();
			return list;
		}

		private static List<Perf> GetRoutinePerf(out string errMsg)
		{
			List<Perf> list = new List<Perf>();
			StringBuilder stringBuilder = new StringBuilder();
			Perf perf = new Perf("Processor", "% Processor Time", "_Total");
			if (perf.HasError)
			{
				stringBuilder.AppendLine("Processor(_Total)\\% Processor Time");
			}
			list.Add(perf);
			perf = new Perf("Processor", "% Privileged Time", "_Total");
			if (perf.HasError)
			{
				stringBuilder.AppendLine("Processor(_Total)\\% Privileged Time");
			}
			perf.Checked = false;
			list.Add(perf);
			perf = new Perf("Processor", "% Interrupt Time", "_Total");
			if (perf.HasError)
			{
				stringBuilder.AppendLine("Processor(_Total)\\% Interrupt Time");
			}
			perf.Checked = false;
			list.Add(perf);
			perf = new Perf("Processor", "% User Time", "_Total");
			if (perf.HasError)
			{
				stringBuilder.AppendLine("Processor(_Total)\\% User Time");
			}
			list.Add(perf);
			PerformanceCounterCategory performanceCounterCategory = null;
			try
			{
				performanceCounterCategory = new PerformanceCounterCategory("Process");
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
			}
			if (performanceCounterCategory == null)
			{
				stringBuilder.AppendLine("SQL Server进程相关计数器");
			}
			else
			{
				string[] array = null;
				try
				{
					array = performanceCounterCategory.GetInstanceNames();
				}
				catch (Exception ex2)
				{
					ErrorLog.Write(ex2);
				}
				if (array == null)
				{
					stringBuilder.AppendLine("SQL Server进程相关计数器");
				}
				else if (array.Length > 0)
				{
					string[] array2 = array;
					for (int i = 0; i < array2.Length; i++)
					{
						string text = array2[i];
						if (text.StartsWith("sqlservr"))
						{
							perf = new Perf(performanceCounterCategory.CategoryName, "% Processor Time", text);
							if (perf.HasError)
							{
								stringBuilder.AppendLine(string.Format("Process({0})\\% Processor Time", text));
							}
							list.Add(perf);
						}
					}
				}
			}
			PerformanceCounterCategory performanceCounterCategory2 = null;
			try
			{
				performanceCounterCategory2 = new PerformanceCounterCategory("PhysicalDisk");
			}
			catch (Exception ex3)
			{
				ErrorLog.Write(ex3);
			}
			if (performanceCounterCategory2 == null)
			{
				stringBuilder.AppendLine("物理磁盘相关计数器");
			}
			else
			{
				string[] array3 = null;
				try
				{
					array3 = performanceCounterCategory2.GetInstanceNames();
				}
				catch (Exception ex4)
				{
					ErrorLog.Write(ex4);
				}
				if (array3 == null)
				{
					stringBuilder.AppendLine("物理磁盘相关计数器");
				}
				else if (array3.Length > 0)
				{
					perf = new Perf(performanceCounterCategory2.CategoryName, "Avg. Disk Queue Length", "_Total");
					if (perf.HasError)
					{
						stringBuilder.AppendLine(string.Format("{0}(_Total)\\Avg. Disk Queue Length", performanceCounterCategory2.CategoryName));
					}
					list.Add(perf);
					string[] array4 = array3;
					for (int j = 0; j < array4.Length; j++)
					{
						string text2 = array4[j];
						if (text2 != "_Total")
						{
							perf = new Perf(performanceCounterCategory2.CategoryName, "Avg. Disk Read Queue Length", text2);
							if (perf.HasError)
							{
								stringBuilder.AppendLine(string.Format("{0}({1})\\Avg. Disk Read Queue Length", performanceCounterCategory2.CategoryName, text2));
							}
							list.Add(perf);
							perf = new Perf(performanceCounterCategory2.CategoryName, "Avg. Disk Write Queue Length", text2);
							if (perf.HasError)
							{
								stringBuilder.AppendLine(string.Format("{0}({1})\\Avg. Disk Write Queue Length", performanceCounterCategory2.CategoryName, text2));
							}
							list.Add(perf);
							perf = new Perf(performanceCounterCategory2.CategoryName, "Avg. Disk sec/Read", text2);
							if (perf.HasError)
							{
								stringBuilder.AppendLine(string.Format("{0}({1})\\Avg. Disk sec/Read", performanceCounterCategory2.CategoryName, text2));
							}
							list.Add(perf);
							perf = new Perf(performanceCounterCategory2.CategoryName, "Avg. Disk sec/Write", text2);
							if (perf.HasError)
							{
								stringBuilder.AppendLine(string.Format("{0}({1})\\Avg. Disk sec/Write", performanceCounterCategory2.CategoryName, text2));
							}
							list.Add(perf);
							perf = new Perf(performanceCounterCategory2.CategoryName, "Disk Read Bytes/sec", text2);
							if (perf.HasError)
							{
								stringBuilder.AppendLine(string.Format("{0}({1})\\Disk Read Bytes/sec", performanceCounterCategory2.CategoryName, text2));
							}
							list.Add(perf);
							perf = new Perf(performanceCounterCategory2.CategoryName, "Disk Write Bytes/sec", text2);
							if (perf.HasError)
							{
								stringBuilder.AppendLine(string.Format("{0}({1})\\Disk Write Bytes/sec", performanceCounterCategory2.CategoryName, text2));
							}
							list.Add(perf);
							perf = new Perf(performanceCounterCategory2.CategoryName, "% Idle Time", text2);
							if (perf.HasError)
							{
								stringBuilder.AppendLine(string.Format("{0}({1})\\% Idle Time", performanceCounterCategory2.CategoryName, text2));
							}
							perf.Checked = false;
							list.Add(perf);
						}
					}
				}
			}
			perf = new Perf("Memory", "Available MBytes", "");
			if (perf.HasError)
			{
				stringBuilder.AppendLine("Memory\\Available MBytes");
			}
			list.Add(perf);
			perf = new Perf("Memory", "Page Reads/sec", "");
			if (perf.HasError)
			{
				stringBuilder.AppendLine("Memory\\Page Reads/sec");
			}
			list.Add(perf);
			perf = new Perf("Memory", "Page Writes/sec", "");
			if (perf.HasError)
			{
				stringBuilder.AppendLine("Memory\\Page Writes/sec");
			}
			errMsg = stringBuilder.ToString();
			list.Add(perf);
			return list;
		}

		private static List<Perf> GetDBPerf(string dbInstanceName, out string errMsg)
		{
			List<Perf> list = new List<Perf>();
			StringBuilder stringBuilder = new StringBuilder();
			List<string> list2 = new List<string>
			{
				"SQL Statistics",
				"Locks",
				"Latches",
				"Access Methods",
				"Buffer Manager",
				"General Statistics",
				"Plan Cache",
				"Memory Manager",
				"Databases"
			};
			List<string> list3 = new List<string>
			{
				"Batch Requests/sec",
				"SQL Compilations/sec",
				"SQL Re-Compilations/sec",
				"Active Temp Tables",
				"Temp Tables For Destruction",
				"Temp Tables Creation Rate",
				"User Connections",
				"Logins/sec",
				"Logouts/sec",
				"Processes blocked",
				"Lock Wait Time (ms)",
				"Average Wait Time (ms)",
				"Lock Requests/sec",
				"Lock Waits/sec",
				"Number of Deadlocks/sec",
				"Full Scans/sec",
				"Page Splits/sec",
				"Table Lock Escalations/sec",
				"Forwarded Records/sec",
				"Workfiles Created/sec",
				"Worktables Created/sec",
				"Mixed page allocations/sec",
				"Lazy writes/sec",
				"Page life expectancy",
				"Connection Memory (KB)",
				"Target Server Memory (KB)",
				"Total Server Memory (KB)",
				"SQL Cache Memory (KB)",
				"Memory Grants Pending",
				"Average Latch Wait Time (ms)",
				"Latch Waits/sec",
				"Cache Hit Ratio",
				"Log Flushes/sec",
				"Active Transactions"
			};
			string text = "_Total";
			PerformanceCounterCategory[] array = null;
			try
			{
				array = PerformanceCounterCategory.GetCategories();
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
			}
			if (array == null)
			{
				stringBuilder.AppendLine("数据库相关性能计数器");
				errMsg = stringBuilder.ToString();
				return list;
			}
			PerformanceCounterCategory[] array2 = array;
			for (int i = 0; i < array2.Length; i++)
			{
				PerformanceCounterCategory performanceCounterCategory = array2[i];
				if (performanceCounterCategory.CategoryName.ToUpper().StartsWith(dbInstanceName))
				{
					int num = performanceCounterCategory.CategoryName.IndexOf(":");
					string item = performanceCounterCategory.CategoryName.Substring(num + 1);
					if (list2.Contains(item))
					{
						string[] instanceNames = performanceCounterCategory.GetInstanceNames();
						if (instanceNames.Length > 0)
						{
							string[] array3 = instanceNames;
							for (int j = 0; j < array3.Length; j++)
							{
								string text2 = array3[j];
								if (text2 == text)
								{
									PerformanceCounter[] counters = performanceCounterCategory.GetCounters(text2);
									if (counters.Length > 0)
									{
										PerformanceCounter[] array4 = counters;
										for (int k = 0; k < array4.Length; k++)
										{
											PerformanceCounter performanceCounter = array4[k];
											if (list3.Contains(performanceCounter.CounterName))
											{
												Perf perf = new Perf(performanceCounterCategory.CategoryName, performanceCounter.CounterName, text);
												if (perf.HasError)
												{
													stringBuilder.AppendLine(string.Format("{0}({1})\\{2}", performanceCounterCategory.CategoryName, text, performanceCounter.CounterName));
												}
												list.Add(perf);
											}
										}
									}
								}
							}
						}
						else
						{
							PerformanceCounter[] counters2 = performanceCounterCategory.GetCounters();
							if (counters2.Length > 0)
							{
								PerformanceCounter[] array5 = counters2;
								for (int l = 0; l < array5.Length; l++)
								{
									PerformanceCounter performanceCounter2 = array5[l];
									if (list3.Contains(performanceCounter2.CounterName))
									{
										Perf perf2 = new Perf(performanceCounterCategory.CategoryName, performanceCounter2.CounterName, "");
										if (perf2.HasError)
										{
											stringBuilder.AppendLine(string.Format("{0}\\{1}", performanceCounterCategory.CategoryName, performanceCounter2.CounterName));
										}
										list.Add(perf2);
									}
								}
							}
						}
					}
				}
			}
			errMsg = stringBuilder.ToString();
			return list;
		}

		private static List<Perf> GetNetworkPerf(out string errMsg)
		{
			List<Perf> list = new List<Perf>();
			StringBuilder stringBuilder = new StringBuilder();
			PerformanceCounterCategory performanceCounterCategory = null;
			try
			{
				performanceCounterCategory = new PerformanceCounterCategory("Network Interface");
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
			}
			if (performanceCounterCategory == null)
			{
				stringBuilder.AppendLine("Network 相关性能计数器");
				errMsg = stringBuilder.ToString();
				return list;
			}
			string[] array = null;
			try
			{
				array = performanceCounterCategory.GetInstanceNames();
			}
			catch (Exception ex2)
			{
				ErrorLog.Write(ex2);
			}
			if (array == null)
			{
				stringBuilder.AppendLine("Network 相关性能计数器");
				errMsg = stringBuilder.ToString();
				return list;
			}
			if (array.Length > 0)
			{
				string[] array2 = new string[]
				{
					"Output Queue Length",
					"Bytes Received/sec",
					"Bytes Sent/sec"
				};
				string[] array3 = array2;
				for (int i = 0; i < array3.Length; i++)
				{
					string text = array3[i];
					string[] array4 = array;
					for (int j = 0; j < array4.Length; j++)
					{
						string text2 = array4[j];
						Perf perf = new Perf(performanceCounterCategory.CategoryName, text, text2);
						if (perf.HasError)
						{
							stringBuilder.AppendLine(string.Format("{0}({1})\\{2}", performanceCounterCategory.CategoryName, text2, text));
						}
						if (text == "Bytes Received/sec" || text == "Bytes Sent/sec")
						{
							perf.Checked = false;
						}
						list.Add(perf);
					}
				}
			}
			errMsg = stringBuilder.ToString();
			return list;
		}

		public void SaveConfig(CollectConfig config)
		{
			if (config != null)
			{
				string filename = Path.Combine(Util.configDir, "Collector.config");
				if (!Directory.Exists(Util.configDir))
				{
					Directory.CreateDirectory(Util.configDir);
				}
				using (XmlTextWriter xmlTextWriter = new XmlTextWriter(filename, TextManager.Encoding))
				{
					xmlTextWriter.Formatting = Formatting.Indented;
					xmlTextWriter.WriteStartDocument();
					xmlTextWriter.WriteStartElement("Config");
					xmlTextWriter.WriteStartElement("Version");
					xmlTextWriter.WriteString(Assembly.GetEntryAssembly().GetName().Version.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("DBInstanceName");
					xmlTextWriter.WriteString(config.DBInstanceName);
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("PerfCounter");
					xmlTextWriter.WriteStartElement("Collect");
					xmlTextWriter.WriteString(config.HasPerfCounter.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("Elapsed");
					xmlTextWriter.WriteString(config.PerfElapsed.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("TempdbInfo");
					xmlTextWriter.WriteStartElement("Collect");
					xmlTextWriter.WriteString(config.HasTempdbInfo.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("Elapsed");
					xmlTextWriter.WriteString(config.TempdbElapsed.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("DBDefine");
					xmlTextWriter.WriteStartElement("Collect");
					xmlTextWriter.WriteString(config.HasDBDefine.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("CheckTable");
					xmlTextWriter.WriteString(config.HasCheckTable.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("Table");
					xmlTextWriter.WriteString(config.DBItemType.HasTable.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("View");
					xmlTextWriter.WriteString(config.DBItemType.HasView.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("StoredProc");
					xmlTextWriter.WriteString(config.DBItemType.HasStoredProc.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("Fuction");
					xmlTextWriter.WriteString(config.DBItemType.HasFuction.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("IsDecrypt");
					xmlTextWriter.WriteString(config.IsDecrypt.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("FilterItems");
					if (config.DBFilterList != null && config.DBFilterList.Count > 0)
					{
						foreach (DBFilter current in config.DBFilterList)
						{
							xmlTextWriter.WriteStartElement("Filter");
							xmlTextWriter.WriteStartElement("DBName");
							xmlTextWriter.WriteString(current.DBName);
							xmlTextWriter.WriteEndElement();
							xmlTextWriter.WriteStartElement("DBType");
							xmlTextWriter.WriteString(current.DBType.ToString());
							xmlTextWriter.WriteEndElement();
							xmlTextWriter.WriteStartElement("FilterString");
							xmlTextWriter.WriteString(current.FilterString);
							xmlTextWriter.WriteEndElement();
							xmlTextWriter.WriteEndElement();
						}
					}
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("DBFile");
					xmlTextWriter.WriteStartElement("Collect");
					xmlTextWriter.WriteString(config.HasDBFile.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("Elapsed");
					xmlTextWriter.WriteString(config.DBFileElapsed.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("Session");
					xmlTextWriter.WriteStartElement("Collect");
					xmlTextWriter.WriteString(config.HasSession.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("WaitElapsed");
					xmlTextWriter.WriteString(config.WaitElapsed.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("UnusedSessionValue");
					xmlTextWriter.WriteString(config.UnusedSessionValue.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("UnusedSessionElapsed");
					xmlTextWriter.WriteString(config.UnusedSessionElapsed.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("SqlText");
					xmlTextWriter.WriteStartElement("Collect");
					xmlTextWriter.WriteString(config.HasSqlText.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("SqlTextValue");
					xmlTextWriter.WriteString(config.SqlTextValue.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("SqlPlan");
					xmlTextWriter.WriteStartElement("Collect");
					xmlTextWriter.WriteString(config.HasSqlPlan.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("Elapsed");
					xmlTextWriter.WriteString(config.SqlPlanElapsed.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("SqlPlanValue");
					xmlTextWriter.WriteString(config.SqlPlanValue.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("ErrorLog");
					xmlTextWriter.WriteStartElement("Collect");
					xmlTextWriter.WriteString(config.HasErrorLog.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("Elapsed");
					xmlTextWriter.WriteString(config.ErrorLogElapsed.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("KeyItems");
					if (config.ErrorLogKeys != null && config.ErrorLogKeys.Length > 0)
					{
						string[] errorLogKeys = config.ErrorLogKeys;
						for (int i = 0; i < errorLogKeys.Length; i++)
						{
							string text = errorLogKeys[i];
							xmlTextWriter.WriteStartElement("Key");
							xmlTextWriter.WriteString(text);
							xmlTextWriter.WriteEndElement();
						}
					}
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("Moebius");
					xmlTextWriter.WriteString(config.HasMoebius.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("TimeOut");
					xmlTextWriter.WriteString(config.TimeOut.ToString());
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteEndDocument();
				}
				if (config.HasPerfCounter && config.AllPerfList != null)
				{
					this.SavePerfConfig(config.DBInstanceName, config.AllPerfList);
				}
			}
		}

		private void SavePerfConfig(string dbInstanceName, List<Perf> perfList)
		{
			if (perfList != null)
			{
				string filename = Path.Combine(Util.configDir, dbInstanceName + ".config");
				if (!Directory.Exists(Util.configDir))
				{
					Directory.CreateDirectory(Util.configDir);
				}
				using (XmlTextWriter xmlTextWriter = new XmlTextWriter(filename, TextManager.Encoding))
				{
					xmlTextWriter.Formatting = Formatting.Indented;
					xmlTextWriter.WriteStartDocument();
					xmlTextWriter.WriteStartElement("PerfCounter");
					xmlTextWriter.WriteStartElement("DBInstanceName");
					xmlTextWriter.WriteString(dbInstanceName);
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteStartElement("Items");
					if (perfList.Count > 0)
					{
						foreach (Perf current in perfList)
						{
							xmlTextWriter.WriteStartElement("Perf");
							xmlTextWriter.WriteStartElement("ObjectName");
							xmlTextWriter.WriteString(current.ObjectName);
							xmlTextWriter.WriteEndElement();
							xmlTextWriter.WriteStartElement("CounterName");
							xmlTextWriter.WriteString(current.CounterName);
							xmlTextWriter.WriteEndElement();
							xmlTextWriter.WriteStartElement("InstanceName");
							xmlTextWriter.WriteString(current.InstanceName);
							xmlTextWriter.WriteEndElement();
							xmlTextWriter.WriteStartElement("Checked");
							xmlTextWriter.WriteString(current.Checked.ToString());
							xmlTextWriter.WriteEndElement();
							xmlTextWriter.WriteStartElement("HasReView");
							xmlTextWriter.WriteString(current.HasReView.ToString());
							xmlTextWriter.WriteEndElement();
							xmlTextWriter.WriteEndElement();
						}
					}
					xmlTextWriter.WriteEndElement();
					xmlTextWriter.WriteEndElement();
				}
			}
		}

		public CollectConfig LoadConfig()
		{
			string text = Path.Combine(Util.configDir, "Collector.config");
			if (File.Exists(text))
			{
				try
				{
					XmlDocument xmlDocument = new XmlDocument();
					xmlDocument.Load(text);
					CollectConfig collectConfig = new CollectConfig();
					collectConfig.SZCVersion = new Version(xmlDocument.SelectSingleNode("Config/Version").InnerText);
					collectConfig.DBInstanceName = xmlDocument.SelectSingleNode("Config/DBInstanceName").InnerText;
					collectConfig.HasPerfCounter = Convert.ToBoolean(xmlDocument.SelectSingleNode("Config/PerfCounter/Collect").InnerText);
					collectConfig.PerfElapsed = Convert.ToInt32(xmlDocument.SelectSingleNode("Config/PerfCounter/Elapsed").InnerText);
					collectConfig.HasTempdbInfo = Convert.ToBoolean(xmlDocument.SelectSingleNode("Config/TempdbInfo/Collect").InnerText);
					collectConfig.TempdbElapsed = Convert.ToInt32(xmlDocument.SelectSingleNode("Config/TempdbInfo/Elapsed").InnerText);
					collectConfig.HasDBDefine = Convert.ToBoolean(xmlDocument.SelectSingleNode("Config/DBDefine/Collect").InnerText);
					collectConfig.HasCheckTable = Convert.ToBoolean(xmlDocument.SelectSingleNode("Config/DBDefine/CheckTable").InnerText);
					collectConfig.DBItemType = new DBItemType
					{
						HasTable = Convert.ToBoolean(xmlDocument.SelectSingleNode("Config/DBDefine/Table").InnerText),
						HasView = Convert.ToBoolean(xmlDocument.SelectSingleNode("Config/DBDefine/View").InnerText),
						HasStoredProc = Convert.ToBoolean(xmlDocument.SelectSingleNode("Config/DBDefine/StoredProc").InnerText),
						HasFuction = Convert.ToBoolean(xmlDocument.SelectSingleNode("Config/DBDefine/Fuction").InnerText)
					};
					collectConfig.IsDecrypt = Convert.ToBoolean(xmlDocument.SelectSingleNode("Config/DBDefine/IsDecrypt").InnerText);
					collectConfig.DBFilterList = new List<DBFilter>();
					XmlNode xmlNode = xmlDocument.SelectSingleNode("Config/DBDefine/FilterItems");
					if (xmlNode.HasChildNodes && xmlNode.ChildNodes.Count > 0)
					{
						foreach (XmlNode xmlNode2 in xmlNode.ChildNodes)
						{
							string innerText = xmlNode2["DBName"].InnerText;
							int dbType = Convert.ToInt32(xmlNode2["DBType"].InnerText);
							string innerText2 = xmlNode2["FilterString"].InnerText;
							DBFilter item = new DBFilter(collectConfig.DBInstanceName, innerText, dbType, innerText2);
							collectConfig.DBFilterList.Add(item);
						}
					}
					collectConfig.DBInstanceName = xmlDocument.SelectSingleNode("Config/DBFile").InnerText;
					collectConfig.HasDBFile = Convert.ToBoolean(xmlDocument.SelectSingleNode("Config/DBFile/Collect").InnerText);
					collectConfig.DBFileElapsed = Convert.ToInt32(xmlDocument.SelectSingleNode("Config/DBFile/Elapsed").InnerText);
					collectConfig.HasSession = Convert.ToBoolean(xmlDocument.SelectSingleNode("Config/Session/Collect").InnerText);
					collectConfig.WaitElapsed = Convert.ToInt32(xmlDocument.SelectSingleNode("Config/Session/WaitElapsed").InnerText);
					collectConfig.UnusedSessionValue = Convert.ToInt32(xmlDocument.SelectSingleNode("Config/Session/UnusedSessionValue").InnerText);
					collectConfig.UnusedSessionElapsed = Convert.ToInt32(xmlDocument.SelectSingleNode("Config/Session/UnusedSessionElapsed").InnerText);
					collectConfig.HasSqlText = Convert.ToBoolean(xmlDocument.SelectSingleNode("Config/SqlText/Collect").InnerText);
					collectConfig.SqlTextValue = Convert.ToInt32(xmlDocument.SelectSingleNode("Config/SqlText/SqlTextValue").InnerText);
					collectConfig.HasSqlPlan = Convert.ToBoolean(xmlDocument.SelectSingleNode("Config/SqlPlan/Collect").InnerText);
					collectConfig.SqlPlanElapsed = Convert.ToInt32(xmlDocument.SelectSingleNode("Config/SqlPlan/Elapsed").InnerText);
					collectConfig.SqlPlanValue = Convert.ToInt32(xmlDocument.SelectSingleNode("Config/SqlPlan/SqlPlanValue").InnerText);
					collectConfig.HasErrorLog = Convert.ToBoolean(xmlDocument.SelectSingleNode("Config/ErrorLog/Collect").InnerText);
					collectConfig.ErrorLogElapsed = Convert.ToInt32(xmlDocument.SelectSingleNode("Config/ErrorLog/Elapsed").InnerText);
					XmlNode xmlNode3 = xmlDocument.SelectSingleNode("Config/ErrorLog/KeyItems");
					if (xmlNode3.HasChildNodes && xmlNode3.ChildNodes.Count > 0)
					{
						collectConfig.ErrorLogKeys = new string[xmlNode3.ChildNodes.Count];
						int num = 0;
						foreach (XmlNode xmlNode4 in xmlNode3.ChildNodes)
						{
							collectConfig.ErrorLogKeys[num] = xmlNode4.InnerText;
							num++;
						}
					}
					collectConfig.HasMoebius = Convert.ToBoolean(xmlDocument.SelectSingleNode("Config/Moebius").InnerText);
					collectConfig.TimeOut = Convert.ToInt32(xmlDocument.SelectSingleNode("Config/TimeOut").InnerText);
					return collectConfig;
				}
				catch (Exception ex)
				{
					ErrorLog.Write(ex);
				}
			}
			return null;
		}

		private static List<Perf> GetPerfList(string dbInstanceName, out string errMsg)
		{
			string text = Path.Combine(Util.configDir, dbInstanceName + ".config");
			if (File.Exists(text))
			{
				try
				{
					List<Perf> list = new List<Perf>();
					StringBuilder stringBuilder = new StringBuilder();
					XmlDocument xmlDocument = new XmlDocument();
					xmlDocument.Load(text);
					string innerText = xmlDocument.SelectSingleNode("PerfCounter/DBInstanceName").InnerText;
					List<Perf> result;
					if (dbInstanceName != innerText)
					{
						errMsg = string.Empty;
						result = null;
						return result;
					}
					XmlNode xmlNode = xmlDocument.SelectSingleNode("PerfCounter/Items");
					if (xmlNode.HasChildNodes && xmlNode.ChildNodes.Count > 0)
					{
						foreach (XmlNode xmlNode2 in xmlNode.ChildNodes)
						{
							string innerText2 = xmlNode2["ObjectName"].InnerText;
							string innerText3 = xmlNode2["CounterName"].InnerText;
							string innerText4 = xmlNode2["InstanceName"].InnerText;
							bool @checked = Convert.ToBoolean(xmlNode2["Checked"].InnerText);
							bool hasReView = Convert.ToBoolean(xmlNode2["HasReView"].InnerText);
							Perf perf = new Perf(innerText2, innerText3, innerText4);
							perf.Checked = @checked;
							perf.HasReView = hasReView;
							list.Add(perf);
							if (perf.HasError && !perf.HasReView)
							{
								if (string.IsNullOrEmpty(innerText4))
								{
									stringBuilder.AppendLine(string.Format("{0}\\{1}", innerText2, innerText3));
								}
								else
								{
									stringBuilder.AppendLine(string.Format("{0}({1})\\{2}", innerText2, innerText4, innerText3));
								}
							}
						}
					}
					errMsg = stringBuilder.ToString();
					result = list;
					return result;
				}
				catch (Exception ex)
				{
					ErrorLog.Write(ex);
					errMsg = string.Empty;
					List<Perf> result = null;
					return result;
				}
			}
			errMsg = string.Empty;
			return null;
		}

		public void ClearConfig()
		{
			if (Directory.Exists(Util.configDir))
			{
				try
				{
					DirectoryInfo directoryInfo = new DirectoryInfo(Util.configDir);
					FileInfo[] files = directoryInfo.GetFiles("*.config");
					if (files.Length > 0)
					{
						FileInfo[] array = files;
						for (int i = 0; i < array.Length; i++)
						{
							FileInfo fileInfo = array[i];
							fileInfo.Delete();
						}
					}
				}
				catch (Exception ex)
				{
					ErrorLog.Write(ex);
				}
			}
		}
	}
}
