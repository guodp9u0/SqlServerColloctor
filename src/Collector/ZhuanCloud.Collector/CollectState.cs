using System;

namespace ZhuanCloud.Collector
{
	public class CollectState
	{
		public int PerfFaild
		{
			get;
			set;
		}

		public int PerfSum
		{
			get;
			set;
		}

		public int TempdbFaild
		{
			get;
			set;
		}

		public int TempdbSum
		{
			get;
			set;
		}

		public int WaitFaild
		{
			get;
			set;
		}

		public int WaitSum
		{
			get;
			set;
		}

		public int DBFileFaild
		{
			get;
			set;
		}

		public int DBFileSum
		{
			get;
			set;
		}

		public int UnusedSessionSum
		{
			get;
			set;
		}

		public int UnusedSessionFaild
		{
			get;
			set;
		}

		public int SqlTextFaild
		{
			get;
			set;
		}

		public int SqlTextSum
		{
			get;
			set;
		}

		public int SqlPlanFaild
		{
			get;
			set;
		}

		public int MoebiusTraceSum
		{
			get;
			set;
		}

		public int MoebiusTraceFaild
		{
			get;
			set;
		}

		public int SqlPlanSum
		{
			get;
			set;
		}

		public int ErrLogFaild
		{
			get;
			set;
		}

		public int ErrLogSum
		{
			get;
			set;
		}
	}
}
