using System;

namespace ZhuanCloud.Collector
{
	public class WaitItem
	{
		public int SessionId
		{
			get;
			set;
		}

		public int BlockSessionId
		{
			get;
			set;
		}

		public int RequestId
		{
			get;
			set;
		}

		public string DBName
		{
			get;
			set;
		}
	}
}
