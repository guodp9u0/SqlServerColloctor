using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace ZhuanCloud.Collector
{
	public class UnusedSessionCollector : OleDbCollector
	{
		public bool SaveData(string oleDBString, string sqlString, int timeOut, int duration)
		{
			string commandText = string.Format("\r\nSELECT  s.session_id ,\r\n        t.transaction_id ,\r\n        a.transaction_begin_time ,\r\n        s.login_time ,\r\n        s.last_request_start_time ,\r\n        s.last_request_end_time ,\r\n        GETDATE() AS collect_time ,\r\n        last_individual_query = qt.text ,\r\n        s.login_name ,\r\n        s.[host_name] ,\r\n        s.[program_name] ,\r\n        s.host_process_id ,\r\n        s.client_interface_name ,        \r\n        s.transaction_isolation_level\r\nFROM    sys.dm_exec_sessions s\r\n        LEFT JOIN sys.dm_tran_session_transactions (NOLOCK) t ON s.session_id = t.session_id\r\n        LEFT JOIN sys.dm_tran_active_transactions (NOLOCK) a ON t.transaction_id = a.transaction_id\r\n        LEFT JOIN sys.dm_exec_connections (NOLOCK) ec ON s.session_id = ec.session_id\r\n        CROSS APPLY sys.dm_exec_sql_text(ec.most_recent_sql_handle) AS qt\r\nWHERE   s.session_id > 50\r\n        AND s.[program_name] <> 'ZhuanCloud'\r\n        AND s.status = 'sleeping'\r\n        AND s.last_request_end_time < DATEADD(minute, -{0}, GETDATE())\r\nOPTION (MAXDOP 2)", duration);
			OleDbConnection oleDbConnection = null;
			if (!Util.TryConnect(oleDBString, out oleDbConnection))
			{
				return false;
			}
			try
			{
				MD5CryptoServiceProvider md = new MD5CryptoServiceProvider();
				List<UnusedSessionHash> list = new List<UnusedSessionHash>();
				oleDbConnection.Open();
				SqlConnection sqlConnection = new SqlConnection(sqlString);
				SqlCommand sqlCommand = sqlConnection.CreateCommand();
				sqlCommand.CommandTimeout = timeOut;
				sqlCommand.CommandText = commandText;
				sqlConnection.Open();
				IDataReader dataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
				while (dataReader.Read())
				{
					UnusedSession unusedSession = new UnusedSession();
					unusedSession.SessionID = Convert.ToInt32(dataReader["session_id"]);
					unusedSession.TransactionID = ((dataReader["transaction_id"] == DBNull.Value) ? 0.0 : Convert.ToDouble(dataReader["transaction_id"]));
					unusedSession.TransactionBeginTime = ((dataReader["transaction_begin_time"] == DBNull.Value) ? DateTime.Parse("1900/01/01 00:00:00") : ((DateTime)dataReader["transaction_begin_time"]));
					unusedSession.LoginTime = (DateTime)dataReader["login_time"];
					unusedSession.LastRequestStartTime = (DateTime)dataReader["last_request_start_time"];
					unusedSession.LastRequestEndTime = ((dataReader["last_request_end_time"] == DBNull.Value) ? DateTime.Now : ((DateTime)dataReader["last_request_end_time"]));
					unusedSession.CollectTime = (DateTime)dataReader["collect_time"];
					string text = (dataReader["last_individual_query"] == DBNull.Value) ? string.Empty : dataReader["last_individual_query"].ToString();
					unusedSession.LoginName = dataReader["login_name"].ToString();
					unusedSession.HostName = ((dataReader["host_name"] == DBNull.Value) ? string.Empty : dataReader["host_name"].ToString());
					unusedSession.ProgramName = ((dataReader["program_name"] == DBNull.Value) ? string.Empty : dataReader["program_name"].ToString());
					unusedSession.HostProcessId = ((dataReader["host_process_id"] == DBNull.Value) ? 0 : Convert.ToInt32(dataReader["host_process_id"]));
					unusedSession.ClientInterfaceName = ((dataReader["client_interface_name"] == DBNull.Value) ? string.Empty : dataReader["client_interface_name"].ToString());
					unusedSession.TransactionIsolationLevel = Convert.ToInt32(dataReader["transaction_isolation_level"]);
					string item = string.Format("{0};{1};{2}", unusedSession.SessionID, unusedSession.LastRequestStartTime.ToString("yyyy-MM-dd HH:mm:ss"), unusedSession.LastRequestEndTime.ToString("yyyy-MM-dd HH:mm:ss"));
					string text2 = string.Empty;
					if (!string.IsNullOrEmpty(text))
					{
						text2 = this.GetTextHashCode(md, text);
						if (!Util.unusedHashList.Contains(text2))
						{
							Util.unusedHashList.Add(text2);
							list.Add(new UnusedSessionHash
							{
								LastIndividualQuery = text,
								TextHashCode = text2
							});
						}
					}
					if (Util.unusedKeyList.Contains(item))
					{
						this.UpdateData(oleDbConnection, timeOut, unusedSession);
					}
					else
					{
						Util.unusedKeyList.Add(item);
						unusedSession.TextHashCode = text2;
						this.InsertData(oleDbConnection, timeOut, unusedSession);
					}
				}
				sqlConnection.Close();
				oleDbConnection.Close();
				oleDbConnection.Open();
				if (list.Count > 0)
				{
					foreach (UnusedSessionHash current in list)
					{
						this.InsertSessionHash(oleDbConnection, timeOut, current);
					}
				}
				oleDbConnection.Close();
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				return false;
			}
			finally
			{
				if (oleDbConnection.State != ConnectionState.Closed)
				{
					oleDbConnection.Close();
				}
				oleDbConnection.Dispose();
			}
			return true;
		}

		private string GetTextHashCode(MD5CryptoServiceProvider md5, string lastIndividualQuery)
		{
			byte[] bytes = TextManager.Encoding.GetBytes(lastIndividualQuery);
			byte[] inArray = md5.ComputeHash(bytes);
			return Convert.ToBase64String(inArray);
		}

		private bool InsertData(OleDbConnection con, int timeOut, UnusedSession session)
		{
			string commandText = "\r\nINSERT  INTO tblUnusedSession\r\n        ( SessionID ,\r\n          TransactionID ,\r\n          TransactionBeginTime ,\r\n          LoginTime ,\r\n          LastRequestStartTime ,\r\n          LastRequestEndTime ,\r\n          CollectTime ,\r\n          TextHashCode ,\r\n          LoginName ,\r\n          HostName ,\r\n          ProgramName ,\r\n          HostProcessId ,\r\n          ClientInterfaceName ,\r\n          TransactionIsolationLevel\r\n        )\r\nVALUES  ( @SessionID ,\r\n          @TransactionID ,\r\n          @TransactionBeginTime ,\r\n          @LoginTime ,\r\n          @LastRequestStartTime ,\r\n          @LastRequestEndTime ,\r\n          @CollectTime ,\r\n          @TextHashCode ,\r\n          @LoginName ,\r\n          @HostName ,\r\n          @ProgramName ,\r\n          @HostProcessId ,\r\n          @ClientInterfaceName ,\r\n          @TransactionIsolationLevel\r\n        )\r\n";
			OleDbParameter[] values = new OleDbParameter[]
			{
				new OleDbParameter("@SessionID", OleDbType.Integer)
				{
					Value = session.SessionID
				},
				new OleDbParameter("@TransactionID", OleDbType.Double)
				{
					Value = session.TransactionID
				},
				new OleDbParameter("@TransactionBeginTime", OleDbType.Date)
				{
					Value = session.TransactionBeginTime
				},
				new OleDbParameter("@LoginTime", OleDbType.Date)
				{
					Value = session.LoginTime
				},
				new OleDbParameter("@LastRequestStartTime", OleDbType.Date)
				{
					Value = session.LastRequestStartTime
				},
				new OleDbParameter("@LastRequestEndTime", OleDbType.Date)
				{
					Value = session.LastRequestEndTime
				},
				new OleDbParameter("@CollectTime", OleDbType.Date)
				{
					Value = session.CollectTime
				},
				new OleDbParameter("@TextHashCode", OleDbType.LongVarChar)
				{
					Value = session.TextHashCode
				},
				new OleDbParameter("@LoginName", OleDbType.VarChar)
				{
					Value = session.LoginName
				},
				new OleDbParameter("@HostName", OleDbType.VarChar)
				{
					Value = session.HostName
				},
				new OleDbParameter("@ProgramName", OleDbType.VarChar)
				{
					Value = session.ProgramName
				},
				new OleDbParameter("@HostProcessId", OleDbType.Integer)
				{
					Value = session.HostProcessId
				},
				new OleDbParameter("@ClientInterfaceName", OleDbType.VarChar)
				{
					Value = session.ClientInterfaceName
				},
				new OleDbParameter("@TransactionIsolationLevel", OleDbType.Integer)
				{
					Value = session.TransactionIsolationLevel
				}
			};
			bool result;
			try
			{
				OleDbCommand oleDbCommand = con.CreateCommand();
				oleDbCommand.CommandTimeout = timeOut;
				oleDbCommand.CommandText = commandText;
				oleDbCommand.Parameters.Clear();
				oleDbCommand.Parameters.AddRange(values);
				int num = oleDbCommand.ExecuteNonQuery();
				result = (num > 0);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private bool UpdateData(OleDbConnection con, int timeOut, UnusedSession session)
		{
			string commandText = "\r\n UPDATE tblUnusedSession\r\n SET    CollectTime = @CollectTime         \r\n WHERE  SessionID = @SessionID\r\n        AND LastRequestStartTime = @LastRequestStartTime\r\n        AND LastRequestEndTime = @LastRequestEndTime\r\n";
			OleDbParameter[] values = new OleDbParameter[]
			{
				new OleDbParameter("@CollectTime", OleDbType.Date)
				{
					Value = session.CollectTime
				},
				new OleDbParameter("@SessionID", OleDbType.Integer)
				{
					Value = session.SessionID
				},
				new OleDbParameter("@LastRequestStartTime", OleDbType.Date)
				{
					Value = session.LastRequestStartTime
				},
				new OleDbParameter("@LastRequestEndTime", OleDbType.Date)
				{
					Value = session.LastRequestEndTime
				}
			};
			bool result;
			try
			{
				OleDbCommand oleDbCommand = con.CreateCommand();
				oleDbCommand.CommandTimeout = timeOut;
				oleDbCommand.CommandText = commandText;
				oleDbCommand.Parameters.Clear();
				oleDbCommand.Parameters.AddRange(values);
				int num = oleDbCommand.ExecuteNonQuery();
				result = (num > 0);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private bool InsertSessionHash(OleDbConnection con, int timeOut, UnusedSessionHash hash)
		{
			string commandText = "\r\nINSERT  INTO tblUnusedSessionHash\r\n        ( LastIndividualQuery ,\r\n          TextHashCode\r\n        )\r\nVALUES  ( @LastIndividualQuery ,\r\n          @TextHashCode\r\n        )\r\n";
			OleDbParameter[] values = new OleDbParameter[]
			{
				new OleDbParameter("@LastIndividualQuery", OleDbType.LongVarWChar)
				{
					Value = hash.LastIndividualQuery
				},
				new OleDbParameter("@TextHashCode", OleDbType.VarChar)
				{
					Value = hash.TextHashCode
				}
			};
			bool result;
			try
			{
				OleDbCommand oleDbCommand = con.CreateCommand();
				oleDbCommand.CommandTimeout = timeOut;
				oleDbCommand.CommandText = commandText;
				oleDbCommand.Parameters.Clear();
				oleDbCommand.Parameters.AddRange(values);
				int num = oleDbCommand.ExecuteNonQuery();
				result = (num > 0);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}
	}
}
