using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text;

namespace ZhuanCloud.Collector
{
	public class CheckTableCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, int timeOut, string condition)
		{
			List<CheckTableItem> checkTableList = this.GetCheckTableList(sqlString, timeOut, condition);
			if (checkTableList.Count > 0)
			{
				SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder(sqlString);
				string initialCatalog = sqlConnectionStringBuilder.InitialCatalog;
				SqlConnection sqlCon = new SqlConnection(sqlString);
				string cmdText = "\r\nINSERT  INTO tblCheckDetail\r\n        ( DBName ,\r\n          [ObjectID] ,\r\n          [Error] ,\r\n          [Level] ,\r\n          [State] ,\r\n          MessageText ,\r\n          RepairLevel ,\r\n          [Status] ,\r\n          IndexId ,\r\n          PartitionId ,\r\n          AllocUnitId ,\r\n          [File] ,\r\n          [Page] ,\r\n          Slot ,\r\n          RefFile ,\r\n          RefPage ,\r\n          RefSlot ,\r\n          Allocation\r\n        )\r\nVALUES  ( @DBName ,\r\n          @ObjectID ,\r\n          @Error ,\r\n          @Level ,\r\n          @State ,\r\n          @MessageText ,\r\n          @RepairLevel ,\r\n          @Status ,\r\n          @IndexId ,\r\n          @PartitionId ,\r\n          @AllocUnitId ,\r\n          @File ,\r\n          @Page ,\r\n          @Slot ,\r\n          @RefFile ,\r\n          @RefPage ,\r\n          @RefSlot ,\r\n          @Allocation\r\n        )\r\n";
				foreach (CheckTableItem current in checkTableList)
				{
					DataTable data = this.GetData(sqlCon, current.SchemeName, current.TableName, timeOut);
					if (data.Rows.Count > 0)
					{
						bool flag = true;
						int count = data.Rows.Count;
						foreach (DataRow dataRow in data.Rows)
						{
							int num = -1;
							if (int.TryParse(dataRow["Error"].ToString(), out num) && num != 2593)
							{
								flag = false;
								break;
							}
						}
						if (!flag)
						{
							OleDbParameter[] parameters = new OleDbParameter[]
							{
								new OleDbParameter("@DBName", OleDbType.VarChar)
								{
									Value = initialCatalog
								},
								new OleDbParameter("@ObjectID", OleDbType.BigInt)
								{
									Value = current.ObjectID
								},
								new OleDbParameter("@Error", OleDbType.VarChar)
								{
									SourceColumn = "Error"
								},
								new OleDbParameter("@Level", OleDbType.Integer)
								{
									SourceColumn = "Level"
								},
								new OleDbParameter("@State", OleDbType.Integer)
								{
									SourceColumn = "State"
								},
								new OleDbParameter("@MessageText", OleDbType.VarChar)
								{
									SourceColumn = "MessageText"
								},
								new OleDbParameter("@RepairLevel", OleDbType.Integer)
								{
									SourceColumn = "RepairLevel"
								},
								new OleDbParameter("@Status", OleDbType.Integer)
								{
									SourceColumn = "Status"
								},
								new OleDbParameter("@IndexId", OleDbType.Integer)
								{
									SourceColumn = "IndexId"
								},
								new OleDbParameter("@PartitionId", OleDbType.Integer)
								{
									SourceColumn = "PartitionId"
								},
								new OleDbParameter("@AllocUnitId", OleDbType.Double)
								{
									SourceColumn = "AllocUnitId"
								},
								new OleDbParameter("@File", OleDbType.Integer)
								{
									SourceColumn = "File"
								},
								new OleDbParameter("@Page", OleDbType.Integer)
								{
									SourceColumn = "Page"
								},
								new OleDbParameter("@Slot", OleDbType.Integer)
								{
									SourceColumn = "Slot"
								},
								new OleDbParameter("@RefFile", OleDbType.Integer)
								{
									SourceColumn = "RefFile"
								},
								new OleDbParameter("@RefPage", OleDbType.Integer)
								{
									SourceColumn = "RefPage"
								},
								new OleDbParameter("@RefSlot", OleDbType.Integer)
								{
									SourceColumn = "RefSlot"
								},
								new OleDbParameter("@Allocation", OleDbType.Integer)
								{
									SourceColumn = "Allocation"
								}
							};
							if (!base.SaveToAccess(oleDBString, data, cmdText, parameters))
							{
								bool result = false;
								return result;
							}
						}
						string cmdText2 = "INSERT INTO tblCheck(DBName,ObjectID,SchemeName,TableName,IsIntact,ErrorCount) VALUES (@DBName,@ObjectID,@SchemeName,@TableName,@IsIntact,@ErrorCount)";
						OleDbParameter[] parameters2 = new OleDbParameter[]
						{
							new OleDbParameter("@DBName", OleDbType.VarChar)
							{
								Value = initialCatalog
							},
							new OleDbParameter("@ObjectID", OleDbType.BigInt)
							{
								Value = current.ObjectID
							},
							new OleDbParameter("@SchemeName", OleDbType.VarChar)
							{
								Value = current.SchemeName
							},
							new OleDbParameter("@TableName", OleDbType.VarChar)
							{
								Value = current.TableName
							},
							new OleDbParameter("@IsIntact", OleDbType.Boolean)
							{
								Value = flag
							},
							new OleDbParameter("@ErrorCount", OleDbType.Integer)
							{
								Value = (flag ? 0 : count)
							}
						};
						if (!base.SaveToAccess(oleDBString, cmdText2, timeOut, parameters2))
						{
							bool result = false;
							return result;
						}
					}
				}
				return true;
			}
			return true;
		}

		private List<CheckTableItem> GetCheckTableList(string sqlString, int timeOut, string condition)
		{
			List<CheckTableItem> list = new List<CheckTableItem>();
			if (!string.IsNullOrEmpty(condition))
			{
				string[] array = condition.Split(new string[]
				{
					";"
				}, StringSplitOptions.RemoveEmptyEntries);
				if (array.Length > 0)
				{
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.Append(" AND ");
					int num = 1;
					string[] array2 = array;
					for (int i = 0; i < array2.Length; i++)
					{
						string text = array2[i];
						string[] array3 = text.Split(new string[]
						{
							"."
						}, StringSplitOptions.RemoveEmptyEntries);
						if (array3.Length == 1)
						{
							stringBuilder.AppendFormat("  t.name NOT LIKE '{0}' ", array3[0]);
						}
						else if (array3.Length == 2)
						{
							stringBuilder.AppendFormat(" s.name NOT LIKE '{0}' OR t.name NOT LIKE '{1}' ", array3[0], array3[1]);
						}
						if (num < array.Length)
						{
							stringBuilder.Append(" AND ");
						}
						num++;
					}
					condition = stringBuilder.ToString();
				}
			}
			string text2 = string.Format("\r\nSELECT  t.[object_id] ,\r\n        s.name AS schemeName ,\r\n        t.name AS tblName\r\nFROM    sys.tables t ,\r\n        sys.schemas s\r\nWHERE   t.is_ms_shipped = 0\r\n        AND t.schema_id = s.schema_id\r\n        {0}\r\n", condition);
			try
			{
				using (SqlConnection sqlConnection = new SqlConnection(sqlString))
				{
					SqlCommand sqlCommand = sqlConnection.CreateCommand();
					sqlCommand.CommandTimeout = timeOut;
					sqlCommand.CommandText = text2;
					sqlConnection.Open();
					SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
					while (sqlDataReader.Read())
					{
						list.Add(new CheckTableItem
						{
							ObjectID = Convert.ToInt32(sqlDataReader["object_id"]),
							SchemeName = sqlDataReader["schemeName"].ToString(),
							TableName = sqlDataReader["tblName"].ToString()
						});
					}
					sqlConnection.Close();
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(new Exception(text2));
				ErrorLog.Write(ex);
			}
			return list;
		}

		private DataTable GetData(SqlConnection sqlCon, string schemeName, string tblName, int timeOut)
		{
			string text = string.Format("EXEC('DBCC CHECKTABLE ('+'''[{0}].[{1}]'''+') WITH TableResults,TABLOCK')", schemeName, tblName);
			DataTable dataTable = new DataTable();
			try
			{
				SqlCommand sqlCommand = sqlCon.CreateCommand();
				sqlCommand.CommandTimeout = timeOut;
				sqlCommand.CommandText = text;
				SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
				sqlDataAdapter.Fill(dataTable);
				if (dataTable.Rows.Count > 0)
				{
					foreach (DataRow dataRow in dataTable.Rows)
					{
						dataRow.SetAdded();
					}
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(new Exception(text));
				ErrorLog.Write(ex);
			}
			return dataTable;
		}
	}
}
