using System;
using System.Diagnostics;

namespace ZhuanCloud.Collector
{
	public class Perf
	{
		public int ID
		{
			get;
			set;
		}

		public string ObjectName
		{
			get;
			set;
		}

		public string CounterName
		{
			get;
			set;
		}

		public string InstanceName
		{
			get;
			set;
		}

		public PerformanceCounter PerfCounter
		{
			get;
			set;
		}

		public bool HasError
		{
			get;
			set;
		}

		public string Status
		{
			get;
			set;
		}

		public bool Checked
		{
			get;
			set;
		}

		public bool HasReView
		{
			get;
			set;
		}

		public Perf(string objectName, string counterName, string instanceName)
		{
			this.ObjectName = objectName;
			this.CounterName = counterName;
			this.InstanceName = instanceName;
			this.HasError = false;
			this.Status = "正常";
			try
			{
				if (string.IsNullOrEmpty(instanceName))
				{
					this.PerfCounter = new PerformanceCounter(objectName, counterName);
					this.PerfCounter.NextValue();
				}
				else
				{
					this.PerfCounter = new PerformanceCounter(objectName, counterName, instanceName);
					this.PerfCounter.NextValue();
				}
				this.Checked = true;
			}
			catch (Exception ex)
			{
				this.HasError = true;
				this.Checked = false;
				this.Status = ex.Message;
				ErrorLog.Write(new Exception(string.Format("获取计数器值错误，CategoryName:{0},CounterName:{1},InstanceName:{2}", objectName, counterName, instanceName), ex));
			}
		}
	}
}
