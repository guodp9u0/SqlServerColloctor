using System;
using System.Data;
using System.Data.OleDb;

namespace ZhuanCloud.Collector
{
	public class BackupCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, int timeOut)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(sqlString, timeOut);
				if (data.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblBackup\r\n        ( DBName ,\r\n          BackupName ,\r\n          BackupFileSize ,\r\n          RecoveryModel ,\r\n          BackupStartDate ,\r\n          BackupFinishDate ,\r\n          CollectDate ,\r\n          BackupType ,\r\n          FirstLsn ,\r\n          LastLsn ,\r\n          BackupPath \r\n        )\r\nVALUES  ( @DBName ,\r\n          @BackupName ,\r\n          @BackupFileSize ,\r\n          @RecoveryModel ,\r\n          @BackupStartDate ,\r\n          @BackupFinishDate ,\r\n          @CollectDate ,\r\n          @BackupType ,\r\n          @FirstLsn ,\r\n          @LastLsn ,\r\n          @BackupPath \r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@DBName", OleDbType.VarChar)
						{
							SourceColumn = "database_name"
						},
						new OleDbParameter("@BackupName", OleDbType.VarChar)
						{
							SourceColumn = "backup_name"
						},
						new OleDbParameter("@BackupFileSize", OleDbType.Double)
						{
							SourceColumn = "backup_size_mb"
						},
						new OleDbParameter("@RecoveryModel", OleDbType.VarChar)
						{
							SourceColumn = "recovery_model"
						},
						new OleDbParameter("@BackupStartDate", OleDbType.Date)
						{
							SourceColumn = "backup_start_date"
						},
						new OleDbParameter("@BackupFinishDate", OleDbType.Date)
						{
							SourceColumn = "backup_finish_date"
						},
						new OleDbParameter("@CollectDate", OleDbType.Date)
						{
							SourceColumn = "collect_date"
						},
						new OleDbParameter("@BackupType", OleDbType.Char)
						{
							SourceColumn = "backup_type"
						},
						new OleDbParameter("@FirstLsn", OleDbType.VarChar)
						{
							SourceColumn = "first_lsn"
						},
						new OleDbParameter("@LastLsn", OleDbType.VarChar)
						{
							SourceColumn = "last_lsn"
						},
						new OleDbParameter("@BackupPath", OleDbType.VarChar)
						{
							SourceColumn = "backup_path"
						}
					};
					result = base.SaveToAccess(oleDBString, data, cmdText, parameters);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, int timeOut)
		{
			string cmdText = "\r\nDECLARE @last_backup_date DATETIME\r\nSELECT TOP 2\r\n        @last_backup_date = backup_finish_date\r\nFROM    msdb.dbo.backupset\r\nWHERE   [type] = 'D' AND database_name = DB_NAME(DB_ID())\r\nORDER BY backup_finish_date DESC\r\nPRINT @last_backup_date\r\nIF @last_backup_date IS NOT NULL \r\n    BEGIN\r\n        SELECT  bs.database_name ,\r\n                bs.name AS backup_name ,\r\n                ROUND(( CONVERT(FLOAT, backup_size) / 1024.0 / 1024 ), 2) AS backup_size_mb ,\r\n                recovery_model ,\r\n                backup_start_date ,\r\n                backup_finish_date ,\r\n                GETDATE() AS collect_date ,\r\n                bs.[type] AS backup_type ,\r\n                first_lsn ,\r\n                last_lsn ,\r\n                bd.physical_device_name AS backup_path\r\n        FROM    msdb.dbo.backupset bs ,\r\n                msdb.dbo.backupmediafamily bd\r\n        WHERE   bs.media_set_id = bd.media_set_id\r\n                AND bs.database_name = DB_NAME(DB_ID())\r\n                AND bs.database_name IS NOT NULL\r\n                AND bs.backup_finish_date >= @last_backup_date\r\n        ORDER BY backup_finish_date DESC\r\n    END\r\n";
			return base.FillDataTable(sqlString, cmdText, timeOut);
		}
	}
}
