using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace ZhuanCloud.Collector
{
	public class TempDBCollector : OleDbCollector
	{
		public bool SaveData(string oleDBString, string sqlString, int timeOut, DateTime collectTime)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(sqlString, timeOut);
				result = this.SaveTempFile(oleDBString, sqlString, timeOut, data, collectTime);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private bool SaveTempFile(string oleDBString, string sqlString, int timeOut, DataTable tblTemp, DateTime collectTime)
		{
			bool result;
			try
			{
				if (tblTemp.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblTempInfo\r\n        ( LogicName ,\r\n          FileType ,\r\n          FileGroup ,\r\n          DataSize ,\r\n          UsedSize ,\r\n          IsPercentGrowth ,\r\n          Growth ,\r\n          MaxSize ,\r\n          IOStall ,\r\n          IOStallRead ,\r\n          IOStallWrite ,\r\n          ReadKB ,\r\n          WrittenKB ,\r\n          Reads ,\r\n          Writes ,\r\n          PendingCount ,\r\n          DBFilePath ,\r\n          CollectTime\r\n        )\r\nVALUES  ( @LogicName ,\r\n          @FileType ,\r\n          @FileGroup ,\r\n          @DataSize ,\r\n          @UsedSize ,\r\n          @IsPercentGrowth ,\r\n          @Growth ,\r\n          @MaxSize ,\r\n          @IOStall ,\r\n          @IOStallRead ,\r\n          @IOStallWrite ,\r\n          @ReadKB ,\r\n          @WrittenKB ,\r\n          @Reads ,\r\n          @Writes ,\r\n          @PendingCount ,\r\n          @DBFilePath ,\r\n          @CollectTime\r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@LogicName", OleDbType.VarChar)
						{
							SourceColumn = "logic_name"
						},
						new OleDbParameter("@FileType", OleDbType.Integer)
						{
							SourceColumn = "type"
						},
						new OleDbParameter("@FileGroup", OleDbType.VarChar)
						{
							SourceColumn = "file_group"
						},
						new OleDbParameter("@DataSize", OleDbType.Double)
						{
							SourceColumn = "data_size_mb"
						},
						new OleDbParameter("@UsedSize", OleDbType.Double)
						{
							SourceColumn = "used_size_mb"
						},
						new OleDbParameter("@IsPercentGrowth", OleDbType.Boolean)
						{
							SourceColumn = "is_percent_growth"
						},
						new OleDbParameter("@Growth", OleDbType.Double)
						{
							SourceColumn = "growth"
						},
						new OleDbParameter("@MaxSize", OleDbType.Double)
						{
							SourceColumn = "max_size_mb"
						},
						new OleDbParameter("@IOStall", OleDbType.Double)
						{
							SourceColumn = "io_stall"
						},
						new OleDbParameter("@IOStallRead", OleDbType.Double)
						{
							SourceColumn = "io_stall_read_ms"
						},
						new OleDbParameter("@IOStallWrite", OleDbType.Double)
						{
							SourceColumn = "io_stall_write_ms"
						},
						new OleDbParameter("@ReadKB", OleDbType.Double)
						{
							SourceColumn = "read_kb"
						},
						new OleDbParameter("@WrittenKB", OleDbType.Double)
						{
							SourceColumn = "written_kb"
						},
						new OleDbParameter("@Reads", OleDbType.Double)
						{
							SourceColumn = "num_of_reads"
						},
						new OleDbParameter("@Writes", OleDbType.Double)
						{
							SourceColumn = "num_of_writes"
						},
						new OleDbParameter("@PendingCount", OleDbType.Double)
						{
							SourceColumn = "pending_count"
						},
						new OleDbParameter("@DBFilePath", OleDbType.VarChar)
						{
							SourceColumn = "physical_name"
						},
						new OleDbParameter("@CollectTime", OleDbType.Date)
						{
							Value = DateTime.Parse(collectTime.ToString("yyyy-MM-dd HH:mm:ss"))
						}
					};
					result = base.SaveToAccess(oleDBString, tblTemp, cmdText, parameters);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, int timeOut)
		{
			string sqlString2 = new SqlConnectionStringBuilder(sqlString)
			{
				InitialCatalog = "tempdb"
			}.ToString();
			string cmdText = "\r\nSELECT  \r\n        df.[name] AS logic_name ,\r\n        df.[type] ,\r\n        ds.name AS file_group ,\r\n        ROUND(( CONVERT(FLOAT, df.size) * ( 8192.0 / 1024.0 ) / 1024 ), 2) AS data_size_mb ,\r\n        CAST(CASE df.type\r\n               WHEN 2 THEN 0\r\n               ELSE ROUND(( CAST(FILEPROPERTY(df.name, 'SpaceUsed') AS FLOAT) * ( 8192.0 / 1024.0 ) / 1024 ), 2) \r\n             END AS FLOAT) AS used_size_mb ,\r\n        df.is_percent_growth ,\r\n        CASE df.is_percent_growth\r\n          WHEN 1 THEN df.growth\r\n          ELSE ROUND(( CONVERT(FLOAT, df.growth) * ( 8192.0 / 1024.0 ) / 1024 ), 2)\r\n        END AS growth ,\r\n        CASE df.max_size\r\n          WHEN -1 THEN -1\r\n          ELSE ROUND(( CONVERT(FLOAT, df.max_size) * ( 8192.0 / 1024.0 ) / 1024 ), 2)\r\n        END AS max_size_mb ,\r\n        vfs.io_stall , \r\n        vfs.io_stall_read_ms ,       \r\n        vfs.io_stall_write_ms ,\r\n        ROUND(( CONVERT(FLOAT, vfs.num_of_bytes_read) / 1024.0 ), 2) AS read_kb ,\r\n        ROUND(( CONVERT(FLOAT, vfs.num_of_bytes_written) / 1024.0 ), 2) AS written_kb ,        \r\n        vfs.num_of_reads ,\r\n        vfs.num_of_writes ,\r\n        ISNULL(db_pending.pending_count,0) AS pending_count,\r\n        df.physical_name\r\nFROM    sys.database_files df\r\n        LEFT JOIN sys.data_spaces ds ON df.data_space_id = ds.data_space_id\r\n        LEFT JOIN sys.dm_io_virtual_file_stats(NULL, NULL) AS vfs\r\n        ON vfs.database_id = DB_ID() AND vfs.file_id = df.file_id\r\n\t\tLEFT JOIN\r\n\t\t(\r\n            SELECT  database_id ,\r\n                    file_id ,\r\n                    COUNT(io_pending) pending_count\r\n            FROM    sys.dm_io_virtual_file_stats(NULL, NULL) t1 ,\r\n                    sys.dm_io_pending_io_requests AS t2\r\n            WHERE   t1.file_handle = t2.io_handle\r\n            GROUP BY database_id ,\r\n                    file_id\r\n\t\t) AS db_pending ON db_pending.database_id = DB_ID() AND db_pending.file_id = df.file_id\r\n";
			return base.FillDataTable(sqlString2, cmdText, timeOut);
		}

		public bool SaveTempSpace(string oleDBString, string sqlString, int timeOut, DateTime collectTime)
		{
			bool result;
			try
			{
				DataSet tempSpaceData = this.GetTempSpaceData(sqlString, timeOut);
				if (tempSpaceData.Tables.Contains("tempdb"))
				{
					DataTable dataTable = tempSpaceData.Tables["tempdb"];
					if (dataTable.Rows.Count > 0 && !this.SaveTempFile(oleDBString, sqlString, timeOut, dataTable, collectTime))
					{
						result = false;
						return result;
					}
				}
				if (tempSpaceData.Tables.Contains("tblTempdbSpace"))
				{
					DataTable dataTable2 = tempSpaceData.Tables["tblTempdbSpace"];
					if (dataTable2.Rows.Count > 0)
					{
						string cmdText = "\r\nINSERT  INTO tblTempdbSpace\r\n        ( CollectTime ,\r\n          UserObjectReserved_KB ,\r\n          InternalObjectReserved_KB ,\r\n          VersionStoreReserved_KB ,\r\n          UnallocatedExtent_KB ,\r\n          MixedExtent_KB\r\n        )\r\nVALUES  ( @CollectTime ,\r\n          @UserObjectReserved_KB ,\r\n          @InternalObjectReserved_KB ,\r\n          @VersionStoreReserved_KB ,\r\n          @UnallocatedExtent_KB ,\r\n          @MixedExtent_KB\r\n        )\r\n";
						OleDbParameter[] parameters = new OleDbParameter[]
						{
							new OleDbParameter("@CollectTime", OleDbType.Date)
							{
								Value = DateTime.Parse(collectTime.ToString("yyyy-MM-dd HH:mm:ss"))
							},
							new OleDbParameter("@UserObjectReserved_KB", OleDbType.Double)
							{
								SourceColumn = "user_object_reserved_kb"
							},
							new OleDbParameter("@InternalObjectReserved_KB", OleDbType.Double)
							{
								SourceColumn = "internal_object_reserved_kb"
							},
							new OleDbParameter("@VersionStoreReserved_KB", OleDbType.Double)
							{
								SourceColumn = "version_store_reserved_kb"
							},
							new OleDbParameter("@UnallocatedExtent_KB", OleDbType.Double)
							{
								SourceColumn = "unallocated_extent_kb"
							},
							new OleDbParameter("@MixedExtent_KB", OleDbType.Double)
							{
								SourceColumn = "mixed_extent_kb"
							}
						};
						if (!base.SaveToAccess(oleDBString, dataTable2, cmdText, parameters))
						{
							result = false;
							return result;
						}
					}
				}
				result = true;
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataSet GetTempSpaceData(string sqlString, int timeOut)
		{
			DataSet dataSet = new DataSet();
			DataTable data = this.GetData(sqlString, timeOut);
			data.TableName = "tempdb";
			dataSet.Tables.Add(data);
			string cmdText = "\r\nSELECT  SUM(user_object_reserved_page_count) * 8 AS user_object_reserved_kb ,\r\n        SUM(internal_object_reserved_page_count) * 8 AS internal_object_reserved_kb ,\r\n        SUM(version_store_reserved_page_count) * 8 AS version_store_reserved_kb ,\r\n        SUM(unallocated_extent_page_count) * 8 AS unallocated_extent_kb ,\r\n        SUM(mixed_extent_page_count) * 8 AS mixed_extent_kb\r\nFROM    tempdb.sys.dm_db_file_space_usage\r\n";
			DataTable dataTable = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable.TableName = "tblTempdbSpace";
			dataSet.Tables.Add(dataTable);
			return dataSet;
		}
	}
}
