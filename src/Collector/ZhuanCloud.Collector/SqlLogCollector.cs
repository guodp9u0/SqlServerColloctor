using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace ZhuanCloud.Collector
{
	public class SqlLogCollector : OleDbCollector
	{
		public bool SaveData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut, string[] filterArrary, DateTime lastLogTime, ref DateTime newLogTime)
		{
			if (filterArrary.Length > 0)
			{
				List<string> list = new List<string>();
				for (int i = 0; i < filterArrary.Length; i++)
				{
					string text = filterArrary[i];
					if (!Util.errDic.ContainsKey(text))
					{
						Util.errDic.Add(text, 0);
					}
					if (!list.Contains(text))
					{
						list.Add(text);
					}
				}
				string cmdText = "\r\nCREATE TABLE #Enum_Err\r\n(\r\n\tFileId INT,\r\n\tSaveDate DATETIME,\r\n\tFileSize BIGINT\r\n)\r\nINSERT INTO #Enum_Err\r\nEXEC xp_enumerrorlogs 1\r\nSELECT MAX(FileId) max_file_id FROM #Enum_Err\r\nDROP TABLE #Enum_Err\r\n";
				int num = -1;
				try
				{
					object obj = base.SQLExecuteScalar(sqlString, cmdText, timeOut);
					if (obj != null)
					{
						num = Convert.ToInt32(obj);
					}
				}
				catch (Exception ex)
				{
					ErrorLog.Write(ex);
					bool result = false;
					return result;
				}
				if (num < 0)
				{
					return true;
				}
				string text2 = string.Empty;
				string arg = string.Empty;
				DateTime now = DateTime.Now;
				newLogTime = now;
				if (lastLogTime != DateTime.Parse("1900/01/01 00:00:00"))
				{
					if (sqlVersion >= SQLServerVersion.SQLServer2008)
					{
						text2 = lastLogTime.ToString("yyyy/MM/dd HH:mm:ss");
						arg = now.ToString("yyyy/MM/dd HH:mm:ss");
					}
					else
					{
						text2 = lastLogTime.ToString("yyyy-MM-dd HH:mm");
						arg = now.ToString("yyyy/MM/dd HH:mm");
					}
				}
				OleDbConnection oleDbConnection = null;
				if (!Util.TryConnect(oleDBString, out oleDbConnection))
				{
					return false;
				}
				try
				{
					SqlConnection sqlConnection = new SqlConnection(sqlString);
					OleDbCommand oleDbCommand = oleDbConnection.CreateCommand();
					oleDbConnection.Open();
					bool result;
					for (int j = 0; j <= num; j++)
					{
						string commandText = string.Empty;
						if (string.IsNullOrEmpty(text2))
						{
							commandText = string.Format("EXEC xp_readerrorlog {0},1,NULL,NULL,NULL,NULL,'ASC'", j);
						}
						else
						{
							commandText = string.Format("EXEC xp_readerrorlog {0},1,NULL,NULL,'{1}','{2}','ASC'", j, text2, arg);
						}
						SqlCommand sqlCommand = sqlConnection.CreateCommand();
						sqlCommand.CommandTimeout = timeOut;
						sqlCommand.CommandText = commandText;
						sqlConnection.Open();
						SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
						while (sqlDataReader.Read())
						{
							string text3 = sqlDataReader[2].ToString();
							DateTime dateTime = (DateTime)sqlDataReader[0];
							string value = sqlDataReader[1].ToString();
							foreach (string current in list)
							{
								if (text3.Contains(current))
								{
									Dictionary<string, int> errDic;
									string key;
									(errDic = Util.errDic)[key = current] = errDic[key] + 1;
									if (Util.errDic[current] > 1000)
									{
										break;
									}
									string commandText2 = "INSERT  INTO tblSqlLog(KeyWord ,LogDate ,ProcessInfo ,ErrorInfo) VALUES (@KeyWord ,@LogDate ,@ProcessInfo ,@ErrorInfo)";
									OleDbParameter[] values = new OleDbParameter[]
									{
										new OleDbParameter("@KeyWord", OleDbType.VarChar)
										{
											Value = current
										},
										new OleDbParameter("@LogDate", OleDbType.Date)
										{
											Value = dateTime
										},
										new OleDbParameter("@ProcessInfo", OleDbType.VarChar)
										{
											Value = value
										},
										new OleDbParameter("@ErrorInfo", OleDbType.VarChar)
										{
											Value = text3
										}
									};
									oleDbCommand.CommandTimeout = timeOut;
									oleDbCommand.CommandText = commandText2;
									oleDbCommand.Parameters.Clear();
									oleDbCommand.Parameters.AddRange(values);
									if (oleDbCommand.ExecuteNonQuery() == 0)
									{
										result = false;
										return result;
									}
									if (!Util.startFlag)
									{
										result = false;
										return result;
									}
									break;
								}
							}
						}
						sqlConnection.Close();
					}
					foreach (KeyValuePair<string, int> current2 in Util.errDic)
					{
						if (current2.Value != 0)
						{
							string commandText3 = "UPDATE tblSqlLogStatics SET ErrorCount=@ErrorCount WHERE KeyWord=@KeyWord";
							OleDbParameter[] values2 = new OleDbParameter[]
							{
								new OleDbParameter("@ErrorCount", OleDbType.Integer)
								{
									Value = current2.Value
								},
								new OleDbParameter("@KeyWord", OleDbType.VarChar)
								{
									Value = current2.Key
								}
							};
							oleDbCommand.CommandTimeout = timeOut;
							oleDbCommand.CommandText = commandText3;
							oleDbCommand.Parameters.Clear();
							oleDbCommand.Parameters.AddRange(values2);
							if (oleDbCommand.ExecuteNonQuery() == 0)
							{
								result = false;
								return result;
							}
							if (!Util.startFlag)
							{
								result = false;
								return result;
							}
						}
					}
					oleDbConnection.Close();
					result = true;
					return result;
				}
				catch (Exception ex2)
				{
					ErrorLog.Write(ex2);
					bool result = false;
					return result;
				}
				finally
				{
					if (oleDbConnection.State != ConnectionState.Closed)
					{
						oleDbConnection.Close();
					}
					oleDbConnection.Dispose();
				}
				return true;
			}
			return true;
		}
	}
}
