using System;
using System.Data;
using System.Data.OleDb;

namespace ZhuanCloud.Collector
{
	public class SessionCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, int timeOut)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(sqlString, timeOut);
				if (data.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblSession\r\n        ( LoginName ,\r\n          HostName ,\r\n          ProgramName ,\r\n          HostProcessId ,\r\n          ClientInterfaceName ,\r\n          NTDomain ,\r\n          TransactionIsolationLevel ,\r\n          SessionCount\r\n        )\r\nVALUES  ( @LoginName ,\r\n          @HostName ,\r\n          @ProgramName ,\r\n          @HostProcessId ,\r\n          @ClientInterfaceName ,\r\n          @NTDomain ,\r\n          @TransactionIsolationLevel ,\r\n          @SessionCount\r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@LoginName", OleDbType.VarChar)
						{
							SourceColumn = "login_name"
						},
						new OleDbParameter("@HostName", OleDbType.VarChar)
						{
							SourceColumn = "host_name"
						},
						new OleDbParameter("@ProgramName", OleDbType.VarChar)
						{
							SourceColumn = "program_name"
						},
						new OleDbParameter("@HostProcessId", OleDbType.Integer)
						{
							SourceColumn = "host_process_id"
						},
						new OleDbParameter("@ClientInterfaceName", OleDbType.VarChar)
						{
							SourceColumn = "client_interface_name"
						},
						new OleDbParameter("@NTDomain", OleDbType.VarChar)
						{
							SourceColumn = "nt_domain"
						},
						new OleDbParameter("@TransactionIsolationLevel", OleDbType.Integer)
						{
							SourceColumn = "transaction_isolation_level"
						},
						new OleDbParameter("@SessionCount", OleDbType.Integer)
						{
							SourceColumn = "session_count"
						}
					};
					result = base.SaveToAccess(oleDBString, data, cmdText, parameters);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, int timeOut)
		{
			string cmdText = string.Empty;
			cmdText = "\r\nSELECT  s.login_name ,\r\n        s.[host_name] ,\r\n        s.[program_name] ,\r\n        s.host_process_id ,\r\n        s.client_interface_name ,\r\n        s.nt_domain ,\r\n        s.transaction_isolation_level ,\r\n        COUNT(s.session_id) AS session_count\r\nFROM    sys.dm_exec_sessions s\r\nWHERE   s.session_id > 50\r\n        AND [program_name] <> 'ZhuanCloud'\r\nGROUP BY s.login_name ,\r\n        s.[host_name] ,\r\n        s.[program_name] ,\r\n        s.host_process_id ,\r\n        s.client_interface_name ,\r\n        s.nt_domain ,\r\n        s.transaction_isolation_level\r\n";
			return base.FillDataTable(sqlString, cmdText, timeOut);
		}
	}
}
