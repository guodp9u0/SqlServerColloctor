using System;
using System.Data;
using System.Data.OleDb;

namespace ZhuanCloud.Collector
{
	public class JobCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, int timeOut)
		{
			bool result;
			try
			{
				DataSet data = this.GetData(sqlString, timeOut);
				if (data.Tables.Contains("tblJobDefine"))
				{
					DataTable dataTable = data.Tables["tblJobDefine"];
					if (dataTable.Rows.Count > 0)
					{
						string cmdText = "\r\nINSERT  INTO tblJobDefine\r\n        ( JobId ,\r\n          JobName ,\r\n          JobDescription ,\r\n          StartStepId ,\r\n          CreateDate ,\r\n          ModifyDate ,\r\n          Enabled\r\n        )\r\nVALUES  ( @JobId ,\r\n          @JobName ,\r\n          @JobDescription ,\r\n          @StartStepId ,\r\n          @CreateDate ,\r\n          @ModifyDate ,\r\n          @Enabled\r\n        )\r\n";
						OleDbParameter[] parameters = new OleDbParameter[]
						{
							new OleDbParameter("@JobId", OleDbType.VarChar)
							{
								SourceColumn = "job_id"
							},
							new OleDbParameter("@JobName", OleDbType.VarChar)
							{
								SourceColumn = "job_name"
							},
							new OleDbParameter("@JobDescription", OleDbType.LongVarWChar)
							{
								SourceColumn = "job_description"
							},
							new OleDbParameter("@StartStepId", OleDbType.Integer)
							{
								SourceColumn = "start_step_id"
							},
							new OleDbParameter("@CreateDate", OleDbType.Date)
							{
								SourceColumn = "date_created"
							},
							new OleDbParameter("@ModifyDate", OleDbType.Date)
							{
								SourceColumn = "date_modified"
							},
							new OleDbParameter("@Enabled", OleDbType.Boolean)
							{
								SourceColumn = "job_enabled"
							}
						};
						if (!base.SaveToAccess(oleDBString, dataTable, cmdText, parameters))
						{
							result = false;
							return result;
						}
					}
				}
				if (data.Tables.Contains("tblJobStep"))
				{
					DataTable dataTable2 = data.Tables["tblJobStep"];
					if (dataTable2.Rows.Count > 0)
					{
						string cmdText2 = "\r\nINSERT  INTO tblJobStep\r\n        ( JobId ,\r\n          StepId ,\r\n          StepName ,\r\n          DatabaseName ,\r\n          Command\r\n        )\r\nVALUES  ( @JobId ,\r\n          @StepId ,\r\n          @StepName ,\r\n          @DatabaseName ,\r\n          @Command\r\n        )\r\n";
						OleDbParameter[] parameters2 = new OleDbParameter[]
						{
							new OleDbParameter("@JobId", OleDbType.VarChar)
							{
								SourceColumn = "job_id"
							},
							new OleDbParameter("@StepId", OleDbType.Integer)
							{
								SourceColumn = "step_id"
							},
							new OleDbParameter("@StepName", OleDbType.VarChar)
							{
								SourceColumn = "step_name"
							},
							new OleDbParameter("@DatabaseName", OleDbType.VarChar)
							{
								SourceColumn = "database_name"
							},
							new OleDbParameter("@Command", OleDbType.LongVarWChar)
							{
								SourceColumn = "command"
							}
						};
						if (!base.SaveToAccess(oleDBString, dataTable2, cmdText2, parameters2))
						{
							result = false;
							return result;
						}
					}
				}
				if (data.Tables.Contains("tblJobStep"))
				{
					DataTable dataTable3 = data.Tables["tblJobHistory"];
					if (dataTable3.Rows.Count > 0)
					{
						string cmdText3 = "\r\nINSERT  INTO tblJobHistory\r\n        ( JobName ,\r\n          InstanceId ,\r\n          JobId ,\r\n          StepId ,\r\n          StepName ,\r\n          SqlMessageId ,\r\n          Message ,\r\n          SqlSeverity ,\r\n          RunStatus ,\r\n          RunDate ,\r\n          RunDuration ,\r\n          RetriesAttempted ,\r\n          ServerName ,\r\n          ExecCount\r\n        )\r\nVALUES  ( @JobName ,\r\n          @InstanceId ,\r\n          @JobId ,\r\n          @StepId ,\r\n          @StepName ,\r\n          @SqlMessageId ,\r\n          @Message ,\r\n          @SqlSeverity ,\r\n          @RunStatus ,\r\n          @RunDate ,\r\n          @RunDuration ,\r\n          @RetriesAttempted ,\r\n          @ServerName ,\r\n          @ExecCount\r\n        )\r\n";
						OleDbParameter[] parameters3 = new OleDbParameter[]
						{
							new OleDbParameter("@JobName", OleDbType.VarChar)
							{
								SourceColumn = "job_name"
							},
							new OleDbParameter("@InstanceId", OleDbType.Integer)
							{
								SourceColumn = "instance_id"
							},
							new OleDbParameter("@JobId", OleDbType.VarChar)
							{
								SourceColumn = "job_id"
							},
							new OleDbParameter("@StepId", OleDbType.Integer)
							{
								SourceColumn = "step_id"
							},
							new OleDbParameter("@StepName", OleDbType.VarChar)
							{
								SourceColumn = "step_name"
							},
							new OleDbParameter("@SqlMessageId", OleDbType.Integer)
							{
								SourceColumn = "sql_message_id"
							},
							new OleDbParameter("@Message", OleDbType.LongVarWChar)
							{
								SourceColumn = "message"
							},
							new OleDbParameter("@SqlSeverity", OleDbType.Integer)
							{
								SourceColumn = "sql_severity"
							},
							new OleDbParameter("@RunStatus", OleDbType.Integer)
							{
								SourceColumn = "run_status"
							},
							new OleDbParameter("@RunDate", OleDbType.Date)
							{
								SourceColumn = "run_date"
							},
							new OleDbParameter("@RunDuration", OleDbType.Double)
							{
								SourceColumn = "run_duration"
							},
							new OleDbParameter("@RetriesAttempted", OleDbType.Double)
							{
								SourceColumn = "retries_attempted"
							},
							new OleDbParameter("@ServerName", OleDbType.VarChar)
							{
								SourceColumn = "server"
							},
							new OleDbParameter("@ExecCount", OleDbType.VarChar)
							{
								SourceColumn = "exec_count"
							}
						};
						if (!base.SaveToAccess(oleDBString, dataTable3, cmdText3, parameters3))
						{
							result = false;
							return result;
						}
					}
				}
				result = true;
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataSet GetData(string sqlString, int timeOut)
		{
			DataSet dataSet = new DataSet();
			string cmdText = "\r\nSELECT  CAST(job_id AS VARCHAR(40)) AS job_id ,\r\n        name AS job_name ,\r\n        [description] AS job_description ,\r\n        start_step_id ,\r\n        date_created ,\r\n        date_modified ,\r\n        [enabled] AS job_enabled\r\nFROM    msdb.dbo.sysjobs\r\nORDER BY name\r\n";
			DataTable dataTable = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable.TableName = "tblJobDefine";
			dataSet.Tables.Add(dataTable);
			string cmdText2 = "\r\nSELECT  CAST(job_id AS VARCHAR(40)) AS job_id ,\r\n        step_id ,\r\n        step_name ,\r\n        database_name ,\r\n        command\r\nFROM    msdb.dbo.sysjobsteps\r\n";
			DataTable dataTable2 = base.FillDataTable(sqlString, cmdText2, timeOut);
			dataTable2.TableName = "tblJobStep";
			dataSet.Tables.Add(dataTable2);
			string cmdText3 = "\r\nDECLARE @tmp_sp_help_jobhistory TABLE\r\n    (\r\n      instance_id INT NULL ,\r\n      job_id UNIQUEIDENTIFIER NULL ,\r\n      job_name SYSNAME NULL ,\r\n      step_id INT NULL ,\r\n      step_name SYSNAME NULL ,\r\n      sql_message_id INT NULL ,\r\n      sql_severity INT NULL ,\r\n      message NVARCHAR(4000) NULL ,\r\n      run_status INT NULL ,\r\n      run_date INT NULL ,\r\n      run_time INT NULL ,\r\n      run_duration INT NULL ,\r\n      operator_emailed SYSNAME NULL ,\r\n      operator_netsent SYSNAME NULL ,\r\n      operator_paged SYSNAME NULL ,\r\n      retries_attempted INT NULL ,\r\n      server SYSNAME NULL\r\n    )\r\n\r\nINSERT  INTO @tmp_sp_help_jobhistory\r\n        EXEC msdb.dbo.sp_help_jobhistory @mode = 'FULL' \r\n\r\nSELECT  t.* , ISNULL(c.exec_count,0) AS exec_count\r\nFROM    ( SELECT    tshj.job_name ,\r\n                    tshj.instance_id ,\r\n                    CAST(tshj.job_id AS VARCHAR(40)) AS job_id ,\r\n                    tshj.step_id ,\r\n                    tshj.step_name ,\r\n                    tshj.sql_message_id ,\r\n                    tshj.message ,\r\n                    tshj.sql_severity ,\r\n                    tshj.run_status ,\r\n                    CASE tshj.run_date\r\n                      WHEN 0 THEN NULL\r\n                      ELSE CONVERT(DATETIME, STUFF(STUFF(CAST(tshj.run_date AS NCHAR(8)),\r\n                                                         7, 0, '-'), 5, 0, '-')\r\n                           + N' '\r\n                           + STUFF(STUFF(SUBSTRING(CAST(1000000\r\n                                                   + tshj.run_time AS NCHAR(7)),\r\n                                                   2, 6), 5, 0, ':'), 3, 0, ':'), 120)\r\n                    END AS run_date ,\r\n                    tshj.run_duration ,\r\n                    tshj.retries_attempted ,\r\n                    tshj.[server] ,\r\n                    ROW_NUMBER() OVER ( PARTITION BY tshj.job_name ORDER BY tshj.instance_id ASC ) AS row_num\r\n          FROM      @tmp_sp_help_jobhistory AS tshj\r\n        ) t\r\n        LEFT JOIN ( SELECT  job_id ,\r\n                            COUNT(job_id) AS exec_count\r\n                    FROM    @tmp_sp_help_jobhistory\r\n                    WHERE   step_id = 0\r\n                    GROUP BY job_id\r\n                  ) c ON t.job_id = c.job_id\r\nWHERE   t.row_num < 200\r\nOPTION  ( MAXDOP 2 )\r\n";
			DataTable dataTable3 = base.FillDataTable(sqlString, cmdText3, timeOut);
			dataTable3.TableName = "tblJobHistory";
			dataSet.Tables.Add(dataTable3);
			return dataSet;
		}
	}
}
