using System;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace ZhuanCloud.Collector
{
	public class TextManager
	{
		public static Encoding Encoding
		{
			get
			{
				return Encoding.UTF8;
			}
		}

		public static string Compress(string sqlText)
		{
			byte[] bytes = TextManager.Encoding.GetBytes(sqlText);
			string result;
			using (MemoryStream memoryStream = new MemoryStream())
			{
				using (GZipStream gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
				{
					gZipStream.Write(bytes, 0, bytes.Length);
					gZipStream.Close();
					result = Convert.ToBase64String(memoryStream.ToArray());
				}
			}
			return result;
		}

		public static string GetHashCode(string sqlText)
		{
			if (string.IsNullOrEmpty(sqlText))
			{
				return string.Empty;
			}
			MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
			byte[] bytes = TextManager.Encoding.GetBytes(sqlText);
			byte[] inArray = mD5CryptoServiceProvider.ComputeHash(bytes);
			return Convert.ToBase64String(inArray);
		}

		public static string GetParserSql(int eventClass, string sqlData)
		{
			string text = string.Empty;
			if (eventClass != 10)
			{
				return TextManager.GetRegexString(sqlData);
			}
			if (Regex.IsMatch(sqlData, "exec(\\s)+sp_prepexec", RegexOptions.IgnoreCase))
			{
				int index = Regex.Match(sqlData, "exec(\\s)+sp_prepexec", RegexOptions.IgnoreCase).Index;
				sqlData = sqlData.Remove(0, index);
				if (sqlData.Substring(sqlData.IndexOf(',') + 1, 4).ToUpper() == "NULL")
				{
					text = TextManager.GetRPCParserString(sqlData, "sp_prepexec", 0);
				}
				else
				{
					text = TextManager.GetRPCParserString(sqlData, "sp_prepexec", 1);
				}
				if (string.IsNullOrEmpty(text))
				{
					text = sqlData;
				}
				return text;
			}
			if (Regex.IsMatch(sqlData, "exec(\\s)+sp_prepare", RegexOptions.IgnoreCase))
			{
				int index2 = Regex.Match(sqlData, "exec(\\s)+sp_prepare", RegexOptions.IgnoreCase).Index;
				sqlData = sqlData.Remove(0, index2);
				if (sqlData.Substring(sqlData.IndexOf(',') + 1, 4).ToUpper() == "NULL")
				{
					text = TextManager.GetRPCParserString(sqlData, "sp_prepare", 0);
				}
				else
				{
					text = TextManager.GetRPCParserString(sqlData, "sp_prepare", 1);
				}
				if (string.IsNullOrEmpty(text))
				{
					text = sqlData;
				}
				return text;
			}
			if (Regex.IsMatch(sqlData, "exec(\\s)+sp_executesql", RegexOptions.IgnoreCase))
			{
				int index3 = Regex.Match(sqlData, "exec(\\s)+sp_executesql", RegexOptions.IgnoreCase).Index;
				sqlData = sqlData.Remove(0, index3);
				text = TextManager.GetRPCParserString(sqlData, "sp_executesql", 0);
				if (string.IsNullOrEmpty(text))
				{
					text = sqlData;
				}
				return text;
			}
			if (Regex.IsMatch(sqlData, "exec(\\s)+sp_cursoropen", RegexOptions.IgnoreCase))
			{
				int index4 = Regex.Match(sqlData, "exec(\\s)+sp_cursoropen", RegexOptions.IgnoreCase).Index;
				sqlData = sqlData.Remove(0, index4);
				text = TextManager.GetRPCParserString(sqlData, "sp_cursoropen", 0);
				if (string.IsNullOrEmpty(text))
				{
					text = sqlData;
				}
				return text;
			}
			if (Regex.IsMatch(sqlData, "exec(\\s)+sp_cursorprepare", RegexOptions.IgnoreCase))
			{
				int index5 = Regex.Match(sqlData, "exec(\\s)+sp_cursorprepare", RegexOptions.IgnoreCase).Index;
				sqlData = sqlData.Remove(0, index5);
				if (sqlData.Substring(sqlData.IndexOf(',') + 1, 4).ToUpper() == "NULL")
				{
					text = TextManager.GetRPCParserString(sqlData, "sp_cursorprepare", 0);
				}
				else
				{
					text = TextManager.GetRPCParserString(sqlData, "sp_cursorprepare", 1);
				}
				if (string.IsNullOrEmpty(text))
				{
					text = sqlData;
				}
				return text;
			}
			if (Regex.IsMatch(sqlData, "exec(\\s)+sp_cursorprepexec", RegexOptions.IgnoreCase))
			{
				int index6 = Regex.Match(sqlData, "exec(\\s)+sp_cursorprepexec", RegexOptions.IgnoreCase).Index;
				sqlData = sqlData.Remove(0, index6);
				if (sqlData.Substring(sqlData.IndexOf(',') + 1, 4).ToUpper() == "NULL")
				{
					text = TextManager.GetRPCParserString(sqlData, "sp_cursorprepexec", 0);
				}
				else
				{
					text = TextManager.GetRPCParserString(sqlData, "sp_cursorprepexec", 1);
				}
				if (string.IsNullOrEmpty(text))
				{
					text = sqlData;
				}
				return text;
			}
			return TextManager.GetRegexString(sqlData);
		}

		private static string GetRPCParserString(string sqlText, string type, int index)
		{
			MatchCollection matchCollection = Regex.Matches(sqlText, "(?:([N])?(')(?:[^']|'')*('))");
			if (matchCollection != null && matchCollection.Count >= index + 1)
			{
				string text = matchCollection[index].Value;
				if (text.StartsWith("N"))
				{
					text = text.Remove(0, 2);
				}
				else
				{
					text = text.Remove(0, 1);
				}
				text = text.Remove(text.Length - 1, 1);
				text = Regex.Replace(text, "\\binto[\\s]+([\\S]+)[\\s]+from\\b", "into @value from", RegexOptions.IgnoreCase);
				string text2 = Regex.Replace(text, "(?:([N])?('')(?:[^']|'''')*('')(?#))", "@value");
				text2 = Regex.Replace(text2, "(?:([\\s,(=<>!])[-+]?\\d+(\\.\\d+)?[\\s]*[\\+\\-\\*\\/\\&\\|\\^]+[\\s]*\\d+(\\.\\d+)?(?#))", "$1@value");
				text2 = Regex.Replace(text2, "(?:([\\s,(=<>!])([\\s]?((?:0x[\\da-fA-F]*)|(\\d+(\\.\\d+)?)))(?#))", "$1@value");
				sqlText = string.Format("exec {0} N'{1}'", type, text2);
			}
			return sqlText;
		}

		public static string GetRegexString(string sqlText)
		{
			if (sqlText == null)
			{
				return string.Empty;
			}
			sqlText = Regex.Replace(sqlText, "\\binto[\\s]+([^#@][\\S]+)[\\s]+from\\b", "into @PhysicalValue from", RegexOptions.IgnoreCase);
			sqlText = Regex.Replace(sqlText, "\\binto[\\s]+(#+[\\S]+)[\\s]+from\\b", "into @TempValue from", RegexOptions.IgnoreCase);
			sqlText = Regex.Replace(sqlText, "\\binto[\\s]+(@+[\\S]+)[\\s]+from\\b", "into @TableValue from", RegexOptions.IgnoreCase);
			string input = Regex.Replace(sqlText, "(?:([N])?(')(?:[^']|'')*(')(?#))", "@value");
			input = Regex.Replace(input, "(?:([\\s,(=<>!])[-+]?\\d+(\\.\\d+)?[\\s]*[\\+\\-\\*\\/\\&\\|\\^]+[\\s]*\\d+(\\.\\d+)?(?#))", "$1@value");
			input = Regex.Replace(input, "(?:([\\s,(=<>!])([\\s]?((?:0x[\\da-fA-F]*)|(\\d+(\\.\\d+)?)))(?#))", "$1@value");
			return Regex.Replace(input, "#[\\S]+\\b", "@value");
		}
	}
}
