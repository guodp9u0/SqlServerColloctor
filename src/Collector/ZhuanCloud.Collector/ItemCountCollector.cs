using System;
using System.Data;
using System.Data.OleDb;

namespace ZhuanCloud.Collector
{
	public class ItemCountCollector
	{
		private static object lockObj = new object();

		public static bool SaveItemCount(string oleDBString, int timeOut)
		{
			bool result;
			try
			{
				string cmdText = "\r\nINSERT  INTO tblItemCount\r\n        ( PerfSum ,\r\n          PerfFaild ,\r\n          TempdbSum ,\r\n          TempdbFaild ,\r\n          WaitSum ,\r\n          WaitFaild ,\r\n          UnusedSessionSum ,\r\n          UnusedSessionFaild ,\r\n          SqlTextSum ,\r\n          SqlTextFaild ,\r\n          SqlPlanSum ,\r\n          SqlPlanFaild ,\r\n          ErrLogSum ,\r\n          ErrLogFaild ,\r\n          MoebiusTraceSum ,\r\n          MoebiusTraceFaild\r\n        )\r\nVALUES  ( 0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 )\r\n";
				result = ItemCountCollector.SaveToAccess(oleDBString, cmdText, timeOut, new OleDbParameter[0]);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		public static bool SaveItemCount(string oleDBString, int timeOut, CollectItem item, int itemSum, int itemFaild)
		{
			bool result;
			lock (ItemCountCollector.lockObj)
			{
				string cmdText = string.Empty;
				switch (item)
				{
				case CollectItem.Perf:
					cmdText = string.Format("UPDATE  tblItemCount  SET  PerfSum = {0} ,PerfFaild = {1}", itemSum, itemFaild);
					break;
				case CollectItem.Tempdb:
					cmdText = string.Format("UPDATE  tblItemCount  SET  TempdbSum = {0} ,TempdbFaild = {1}", itemSum, itemFaild);
					break;
				case CollectItem.Wait:
					cmdText = string.Format("UPDATE  tblItemCount  SET  WaitSum = {0} ,WaitFaild = {1}", itemSum, itemFaild);
					break;
				case CollectItem.UnusedSession:
					cmdText = string.Format("UPDATE  tblItemCount  SET  UnusedSessionSum = {0} ,UnusedSessionFaild = {1}", itemSum, itemFaild);
					break;
				case CollectItem.SqlText:
					cmdText = string.Format("UPDATE  tblItemCount  SET  SqlTextSum = {0} ,SqlTextFaild = {1}", itemSum, itemFaild);
					break;
				case CollectItem.SqlPlan:
					cmdText = string.Format("UPDATE  tblItemCount  SET  SqlPlanSum = {0} ,SqlPlanFaild = {1}", itemSum, itemFaild);
					break;
				case CollectItem.ErrLog:
					cmdText = string.Format("UPDATE  tblItemCount  SET  ErrLogSum = {0} ,ErrLogFaild = {1}", itemSum, itemFaild);
					break;
				case CollectItem.DBFile:
					cmdText = string.Format("UPDATE  tblItemCount  SET  DBFileSum = {0} ,DBFileFaild = {1}", itemSum, itemFaild);
					break;
				case CollectItem.Moebius:
					cmdText = string.Format("UPDATE  tblItemCount  SET  MoebiusTraceSum = {0} ,MoebiusTraceFaild = {1}", itemSum, itemFaild);
					break;
				default:
					result = false;
					return result;
				}
				try
				{
					result = ItemCountCollector.SaveToAccess(oleDBString, cmdText, timeOut, new OleDbParameter[0]);
				}
				catch (Exception ex)
				{
					ErrorLog.Write(ex);
					result = false;
				}
			}
			return result;
		}

		private static bool SaveToAccess(string oleDBString, string cmdText, int timeOut, params OleDbParameter[] parameters)
		{
			OleDbConnection oleDbConnection = null;
			if (!Util.TryConnect(oleDBString, out oleDbConnection))
			{
				return false;
			}
			bool result;
			try
			{
				OleDbCommand oleDbCommand = oleDbConnection.CreateCommand();
				oleDbCommand.CommandTimeout = timeOut;
				oleDbCommand.CommandText = cmdText;
				if (parameters != null && parameters.Length > 0)
				{
					oleDbCommand.Parameters.Clear();
					oleDbCommand.Parameters.AddRange(parameters);
				}
				oleDbConnection.Open();
				int num = oleDbCommand.ExecuteNonQuery();
				oleDbConnection.Close();
				result = (num > 0);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(new Exception(cmdText));
				ErrorLog.Write(ex);
				result = false;
			}
			finally
			{
				if (oleDbConnection.State != ConnectionState.Closed)
				{
					oleDbConnection.Close();
				}
				oleDbConnection.Dispose();
			}
			return result;
		}
	}
}
