using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text;

namespace ZhuanCloud.Collector
{
	public class FunctionCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut, string condition, bool isDecrypt)
		{
			bool result;
			try
			{
				int majorVersion = base.GetMajorVersion(sqlVersion);
				DataTable data = this.GetData(sqlString, timeOut, condition);
				if (data.Rows.Count > 0)
				{
					DataTable dataTable = new DataTable();
					dataTable.Columns.Add(new DataColumn("db_name"));
					dataTable.Columns.Add(new DataColumn("schema_name"));
					dataTable.Columns.Add(new DataColumn("object_name"));
					dataTable.Columns.Add(new DataColumn("define"));
					dataTable.Columns.Add(new DataColumn("create_date"));
					dataTable.Columns.Add(new DataColumn("modify_date"));
					SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder(sqlString);
					string initialCatalog = sqlConnectionStringBuilder.InitialCatalog;
					List<string> list = new List<string>();
					list.AddRange(new string[]
					{
						"AF",
						"FS",
						"FT"
					});
					foreach (DataRow dataRow in data.Rows)
					{
						StringBuilder stringBuilder = new StringBuilder();
						string objectID = dataRow.ItemArray[0].ToString();
						string text = dataRow.ItemArray[1].ToString();
						string text2 = dataRow.ItemArray[2].ToString();
						string item = dataRow.ItemArray[3].ToString().Trim().ToUpper();
						string value = string.Empty;
						if (list.Contains(item))
						{
							value = "-- CLR 函数，无法解密";
						}
						else
						{
							value = dataRow.ItemArray[4].ToString();
							if (dataRow.ItemArray[5] != DBNull.Value)
							{
								bool flag = Convert.ToBoolean(dataRow.ItemArray[5]);
								stringBuilder.AppendLine(string.Format("SET ANSI_NULLS {0}", flag ? "ON" : "OFF"));
								stringBuilder.AppendLine("GO");
							}
							if (dataRow.ItemArray[6] != DBNull.Value)
							{
								bool flag2 = Convert.ToBoolean(dataRow.ItemArray[6]);
								stringBuilder.AppendLine(string.Format("SET QUOTED_IDENTIFIER {0}", flag2 ? "ON" : "OFF"));
								stringBuilder.AppendLine("GO");
							}
							if (string.IsNullOrEmpty(value))
							{
								if (isDecrypt)
								{
									try
									{
										value = Engine.Decrypt(sqlString, initialCatalog, objectID, majorVersion);
										goto IL_275;
									}
									catch (Exception innerException)
									{
										value = "-- 对象无法解密";
										ErrorLog.Write(new Exception(string.Format("db_name:{0},functionName:{1}.{2}", initialCatalog, text, text2), innerException));
										continue;
									}
								}
								value = "-- 对象不允许解密";
							}
						}
						IL_275:
						stringBuilder.AppendLine(value);
						stringBuilder.AppendLine("GO");
						DateTime dateTime = DateTime.Parse(dataRow.ItemArray[7].ToString());
						DateTime dateTime2 = DateTime.Parse(dataRow.ItemArray[8].ToString());
						string text3 = TextManager.Compress(stringBuilder.ToString());
						dataTable.Rows.Add(new object[]
						{
							initialCatalog,
							text,
							text2,
							text3,
							dateTime,
							dateTime2
						});
					}
					if (dataTable.Rows.Count > 0)
					{
						if (!Util.startFlag)
						{
							result = false;
							return result;
						}
						string cmdText = "\r\nINSERT  INTO tblDBFunction\r\n        ( DBName ,\r\n          SchemaName ,\r\n          ObjectName ,\r\n          Define ,\r\n          CreateDate ,\r\n          ModifyDate \r\n        )\r\nVALUES  ( @DBName ,\r\n          @SchemaName ,\r\n          @ObjectName ,\r\n          @Define ,\r\n          @CreateDate ,\r\n          @ModifyDate \r\n        )\r\n";
						OleDbParameter[] parameters = new OleDbParameter[]
						{
							new OleDbParameter("@DBName", OleDbType.VarChar)
							{
								SourceColumn = "db_name"
							},
							new OleDbParameter("@SchemaName", OleDbType.VarChar)
							{
								SourceColumn = "schema_name"
							},
							new OleDbParameter("@ObjectName", OleDbType.VarChar)
							{
								SourceColumn = "object_name"
							},
							new OleDbParameter("@Define", OleDbType.VarChar)
							{
								SourceColumn = "define"
							},
							new OleDbParameter("@CreateDate", OleDbType.Date)
							{
								SourceColumn = "create_date"
							},
							new OleDbParameter("@ModifyDate", OleDbType.Date)
							{
								SourceColumn = "modify_date"
							}
						};
						result = base.SaveToAccess(oleDBString, dataTable, cmdText, parameters);
						return result;
					}
				}
				result = true;
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, int timeOut, string condition)
		{
			string arg = string.Empty;
			if (!string.IsNullOrEmpty(condition))
			{
				string[] array = condition.Split(new string[]
				{
					";"
				}, StringSplitOptions.RemoveEmptyEntries);
				if (array.Length > 0)
				{
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.Append(" AND ");
					int num = 1;
					string[] array2 = array;
					for (int i = 0; i < array2.Length; i++)
					{
						string text = array2[i];
						string[] array3 = text.Split(new string[]
						{
							"."
						}, StringSplitOptions.RemoveEmptyEntries);
						if (array3.Length == 1)
						{
							stringBuilder.AppendFormat("  o.name NOT LIKE '{0}' ", array3[0]);
						}
						else if (array3.Length == 2)
						{
							stringBuilder.AppendFormat(" (s.name NOT LIKE '{0}' OR o.name NOT LIKE '{1}') ", array3[0], array3[1]);
						}
						if (num < array.Length)
						{
							stringBuilder.Append(" AND ");
						}
						num++;
					}
					arg = stringBuilder.ToString();
				}
			}
			string cmdText = string.Format("\r\nSELECT  o.[object_id] AS [objId] ,\r\n        s.name AS [schema_name] ,\r\n        o.name AS function_name ,\r\n        o.[type] ,\r\n        m.[definition] ,\r\n        m.uses_ansi_nulls AS ANSI ,\r\n        m.uses_quoted_identifier AS Quoted ,\r\n        o.create_date ,\r\n        o.modify_date\r\nFROM    sys.objects o\r\n        INNER JOIN sys.schemas s ON o.schema_id = s.schema_id\r\n                                    AND OBJECTPROPERTY(o.[object_id], 'IsMSShipped') = 0\r\n                                    AND o.type IN ( 'FN', 'IF', 'TF', 'AF', 'FS', 'FT' ) {0} \r\n        LEFT JOIN sys.sql_modules m ON o.[object_id] = m.[object_id]\r\nOPTION (MAXDOP 2)\r\n", arg);
			return base.FillDataTable(sqlString, cmdText, timeOut);
		}
	}
}
