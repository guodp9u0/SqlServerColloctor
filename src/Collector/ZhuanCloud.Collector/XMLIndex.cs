using System;

namespace ZhuanCloud.Collector
{
	internal class XMLIndex
	{
		public string IndexName
		{
			get;
			set;
		}

		public int IndexType
		{
			get;
			set;
		}

		public string PriName
		{
			get;
			set;
		}

		public string SecondTypeDesc
		{
			get;
			set;
		}

		public string ColName
		{
			get;
			set;
		}

		public int Factor
		{
			get;
			set;
		}

		public bool HasPadIndex
		{
			get;
			set;
		}

		public bool IsIgnorekey
		{
			get;
			set;
		}

		public bool HasRowLock
		{
			get;
			set;
		}

		public bool HasPageLock
		{
			get;
			set;
		}

		public bool IsDisabled
		{
			get;
			set;
		}
	}
}
