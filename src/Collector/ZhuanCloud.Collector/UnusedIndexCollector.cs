using System;
using System.Data;
using System.Data.OleDb;

namespace ZhuanCloud.Collector
{
	public class UnusedIndexCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(sqlString, sqlVersion, timeOut);
				if (data.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblUnusedIndex\r\n        ( DBName ,\r\n          SchemaName ,\r\n          TableName ,\r\n          IndexName ,\r\n          UserSeeks ,\r\n          UserScans ,\r\n          UserLookups ,\r\n          UserUpdates ,\r\n          LastUserSeek ,\r\n          LastUserScan ,\r\n          LastUserLookup ,\r\n          LastUserUpdate ,\r\n          SytemSeeks ,\r\n          SytemScans ,\r\n          SytemLookups ,\r\n          SytemUpdates ,\r\n          LastSytemSeek ,\r\n          LastSytemScan ,\r\n          LastSytemLookup ,\r\n          LastSytemUpdate ,\r\n          [RowCount] ,\r\n          IsDisabled ,\r\n          [FillFactor] ,\r\n          HasFilter ,\r\n          FilterDefinition\r\n        )\r\nVALUES  ( @DBName ,\r\n          @SchemaName ,\r\n          @TableName ,\r\n          @IndexName ,\r\n          @UserSeeks ,\r\n          @UserScans ,\r\n          @UserLookups ,\r\n          @UserUpdates ,\r\n          @LastUserSeek ,\r\n          @LastUserScan ,\r\n          @LastUserLookup ,\r\n          @LastUserUpdate ,\r\n          @SytemSeeks ,\r\n          @SytemScans ,\r\n          @SytemLookups ,\r\n          @SytemUpdates ,\r\n          @LastSytemSeek ,\r\n          @LastSytemScan ,\r\n          @LastSytemLookup ,\r\n          @LastSytemUpdate ,\r\n          @RowCount ,\r\n          @IsDisabled ,\r\n          @FillFactor ,\r\n          @HasFilter ,\r\n          @FilterDefinition\t  \r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@DBName", OleDbType.VarChar)
						{
							SourceColumn = "db_name"
						},
						new OleDbParameter("@SchemaName", OleDbType.VarChar)
						{
							SourceColumn = "schema_name"
						},
						new OleDbParameter("@TableName", OleDbType.VarChar)
						{
							SourceColumn = "table_name"
						},
						new OleDbParameter("@IndexName", OleDbType.VarChar)
						{
							SourceColumn = "index_name"
						},
						new OleDbParameter("@UserSeeks", OleDbType.Double)
						{
							SourceColumn = "user_seeks"
						},
						new OleDbParameter("@UserScans", OleDbType.Double)
						{
							SourceColumn = "user_scans"
						},
						new OleDbParameter("@UserLookups", OleDbType.Double)
						{
							SourceColumn = "user_lookups"
						},
						new OleDbParameter("@UserUpdates", OleDbType.Double)
						{
							SourceColumn = "user_updates"
						},
						new OleDbParameter("@LastUserSeek", OleDbType.Date)
						{
							SourceColumn = "last_user_seek"
						},
						new OleDbParameter("@LastUserScan", OleDbType.Date)
						{
							SourceColumn = "last_user_scan"
						},
						new OleDbParameter("@LastUserLookup", OleDbType.Date)
						{
							SourceColumn = "last_user_lookup"
						},
						new OleDbParameter("@LastUserUpdate", OleDbType.Date)
						{
							SourceColumn = "last_user_update"
						},
						new OleDbParameter("@SytemSeeks", OleDbType.Double)
						{
							SourceColumn = "system_seeks"
						},
						new OleDbParameter("@SytemScans", OleDbType.Double)
						{
							SourceColumn = "system_scans"
						},
						new OleDbParameter("@SytemLookups", OleDbType.Double)
						{
							SourceColumn = "system_lookups"
						},
						new OleDbParameter("@SytemUpdates", OleDbType.Double)
						{
							SourceColumn = "system_updates"
						},
						new OleDbParameter("@LastSytemSeek", OleDbType.Date)
						{
							SourceColumn = "last_system_seek"
						},
						new OleDbParameter("@LastSytemScan", OleDbType.Date)
						{
							SourceColumn = "last_system_scan"
						},
						new OleDbParameter("@LastSytemLookup", OleDbType.Date)
						{
							SourceColumn = "last_system_lookup"
						},
						new OleDbParameter("@LastSytemUpdate", OleDbType.Date)
						{
							SourceColumn = "last_system_update"
						},
						new OleDbParameter("@RowCount", OleDbType.Double)
						{
							SourceColumn = "row_count"
						},
						new OleDbParameter("@IsDisabled", OleDbType.Boolean)
						{
							SourceColumn = "is_disabled"
						},
						new OleDbParameter("@FillFactor", OleDbType.Integer)
						{
							SourceColumn = "fill_factor"
						},
						new OleDbParameter("@HasFilter", OleDbType.Boolean)
						{
							SourceColumn = "has_filter"
						},
						new OleDbParameter("@FilterDefinition", OleDbType.VarChar)
						{
							SourceColumn = "filter_definition"
						}
					};
					result = base.SaveToAccess(oleDBString, data, cmdText, parameters);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			string cmdText = string.Empty;
			if (sqlVersion >= SQLServerVersion.SQLServer2008)
			{
				cmdText = "\r\nSET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED\r\nDECLARE @start_time DATETIME\r\nSELECT  @start_time = create_date\r\nFROM    sys.databases\r\nWHERE   name = 'tempdb'\r\nIF DATEDIFF(DAY, @start_time, GETDATE()) > 60 \r\n    BEGIN\r\n        SELECT DISTINCT\r\n                DB_NAME(DB_ID()) [db_name] ,\r\n                s.name AS [schema_name] ,\r\n                t.name AS table_name ,\r\n                i.name AS index_name ,\r\n                user_seeks ,\r\n                user_scans ,\r\n                user_lookups ,\r\n                user_updates ,\r\n                last_user_seek ,\r\n                last_user_scan ,\r\n                last_user_lookup ,\r\n                last_user_update ,\r\n                system_seeks ,\r\n                system_scans ,\r\n                system_lookups ,\r\n                system_updates ,\r\n                last_system_seek ,\r\n                last_system_scan ,\r\n                last_system_lookup ,\r\n                last_system_update ,\r\n                dps.row_count ,\r\n                i.is_disabled ,\r\n                i.fill_factor ,\r\n                i.has_filter ,\r\n                i.filter_definition\r\n        FROM    sys.tables t\r\n                LEFT JOIN sys.indexes i ON i.[object_id] = t.[object_id]\r\n                                           AND OBJECTPROPERTY(t.[object_id],\r\n                                                              'IsMSShipped') = 0\r\n                LEFT JOIN sys.schemas s ON s.[schema_id] = t.[schema_id]\r\n                LEFT JOIN ( SELECT  object_id ,\r\n                                            SUM(row_count) AS row_count\r\n                                    FROM    sys.dm_db_partition_stats\r\n                                    WHERE   index_id < 2\r\n                                    GROUP BY object_id ,\r\n                                            index_id\r\n                                  ) dps ON i.[object_id] = dps.[object_id]\r\n                LEFT JOIN sys.dm_db_index_usage_stats di ON di.[object_id] = i.[object_id]\r\n                                                            AND di.[object_id] = t.[object_id]\r\n                                                            AND di.[object_id] = dps.[object_id]\r\n                                                            AND di.index_id = i.index_id\r\n                                                            AND di.database_id = DB_ID()\r\n        WHERE   i.name IS NOT NULL\r\n        ORDER BY dps.row_count DESC\t\r\n        OPTION (MAXDOP 2)\r\n    END\r\n";
			}
			else
			{
				cmdText = "\r\nSET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED\r\nDECLARE @start_time DATETIME\r\nSELECT  @start_time = create_date\r\nFROM    sys.databases\r\nWHERE   name = 'tempdb'\r\nIF DATEDIFF(DAY, @start_time, GETDATE()) > 60 \r\n    BEGIN\r\n        SELECT DISTINCT\r\n                DB_NAME(DB_ID()) [db_name] ,\r\n                s.name AS [schema_name] ,\r\n                t.name AS table_name ,\r\n                i.name AS index_name ,\r\n                user_seeks ,\r\n                user_scans ,\r\n                user_lookups ,\r\n                user_updates ,\r\n                last_user_seek ,\r\n                last_user_scan ,\r\n                last_user_lookup ,\r\n                last_user_update ,\r\n                system_seeks ,\r\n                system_scans ,\r\n                system_lookups ,\r\n                system_updates ,\r\n                last_system_seek ,\r\n                last_system_scan ,\r\n                last_system_lookup ,\r\n                last_system_update ,\r\n                dps.row_count ,\r\n                i.is_disabled ,\r\n                i.fill_factor ,\r\n                0 AS has_filter ,\r\n                NULL AS filter_definition\r\n        FROM    sys.tables t\r\n                LEFT JOIN sys.indexes i ON i.[object_id] = t.[object_id]\r\n                                           AND OBJECTPROPERTY(t.[object_id],\r\n                                                              'IsMSShipped') = 0\r\n                LEFT JOIN sys.schemas s ON s.[schema_id] = t.[schema_id]\r\n                LEFT JOIN ( SELECT  object_id ,\r\n                                            SUM(row_count) AS row_count\r\n                                    FROM    sys.dm_db_partition_stats\r\n                                    WHERE   index_id < 2\r\n                                    GROUP BY object_id ,\r\n                                            index_id\r\n                                  ) dps ON i.[object_id] = dps.[object_id]\r\n                LEFT JOIN sys.dm_db_index_usage_stats di ON di.[object_id] = i.[object_id]\r\n                                                            AND di.[object_id] = t.[object_id]\r\n                                                            AND di.[object_id] = dps.[object_id]\r\n                                                            AND di.index_id = i.index_id\r\n                                                            AND di.database_id = DB_ID()\r\n        WHERE   i.name IS NOT NULL\r\n        ORDER BY dps.row_count DESC\t\r\n        OPTION (MAXDOP 2)\r\n    END\r\n";
			}
			return base.FillDataTable(sqlString, cmdText, timeOut);
		}
	}
}
