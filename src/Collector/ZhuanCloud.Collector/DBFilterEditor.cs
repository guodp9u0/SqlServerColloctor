using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace ZhuanCloud.Collector
{
	public class DBFilterEditor : UITypeEditor
	{
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			return UITypeEditorEditStyle.Modal;
		}

		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			IWindowsFormsEditorService windowsFormsEditorService = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
			if (windowsFormsEditorService != null)
			{
				CollectConfig collectConfig = context.Instance as CollectConfig;
				if (collectConfig != null)
				{
					FrmDBFilter frmDBFilter = new FrmDBFilter(collectConfig.DBInstanceName, collectConfig.DBList, collectConfig.DBFilterList);
					if (windowsFormsEditorService.ShowDialog(frmDBFilter) == DialogResult.OK)
					{
						return frmDBFilter.FilterList;
					}
				}
			}
			return value;
		}
	}
}
