using System;

namespace ZhuanCloud.Collector
{
	internal class ColumnStoredIndex
	{
		public string IndexName
		{
			get;
			set;
		}

		public int IndexType
		{
			get;
			set;
		}

		public string TypeDesc
		{
			get;
			set;
		}

		public string ColName
		{
			get;
			set;
		}

		public string FileGroupName
		{
			get;
			set;
		}

		public bool IsDisabled
		{
			get;
			set;
		}
	}
}
