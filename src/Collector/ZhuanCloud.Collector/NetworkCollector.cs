using System;
using System.Data;
using System.Data.OleDb;
using System.Net.NetworkInformation;

namespace ZhuanCloud.Collector
{
	public class NetworkCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, int timeOut)
		{
			bool result;
			try
			{
				DataTable dataTable = new DataTable();
				dataTable.Columns.Add(new DataColumn("network_name"));
				dataTable.Columns.Add(new DataColumn("device_name"));
				dataTable.Columns.Add(new DataColumn("speed"));
				dataTable.Columns.Add(new DataColumn("receivedBytes"));
				dataTable.Columns.Add(new DataColumn("sendBytes"));
				dataTable.Columns.Add(new DataColumn("physicalMac"));
				dataTable.Columns.Add(new DataColumn("connect_status"));
				NetworkInterface[] allNetworkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
				if (allNetworkInterfaces.Length > 0)
				{
					NetworkInterface[] array = allNetworkInterfaces;
					for (int i = 0; i < array.Length; i++)
					{
						NetworkInterface networkInterface = array[i];
						if (networkInterface.NetworkInterfaceType == NetworkInterfaceType.Ethernet && (networkInterface.OperationalStatus == OperationalStatus.Up || networkInterface.OperationalStatus == OperationalStatus.Down))
						{
							string name = networkInterface.Name;
							string description = networkInterface.Description;
							long num = networkInterface.Speed / 1000000L;
							long bytesReceived = networkInterface.GetIPv4Statistics().BytesReceived;
							long bytesSent = networkInterface.GetIPv4Statistics().BytesSent;
							string text = BitConverter.ToString(networkInterface.GetPhysicalAddress().GetAddressBytes());
							int num2 = (networkInterface.OperationalStatus == OperationalStatus.Up) ? 1 : 2;
							dataTable.Rows.Add(new object[]
							{
								name,
								description,
								num,
								bytesReceived,
								bytesSent,
								text,
								num2
							});
						}
					}
				}
				if (dataTable.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblNetwork\r\n        ( NetworkName ,\r\n          DeviceName ,\r\n          Speed ,\r\n          ReceivedBytes ,\r\n          SendBytes ,\r\n          PhysicalMac ,\r\n          ConnectStatus\r\n        )\r\nVALUES  ( @NetworkName ,\r\n          @DeviceName ,\r\n          @Speed ,\r\n          @ReceivedBytes ,\r\n          @SendBytes ,\r\n          @PhysicalMac ,\r\n          @ConnectStatus\r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@NetworkName", OleDbType.VarChar)
						{
							SourceColumn = "network_name"
						},
						new OleDbParameter("@DeviceName", OleDbType.VarChar)
						{
							SourceColumn = "device_name"
						},
						new OleDbParameter("@Speed", OleDbType.Single)
						{
							SourceColumn = "speed"
						},
						new OleDbParameter("@ReceivedBytes", OleDbType.Double)
						{
							SourceColumn = "receivedBytes"
						},
						new OleDbParameter("@ReceivedBytes", OleDbType.Double)
						{
							SourceColumn = "sendBytes"
						},
						new OleDbParameter("@PhysicalMac", OleDbType.VarChar)
						{
							SourceColumn = "physicalMac"
						},
						new OleDbParameter("@ConnectStatus", OleDbType.VarChar)
						{
							SourceColumn = "connect_status"
						}
					};
					bool flag = base.SaveToAccess(oleDBString, dataTable, cmdText, parameters);
					result = flag;
				}
				else
				{
					ErrorLog.Write(new Exception("无法获取网卡信息，请启用网卡。"));
					result = false;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}
	}
}
