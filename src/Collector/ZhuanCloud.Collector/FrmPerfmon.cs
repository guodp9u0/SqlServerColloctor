using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace ZhuanCloud.Collector
{
	internal class FrmPerfmon : Form
	{
		private CollectConfig config;

		private List<Perf> perfList;

		private IContainer components;

		private ListView lvPerfmon;

		private ColumnHeader columnHeader1;

		private ColumnHeader columnHeader2;

		private ColumnHeader columnHeader3;

		private Button btnCancel;

		private Button btnOk;

		private CheckBox cbAll;

		private ColumnHeader columnHeader4;

		private Label lblLoadInfo;

		public List<Perf> PerfList
		{
			get
			{
				return this.perfList;
			}
		}

		public FrmPerfmon()
		{
			this.InitializeComponent();
		}

		internal FrmPerfmon(CollectConfig config)
		{
			this.InitializeComponent();
			this.config = config;
		}

		private void FrmPerfmon_Load(object sender, EventArgs e)
		{
			ThreadPool.QueueUserWorkItem(new WaitCallback(this.LoadPerf));
		}

		private void LoadPerf(object sender)
		{
			if (this.config != null)
			{
				if (this.config.PerfList == null)
				{
					string empty = string.Empty;
					this.perfList = ConfigManager.LoadPerfCounter(this.config.DBInstanceName, out empty);
					if (this.perfList != null)
					{
						List<Perf> list = new List<Perf>();
						foreach (Perf current in this.perfList)
						{
							if (current.Checked)
							{
								list.Add(current);
							}
						}
						this.config.PerfList = list;
						this.config.AllPerfList = this.perfList;
					}
					if (empty.Length > 0)
					{
						MessageBox.Show("部分性能计数器无法收集:\r\n" + empty, "SQL专家云");
					}
				}
				else
				{
					this.perfList = this.config.AllPerfList;
				}
			}
			if (this.perfList != null && this.perfList.Count > 0)
			{
				foreach (Perf current2 in this.perfList)
				{
					ListViewItem subItem = new ListViewItem(new string[]
					{
						current2.ObjectName,
						current2.CounterName,
						current2.InstanceName,
						current2.Status
					});
					subItem.Checked = current2.Checked;
					subItem.Tag = current2;
					if (current2.HasError)
					{
						subItem.ForeColor = Color.Gray;
					}
					base.Invoke(new EventHandler(delegate
					{
						this.lvPerfmon.Items.Add(subItem);
					}));
				}
				this.lvPerfmon.ItemCheck += new ItemCheckEventHandler(this.lvPerfmon_ItemCheck);
			}
			base.Invoke(new EventHandler(delegate
			{
				this.lblLoadInfo.Visible = false;
				this.btnOk.Enabled = (this.lvPerfmon.Items.Count > 0);
			}));
		}

		private void lvPerfmon_ItemCheck(object sender, ItemCheckEventArgs e)
		{
			if (e.Index >= 0)
			{
				Perf perf = this.lvPerfmon.Items[e.Index].Tag as Perf;
				if (perf != null && perf.HasError)
				{
					e.NewValue = CheckState.Unchecked;
				}
			}
		}

		private void cbAll_CheckedChanged(object sender, EventArgs e)
		{
			if (this.lvPerfmon.Items.Count > 0)
			{
				foreach (ListViewItem listViewItem in this.lvPerfmon.Items)
				{
					listViewItem.Checked = this.cbAll.Checked;
				}
			}
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			this.perfList = this.GetPerfList();
			base.DialogResult = DialogResult.OK;
		}

		private List<Perf> GetPerfList()
		{
			List<Perf> list = new List<Perf>();
			List<Perf> list2 = new List<Perf>();
			if (this.lvPerfmon.Items.Count > 0)
			{
				foreach (ListViewItem listViewItem in this.lvPerfmon.Items)
				{
					Perf perf = listViewItem.Tag as Perf;
					if (perf != null)
					{
						perf.Checked = listViewItem.Checked;
						if (listViewItem.Checked)
						{
							list.Add(perf);
						}
						if (perf.HasError)
						{
							perf.HasReView = true;
						}
						else
						{
							perf.HasReView = false;
						}
						list2.Add(perf);
					}
				}
			}
			this.config.AllPerfList = list2;
			return list;
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.perfList = null;
			base.DialogResult = DialogResult.Cancel;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(FrmPerfmon));
			this.lvPerfmon = new ListView();
			this.columnHeader1 = new ColumnHeader();
			this.columnHeader2 = new ColumnHeader();
			this.columnHeader3 = new ColumnHeader();
			this.columnHeader4 = new ColumnHeader();
			this.btnCancel = new Button();
			this.btnOk = new Button();
			this.cbAll = new CheckBox();
			this.lblLoadInfo = new Label();
			base.SuspendLayout();
			this.lvPerfmon.CheckBoxes = true;
			this.lvPerfmon.Columns.AddRange(new ColumnHeader[]
			{
				this.columnHeader1,
				this.columnHeader2,
				this.columnHeader3,
				this.columnHeader4
			});
			this.lvPerfmon.FullRowSelect = true;
			this.lvPerfmon.Location = new Point(7, 5);
			this.lvPerfmon.Name = "lvPerfmon";
			this.lvPerfmon.Size = new Size(621, 265);
			this.lvPerfmon.TabIndex = 3;
			this.lvPerfmon.UseCompatibleStateImageBehavior = false;
			this.lvPerfmon.View = View.Details;
			this.columnHeader1.Text = "   对象";
			this.columnHeader1.Width = 260;
			this.columnHeader2.Text = "计数器";
			this.columnHeader2.Width = 185;
			this.columnHeader3.Text = "实例";
			this.columnHeader3.Width = 80;
			this.columnHeader4.Text = "状态";
			this.columnHeader4.Width = 80;
			this.btnCancel.Location = new Point(553, 277);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new Size(75, 23);
			this.btnCancel.TabIndex = 2;
			this.btnCancel.Text = "取消";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
			this.btnOk.Enabled = false;
			this.btnOk.Location = new Point(472, 277);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new Size(75, 23);
			this.btnOk.TabIndex = 1;
			this.btnOk.Text = "确定";
			this.btnOk.UseVisualStyleBackColor = true;
			this.btnOk.Click += new EventHandler(this.btnOk_Click);
			this.cbAll.AutoSize = true;
			this.cbAll.Checked = true;
			this.cbAll.CheckState = CheckState.Checked;
			this.cbAll.FlatStyle = FlatStyle.Flat;
			this.cbAll.Location = new Point(16, 10);
			this.cbAll.Name = "cbAll";
			this.cbAll.Size = new Size(12, 11);
			this.cbAll.TabIndex = 4;
			this.cbAll.UseVisualStyleBackColor = true;
			this.cbAll.CheckedChanged += new EventHandler(this.cbAll_CheckedChanged);
			this.lblLoadInfo.AutoSize = true;
			this.lblLoadInfo.Location = new Point(7, 284);
			this.lblLoadInfo.Name = "lblLoadInfo";
			this.lblLoadInfo.Size = new Size(179, 12);
			this.lblLoadInfo.TabIndex = 5;
			this.lblLoadInfo.Text = "正在获取计数器列表，请稍候...";
			base.AutoScaleDimensions = new SizeF(6f, 12f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(634, 307);
			base.Controls.Add(this.lblLoadInfo);
			base.Controls.Add(this.cbAll);
			base.Controls.Add(this.btnOk);
			base.Controls.Add(this.btnCancel);
			base.Controls.Add(this.lvPerfmon);
			base.FormBorderStyle = FormBorderStyle.FixedSingle;
			base.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "FrmPerfmon";
			base.StartPosition = FormStartPosition.CenterScreen;
			this.Text = "性能计数器";
			base.Load += new EventHandler(this.FrmPerfmon_Load);
			base.ResumeLayout(false);
			base.PerformLayout();
		}
	}
}
