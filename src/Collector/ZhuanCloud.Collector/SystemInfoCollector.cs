using Microsoft.VisualBasic.Devices;
using System;
using System.Data;
using System.Data.OleDb;
using System.DirectoryServices.ActiveDirectory;
using System.Management;
using System.Net;

namespace ZhuanCloud.Collector
{
	public class SystemInfoCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, int timeOut)
		{
			bool result;
			try
			{
				string versionInfo = this.GetVersionInfo(sqlString, timeOut);
				string text = "X86";
				if (!string.IsNullOrEmpty(versionInfo) && versionInfo.Contains("Copyright") && versionInfo.Substring(versionInfo.IndexOf("Copyright")).Contains("64"))
				{
					text = "X64";
				}
				ComputerInfo computerInfo = new ComputerInfo();
				string oSFullName = computerInfo.OSFullName;
				string hostName = Dns.GetHostName();
				string text2 = Environment.OSVersion.Version.ToString();
				string servicePack = Environment.OSVersion.ServicePack;
				float virtualMemoryInfo = this.GetVirtualMemoryInfo();
				string text3 = string.Empty;
				try
				{
					text3 = Domain.GetComputerDomain().Name;
				}
				catch
				{
					text3 = string.Empty;
				}
				string cmdText = string.Format("\r\nINSERT  INTO tblSystemInfo\r\n        ( OSName ,\r\n          HostName ,\r\n          OSBit ,\r\n          OSVersion ,\r\n          ServicePack ,\r\n          TotalVirMemory ,\r\n          DomainName \r\n        )\r\nVALUES  ( '{0}','{1}','{2}','{3}','{4}',{5},'{6}' )\r\n", new object[]
				{
					oSFullName,
					hostName,
					text,
					text2,
					servicePack,
					virtualMemoryInfo,
					text3
				});
				if (!base.SaveToAccess(oleDBString, cmdText, timeOut, new OleDbParameter[0]))
				{
					result = false;
				}
				else
				{
					DataTable memoryInfo = this.GetMemoryInfo();
					if (memoryInfo.Rows.Count == 0)
					{
						float num = Convert.ToSingle((computerInfo.TotalPhysicalMemory / 1024f / 1024f).ToString("F0"));
						memoryInfo.Rows.Add(new object[]
						{
							"total",
							num,
							string.Empty,
							0
						});
					}
					if (memoryInfo.Rows.Count > 0)
					{
						cmdText = "\r\nINSERT  INTO tblMemory\r\n        ( PCIName ,\r\n          PCISize ,\r\n          Manufacturer ,\r\n          Speed \r\n        )\r\nVALUES  ( @PCIName ,\r\n          @PCISize ,\r\n          @Manufacturer ,\r\n          @Speed \r\n        )\r\n";
						OleDbParameter[] parameters = new OleDbParameter[]
						{
							new OleDbParameter("@PCIName", OleDbType.VarChar)
							{
								SourceColumn = "pci_name"
							},
							new OleDbParameter("@PCISize", OleDbType.VarChar)
							{
								SourceColumn = "pci_size"
							},
							new OleDbParameter("@Manufacturer", OleDbType.VarChar)
							{
								SourceColumn = "manufacturer"
							},
							new OleDbParameter("@Speed", OleDbType.Single)
							{
								SourceColumn = "speed"
							}
						};
						if (!base.SaveToAccess(oleDBString, memoryInfo, cmdText, parameters))
						{
							result = false;
							return result;
						}
					}
					try
					{
						ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher("SELECT * FROM Win32_Processor");
						if (managementObjectSearcher != null)
						{
							DataTable dataTable = new DataTable();
							dataTable.Columns.Add(new DataColumn("cpu_id"));
							dataTable.Columns.Add(new DataColumn("cpu_name"));
							dataTable.Columns.Add(new DataColumn("speed"));
							dataTable.Columns.Add(new DataColumn("cores"));
							dataTable.Columns.Add(new DataColumn("threads"));
							dataTable.Columns.Add(new DataColumn("status"));
							using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = managementObjectSearcher.Get().GetEnumerator())
							{
								while (enumerator.MoveNext())
								{
									ManagementObject managementObject = (ManagementObject)enumerator.Current;
									string text4 = managementObject["DeviceID"].ToString();
									string text5 = managementObject["Name"].ToString();
									int num2 = Convert.ToInt32(managementObject["MaxClockSpeed"]);
									int num3 = 0;
									try
									{
										num3 = Convert.ToInt32(managementObject["NumberOfCores"]);
									}
									catch (Exception)
									{
										num3 = 0;
									}
									int num4 = 0;
									try
									{
										num4 = Convert.ToInt32(managementObject["NumberOfLogicalProcessors"]);
									}
									catch (Exception)
									{
										num4 = 0;
									}
									string text6 = managementObject["Status"].ToString();
									dataTable.Rows.Add(new object[]
									{
										text4,
										text5,
										num2,
										num3,
										num4,
										text6
									});
								}
							}
							if (dataTable.Rows.Count > 0)
							{
								cmdText = "\r\nINSERT  INTO tblCPUInfo\r\n        ( CPUID ,\r\n          CPUName ,\r\n          Speed ,\r\n          Cores ,\r\n          Threads ,\r\n          Status\r\n        )\r\nVALUES  ( @CPUID ,\r\n          @CPUName ,\r\n          @Speed ,\r\n          @Cores ,\r\n          @Threads ,\r\n          @Status\r\n        )\r\n";
								OleDbParameter[] parameters2 = new OleDbParameter[]
								{
									new OleDbParameter("@CPUID", OleDbType.VarChar)
									{
										SourceColumn = "cpu_id"
									},
									new OleDbParameter("@CPUName", OleDbType.VarChar)
									{
										SourceColumn = "cpu_name"
									},
									new OleDbParameter("@Speed", OleDbType.Integer)
									{
										SourceColumn = "speed"
									},
									new OleDbParameter("@Cores", OleDbType.Integer)
									{
										SourceColumn = "cores"
									},
									new OleDbParameter("@Threads", OleDbType.Integer)
									{
										SourceColumn = "threads"
									},
									new OleDbParameter("@Threads", OleDbType.VarChar)
									{
										SourceColumn = "status"
									}
								};
								if (!base.SaveToAccess(oleDBString, dataTable, cmdText, parameters2))
								{
									result = false;
									return result;
								}
							}
						}
					}
					catch (Exception ex)
					{
						ErrorLog.Write(ex);
						result = false;
						return result;
					}
					result = true;
				}
			}
			catch (Exception ex2)
			{
				ErrorLog.Write(ex2);
				result = false;
			}
			return result;
		}

		private string GetVersionInfo(string sqlString, int timeOut)
		{
			string cmdText = "SELECT  @@VERSION AS version_info FROM  sys.dm_os_sys_info";
			try
			{
				object obj = base.SQLExecuteScalar(sqlString, cmdText, timeOut);
				if (obj != null)
				{
					return obj.ToString();
				}
			}
			catch
			{
			}
			return string.Empty;
		}

		private float GetVirtualMemoryInfo()
		{
			float result;
			try
			{
				float num = 0f;
				ManagementClass managementClass = new ManagementClass("Win32_PageFileUsage");
				ManagementObjectCollection instances = managementClass.GetInstances();
				using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = instances.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						ManagementObject managementObject = (ManagementObject)enumerator.Current;
						num = Convert.ToSingle(managementObject.GetPropertyValue("AllocatedBaseSize"));
					}
				}
				result = num;
			}
			catch
			{
				result = 0f;
			}
			return result;
		}

		private DataTable GetMemoryInfo()
		{
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add(new DataColumn("pci_name"));
			dataTable.Columns.Add(new DataColumn("pci_size"));
			dataTable.Columns.Add(new DataColumn("manufacturer"));
			dataTable.Columns.Add(new DataColumn("speed"));
			try
			{
				ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMemory");
				if (managementObjectSearcher != null)
				{
					using (ManagementObjectCollection.ManagementObjectEnumerator enumerator = managementObjectSearcher.Get().GetEnumerator())
					{
						while (enumerator.MoveNext())
						{
							ManagementObject managementObject = (ManagementObject)enumerator.Current;
							string text = string.Empty;
							float num = 0f;
							try
							{
								text = managementObject["Tag"].ToString();
								num = Convert.ToSingle(managementObject["Capacity"]) / 1024f / 1024f;
							}
							catch
							{
							}
							if (!string.IsNullOrEmpty(text) && num > 0f)
							{
								string text2 = string.Empty;
								try
								{
									text2 = managementObject["Manufacturer"].ToString();
								}
								catch
								{
								}
								int num2 = 0;
								try
								{
									num2 = Convert.ToInt32(managementObject["Speed"]);
								}
								catch
								{
								}
								dataTable.Rows.Add(new object[]
								{
									text,
									num,
									text2,
									num2
								});
							}
						}
					}
				}
			}
			catch
			{
			}
			return dataTable;
		}
	}
}
