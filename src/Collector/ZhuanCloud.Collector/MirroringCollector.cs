using System;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace ZhuanCloud.Collector
{
	public class MirroringCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, int timeOut)
		{
			bool result;
			try
			{
				bool data = this.GetData(sqlString, timeOut);
				string cmdText = "\r\nINSERT  INTO tblSinglePoint\r\n        ( DatabaseName, TypeID, Enabled )\r\nVALUES  ( @DatabaseName, @TypeID, @Enabled )\r\n";
				SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder(sqlString);
				OleDbParameter[] parameters = new OleDbParameter[]
				{
					new OleDbParameter("@DatabaseName", OleDbType.VarChar)
					{
						Value = sqlConnectionStringBuilder.InitialCatalog
					},
					new OleDbParameter("@TypeID", OleDbType.Integer)
					{
						Value = 3
					},
					new OleDbParameter("@Enabled", OleDbType.Boolean)
					{
						Value = data
					}
				};
				result = base.SaveToAccess(oleDBString, cmdText, timeOut, parameters);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private bool GetData(string sqlString, int timeOut)
		{
			string cmdText = "SELECT mirroring_guid from sys.database_mirroring where database_id = DB_ID()";
			object obj = base.SQLExecuteScalar(sqlString, cmdText, timeOut);
			return obj != DBNull.Value;
		}
	}
}
