using System;

namespace ZhuanCloud.Collector
{
	internal class Index
	{
		public string IndexName
		{
			get;
			set;
		}

		public bool IsUnique
		{
			get;
			set;
		}

		public int IndexType
		{
			get;
			set;
		}

		public string TypeDesc
		{
			get;
			set;
		}

		public string ColName
		{
			get;
			set;
		}

		public bool IsDescCol
		{
			get;
			set;
		}

		public int Factor
		{
			get;
			set;
		}

		public bool HasFilter
		{
			get;
			set;
		}

		public string FilterText
		{
			get;
			set;
		}

		public int Ordinal
		{
			get;
			set;
		}

		public bool IsIncludedCol
		{
			get;
			set;
		}

		public bool HasPadIndex
		{
			get;
			set;
		}

		public bool IsRecompute
		{
			get;
			set;
		}

		public bool IsIgnorekey
		{
			get;
			set;
		}

		public bool HasRowLock
		{
			get;
			set;
		}

		public bool HasPageLock
		{
			get;
			set;
		}

		public string StoredType
		{
			get;
			set;
		}

		public string StoredName
		{
			get;
			set;
		}

		public bool IsDisabled
		{
			get;
			set;
		}
	}
}
