using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;

namespace ZhuanCloud.Collector
{
	public class Util
	{
		public static bool startFlag = false;

		public static string configDir = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\GreenTrend\\ZhuanCloud\\Collector";

		public static Dictionary<string, int> errDic = null;

		public static string collectDir = string.Empty;

		public static string oleDbString = string.Empty;

		public static string filePath = string.Empty;

		public static List<string> waitHashList = new List<string>();

		public static List<string> sqlHashList = new List<string>();

		public static List<string> moebiusHashList = new List<string>();

		public static List<string> unusedKeyList = new List<string>();

		public static List<string> unusedHashList = new List<string>();

		public static Dictionary<string, SqlPlanExec> planExecDic = new Dictionary<string, SqlPlanExec>();

		public static bool IsSysadmin(string conn)
		{
			string commandText = "SELECT IS_SRVROLEMEMBER('sysadmin') AS ISSYSADMIN";
			bool result = false;
			using (SqlConnection sqlConnection = new SqlConnection(conn))
			{
				using (SqlCommand sqlCommand = sqlConnection.CreateCommand())
				{
					sqlConnection.Open();
					sqlCommand.CommandText = commandText;
					using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
					{
						while (sqlDataReader.Read())
						{
							int num = (int)sqlDataReader["ISSYSADMIN"];
							result = (num > 0);
						}
					}
				}
			}
			return result;
		}

		public static bool TryGetInstance(string serverName, bool isWindowsAuthent, string userName, string passWord, out string instanceName)
		{
			SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder();
			sqlConnectionStringBuilder.DataSource = serverName;
			sqlConnectionStringBuilder.InitialCatalog = "master";
			sqlConnectionStringBuilder.IntegratedSecurity = isWindowsAuthent;
			sqlConnectionStringBuilder.ApplicationName = "ZhuanCloud";
			if (!sqlConnectionStringBuilder.IntegratedSecurity)
			{
				sqlConnectionStringBuilder.UserID = userName;
				sqlConnectionStringBuilder.Password = passWord;
			}
			string connectionString = sqlConnectionStringBuilder.ToString();
			try
			{
				using (SqlConnection sqlConnection = new SqlConnection(connectionString))
				{
					SqlCommand sqlCommand = sqlConnection.CreateCommand();
					sqlCommand.CommandText = "SELECT SERVERPROPERTY('ServerName') AS server_name";
					sqlConnection.Open();
					object obj = sqlCommand.ExecuteScalar();
					if (obj != null)
					{
						instanceName = obj.ToString();
						return true;
					}
				}
			}
			catch (Exception)
			{
			}
			instanceName = string.Empty;
			return false;
		}

		public static bool IsClustered(string conn)
		{
			try
			{
				string arg_05_0 = string.Empty;
				using (SqlConnection sqlConnection = new SqlConnection(conn))
				{
					SqlCommand sqlCommand = sqlConnection.CreateCommand();
					sqlCommand.CommandText = "SELECT SERVERPROPERTY('ISCLUSTERED') AS CLUSTER";
					sqlConnection.Open();
					object obj = sqlCommand.ExecuteScalar();
					if (obj != null && Convert.ToInt32(obj) == 1)
					{
						return true;
					}
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
			}
			return false;
		}

		public static bool IsLocalIP(string ipAddress)
		{
			List<NetworkInterface> list = new List<NetworkInterface>();
			NetworkInterface[] allNetworkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
			for (int i = 0; i < allNetworkInterfaces.Length; i++)
			{
				NetworkInterface networkInterface = allNetworkInterfaces[i];
				if (networkInterface.NetworkInterfaceType != NetworkInterfaceType.Loopback)
				{
					list.Add(networkInterface);
				}
			}
			if (list.Count > 0)
			{
				foreach (NetworkInterface current in list)
				{
					IPInterfaceProperties iPProperties = current.GetIPProperties();
					if (iPProperties != null)
					{
						UnicastIPAddressInformationCollection unicastAddresses = iPProperties.UnicastAddresses;
						if (unicastAddresses.Count > 0)
						{
							foreach (UnicastIPAddressInformation current2 in unicastAddresses)
							{
								if (current2.Address.ToString() == ipAddress)
								{
									return true;
								}
							}
						}
					}
				}
				return false;
			}
			return false;
		}

		public static string GetFileHashCode(string filePath)
		{
			if (File.Exists(filePath))
			{
				using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
				{
					MD5 mD = MD5.Create();
					byte[] inArray = mD.ComputeHash(fileStream);
					mD.Clear();
					return Convert.ToBase64String(inArray);
				}
			}
			return string.Empty;
		}

		public static bool IsBigTableOrView(string connStr, List<string> dbList, List<DBFilter> filterList, out string result)
		{
			result = string.Empty;
			try
			{
				if (dbList != null && dbList.Count > 0)
				{
					StringBuilder stringBuilder = new StringBuilder();
					int num = 1;
					foreach (string current in dbList)
					{
						stringBuilder.AppendFormat("SELECT  '{0}' AS [db_name] ,( SELECT  COUNT([object_id]) FROM  [{0}].sys.tables t,[{0}].sys.schemas s  WHERE t.[schema_id]=s.[schema_id]", current);
						string filterString = Util.GetFilterString(current, 1, filterList);
						if (!string.IsNullOrEmpty(filterString))
						{
							stringBuilder.Append(filterString);
						}
						stringBuilder.Append(") AS table_count ,");
						stringBuilder.AppendFormat("( SELECT  COUNT([object_id]) FROM  [{0}].sys.views t,[{0}].sys.schemas s  WHERE t.[schema_id]=s.[schema_id]", current);
						stringBuilder.Append(") AS view_count ");
						if (num < dbList.Count)
						{
							stringBuilder.Append(" UNION ");
						}
						num++;
					}
					using (SqlConnection sqlConnection = new SqlConnection(connStr))
					{
						SqlCommand sqlCommand = sqlConnection.CreateCommand();
						sqlCommand.CommandText = stringBuilder.ToString();
						sqlConnection.Open();
						SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
						StringBuilder stringBuilder2 = new StringBuilder();
						while (sqlDataReader.Read())
						{
							int num2 = 0;
							string value = sqlDataReader["db_name"].ToString();
							int num3 = Convert.ToInt32(sqlDataReader["table_count"]);
							if (num3 > 8000)
							{
								num2++;
							}
							int num4 = Convert.ToInt32(sqlDataReader["table_count"]);
							if (num4 > 8000)
							{
								num2++;
							}
							if (num2 == 1)
							{
								stringBuilder2.Append(value);
								stringBuilder2.AppendLine(" 数据库表");
							}
							else if (num2 == 2)
							{
								stringBuilder2.Append(value);
								stringBuilder2.AppendLine(" 数据库表、视图");
							}
						}
						if (stringBuilder2.Length > 0)
						{
							stringBuilder2.Append("数量过大，是否继续收集？");
							result = stringBuilder2.ToString();
							return true;
						}
					}
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
			}
			return false;
		}

		private static string GetFilterString(string dbName, int type, List<DBFilter> filterList)
		{
			if (filterList != null && filterList.Count > 0)
			{
				foreach (DBFilter current in filterList)
				{
					if (current.DBName == dbName && current.DBType == type)
					{
						string filterString = current.FilterString;
						if (string.IsNullOrEmpty(filterString))
						{
							string result = string.Empty;
							return result;
						}
						string[] array = filterString.Split(new string[]
						{
							";"
						}, StringSplitOptions.RemoveEmptyEntries);
						if (array.Length > 0)
						{
							StringBuilder stringBuilder = new StringBuilder();
							stringBuilder.Append(" AND ");
							int num = 1;
							string[] array2 = array;
							for (int i = 0; i < array2.Length; i++)
							{
								string text = array2[i];
								string[] array3 = text.Split(new string[]
								{
									"."
								}, StringSplitOptions.RemoveEmptyEntries);
								if (array3.Length == 1)
								{
									stringBuilder.AppendFormat("  t.name NOT LIKE '{0}' ", array3[0]);
								}
								else if (array3.Length == 2)
								{
									stringBuilder.AppendFormat(" s.name NOT LIKE '{0}' OR t.name NOT LIKE '{1}' ", array3[0], array3[1]);
								}
								if (num < array.Length)
								{
									stringBuilder.Append(" AND ");
								}
								num++;
							}
							string result = stringBuilder.ToString();
							return result;
						}
					}
				}
			}
			return string.Empty;
		}

		public static double CheckTableTimes(string connStr, List<string> dbList, List<DBFilter> filterList)
		{
			try
			{
				if (dbList != null && dbList.Count > 0)
				{
					StringBuilder stringBuilder = new StringBuilder();
					int num = 1;
					stringBuilder.Append("WITH    row_tmp   AS ( ");
					foreach (string current in dbList)
					{
						stringBuilder.AppendFormat("\r\nSELECT  SUM(row_count) AS row_count\r\nFROM    [{0}].sys.dm_db_partition_stats dps ,\r\n        [{0}].sys.tables t ,\r\n        [{0}].sys.schemas s\r\nWHERE   t.[schema_id] = s.[schema_id]\r\n        AND dps.[object_id] = t.[object_id]\r\n        AND dps.index_id < 2\r\n        AND t.is_ms_shipped = 0 \r\n", current);
						string filterString = Util.GetFilterString(current, 1, filterList);
						if (!string.IsNullOrEmpty(filterString))
						{
							stringBuilder.Append(filterString);
						}
						if (num < dbList.Count)
						{
							stringBuilder.Append(" UNION ");
						}
						num++;
					}
					stringBuilder.Append(") SELECT  SUM(row_count) AS sum_count  FROM  row_tmp");
					using (SqlConnection sqlConnection = new SqlConnection(connStr))
					{
						SqlCommand sqlCommand = sqlConnection.CreateCommand();
						sqlCommand.CommandText = stringBuilder.ToString();
						sqlConnection.Open();
						object obj = sqlCommand.ExecuteScalar();
						if (obj != null)
						{
							return Convert.ToDouble(obj);
						}
					}
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
			}
			return 0.0;
		}

		public static bool SaveErrorInfo(DateTime bugTime, string message)
		{
			if (string.IsNullOrEmpty(Util.oleDbString))
			{
				return false;
			}
			string commandText = "\r\nINSERT  INTO tblException\r\n        ( BugTime, [Message] )\r\nVALUES  ( @BugTime, @Message )\r\n";
			OleDbParameter[] values = new OleDbParameter[]
			{
				new OleDbParameter("@BugTime", OleDbType.Date)
				{
					Value = bugTime
				},
				new OleDbParameter("@Message", OleDbType.LongVarWChar)
				{
					Value = message
				}
			};
			OleDbConnection oleDbConnection = null;
			if (!Util.TryConnect(Util.oleDbString, out oleDbConnection))
			{
				return false;
			}
			bool result;
			try
			{
				OleDbCommand oleDbCommand = oleDbConnection.CreateCommand();
				oleDbCommand.CommandTimeout = 220;
				oleDbCommand.CommandText = commandText;
				oleDbCommand.Parameters.AddRange(values);
				oleDbConnection.Open();
				int num = oleDbCommand.ExecuteNonQuery();
				oleDbConnection.Close();
				result = (num > 0);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			finally
			{
				if (oleDbConnection.State != ConnectionState.Closed)
				{
					oleDbConnection.Close();
				}
				oleDbConnection.Dispose();
			}
			return result;
		}

		public static void GZipCompress(string s, string path)
		{
			byte[] bytes = TextManager.Encoding.GetBytes(s);
			try
			{
				FileStream stream = new FileStream(path, FileMode.Create);
				GZipStream gZipStream = new GZipStream(stream, CompressionMode.Compress, true);
				gZipStream.Write(bytes, 0, bytes.Length);
				gZipStream.Close();
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
			}
		}

		public static bool IsRootPath(string directory)
		{
			DriveInfo[] drives = DriveInfo.GetDrives();
			if (drives.Length > 0)
			{
				DriveInfo[] array = drives;
				for (int i = 0; i < array.Length; i++)
				{
					DriveInfo driveInfo = array[i];
					if (driveInfo.RootDirectory.FullName.Contains(directory))
					{
						return true;
					}
				}
			}
			return false;
		}

		public static bool TryConnect(string conString, out OleDbConnection con)
		{
			bool result;
			try
			{
				con = new OleDbConnection(conString);
				result = true;
			}
			catch (Exception ex)
			{
				ErrorLog.Write(new Exception(conString));
				ErrorLog.Write(ex);
				con = null;
				result = false;
			}
			return result;
		}

		public static bool DateIsNull(object date, out DateTime time)
		{
			time = new DateTime(1900, 1, 1);
			return date == null || date == DBNull.Value || !DateTime.TryParse(date.ToString(), out time);
		}

		public static bool ValueIsNull(object value)
		{
			return value == null || value == DBNull.Value;
		}

		public static DateTime ConvertToDate(object date)
		{
			DateTime result = new DateTime(1900, 1, 1);
			if (date == null || date == DBNull.Value)
			{
				return result;
			}
			if (string.IsNullOrEmpty(date.ToString()))
			{
				return result;
			}
			if (DateTime.TryParse(date.ToString(), out result))
			{
				return result;
			}
			return result;
		}
	}
}
