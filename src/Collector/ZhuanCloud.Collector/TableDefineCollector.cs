using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text;

namespace ZhuanCloud.Collector
{
	public class TableDefineCollector : OleDbCollector
	{
		private DataTable GetTableDefine(string conn, string dbName, DataSet tableSet, SQLServerVersion sqlVersion, int version, bool isDecrypt)
		{
			DataTable dataTable = new DataTable();
			dataTable.Columns.Add(new DataColumn("db_name"));
			dataTable.Columns.Add(new DataColumn("schema_name"));
			dataTable.Columns.Add(new DataColumn("object_name"));
			dataTable.Columns.Add(new DataColumn("table_rows"));
			dataTable.Columns.Add(new DataColumn("define"));
			dataTable.Columns.Add(new DataColumn("create_date"));
			dataTable.Columns.Add(new DataColumn("modify_date"));
			try
			{
				if (tableSet.Tables.Contains("IDs"))
				{
					DataTable dataTable2 = tableSet.Tables["IDs"];
					if (dataTable2.Rows.Count > 0)
					{
						for (int i = 0; i < dataTable2.Rows.Count; i++)
						{
							StringBuilder stringBuilder = new StringBuilder();
							string id = dataTable2.Rows[i].ItemArray[0].ToString();
							string text = dataTable2.Rows[i].ItemArray[1].ToString();
							string text2 = dataTable2.Rows[i].ItemArray[2].ToString();
							string filegroup = dataTable2.Rows[i].ItemArray[3].ToString();
							bool hasTextGroup = dataTable2.Rows[i].ItemArray[4] != DBNull.Value && Convert.ToInt32(dataTable2.Rows[i].ItemArray[4]) > 0;
							string textgroup = dataTable2.Rows[i].ItemArray[5].ToString();
							bool hasANSI = true;
							if (dataTable2.Rows[i].ItemArray[6] != DBNull.Value)
							{
								hasANSI = Convert.ToBoolean(dataTable2.Rows[i].ItemArray[6]);
							}
							bool hasQuoted = true;
							if (dataTable2.Rows[i].ItemArray[7] != DBNull.Value)
							{
								hasQuoted = Convert.ToBoolean(dataTable2.Rows[i].ItemArray[7]);
							}
							bool lockTypeHasChanged = false;
							string lockTypeText = string.Empty;
							if (sqlVersion >= SQLServerVersion.SQLServer2008)
							{
								lockTypeHasChanged = (dataTable2.Rows[i].ItemArray[8] != DBNull.Value && Convert.ToInt32(dataTable2.Rows[i].ItemArray[8]) > 0);
								lockTypeText = ((dataTable2.Rows[i].ItemArray[9] == DBNull.Value) ? string.Empty : dataTable2.Rows[i].ItemArray[9].ToString());
							}
							double num = Convert.ToDouble(dataTable2.Rows[i].ItemArray[10]);
							string text3 = DateTime.Parse(dataTable2.Rows[i].ItemArray[11].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
							string text4 = DateTime.Parse(dataTable2.Rows[i].ItemArray[12].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
							try
							{
								this.GetTableText(tableSet, stringBuilder, id, text, text2, filegroup, hasTextGroup, textgroup, hasANSI, hasQuoted, lockTypeHasChanged, lockTypeText, sqlVersion);
								if (!Util.startFlag)
								{
									DataTable result = new DataTable();
									return result;
								}
							}
							catch (Exception ex)
							{
								ErrorLog.Write(ex);
							}
							try
							{
								this.GetKeyText(tableSet, stringBuilder, id, text, text2);
								if (!Util.startFlag)
								{
									DataTable result = new DataTable();
									return result;
								}
							}
							catch (Exception ex2)
							{
								ErrorLog.Write(ex2);
							}
							try
							{
								this.GetIndexText(tableSet, stringBuilder, id, text, text2, sqlVersion);
								if (!Util.startFlag)
								{
									DataTable result = new DataTable();
									return result;
								}
							}
							catch (Exception ex3)
							{
								ErrorLog.Write(ex3);
							}
							try
							{
								this.GetTriggerText(tableSet, stringBuilder, id, text, text2, conn, dbName, version, isDecrypt);
								if (!Util.startFlag)
								{
									DataTable result = new DataTable();
									return result;
								}
							}
							catch (Exception ex4)
							{
								ErrorLog.Write(ex4);
							}
							string text5 = TextManager.Compress(stringBuilder.ToString());
							dataTable.Rows.Add(new object[]
							{
								dbName,
								text,
								text2,
								num,
								text5,
								text3,
								text4
							});
						}
					}
				}
				if (tableSet.Tables.Contains("FileTables"))
				{
					DataTable dataTable3 = tableSet.Tables["FileTables"];
					if (dataTable3.Rows.Count > 0)
					{
						for (int j = 0; j < dataTable3.Rows.Count; j++)
						{
							StringBuilder stringBuilder2 = new StringBuilder();
							string id2 = dataTable3.Rows[j].ItemArray[0].ToString();
							string text6 = dataTable3.Rows[j].ItemArray[1].ToString();
							string text7 = dataTable3.Rows[j].ItemArray[2].ToString();
							string filegroup2 = dataTable3.Rows[j].ItemArray[3].ToString();
							string filestream = dataTable3.Rows[j].ItemArray[4].ToString();
							string directoryName = dataTable3.Rows[j].ItemArray[5].ToString();
							string collateName = dataTable3.Rows[j].ItemArray[6].ToString();
							double num2 = Convert.ToDouble(dataTable3.Rows[j].ItemArray[7]);
							string text8 = DateTime.Parse(dataTable3.Rows[j].ItemArray[8].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
							string text9 = DateTime.Parse(dataTable3.Rows[j].ItemArray[9].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
							try
							{
								this.GetFileTableText(tableSet, stringBuilder2, id2, text6, text7, filegroup2, filestream, directoryName, collateName);
								if (!Util.startFlag)
								{
									DataTable result = new DataTable();
									return result;
								}
							}
							catch (Exception ex5)
							{
								ErrorLog.Write(ex5);
							}
							try
							{
								this.GetKeyText(tableSet, stringBuilder2, id2, text6, text7);
								if (!Util.startFlag)
								{
									DataTable result = new DataTable();
									return result;
								}
							}
							catch (Exception ex6)
							{
								ErrorLog.Write(ex6);
							}
							try
							{
								this.GetTriggerText(tableSet, stringBuilder2, id2, text6, text7, conn, dbName, version, isDecrypt);
								if (!Util.startFlag)
								{
									DataTable result = new DataTable();
									return result;
								}
							}
							catch (Exception ex7)
							{
								ErrorLog.Write(ex7);
							}
							string text10 = TextManager.Compress(stringBuilder2.ToString());
							dataTable.Rows.Add(new object[]
							{
								dbName,
								text6,
								text7,
								num2,
								text10,
								text8,
								text9
							});
						}
					}
				}
				if (tableSet.Tables.Contains("MemoryTables"))
				{
					DataTable dataTable4 = tableSet.Tables["MemoryTables"];
					if (dataTable4.Rows.Count > 0)
					{
						for (int k = 0; k < dataTable4.Rows.Count; k++)
						{
							StringBuilder stringBuilder3 = new StringBuilder();
							string id3 = dataTable4.Rows[k].ItemArray[0].ToString();
							string text11 = dataTable4.Rows[k].ItemArray[1].ToString();
							string text12 = dataTable4.Rows[k].ItemArray[2].ToString();
							string duraDesc = dataTable4.Rows[k].ItemArray[3].ToString();
							bool hasANSI2 = true;
							if (dataTable4.Rows[k].ItemArray[4] != DBNull.Value)
							{
								hasANSI2 = Convert.ToBoolean(dataTable4.Rows[k].ItemArray[4]);
							}
							bool hasQuoted2 = true;
							if (dataTable4.Rows[k].ItemArray[5] != DBNull.Value)
							{
								hasQuoted2 = Convert.ToBoolean(dataTable4.Rows[k].ItemArray[5]);
							}
							double num3 = Convert.ToDouble(dataTable4.Rows[k].ItemArray[6]);
							string text13 = DateTime.Parse(dataTable4.Rows[k].ItemArray[7].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
							string text14 = DateTime.Parse(dataTable4.Rows[k].ItemArray[8].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
							try
							{
								this.GetMemoryTableText(tableSet, stringBuilder3, id3, text11, text12, duraDesc, hasANSI2, hasQuoted2);
								if (!Util.startFlag)
								{
									DataTable result = new DataTable();
									return result;
								}
							}
							catch (Exception ex8)
							{
								ErrorLog.Write(ex8);
							}
							string text15 = TextManager.Compress(stringBuilder3.ToString());
							dataTable.Rows.Add(new object[]
							{
								dbName,
								text11,
								text12,
								num3,
								text15,
								text13,
								text14
							});
						}
					}
				}
			}
			catch (Exception ex9)
			{
				ErrorLog.Write(ex9);
				throw new Exception(ex9.Message);
			}
			return dataTable;
		}

		private void GetTableText(DataSet tableSet, StringBuilder tableSql, string id, string schema, string name, string filegroup, bool hasTextGroup, string textgroup, bool hasANSI, bool hasQuoted, bool lockTypeHasChanged, string lockTypeText, SQLServerVersion sqlVersion)
		{
			DataRow[] array = tableSet.Tables["Cols"].Select("objId=" + id);
			if (array.Length > 0)
			{
				tableSql.AppendLine(string.Format("SET ANSI_NULLS {0}", hasANSI ? "ON" : "OFF"));
				tableSql.AppendLine("GO");
				tableSql.AppendLine(string.Format("SET QUOTED_IDENTIFIER {0}", hasQuoted ? "ON" : "OFF"));
				tableSql.AppendLine("GO");
				tableSql.Append("CREATE TABLE ");
				tableSql.Append(string.Concat(new string[]
				{
					"[",
					schema,
					"].[",
					name,
					"]"
				}));
				tableSql.Append("(" + Environment.NewLine);
				for (int i = 0; i < array.Length; i++)
				{
					string str = array[i].ItemArray[2].ToString();
					tableSql.Append(new string(' ', 4));
					tableSql.Append("[" + str + "]");
					string text = array[i].ItemArray[3].ToString();
					if (!string.IsNullOrEmpty(text))
					{
						tableSql.Append(" AS " + text + " ," + Environment.NewLine);
					}
					else
					{
						bool flag = Convert.ToBoolean(array[i].ItemArray[4]);
						string text2 = array[i].ItemArray[18].ToString();
						if (flag)
						{
							string text3 = array[i].ItemArray[5].ToString();
							string text4 = array[i].ItemArray[6].ToString();
							tableSql.Append(string.Concat(new string[]
							{
								" [",
								text3,
								"].[",
								text4,
								"] "
							}));
						}
						else
						{
							string typeName = array[i].ItemArray[6].ToString();
							int maxLength = Convert.ToInt32(array[i].ItemArray[7]);
							int precision = Convert.ToInt32(array[i].ItemArray[8]);
							int scale = Convert.ToInt32(array[i].ItemArray[9]);
							string typeText = this.GetTypeText(typeName, maxLength, precision, scale);
							tableSql.Append(" " + typeText + " ");
							string text5 = array[i].ItemArray[10].ToString();
							if (string.IsNullOrEmpty(text2) && !string.IsNullOrEmpty(text5))
							{
								tableSql.Append("COLLATE " + text5 + " ");
							}
						}
						string text6 = array[i].ItemArray[11].ToString();
						if (!string.IsNullOrEmpty(text6))
						{
							tableSql.Append(text6 + " ");
						}
						if (array[i].ItemArray[16].ToString() == "1")
						{
							tableSql.Append("SPARSE ");
						}
						if (array[i].ItemArray[17].ToString() == "1")
						{
							tableSql.Append("COLUMN_SET FOR ALL_SPARSE_COLUMNS ");
						}
						if (!string.IsNullOrEmpty(text2))
						{
							tableSql.Append("DEFAULT " + text2 + " ");
						}
						string value = Convert.ToBoolean(array[i].ItemArray[12]) ? "NULL" : "NOT NULL";
						tableSql.Append(value);
						tableSql.Append("," + Environment.NewLine);
					}
				}
				tableSql.Remove(tableSql.Length - Environment.NewLine.Length - 1, 1);
				tableSql.Append(string.Format(") ON [{0}]", filegroup));
				if (hasTextGroup)
				{
					tableSql.AppendLine(string.Format(" TEXTIMAGE_ON [{0}]", textgroup));
				}
				tableSql.AppendLine();
				tableSql.AppendLine("GO");
				if (sqlVersion >= SQLServerVersion.SQLServer2008 && lockTypeHasChanged)
				{
					tableSql.AppendLine(string.Format("ALTER TABLE [{0}].[{1}] SET (LOCK_ESCALATION = {2})", schema, name, lockTypeText));
					tableSql.AppendLine("GO");
				}
			}
		}

		private void GetFileTableText(DataSet tableSet, StringBuilder tableSql, string id, string schema, string name, string filegroup, string filestream, string directoryName, string collateName)
		{
			tableSql.AppendLine("SET ANSI_NULLS ON");
			tableSql.AppendLine("SET QUOTED_IDENTIFIER ON");
			tableSql.AppendLine("SET ARITHABORT ON");
			tableSql.AppendLine("SET ANSI_PADDING ON");
			tableSql.AppendLine("GO");
			tableSql.AppendLine(string.Format("CREATE TABLE [{0}].[{1}] AS FILETABLE ON [{2}] FILESTREAM_ON [{3}]", new object[]
			{
				schema,
				name,
				filegroup,
				filestream
			}));
			tableSql.AppendLine("WITH");
			tableSql.AppendLine("(");
			tableSql.AppendLine(string.Format("    FILETABLE_DIRECTORY = N'{0}', FILETABLE_COLLATE_FILENAME = {1}", directoryName, collateName));
			tableSql.AppendLine(")");
			tableSql.AppendLine("GO");
		}

		private void GetMemoryTableText(DataSet tableSet, StringBuilder tableSql, string id, string schema, string name, string duraDesc, bool hasANSI, bool hasQuoted)
		{
			DataRow[] array = tableSet.Tables["Cols"].Select("objId=" + id);
			if (array.Length > 0)
			{
				tableSql.AppendLine(string.Format("SET ANSI_NULLS {0}", hasANSI ? "ON" : "OFF"));
				tableSql.AppendLine(string.Format("SET QUOTED_IDENTIFIER {0}", hasQuoted ? "ON" : "OFF"));
				tableSql.AppendLine("GO");
				tableSql.AppendLine(string.Format("CREATE TABLE [{0}].[{1}]", schema, name));
				tableSql.AppendLine("(");
				for (int i = 0; i < array.Length; i++)
				{
					string str = array[i].ItemArray[2].ToString();
					tableSql.Append(new string(' ', 4));
					tableSql.Append("[" + str + "]");
					string text = array[i].ItemArray[3].ToString();
					if (!string.IsNullOrEmpty(text))
					{
						tableSql.Append(" AS " + text + " ," + Environment.NewLine);
					}
					else
					{
						bool flag = Convert.ToBoolean(array[i].ItemArray[4]);
						if (flag)
						{
							string text2 = array[i].ItemArray[5].ToString();
							string text3 = array[i].ItemArray[6].ToString();
							tableSql.Append(string.Concat(new string[]
							{
								" [",
								text2,
								"].[",
								text3,
								"] "
							}));
						}
						else
						{
							string typeName = array[i].ItemArray[6].ToString();
							int maxLength = Convert.ToInt32(array[i].ItemArray[7]);
							int precision = Convert.ToInt32(array[i].ItemArray[8]);
							int scale = Convert.ToInt32(array[i].ItemArray[9]);
							string typeText = this.GetTypeText(typeName, maxLength, precision, scale);
							tableSql.Append(" " + typeText + " ");
						}
						string text4 = array[i].ItemArray[18].ToString();
						if (!string.IsNullOrEmpty(text4))
						{
							tableSql.Append("DEFAULT " + text4 + " ");
						}
						string value = Convert.ToBoolean(array[i].ItemArray[12]) ? "NULL" : "NOT NULL";
						tableSql.Append(value);
						tableSql.Append("," + Environment.NewLine);
					}
				}
				array = tableSet.Tables["HashIndexs"].Select("objId=" + id);
				if (array.Length > 0)
				{
					tableSql.AppendLine();
					Dictionary<string, List<HashIndex>> dictionary = new Dictionary<string, List<HashIndex>>();
					for (int j = 0; j < array.Length; j++)
					{
						string text5 = array[j].ItemArray[1].ToString();
						if (!dictionary.ContainsKey(text5))
						{
							dictionary.Add(text5, new List<HashIndex>());
						}
						dictionary[text5].Add(new HashIndex
						{
							IndexName = text5,
							IndexType = Convert.ToInt32(array[j].ItemArray[2]),
							IsPriKey = Convert.ToBoolean(array[j].ItemArray[3]),
							TypeDesc = array[j].ItemArray[4].ToString(),
							ColName = array[j].ItemArray[5].ToString(),
							IsDescCol = Convert.ToBoolean(array[j].ItemArray[6])
						});
					}
					if (dictionary.Count > 0)
					{
						foreach (KeyValuePair<string, List<HashIndex>> current in dictionary)
						{
							StringBuilder stringBuilder = new StringBuilder();
							if (current.Value[0].IsPriKey)
							{
								stringBuilder.AppendLine(string.Format("CONSTRAINT [{0}] PRIMARY KEY NONCLUSTERED", current.Key));
								stringBuilder.AppendLine("(");
								foreach (HashIndex current2 in current.Value)
								{
									stringBuilder.AppendLine(string.Format("    [{0}] {1},", current2.ColName, current2.IsDescCol ? "DESC" : "ASC"));
								}
								stringBuilder.Remove(stringBuilder.Length - Environment.NewLine.Length - 1, 1);
								stringBuilder.AppendLine("),");
							}
							else
							{
								stringBuilder.AppendLine(string.Format("INDEX [{0}] {1}", current.Key, current.Value[0].TypeDesc));
								stringBuilder.AppendLine("(");
								foreach (HashIndex current3 in current.Value)
								{
									stringBuilder.AppendLine(string.Format("    [{0}] {1},", current3.ColName, current3.IsDescCol ? "DESC" : "ASC"));
								}
								stringBuilder.Remove(stringBuilder.Length - Environment.NewLine.Length - 1, 1);
								stringBuilder.AppendLine(string.Format("){0},", (current.Value[0].IndexType == 7) ? "WITH ( BUCKET_COUNT = 128)" : string.Empty));
							}
							tableSql.AppendLine(stringBuilder.ToString());
						}
						tableSql.Remove(tableSql.Length - Environment.NewLine.Length * 2 - 1, 1 + Environment.NewLine.Length);
					}
				}
				else
				{
					tableSql = tableSql.Remove(tableSql.Length - Environment.NewLine.Length - 1, 1);
				}
				tableSql.AppendLine(string.Format(")WITH ( MEMORY_OPTIMIZED = ON , DURABILITY = {0} )", duraDesc));
				tableSql.AppendLine("GO");
			}
		}

		private void GetKeyText(DataSet tableSet, StringBuilder tableSql, string id, string schema, string name)
		{
			string value = string.Empty;
			DataRow[] array = tableSet.Tables["PKs"].Select("objId=" + id);
			if (array.Length > 0)
			{
				if (string.IsNullOrEmpty(value))
				{
					value = "----------------------------键----------------------------";
					tableSql.AppendLine(value);
				}
				Dictionary<string, List<PKCol>> dictionary = new Dictionary<string, List<PKCol>>();
				for (int i = 0; i < array.Length; i++)
				{
					string key = array[i].ItemArray[2].ToString();
					if (!dictionary.ContainsKey(key))
					{
						dictionary.Add(key, new List<PKCol>());
					}
					dictionary[key].Add(new PKCol
					{
						Type = array[i].ItemArray[1].ToString(),
						TypeDesc = array[i].ItemArray[3].ToString(),
						ColName = array[i].ItemArray[4].ToString(),
						IsDesc = Convert.ToBoolean(array[i].ItemArray[5]),
						HasPadIndex = Convert.ToBoolean(array[i].ItemArray[6]),
						IsRecompute = Convert.ToBoolean(array[i].ItemArray[7]),
						IsIgnorekey = Convert.ToBoolean(array[i].ItemArray[8]),
						HasRowLock = Convert.ToBoolean(array[i].ItemArray[9]),
						HasPageLock = Convert.ToBoolean(array[i].ItemArray[10]),
						FileGroupName = array[i].ItemArray[11].ToString(),
						HasANSINULL = Convert.ToBoolean(array[i].ItemArray[12].ToString())
					});
				}
				foreach (KeyValuePair<string, List<PKCol>> current in dictionary)
				{
					if (current.Value[0].Type == "PK")
					{
						tableSql.AppendLine(string.Format("ALTER TABLE [{0}].[{1}] ADD CONSTRAINT [{2}] PRIMARY KEY {3}", new object[]
						{
							schema,
							name,
							current.Key,
							current.Value[0].TypeDesc
						}));
					}
					else
					{
						if (current.Value[0].HasANSINULL)
						{
							tableSql.AppendLine("SET ANSI_PADDING ON");
						}
						tableSql.AppendLine(string.Format("ALTER TABLE [{0}].[{1}] ADD CONSTRAINT [{2}] UNIQUE {3}", new object[]
						{
							schema,
							name,
							current.Key,
							current.Value[0].TypeDesc
						}));
					}
					tableSql.AppendLine("(");
					foreach (PKCol current2 in current.Value)
					{
						tableSql.AppendLine(string.Format("    [{0}] {1},", current2.ColName, current2.IsDesc ? "DESC" : "ASC"));
					}
					tableSql.Remove(tableSql.Length - Environment.NewLine.Length - 1, 1);
					tableSql.AppendLine(string.Format(")WITH (PAD_INDEX = {0}, STATISTICS_NORECOMPUTE = {1},SORT_IN_TEMPDB = OFF,IGNORE_DUP_KEY = {2},ONLINE = OFF,ALLOW_ROW_LOCKS = {3}, ALLOW_PAGE_LOCKS = {4}) ON [{5}]", new object[]
					{
						current.Value[0].HasPadIndex ? "ON" : "OFF",
						current.Value[0].IsRecompute ? "ON" : "OFF",
						current.Value[0].IsIgnorekey ? "ON" : "OFF",
						current.Value[0].HasRowLock ? "ON" : "OFF",
						current.Value[0].HasPageLock ? "ON" : "OFF",
						current.Value[0].FileGroupName
					}));
					tableSql.AppendLine("GO");
				}
			}
			array = tableSet.Tables["FKs"].Select("objId=" + id);
			if (array.Length > 0)
			{
				if (string.IsNullOrEmpty(value))
				{
					value = "----------------------------键----------------------------";
					tableSql.AppendLine(value);
				}
				Dictionary<string, List<FKCol>> dictionary2 = new Dictionary<string, List<FKCol>>();
				for (int j = 0; j < array.Length; j++)
				{
					string key2 = array[j].ItemArray[1].ToString();
					if (!dictionary2.ContainsKey(key2))
					{
						dictionary2.Add(key2, new List<FKCol>());
					}
					dictionary2[key2].Add(new FKCol
					{
						FKName = array[j].ItemArray[1].ToString(),
						IsDisable = Convert.ToBoolean(array[j].ItemArray[2]),
						FKColName = array[j].ItemArray[3].ToString(),
						RFSchema = array[j].ItemArray[4].ToString(),
						RFName = array[j].ItemArray[5].ToString(),
						RFColName = array[j].ItemArray[6].ToString()
					});
				}
				foreach (KeyValuePair<string, List<FKCol>> current3 in dictionary2)
				{
					tableSql.Append(string.Format("ALTER TABLE [{0}].[{1}] ADD CONSTRAINT [{2}] FOREIGN KEY(", schema, name, current3.Key));
					StringBuilder stringBuilder = new StringBuilder();
					StringBuilder stringBuilder2 = new StringBuilder();
					foreach (FKCol current4 in current3.Value)
					{
						stringBuilder.Append("[" + current4.FKColName + "],");
						stringBuilder2.Append("[" + current4.RFColName + "],");
					}
					stringBuilder.Remove(stringBuilder.Length - 1, 1);
					stringBuilder2.Remove(stringBuilder2.Length - 1, 1);
					tableSql.Append(stringBuilder.ToString());
					tableSql.AppendLine(")");
					tableSql.AppendLine(string.Format("REFERENCES [{0}].[{1}] ({2})", current3.Value[0].RFSchema, current3.Value[0].RFName, stringBuilder2.ToString()));
					tableSql.AppendLine("GO");
					if (current3.Value[0].IsDisable)
					{
						tableSql.AppendLine(string.Format("ALTER TABLE [{0}].[{1}] NOCHECK CONSTRAINT {2}", schema, name, current3.Value[0].FKName));
						tableSql.AppendLine("GO");
					}
				}
			}
		}

		private void GetTriggerText(DataSet tableSet, StringBuilder tableSql, string id, string schema, string name, string conn, string dbName, int version, bool isDecrypt)
		{
			DataRow[] array = tableSet.Tables["Triggers"].Select("objId=" + id);
			if (array.Length > 0)
			{
				tableSql.AppendLine("----------------------------触发器----------------------------");
				int i = 0;
				while (i < array.Length)
				{
					tableSql.AppendLine("-- =============================================");
					string arg = DateTime.Parse(array[i].ItemArray[9].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
					tableSql.AppendLine(string.Format("-- Trigger Create date: {0}", arg));
					string arg2 = DateTime.Parse(array[i].ItemArray[10].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
					tableSql.AppendLine(string.Format("-- Trigger Modify date: {0}", arg2));
					tableSql.AppendLine("-- =============================================");
					if (array[i].ItemArray[7] != DBNull.Value)
					{
						bool flag = Convert.ToBoolean(array[i].ItemArray[7]);
						if (flag)
						{
							tableSql.AppendLine("SET ANSI_NULLS ON");
							tableSql.AppendLine("GO");
						}
					}
					if (array[i].ItemArray[8] != DBNull.Value)
					{
						bool flag2 = Convert.ToBoolean(array[i].ItemArray[8]);
						if (flag2)
						{
							tableSql.AppendLine("SET QUOTED_IDENTIFIER ON");
							tableSql.AppendLine("GO");
						}
					}
					string text = array[i].ItemArray[1].ToString();
					string a = array[i].ItemArray[2].ToString().Trim().ToUpper();
					string text2 = array[i].ItemArray[4].ToString();
					string text3 = array[i].ItemArray[5].ToString();
					string value = string.Empty;
					if (!(a == "TR"))
					{
						value = "-- CLR 触发器，无法解密";
						goto IL_217;
					}
					value = array[i].ItemArray[3].ToString();
					if (string.IsNullOrEmpty(value))
					{
						if (isDecrypt && !text3.StartsWith("MoebiusTrigger_"))
						{
							try
							{
								value = Engine.Decrypt(conn, dbName, text, version);
								goto IL_217;
							}
							catch (Exception innerException)
							{
								value = "-- 对象无法解密";
								ErrorLog.Write(new Exception(string.Format("db_name:{0},table_name:{1}.{2}", dbName, schema, name), innerException));
								goto IL_39E;
							}
						}
						value = "-- 对象不允许解密";
						goto IL_217;
					}
					goto IL_217;
					IL_39E:
					i++;
					continue;
					IL_217:
					tableSql.AppendLine(value);
					tableSql.AppendLine("GO");
					DataRow[] array2 = tableSet.Tables["TriggerOrder"].Select("object_id=" + text);
					if (array2.Length > 0)
					{
						DataRow[] array3 = array2;
						for (int j = 0; j < array3.Length; j++)
						{
							DataRow dataRow = array3[j];
							bool flag3 = false;
							bool flag4 = false;
							string text4 = dataRow.ItemArray[1].ToString();
							string text5 = string.Empty;
							if (dataRow.ItemArray[2] != DBNull.Value)
							{
								flag3 = Convert.ToBoolean(dataRow.ItemArray[2]);
							}
							if (dataRow.ItemArray[3] != DBNull.Value)
							{
								flag4 = Convert.ToBoolean(dataRow.ItemArray[3]);
							}
							if (flag3)
							{
								text5 = "First";
							}
							else if (flag4)
							{
								text5 = "Last";
							}
							else
							{
								text5 = "None";
							}
							tableSql.AppendLine(string.Format("EXEC sp_settriggerorder @triggername=N'[{0}].[{1}]', @order=N'{2}', @stmttype=N'{3}'", new object[]
							{
								text2,
								text3,
								text5,
								text4
							}));
							tableSql.AppendLine("GO");
						}
					}
					string text6 = Convert.ToBoolean(array[i].ItemArray[6]) ? "DISABLE" : "ENABLE";
					tableSql.AppendLine(string.Format("{0} TRIGGER [{1}].[{2}] ON [{3}].[{4}]", new object[]
					{
						text6,
						text2,
						text3,
						schema,
						name
					}));
					tableSql.AppendLine("GO");
					goto IL_39E;
				}
			}
		}

		private void GetIndexText(DataSet tableSet, StringBuilder tableSql, string id, string schema, string name, SQLServerVersion sqlVersion)
		{
			DataRow[] array = tableSet.Tables["ClusIndexs"].Select("objId=" + id);
			string value = string.Empty;
			if (array.Length > 0)
			{
				value = "----------------------------索引----------------------------";
				tableSql.AppendLine(value);
				Dictionary<string, List<Index>> dictionary = new Dictionary<string, List<Index>>();
				for (int i = 0; i < array.Length; i++)
				{
					string key = array[i].ItemArray[1].ToString();
					if (!dictionary.ContainsKey(key))
					{
						dictionary.Add(key, new List<Index>());
					}
					Index index = new Index();
					index.IsUnique = Convert.ToBoolean(array[i].ItemArray[2]);
					index.IndexType = Convert.ToInt32(array[i].ItemArray[3]);
					index.TypeDesc = array[i].ItemArray[4].ToString();
					index.ColName = array[i].ItemArray[5].ToString();
					index.IsDescCol = Convert.ToBoolean(array[i].ItemArray[6]);
					index.Factor = Convert.ToInt32(array[i].ItemArray[7]);
					index.Ordinal = Convert.ToInt32(array[i].ItemArray[8]);
					index.IsIncludedCol = Convert.ToBoolean(array[i].ItemArray[9]);
					index.HasPadIndex = Convert.ToBoolean(array[i].ItemArray[10]);
					index.IsRecompute = Convert.ToBoolean(array[i].ItemArray[11]);
					index.IsIgnorekey = Convert.ToBoolean(array[i].ItemArray[12]);
					index.HasRowLock = Convert.ToBoolean(array[i].ItemArray[13]);
					index.HasPageLock = Convert.ToBoolean(array[i].ItemArray[14]);
					index.StoredType = array[i].ItemArray[15].ToString();
					index.StoredName = array[i].ItemArray[16].ToString();
					index.IsDisabled = Convert.ToBoolean(array[i].ItemArray[17]);
					if (sqlVersion >= SQLServerVersion.SQLServer2008)
					{
						index.HasFilter = Convert.ToBoolean(array[i].ItemArray[18]);
						index.FilterText = array[i].ItemArray[19].ToString();
					}
					dictionary[key].Add(index);
				}
				foreach (KeyValuePair<string, List<Index>> current in dictionary)
				{
					tableSql.Append("CREATE ");
					string text = string.Empty;
					if (current.Value[0].IsUnique)
					{
						tableSql.Append("UNIQUE ");
						text = string.Format(" IGNORE_DUP_KEY={0}, ", current.Value[0].IsIgnorekey ? "ON" : "OFF");
					}
					tableSql.Append(current.Value[0].TypeDesc + " ");
					tableSql.AppendLine(string.Format("INDEX [{0}] ON [{1}].[{2}]", current.Key, schema, name));
					tableSql.AppendLine("(");
					StringBuilder stringBuilder = new StringBuilder();
					StringBuilder stringBuilder2 = new StringBuilder();
					foreach (Index current2 in current.Value)
					{
						if (current2.IsIncludedCol)
						{
							stringBuilder.AppendLine(string.Format("    [{0}],", current2.ColName));
						}
						else
						{
							stringBuilder2.AppendLine(string.Format("    [{0}] {1},", current2.ColName, current2.IsDescCol ? "DESC" : "ASC"));
						}
					}
					stringBuilder2.Remove(stringBuilder2.Length - Environment.NewLine.Length - 1, Environment.NewLine.Length + 1);
					tableSql.AppendLine(stringBuilder2.ToString());
					tableSql.AppendLine(")");
					if (stringBuilder.Length > 0)
					{
						stringBuilder.Remove(stringBuilder.Length - Environment.NewLine.Length - 1, Environment.NewLine.Length + 1);
						tableSql.AppendLine("INCLUDE ");
						tableSql.AppendLine("(");
						tableSql.AppendLine(stringBuilder.ToString());
						tableSql.AppendLine(")");
					}
					if (sqlVersion >= SQLServerVersion.SQLServer2008 && current.Value[0].HasFilter)
					{
						tableSql.AppendLine("WHERE " + current.Value[0].FilterText);
					}
					string text2 = (current.Value[0].Factor > 0) ? (", FILLFACTOR = " + current.Value[0].Factor.ToString()) : string.Empty;
					tableSql.AppendLine(string.Format("WITH (PAD_INDEX = {0}, STATISTICS_NORECOMPUTE = {1}, SORT_IN_TEMPDB = OFF,{2} DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = {3}, ALLOW_PAGE_LOCKS = {4}{5}) ", new object[]
					{
						current.Value[0].HasPadIndex ? "ON" : "OFF",
						current.Value[0].IsRecompute ? "ON" : "OFF",
						text,
						current.Value[0].HasRowLock ? "ON" : "OFF",
						current.Value[0].HasPageLock ? "ON" : "OFF",
						text2
					}));
					tableSql.AppendLine("GO");
					if (current.Value[0].IsDisabled)
					{
						tableSql.AppendLine(string.Format("ALTER INDEX [{0}] ON [{1}].[{2}] DISABLE", current.Key, schema, name));
						tableSql.AppendLine("GO");
					}
				}
			}
			array = tableSet.Tables["XMLIndexs"].Select("objId=" + id);
			if (array.Length > 0)
			{
				if (string.IsNullOrEmpty(value))
				{
					value = "----------------------------索引----------------------------";
					tableSql.AppendLine(value);
				}
				tableSql.AppendLine("SET ARITHABORT ON");
				tableSql.AppendLine("SET CONCAT_NULL_YIELDS_NULL ON");
				tableSql.AppendLine("SET QUOTED_IDENTIFIER ON");
				tableSql.AppendLine("SET ANSI_NULLS ON");
				tableSql.AppendLine("SET ANSI_PADDING ON");
				tableSql.AppendLine("SET ANSI_WARNINGS ON");
				tableSql.AppendLine("SET NUMERIC_ROUNDABORT OFF");
				tableSql.AppendLine("GO");
				Dictionary<string, List<XMLIndex>> dictionary2 = new Dictionary<string, List<XMLIndex>>();
				for (int j = 0; j < array.Length; j++)
				{
					string key2 = array[j].ItemArray[1].ToString();
					if (!dictionary2.ContainsKey(key2))
					{
						dictionary2.Add(key2, new List<XMLIndex>());
					}
					XMLIndex xMLIndex = new XMLIndex();
					xMLIndex.PriName = array[j].ItemArray[2].ToString();
					xMLIndex.SecondTypeDesc = array[j].ItemArray[3].ToString();
					xMLIndex.ColName = array[j].ItemArray[4].ToString();
					xMLIndex.Factor = Convert.ToInt32(array[j].ItemArray[5]);
					xMLIndex.HasPadIndex = Convert.ToBoolean(array[j].ItemArray[6]);
					xMLIndex.IsIgnorekey = Convert.ToBoolean(array[j].ItemArray[7]);
					xMLIndex.HasRowLock = Convert.ToBoolean(array[j].ItemArray[8]);
					xMLIndex.HasPageLock = Convert.ToBoolean(array[j].ItemArray[9]);
					xMLIndex.IsDisabled = Convert.ToBoolean(array[j].ItemArray[10]);
					if (sqlVersion >= SQLServerVersion.SQLServer2014)
					{
						xMLIndex.IndexType = Convert.ToInt32(array[j].ItemArray[11]);
					}
					dictionary2[key2].Add(xMLIndex);
				}
				foreach (KeyValuePair<string, List<XMLIndex>> current3 in dictionary2)
				{
					tableSql.Append("CREATE ");
					string value2 = string.Empty;
					string arg_7E2_0 = string.Empty;
					if (sqlVersion >= SQLServerVersion.SQLServer2014)
					{
						if (current3.Value[0].IndexType == 0)
						{
							tableSql.Append("PRIMARY XML ");
						}
						else
						{
							string arg_81C_0 = current3.Value[0].PriName;
							value2 = current3.Value[0].SecondTypeDesc;
						}
					}
					else
					{
						tableSql.Append("PRIMARY XML ");
					}
					tableSql.AppendLine(string.Format("Index [{0}] ON [{1}].[{2}]", current3.Key, schema, name));
					tableSql.AppendLine("(");
					StringBuilder stringBuilder3 = new StringBuilder();
					foreach (XMLIndex current4 in current3.Value)
					{
						stringBuilder3.AppendLine(string.Format("    [{0}],", current4.ColName));
					}
					stringBuilder3.Remove(stringBuilder3.Length - Environment.NewLine.Length - 1, Environment.NewLine.Length + 1);
					tableSql.AppendLine(stringBuilder3.ToString());
					tableSql.AppendLine(")");
					if (!string.IsNullOrEmpty(value2))
					{
						tableSql.Append(string.Format("USING XML INDEX [{0}] FOR {1} ", current3.Value[0].PriName, current3.Value[0].SecondTypeDesc));
					}
					string text3 = (current3.Value[0].Factor > 0) ? (", FILLFACTOR = " + current3.Value[0].Factor.ToString()) : string.Empty;
					tableSql.AppendLine(string.Format(")WITH (PAD_INDEX = {0}, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = {1}, ALLOW_PAGE_LOCKS = {2}{3}) ", new object[]
					{
						current3.Value[0].HasPadIndex ? "ON" : "OFF",
						current3.Value[0].HasRowLock ? "ON" : "OFF",
						current3.Value[0].HasPageLock ? "ON" : "OFF",
						text3
					}));
					tableSql.AppendLine("GO");
					if (current3.Value[0].IsDisabled)
					{
						tableSql.AppendLine(string.Format("ALTER INDEX [{0}] ON [{1}].[{2}] DISABLE", current3.Key, schema, name));
						tableSql.AppendLine("GO");
					}
				}
			}
			if (tableSet.Tables.Contains("SpatialIndexs"))
			{
				array = tableSet.Tables["SpatialIndexs"].Select("objId=" + id);
				if (array.Length > 0)
				{
					if (string.IsNullOrEmpty(value))
					{
						value = "----------------------------索引----------------------------";
						tableSql.AppendLine(value);
					}
					tableSql.AppendLine("SET ARITHABORT ON");
					tableSql.AppendLine("SET CONCAT_NULL_YIELDS_NULL ON");
					tableSql.AppendLine("SET QUOTED_IDENTIFIER ON");
					tableSql.AppendLine("SET ANSI_NULLS ON");
					tableSql.AppendLine("SET ANSI_PADDING ON");
					tableSql.AppendLine("SET ANSI_WARNINGS ON");
					tableSql.AppendLine("SET NUMERIC_ROUNDABORT OFF");
					tableSql.AppendLine("GO");
					Dictionary<string, List<SpatialIndex>> dictionary3 = new Dictionary<string, List<SpatialIndex>>();
					for (int k = 0; k < array.Length; k++)
					{
						string key3 = array[k].ItemArray[1].ToString();
						if (!dictionary3.ContainsKey(key3))
						{
							dictionary3.Add(key3, new List<SpatialIndex>());
						}
						dictionary3[key3].Add(new SpatialIndex
						{
							ColName = array[k].ItemArray[2].ToString(),
							IndexType = Convert.ToInt32(array[k].ItemArray[3]),
							TesseSchme = array[k].ItemArray[4].ToString(),
							Xmin = ((array[k].ItemArray[5] == DBNull.Value) ? 0 : Convert.ToInt32(array[k].ItemArray[5])),
							Ymin = ((array[k].ItemArray[6] == DBNull.Value) ? 0 : Convert.ToInt32(array[k].ItemArray[6])),
							Xmax = ((array[k].ItemArray[7] == DBNull.Value) ? 0 : Convert.ToInt32(array[k].ItemArray[7])),
							Ymax = ((array[k].ItemArray[8] == DBNull.Value) ? 0 : Convert.ToInt32(array[k].ItemArray[8])),
							Level_1 = array[k].ItemArray[9].ToString(),
							Level_2 = array[k].ItemArray[10].ToString(),
							Level_3 = array[k].ItemArray[11].ToString(),
							Level_4 = array[k].ItemArray[12].ToString(),
							Cells = Convert.ToInt32(array[k].ItemArray[13]),
							Factor = Convert.ToInt32(array[k].ItemArray[14]),
							HasPadIndex = Convert.ToBoolean(array[k].ItemArray[15]),
							IsIgnorekey = Convert.ToBoolean(array[k].ItemArray[16]),
							HasRowLock = Convert.ToBoolean(array[k].ItemArray[17]),
							HasPageLock = Convert.ToBoolean(array[k].ItemArray[18]),
							FileGroupName = array[k].ItemArray[19].ToString(),
							IsDisabled = Convert.ToBoolean(array[k].ItemArray[20])
						});
					}
					foreach (KeyValuePair<string, List<SpatialIndex>> current5 in dictionary3)
					{
						tableSql.AppendLine(string.Format("CREATE SPATIAL INDEX [{0}] ON [{1}].[{2}]", current5.Key, schema, name));
						tableSql.AppendLine("(");
						StringBuilder stringBuilder4 = new StringBuilder();
						foreach (SpatialIndex current6 in current5.Value)
						{
							stringBuilder4.AppendLine(string.Format("    [{0}],", current6.ColName));
						}
						stringBuilder4.Remove(stringBuilder4.Length - Environment.NewLine.Length - 1, Environment.NewLine.Length + 1);
						tableSql.AppendLine(stringBuilder4.ToString());
						tableSql.AppendLine(string.Format(")USING {0}", current5.Value[0].TesseSchme));
						string text4 = (current5.Value[0].IndexType == 1) ? string.Format("BOUNDING_BOX =({0}, {1}, {2}, {3}), ", new object[]
						{
							current5.Value[0].Xmin,
							current5.Value[0].Ymin,
							current5.Value[0].Xmax,
							current5.Value[0].Ymax
						}) : string.Empty;
						tableSql.AppendLine(string.Format("WITH ({0}GRIDS =(LEVEL_1 = {1},LEVEL_2 = {2},LEVEL_3 = {3},LEVEL_4 = {4}),", new object[]
						{
							text4,
							current5.Value[0].Level_1,
							current5.Value[0].Level_2,
							current5.Value[0].Level_3,
							current5.Value[0].Level_4
						}));
						string text5 = (current5.Value[0].Factor > 0) ? (", FILLFACTOR = " + current5.Value[0].Factor.ToString()) : string.Empty;
						tableSql.AppendLine(string.Format("CELLS_PER_OBJECT = {0}, PAD_INDEX = {1}, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = {2}, ALLOW_PAGE_LOCKS = {3}{4}) ON [{5}]", new object[]
						{
							current5.Value[0].Cells,
							current5.Value[0].HasPadIndex ? "ON" : "OFF",
							current5.Value[0].HasRowLock ? "ON" : "OFF",
							current5.Value[0].HasPageLock ? "ON" : "OFF",
							text5,
							current5.Value[0].FileGroupName
						}));
						tableSql.AppendLine("GO");
						if (current5.Value[0].IsDisabled)
						{
							tableSql.AppendLine(string.Format("ALTER INDEX [{0}] ON [{1}].[{2}] DISABLE", current5.Key, schema, name));
							tableSql.AppendLine("GO");
						}
					}
				}
			}
			array = tableSet.Tables["ColumnStoredIndexs"].Select("objId=" + id);
			if (array.Length > 0)
			{
				if (string.IsNullOrEmpty(value))
				{
					value = "----------------------------索引----------------------------";
					tableSql.AppendLine(value);
				}
				tableSql.AppendLine("SET ANSI_PADDING ON");
				tableSql.AppendLine("GO");
				Dictionary<string, List<ColumnStoredIndex>> dictionary4 = new Dictionary<string, List<ColumnStoredIndex>>();
				for (int l = 0; l < array.Length; l++)
				{
					string key4 = array[l].ItemArray[1].ToString();
					if (!dictionary4.ContainsKey(key4))
					{
						dictionary4.Add(key4, new List<ColumnStoredIndex>());
					}
					dictionary4[key4].Add(new ColumnStoredIndex
					{
						IndexType = Convert.ToInt32(array[l].ItemArray[2]),
						TypeDesc = array[l].ItemArray[3].ToString(),
						ColName = array[l].ItemArray[4].ToString(),
						FileGroupName = array[l].ItemArray[5].ToString(),
						IsDisabled = Convert.ToBoolean(array[l].ItemArray[6])
					});
				}
				foreach (KeyValuePair<string, List<ColumnStoredIndex>> current7 in dictionary4)
				{
					tableSql.AppendLine(string.Format("CREATE {0} INDEX [{1}] ON [{2}].[{3}]", new object[]
					{
						current7.Value[0].TypeDesc,
						current7.Key,
						schema,
						name
					}));
					if (current7.Value[0].IndexType == 6)
					{
						tableSql.AppendLine("(");
						StringBuilder stringBuilder5 = new StringBuilder();
						foreach (ColumnStoredIndex current8 in current7.Value)
						{
							stringBuilder5.AppendLine(string.Format("    [{0}],", current8.ColName));
						}
						stringBuilder5.Remove(stringBuilder5.Length - Environment.NewLine.Length - 1, Environment.NewLine.Length + 1);
						tableSql.AppendLine(stringBuilder5.ToString());
						tableSql.Append(")");
					}
					tableSql.AppendLine(string.Format("WITH (DROP_EXISTING = ON, DATA_COMPRESSION = COLUMNSTORE) ON [{0}]", current7.Value[0].FileGroupName));
					tableSql.AppendLine("GO");
					if (current7.Value[0].IsDisabled)
					{
						tableSql.AppendLine(string.Format("ALTER INDEX [{0}] ON [{1}].[{2}] DISABLE", current7.Key, schema, name));
						tableSql.AppendLine("GO");
					}
				}
			}
		}

		public override bool SaveData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut, string condition, bool isDecrypt)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(sqlString, sqlVersion, timeOut, condition, isDecrypt);
				if (data.Rows.Count > 0)
				{
					if (!Util.startFlag)
					{
						result = false;
					}
					else
					{
						string cmdText = "\r\nINSERT  INTO tblDBTable\r\n        ( DBName ,\r\n          SchemaName ,\r\n          ObjectName ,\r\n          TableRows ,\r\n          Define ,\r\n          CreateDate ,\r\n          ModifyDate\r\n        )\r\nVALUES  ( @DBName ,\r\n          @SchemaName ,\r\n          @ObjectName ,\r\n          @TableRows ,\r\n          @Define ,\r\n          @CreateDate ,\r\n          @ModifyDate\r\n        )\r\n";
						OleDbParameter[] parameters = new OleDbParameter[]
						{
							new OleDbParameter("@DBName", OleDbType.VarChar)
							{
								SourceColumn = "db_name"
							},
							new OleDbParameter("@SchemaName", OleDbType.VarChar)
							{
								SourceColumn = "schema_name"
							},
							new OleDbParameter("@ObjectName", OleDbType.VarChar)
							{
								SourceColumn = "object_name"
							},
							new OleDbParameter("@TableRows", OleDbType.Double)
							{
								SourceColumn = "table_rows"
							},
							new OleDbParameter("@Define", OleDbType.VarChar)
							{
								SourceColumn = "define"
							},
							new OleDbParameter("@CreateDate", OleDbType.Date)
							{
								SourceColumn = "create_date"
							},
							new OleDbParameter("@ModifyDate", OleDbType.Date)
							{
								SourceColumn = "modify_date"
							}
						};
						result = base.SaveToAccess(oleDBString, data, cmdText, parameters);
					}
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, SQLServerVersion sqlVersion, int timeOut, string condition, bool isDecrypt)
		{
			DataSet tableSet = new DataSet();
			int majorVersion = base.GetMajorVersion(sqlVersion);
			SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder(sqlString);
			string initialCatalog = sqlConnectionStringBuilder.InitialCatalog;
			this.GetTableInfo(tableSet, sqlString, sqlVersion, timeOut, condition);
			if (!Util.startFlag)
			{
				return new DataTable();
			}
			if (sqlVersion >= SQLServerVersion.SQLServer2012)
			{
				this.GetFileTableInfo(tableSet, sqlString, timeOut, condition);
			}
			if (sqlVersion >= SQLServerVersion.SQLServer2014)
			{
				this.GetMemoryTableInfo(tableSet, sqlString, timeOut, condition);
			}
			this.GetColumns(tableSet, sqlString, sqlVersion, timeOut);
			if (!Util.startFlag)
			{
				return new DataTable();
			}
			this.GetPriKeys(tableSet, sqlString, timeOut);
			if (!Util.startFlag)
			{
				return new DataTable();
			}
			this.GetForeignKeys(tableSet, sqlString, timeOut);
			if (!Util.startFlag)
			{
				return new DataTable();
			}
			this.GeTTriggers(tableSet, sqlString, timeOut);
			if (!Util.startFlag)
			{
				return new DataTable();
			}
			this.GeTTriggerOrder(tableSet, sqlString, timeOut);
			if (!Util.startFlag)
			{
				return new DataTable();
			}
			this.GetClusIndexes(tableSet, sqlString, sqlVersion, timeOut);
			if (!Util.startFlag)
			{
				return new DataTable();
			}
			this.GetXMLIndexes(tableSet, sqlString, sqlVersion, timeOut);
			if (!Util.startFlag)
			{
				return new DataTable();
			}
			if (sqlVersion >= SQLServerVersion.SQLServer2008)
			{
				this.GetSpatialIndexs(tableSet, sqlString, timeOut);
			}
			if (!Util.startFlag)
			{
				return new DataTable();
			}
			this.GetColumnStoredIndexs(tableSet, sqlString, timeOut);
			if (!Util.startFlag)
			{
				return new DataTable();
			}
			this.GetHashIndexes(tableSet, sqlString, timeOut);
			if (!Util.startFlag)
			{
				return new DataTable();
			}
			return this.GetTableDefine(sqlString, initialCatalog, tableSet, sqlVersion, majorVersion, isDecrypt);
		}

		private void GetTableInfo(DataSet tableSet, string sqlString, SQLServerVersion sqlVersion, int timeOut, string condition)
		{
			string str = string.Empty;
			if (sqlVersion >= SQLServerVersion.SQLServer2014)
			{
				str = "\r\nSELECT DISTINCT\r\n        t.[object_id] AS objId ,\r\n        s.name AS SchemaName ,\r\n        t.name AS TableName ,\r\n        fg.name AS FileGroupName ,\r\n        t.lob_data_space_id AS TGid ,\r\n        CASE t.lob_data_space_id\r\n          WHEN 0 THEN dg.name\r\n          ELSE tg.name\r\n        END AS TextGroupName ,\r\n        t.uses_ansi_nulls AS ANSI ,\r\n        OBJECTPROPERTY(t.object_id, N'IsQuotedIdentOn') AS Quoted ,\r\n        t.lock_escalation AS LockType ,\r\n        t.lock_escalation_desc AS LockTypeText ,\r\n        tr.row_count ,\r\n        t.create_date ,\r\n        t.modify_date\r\nFROM    sys.tables AS t\r\n        JOIN sys.indexes AS i ON t.object_id = i.object_id\r\n        JOIN sys.schemas AS s ON t.schema_id = s.schema_id\r\n        JOIN sys.data_spaces AS fg ON i.data_space_id = fg.data_space_id\r\n                                      AND i.index_id < 2\r\n        JOIN ( SELECT   object_id ,\r\n                        SUM(row_count) AS row_count\r\n               FROM     sys.dm_db_partition_stats\r\n               WHERE    index_id < 2\r\n               GROUP BY object_id ,\r\n                        index_id\r\n             ) tr ON t.[object_id] = tr.[object_id]\r\n        LEFT JOIN sys.data_spaces AS tg ON t.lob_data_space_id = tg.data_space_id\r\n        LEFT JOIN sys.data_spaces AS dg ON dg.is_default = 1\r\n                                           AND dg.type IN ( 'FG', 'PS' )\r\nWHERE   OBJECTPROPERTY(t.[object_id], 'IsMSShipped') = 0\r\n        AND t.is_filetable = 0\r\n        AND t.is_memory_optimized = 0\r\n";
			}
			else if (sqlVersion >= SQLServerVersion.SQLServer2012)
			{
				str = "\r\nSELECT DISTINCT\r\n        t.[object_id] AS objId ,\r\n        s.name AS SchemaName ,\r\n        t.name AS TableName ,\r\n        fg.name AS FileGroupName ,\r\n        t.lob_data_space_id AS TGid ,\r\n        CASE t.lob_data_space_id\r\n          WHEN 0 THEN dg.name\r\n          ELSE tg.name\r\n        END AS TextGroupName ,\r\n        t.uses_ansi_nulls AS ANSI ,\r\n        OBJECTPROPERTY(t.object_id, N'IsQuotedIdentOn') AS Quoted ,\r\n        t.lock_escalation AS LockType ,\r\n        t.lock_escalation_desc AS LockTypeText ,\r\n        tr.row_count ,\r\n        t.create_date ,\r\n        t.modify_date\r\nFROM    sys.tables AS t\r\n        JOIN sys.indexes AS i ON t.[object_id] = i.[object_id]\r\n        JOIN sys.schemas AS s ON t.[schema_id] = s.[schema_id]\r\n        JOIN sys.data_spaces AS fg ON i.data_space_id = fg.data_space_id\r\n                                      AND i.index_id < 2\r\n        JOIN ( SELECT   object_id ,\r\n                        SUM(row_count) AS row_count\r\n               FROM     sys.dm_db_partition_stats\r\n               WHERE    index_id < 2\r\n               GROUP BY object_id ,\r\n                        index_id\r\n             ) tr ON t.[object_id] = tr.[object_id]\r\n        LEFT JOIN sys.data_spaces AS tg ON t.lob_data_space_id = tg.data_space_id\r\n        LEFT JOIN sys.data_spaces AS dg ON dg.is_default = 1\r\n                                           AND dg.type IN ( 'FG', 'PS' )\r\nWHERE   OBJECTPROPERTY(t.[object_id], 'IsMSShipped') = 0\r\n        AND t.is_filetable = 0\r\n";
			}
			else if (sqlVersion >= SQLServerVersion.SQLServer2008)
			{
				str = "\r\nSELECT DISTINCT\r\n        t.[object_id] AS objId ,\r\n        s.name AS SchemaName ,\r\n        t.name AS TableName ,\r\n        fg.name AS FileGroupName ,\r\n        t.lob_data_space_id AS TGid ,\r\n        CASE t.lob_data_space_id\r\n          WHEN 0 THEN dg.name\r\n          ELSE tg.name\r\n        END AS TextGroupName ,\r\n        t.uses_ansi_nulls AS ANSI ,\r\n        OBJECTPROPERTY(t.object_id, N'IsQuotedIdentOn') AS Quoted ,\r\n        t.lock_escalation AS LockType ,\r\n        t.lock_escalation_desc AS LockTypeText ,\r\n        tr.row_count ,\r\n        t.create_date ,\r\n        t.modify_date\r\nFROM    sys.tables AS t\r\n        JOIN sys.indexes AS i ON t.object_id = i.object_id\r\n        JOIN sys.schemas AS s ON t.schema_id = s.schema_id\r\n        JOIN sys.data_spaces AS fg ON i.data_space_id = fg.data_space_id\r\n                                      AND i.index_id < 2\r\n        JOIN ( SELECT   object_id ,\r\n                        SUM(row_count) AS row_count\r\n               FROM     sys.dm_db_partition_stats\r\n               WHERE    index_id < 2\r\n               GROUP BY object_id ,\r\n                        index_id\r\n             ) tr ON t.[object_id] = tr.[object_id]\r\n        LEFT JOIN sys.data_spaces AS tg ON t.lob_data_space_id = tg.data_space_id\r\n        LEFT JOIN sys.data_spaces AS dg ON dg.is_default = 1\r\n                                           AND dg.type IN ( 'FG', 'PS' ) \r\nWHERE  OBJECTPROPERTY(t.[object_id],'IsMSShipped') = 0\r\n";
			}
			else
			{
				str = "\r\nSELECT DISTINCT\r\n        t.object_id AS objId ,\r\n        s.name AS SchemaName ,\r\n        t.name AS TableName ,\r\n        fg.name AS FileGroupName ,\r\n        t.lob_data_space_id AS TGid ,\r\n        CASE t.lob_data_space_id\r\n          WHEN 0 THEN dg.name\r\n          ELSE tg.name\r\n        END AS TextGroupName ,\r\n        t.uses_ansi_nulls AS ANSI ,\r\n        OBJECTPROPERTY(t.object_id, N'IsQuotedIdentOn') AS Quoted ,\r\n        NULL AS LockType ,\r\n        NULL AS LockTypeText ,\r\n        tr.row_count ,\r\n        t.create_date ,\r\n        t.modify_date\r\nFROM    sys.tables AS t\r\n        JOIN sys.indexes AS i ON t.object_id = i.object_id\r\n        JOIN sys.schemas AS s ON t.schema_id = s.schema_id\r\n        JOIN sys.data_spaces AS fg ON i.data_space_id = fg.data_space_id\r\n                                      AND i.index_id < 2\r\n        JOIN ( SELECT   object_id ,\r\n                        SUM(row_count) AS row_count\r\n               FROM     sys.dm_db_partition_stats\r\n               WHERE    index_id < 2\r\n               GROUP BY object_id ,\r\n                        index_id\r\n             ) tr ON t.[object_id] = tr.[object_id]\r\n        LEFT JOIN sys.data_spaces AS tg ON t.lob_data_space_id = tg.data_space_id\r\n        LEFT JOIN sys.data_spaces AS dg ON dg.is_default = 1\r\n                                           AND dg.type IN ( 'FG', 'PS' ) \r\nWHERE  OBJECTPROPERTY(t.[object_id],'IsMSShipped') = 0\r\n";
			}
			if (!string.IsNullOrEmpty(condition))
			{
				string[] array = condition.Split(new string[]
				{
					";"
				}, StringSplitOptions.RemoveEmptyEntries);
				if (array.Length > 0)
				{
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.Append(" AND ");
					int num = 1;
					string[] array2 = array;
					for (int i = 0; i < array2.Length; i++)
					{
						string text = array2[i];
						string[] array3 = text.Split(new string[]
						{
							"."
						}, StringSplitOptions.RemoveEmptyEntries);
						if (array3.Length == 1)
						{
							stringBuilder.AppendFormat("  t.name NOT LIKE '{0}' ", array3[0]);
						}
						else if (array3.Length == 2)
						{
							stringBuilder.AppendFormat(" (s.name NOT LIKE '{0}' OR t.name NOT LIKE '{1}') ", array3[0], array3[1]);
						}
						if (num < array.Length)
						{
							stringBuilder.Append(" AND ");
						}
						num++;
					}
					str += stringBuilder.ToString();
				}
			}
			DataTable dataTable = base.FillDataTable(sqlString, str + "  OPTION (MAXDOP 2)", timeOut);
			dataTable.TableName = "IDs";
			tableSet.Tables.Add(dataTable);
		}

		private void GetFileTableInfo(DataSet tableSet, string sqlString, int timeOut, string condition)
		{
			string text = "\r\nSELECT DISTINCT\r\n        t.object_id AS objId ,\r\n        s.name AS SchemaName ,\r\n        t.name AS TableName ,\r\n        fg.name AS FileGroupName ,\r\n        fs.name AS FileStreamName ,\r\n        ft.directory_name AS DirectoryName ,\r\n        ft.filename_collation_name AS CollatName ,\r\n        tr.row_count ,\r\n        t.create_date ,\r\n        t.modify_date\r\nFROM    sys.tables t\r\n        JOIN sys.schemas s ON t.[schema_id] = s.[schema_id]\r\n        JOIN sys.filetables ft ON t.[object_id] = ft.[object_id]\r\n        JOIN sys.data_spaces fg ON fg.is_default = 1\r\n                                   AND fg.type = 'FG'\r\n        JOIN sys.data_spaces fs ON t.filestream_data_space_id = fs.data_space_id\r\n        JOIN ( SELECT   object_id ,\r\n                        SUM(row_count) AS row_count\r\n               FROM     sys.dm_db_partition_stats\r\n               WHERE    index_id < 2\r\n               GROUP BY object_id ,\r\n                        index_id\r\n             ) tr ON t.[object_id] = tr.[object_id]\r\nWHERE   t.is_filetable = 1\r\n";
			if (!string.IsNullOrEmpty(condition))
			{
				string[] array = condition.Split(new string[]
				{
					";"
				}, StringSplitOptions.RemoveEmptyEntries);
				if (array.Length > 0)
				{
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.Append(" AND ");
					int num = 1;
					string[] array2 = array;
					for (int i = 0; i < array2.Length; i++)
					{
						string text2 = array2[i];
						string[] array3 = text2.Split(new string[]
						{
							"."
						}, StringSplitOptions.RemoveEmptyEntries);
						if (array3.Length == 1)
						{
							stringBuilder.AppendFormat("  t.name NOT LIKE '{0}' ", array3[0]);
						}
						else if (array3.Length == 2)
						{
							stringBuilder.AppendFormat(" s.name NOT LIKE '{0}' OR t.name NOT LIKE '{1}' ", array3[0], array3[1]);
						}
						if (num < array.Length)
						{
							stringBuilder.Append(" AND ");
						}
						num++;
					}
					text += stringBuilder.ToString();
				}
			}
			DataTable dataTable = base.FillDataTable(sqlString, text, timeOut);
			dataTable.TableName = "FileTables";
			tableSet.Tables.Add(dataTable);
		}

		private void GetMemoryTableInfo(DataSet tableSet, string sqlString, int timeOut, string condition)
		{
			string text = "\r\nSELECT DISTINCT\r\n        t.object_id AS objId ,\r\n        s.name AS SchemaName ,\r\n        t.name AS TableName ,\r\n        t.durability_desc AS DuraDesc ,\r\n        t.uses_ansi_nulls AS ANSI ,\r\n        OBJECTPROPERTY(t.object_id, N'IsQuotedIdentOn') AS Quoted ,\r\n        tr.row_count ,\r\n        t.create_date ,\r\n        t.modify_date\r\nFROM    sys.tables t\r\n        JOIN sys.schemas s ON t.schema_id = s.schema_id\r\n        JOIN ( SELECT   object_id ,\r\n                        SUM(row_count) AS row_count\r\n               FROM     sys.dm_db_partition_stats\r\n               WHERE    index_id < 2\r\n               GROUP BY object_id ,\r\n                        index_id\r\n             ) tr ON t.[object_id] = tr.[object_id]\r\nWHERE   t.is_memory_optimized = 1\r\n";
			if (!string.IsNullOrEmpty(condition))
			{
				string[] array = condition.Split(new string[]
				{
					";"
				}, StringSplitOptions.RemoveEmptyEntries);
				if (array.Length > 0)
				{
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.Append(" AND ");
					int num = 1;
					string[] array2 = array;
					for (int i = 0; i < array2.Length; i++)
					{
						string text2 = array2[i];
						string[] array3 = text2.Split(new string[]
						{
							"."
						}, StringSplitOptions.RemoveEmptyEntries);
						if (array3.Length == 1)
						{
							stringBuilder.AppendFormat("  t.name NOT LIKE '{0}' ", array3[0]);
						}
						else if (array3.Length == 2)
						{
							stringBuilder.AppendFormat(" s.name NOT LIKE '{0}' OR t.name NOT LIKE '{1}' ", array3[0], array3[1]);
						}
						if (num < array.Length)
						{
							stringBuilder.Append(" AND ");
						}
						num++;
					}
					text += stringBuilder.ToString();
				}
			}
			DataTable dataTable = base.FillDataTable(sqlString, text, timeOut);
			dataTable.TableName = "MemoryTables";
			tableSet.Tables.Add(dataTable);
		}

		private void GetColumns(DataSet tableSet, string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			string cmdText = string.Empty;
			if (sqlVersion <= SQLServerVersion.SQLServer2005)
			{
				cmdText = "\r\nSELECT DISTINCT\r\n        c.object_id AS objId ,\r\n        c.column_id ,\r\n        c.name ,\r\n        CASE c.is_computed\r\n          WHEN 1 THEN e.text\r\n          ELSE NULL\r\n        END AS express ,\r\n        CASE c.system_type_id\r\n          WHEN c.user_type_id THEN 0\r\n          ELSE 1\r\n        END AS is_usertype ,\r\n        s.name 'UserTypeSchema' ,\r\n        t.name 'TypeName' ,\r\n        c.max_length ,\r\n        c.precision ,\r\n        c.scale ,        \r\n        c.collation_name ,\r\n        CASE c.is_identity\r\n          WHEN 1\r\n          THEN 'identity(' + CONVERT(NVARCHAR(20), i.seed_value) + ','\r\n               + CONVERT(NVARCHAR(20), i.increment_value) + ')'\r\n          ELSE NULL\r\n        END AS colidentity ,        \r\n        c.is_nullable ,\r\n        c.is_computed ,\r\n        c.is_filestream ,\r\n        c.is_replicated ,\r\n        NULL AS is_sparse ,\r\n        NULL AS is_column_set,\r\n        d.definition\r\nFROM    sys.columns AS c\r\n        JOIN sys.types AS t ON c.user_type_id = t.user_type_id\r\n        JOIN sys.schemas AS s ON t.schema_id = s.schema_id\r\n        LEFT JOIN syscomments AS e ON c.object_id = e.id\r\n        LEFT JOIN sys.identity_columns AS i ON c.object_id = i.object_id\r\n        LEFT JOIN sys.default_constraints d ON c.object_id=d.parent_object_id\r\n        AND c.column_id=d.parent_column_id\r\nORDER BY c.column_id\r\nOPTION (MAXDOP 2)\r\n";
			}
			else
			{
				cmdText = "\r\nSELECT DISTINCT\r\n        c.object_id AS objId ,\r\n        c.column_id ,\r\n        c.name ,\r\n        CASE c.is_computed\r\n          WHEN 1 THEN e.text\r\n          ELSE NULL\r\n        END AS express ,\r\n        CASE c.system_type_id\r\n          WHEN c.user_type_id THEN 0\r\n          ELSE 1\r\n        END AS is_usertype ,\r\n        s.name 'UserTypeSchema' ,\r\n        t.name 'TypeName' ,\r\n        c.max_length ,\r\n        c.precision ,\r\n        c.scale ,        \r\n        c.collation_name ,\r\n        CASE c.is_identity\r\n          WHEN 1\r\n          THEN 'identity(' + CONVERT(NVARCHAR(20), i.seed_value) + ','\r\n               + CONVERT(NVARCHAR(20), i.increment_value) + ')'\r\n          ELSE NULL\r\n        END AS colidentity ,         \r\n        c.is_nullable ,        \r\n        c.is_computed ,\r\n        c.is_filestream ,\r\n        c.is_replicated ,\r\n        c.is_sparse ,\r\n        c.is_column_set ,\r\n        d.definition\r\nFROM    sys.columns AS c\r\n        JOIN sys.types AS t ON c.user_type_id = t.user_type_id\r\n        JOIN sys.schemas AS s ON t.schema_id = s.schema_id\r\n        LEFT JOIN syscomments AS e ON c.object_id = e.id\r\n        LEFT JOIN sys.identity_columns AS i ON c.object_id = i.object_id\r\n        LEFT JOIN sys.default_constraints d ON c.object_id=d.parent_object_id\r\n        AND c.column_id=d.parent_column_id\r\nORDER BY c.column_id\r\nOPTION (MAXDOP 2)\r\n";
			}
			DataTable dataTable = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable.TableName = "Cols";
			tableSet.Tables.Add(dataTable);
		}

		private void GetPriKeys(DataSet tableSet, string sqlString, int timeOut)
		{
			string cmdText = "\r\nSELECT  kc.parent_object_id AS objId ,\r\n        kc.type ,\r\n        kc.name AS PKName ,\r\n        i.type_desc ,\r\n        c.name AS ColName ,\r\n        a.is_descending_key ,\r\n        i.is_padded AS padindex ,\r\n        s.no_recompute AS Recompute ,\r\n        i.ignore_dup_key AS Ignorekey ,\r\n        i.allow_row_locks AS RowLock ,\r\n        i.allow_page_locks AS PageLock ,\r\n        fg.name AS FGName ,\r\n        c.is_ansi_padded AS ANSI\r\nFROM    sys.key_constraints AS kc ,\r\n        sys.indexes AS i ,\r\n        sys.index_columns AS a ,\r\n        sys.columns AS c ,\r\n        sys.stats AS s ,\r\n        sys.filegroups AS fg\r\nWHERE   kc.unique_index_id = i.index_id\r\n        AND kc.parent_object_id = i.object_id\r\n        AND kc.parent_object_id = a.object_id\r\n        AND kc.parent_object_id = c.object_id\r\n        AND kc.parent_object_id = s.object_id\r\n        AND i.name = s.name\r\n        AND i.data_space_id = fg.data_space_id\r\n        AND i.index_id = a.index_id\r\n        AND a.column_id = c.column_id\r\n        AND ( i.is_primary_key = 1\r\n              OR i.is_unique_constraint = 1\r\n            )\r\nOPTION (MAXDOP 2)\r\n";
			DataTable dataTable = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable.TableName = "PKs";
			tableSet.Tables.Add(dataTable);
		}

		private void GetForeignKeys(DataSet tableSet, string sqlString, int timeOut)
		{
			string cmdText = "\r\nSELECT  fk.parent_object_id AS objId ,\r\n        fk.name AS FKName ,\r\n        fk.is_disabled ,\r\n        c1.name AS FKColName ,\r\n        s.name AS RFSchema ,\r\n        o.name AS RFName ,\r\n        c2.name AS RFCOlName\r\nFROM    sys.foreign_keys AS fk ,\r\n        sys.foreign_key_columns AS fc ,\r\n        sys.columns AS c1 ,\r\n        sys.columns AS c2 ,\r\n        sys.all_objects AS o ,\r\n        sys.schemas AS s\r\nWHERE   fk.object_id = fc.constraint_object_id\r\n        AND fc.referenced_object_id = o.object_id\r\n        AND fc.parent_object_id = c1.object_id\r\n        AND fc.referenced_object_id = c2.object_id\r\n        AND fc.parent_column_id = c1.column_id\r\n        AND fc.referenced_column_id = c2.column_id\r\n        AND o.schema_id = s.schema_id\r\nOPTION (MAXDOP 2)\r\n";
			DataTable dataTable = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable.TableName = "FKs";
			tableSet.Tables.Add(dataTable);
		}

		private void GeTTriggers(DataSet tableSet, string sqlString, int timeOut)
		{
			string cmdText = "\r\nSELECT  t.parent_id AS [objId] ,\r\n        t.[object_id] AS triggerId ,\r\n        t.[type] AS [Type] ,\r\n        sm.[definition] ,\r\n        s.name AS SchemaName ,\r\n        o.name AS TriggerName ,\r\n        t.is_disabled AS isDisabled ,\r\n        sm.uses_ansi_nulls AS ANSI ,\r\n        sm.uses_quoted_identifier AS Quoted ,\r\n        o.create_date ,\r\n        o.modify_date\r\nFROM    sys.triggers t\r\n        INNER JOIN sys.objects o ON t.object_id = o.object_id\r\n                                    AND t.parent_class = 1\r\n        INNER JOIN sys.schemas s ON o.schema_id = s.schema_id\r\n        LEFT JOIN sys.sql_modules sm ON t.object_id = sm.object_id\r\nOPTION (MAXDOP 2)\r\n";
			DataTable dataTable = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable.TableName = "Triggers";
			tableSet.Tables.Add(dataTable);
		}

		private void GeTTriggerOrder(DataSet tableSet, string sqlString, int timeOut)
		{
			string cmdText = "\r\nSELECT  [object_id] ,\r\n        type_desc ,\r\n        is_first ,\r\n        is_last\r\nFROM    sys.trigger_events\r\nWHERE   is_first > 0\r\n        OR is_last > 0\r\n";
			DataTable dataTable = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable.TableName = "TriggerOrder";
			tableSet.Tables.Add(dataTable);
		}

		private void GetClusIndexes(DataSet tableSet, string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			string cmdText = string.Empty;
			if (sqlVersion >= SQLServerVersion.SQLServer2008)
			{
				cmdText = "\r\nSELECT  i.object_id AS objId ,\r\n        i.name AS IndexName ,\r\n        i.is_unique AS IsUnique ,\r\n        i.[type] AS IndexType ,\r\n        i.type_desc AS TypeDesc ,\r\n        c.name AS ColName ,\r\n        ic.is_descending_key AS IsDescCol ,\r\n        i.fill_factor AS Factor ,\r\n        ic.partition_ordinal AS Ordinal ,\r\n        ic.is_included_column AS IsIncludedCol ,\r\n        i.is_padded AS Padindex ,\r\n        s.no_recompute AS Recompute ,\r\n        i.ignore_dup_key AS Ignorekey ,\r\n        i.allow_row_locks AS RowLock ,\r\n        i.allow_page_locks AS PageLock ,\r\n        ds.type AS StorType ,\r\n        ds.name AS FGName ,\r\n        i.is_disabled AS IsDisabled ,\r\n        i.has_filter AS HasFilter ,\r\n        i.filter_definition AS FilterText\r\nFROM    sys.indexes i ,\r\n        sys.index_columns AS ic ,\r\n        sys.columns AS c ,\r\n        sys.stats AS s ,\r\n        sys.data_spaces AS ds\r\nWHERE   is_primary_key = 0\r\n        AND is_unique_constraint = 0\r\n        AND i.object_id = ic.object_id\r\n        AND i.object_id = c.object_id\r\n        AND i.object_id = s.object_id\r\n        AND i.name = s.name\r\n        AND i.data_space_id = ds.data_space_id\r\n        AND i.index_id = ic.index_id\r\n        AND ic.column_id = c.column_id\r\n        AND i.type IN ( 0, 1, 2 )\r\nOPTION (MAXDOP 2)\r\n";
			}
			else
			{
				cmdText = "\r\nSELECT  i.object_id AS objId ,\r\n        i.name AS IndexName ,\r\n        i.is_unique AS IsUnique ,\r\n        i.[type] AS IndexType ,\r\n        i.type_desc AS TypeDesc ,\r\n        c.name AS ColName ,\r\n        ic.is_descending_key AS IsDescCol ,\r\n        i.fill_factor AS Factor ,\r\n        ic.partition_ordinal AS Ordinal ,\r\n        ic.is_included_column AS IsIncludedCol ,\r\n        i.is_padded AS Padindex ,\r\n        s.no_recompute AS Recompute ,\r\n        i.ignore_dup_key AS Ignorekey ,\r\n        i.allow_row_locks AS RowLock ,\r\n        i.allow_page_locks AS PageLock ,\r\n        ds.type AS StorType ,\r\n        ds.name AS FGName ,\r\n        i.is_disabled AS IsDisabled\r\nFROM    sys.indexes i ,\r\n        sys.index_columns AS ic ,\r\n        sys.columns AS c ,\r\n        sys.stats AS s ,\r\n        sys.data_spaces AS ds\r\nWHERE   is_primary_key = 0\r\n        AND is_unique_constraint = 0\r\n        AND i.object_id = ic.object_id\r\n        AND i.object_id = c.object_id\r\n        AND i.object_id = s.object_id\r\n        AND i.name = s.name\r\n        AND i.data_space_id = ds.data_space_id\r\n        AND i.index_id = ic.index_id\r\n        AND ic.column_id = c.column_id\r\n        AND i.type IN ( 0, 1, 2 )\r\nOPTION (MAXDOP 2)\r\n";
			}
			DataTable dataTable = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable.TableName = "ClusIndexs";
			tableSet.Tables.Add(dataTable);
		}

		private void GetXMLIndexes(DataSet tableSet, string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			string cmdText = string.Empty;
			if (sqlVersion >= SQLServerVersion.SQLServer2014)
			{
				cmdText = "\r\nSELECT  xi.object_id AS objId ,\r\n        xi.name AS IndexName ,\r\n        pxi.name AS PriName ,\r\n        xi.secondary_type_desc AS SecondTypeDesc ,\r\n        c.name AS ColName ,\r\n        xi.fill_factor AS Factor ,\r\n        xi.is_padded AS Padindex ,\r\n        xi.ignore_dup_key AS Ignorekey ,\r\n        xi.allow_row_locks AS RowLock ,\r\n        xi.allow_page_locks AS PageLock ,\r\n        xi.is_disabled AS IsDisabled ,\r\n        xi.xml_index_type AS IndexType\r\nFROM    sys.xml_indexes xi\r\n        JOIN sys.index_columns ic ON xi.object_id = ic.object_id\r\n                                     AND xi.index_id = ic.index_id\r\n        JOIN sys.columns c ON ic.object_id = c.object_id\r\n                              AND ic.column_id = c.column_id\r\n        LEFT JOIN sys.xml_indexes pxi ON xi.using_xml_index_id = pxi.index_id\r\n                                         AND xi.object_id = pxi.object_id\r\nORDER BY IndexType\r\nOPTION (MAXDOP 2)\r\n";
			}
			else
			{
				cmdText = "\r\nSELECT  xi.object_id AS objId ,\r\n        xi.name AS IndexName ,\r\n        pxi.name AS PriName ,\r\n        xi.secondary_type_desc AS SecondTypeDesc ,\r\n        c.name AS ColName ,\r\n        xi.fill_factor AS Factor ,\r\n        xi.is_padded AS Padindex ,\r\n        xi.ignore_dup_key AS Ignorekey ,\r\n        xi.allow_row_locks AS RowLock ,\r\n        xi.allow_page_locks AS PageLock ,\r\n        xi.is_disabled AS IsDisabled\r\nFROM    sys.xml_indexes xi\r\n        JOIN sys.index_columns ic ON xi.object_id = ic.object_id\r\n                                     AND xi.index_id = ic.index_id\r\n        JOIN sys.columns c ON ic.object_id = c.object_id\r\n                              AND ic.column_id = c.column_id\r\n        LEFT JOIN sys.xml_indexes pxi ON xi.using_xml_index_id = pxi.index_id\r\n                                         AND xi.object_id = pxi.object_id\r\nOPTION (MAXDOP 2)\r\n";
			}
			DataTable dataTable = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable.TableName = "XMLIndexs";
			tableSet.Tables.Add(dataTable);
		}

		private void GetSpatialIndexs(DataSet tableSet, string sqlString, int timeOut)
		{
			string cmdText = "\r\nSELECT  si.object_id AS objId ,\r\n        si.name AS IndexName ,\r\n        c.name AS ColName ,\r\n        si.spatial_index_type AS IndexType ,\r\n        si.tessellation_scheme AS TesseScheme ,\r\n        sit.bounding_box_xmin AS Xmin ,\r\n        sit.bounding_box_ymin AS Ymin ,\r\n        sit.bounding_box_xmax AS Xmax ,\r\n        sit.bounding_box_ymax AS Ymax ,\r\n        sit.level_1_grid_desc AS Level_1 ,\r\n        sit.level_2_grid_desc AS Level_2 ,\r\n        sit.level_3_grid_desc AS Level_3 ,\r\n        sit.level_4_grid_desc AS Level_4 ,\r\n        sit.cells_per_object AS Cells ,\r\n        si.fill_factor AS Factor ,\r\n        si.is_padded AS Padindex ,\r\n        si.ignore_dup_key AS Ignorekey ,\r\n        si.allow_row_locks AS RowLock ,\r\n        si.allow_page_locks AS PageLock ,\r\n        ds.name AS FGName ,\r\n        si.is_disabled AS IsDisabled\r\nFROM    sys.spatial_indexes si ,\r\n        sys.spatial_index_tessellations sit ,\r\n        sys.columns c ,\r\n        sys.index_columns AS ic ,\r\n        sys.data_spaces ds\r\nWHERE   si.object_id = sit.object_id\r\n        AND si.object_id = c.object_id\r\n        AND si.object_id = ic.object_id\r\n        AND si.index_id = ic.index_id\r\n        AND si.index_id = sit.index_id\r\n        AND c.column_id = ic.column_id\r\n        AND si.data_space_id = ds.data_space_id\r\nOPTION (MAXDOP 2)\r\n";
			DataTable dataTable = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable.TableName = "SpatialIndexs";
			tableSet.Tables.Add(dataTable);
		}

		private void GetColumnStoredIndexs(DataSet tableSet, string sqlString, int timeOut)
		{
			string cmdText = "\r\nSELECT DISTINCT\r\n        i.object_id AS objId ,\r\n        i.name AS IndexName ,\r\n        i.type AS IndexType ,\r\n        i.type_desc AS TypeDesc ,\r\n        CASE i.type\r\n          WHEN 6 THEN c.name\r\n          ELSE NULL\r\n        END AS ColName ,\r\n        ds.name AS FGName ,\r\n        i.is_disabled AS IsDisabled\r\nFROM    sys.indexes i ,\r\n        sys.index_columns ic ,\r\n        sys.columns c ,\r\n        sys.data_spaces ds\r\nWHERE   i.object_id = ic.object_id\r\n        AND i.object_id = c.object_id\r\n        AND i.index_id = ic.index_id\r\n        AND ic.column_id = c.column_id\r\n        AND i.data_space_id = ds.data_space_id\r\n        AND i.type IN ( 5, 6 )\r\nOPTION (MAXDOP 2)\r\n";
			DataTable dataTable = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable.TableName = "ColumnStoredIndexs";
			tableSet.Tables.Add(dataTable);
		}

		private void GetHashIndexes(DataSet tableSet, string sqlString, int timeOut)
		{
			string cmdText = "\r\nSELECT  i.object_id AS objId ,\r\n        i.name AS IndexName ,\r\n        i.type AS IndexType ,\r\n        i.is_primary_key AS IsPriKey ,\r\n        i.type_desc AS TypeDesc ,\r\n        c.name AS ColName ,\r\n        ic.is_descending_key AS IsDescCol\r\nFROM    sys.indexes i ,\r\n        sys.index_columns ic ,\r\n        sys.columns c\r\nWHERE   i.object_id = ic.object_id\r\n        AND i.object_id = c.object_id\r\n        AND i.index_id = ic.index_id\r\n        AND ic.column_id = c.column_id\r\n        AND i.type IN ( 2, 7 )\r\nORDER BY i.is_primary_key\r\n";
			DataTable dataTable = base.FillDataTable(sqlString, cmdText, timeOut);
			dataTable.TableName = "HashIndexs";
			tableSet.Tables.Add(dataTable);
		}

		private string GetTypeText(string typeName, int maxLength, int precision, int scale)
		{
			string arg_05_0 = string.Empty;
			string key;
			switch (key = typeName.ToLower())
			{
			case "nvarchar":
				if (maxLength == -1)
				{
					return "nvarchar(MAX)";
				}
				return string.Format("nvarchar({0})", maxLength / 2);
			case "nchar":
				return string.Format("nchar({0})", maxLength / 2);
			case "binary":
			case "char":
			case "varchar":
			case "varbinary":
				return string.Format("{0}({1})", typeName, (maxLength == -1) ? "MAX" : maxLength.ToString());
			case "time":
			case "datetime2":
			case "datetimeoffset":
				return string.Format("{0}({1})", typeName, scale);
			case "decimal":
			case "numeric":
				return string.Format("{0}({1},{2})", typeName, precision, scale);
			}
			return typeName;
		}
	}
}
