using System;

namespace ZhuanCloud.Collector
{
	public class DiskPartion
	{
		public string DiskPartition
		{
			get;
			set;
		}

		public string Model
		{
			get;
			set;
		}

		public DiskPartion(string part, string model)
		{
			this.DiskPartition = part;
			this.Model = model;
		}
	}
}
