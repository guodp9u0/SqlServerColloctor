using System;
using System.Data;
using System.Data.OleDb;

namespace ZhuanCloud.Collector
{
	public class IndexUsedCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, int timeOut)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(sqlString, timeOut);
				if (data.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblIndexUsed\r\n        ( DBName ,\r\n          ObjectId ,\r\n          IndexName ,\r\n          UserSeeks ,\r\n          UserScans ,\r\n          UserLookups ,\r\n          LastUserSeek ,\r\n          LastUserScan ,\r\n          LastUserLookup\r\n        )\r\nVALUES  ( @DBName ,\r\n          @ObjectId ,\r\n          @IndexName ,\r\n          @UserSeeks ,\r\n          @UserScans ,\r\n          @UserLookups ,\r\n          @LastUserSeek ,\r\n          @LastUserScan ,\r\n          @LastUserLookup\r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@DBName", OleDbType.VarChar)
						{
							SourceColumn = "db_name"
						},
						new OleDbParameter("@ObjectId", OleDbType.Integer)
						{
							SourceColumn = "object_id"
						},
						new OleDbParameter("@IndexName", OleDbType.VarChar)
						{
							SourceColumn = "name"
						},
						new OleDbParameter("@UserSeeks", OleDbType.Double)
						{
							SourceColumn = "user_seeks"
						},
						new OleDbParameter("@UserScans", OleDbType.Double)
						{
							SourceColumn = "user_scans"
						},
						new OleDbParameter("@UserLookups", OleDbType.Double)
						{
							SourceColumn = "user_lookups"
						},
						new OleDbParameter("@LastUserSeek", OleDbType.Date)
						{
							SourceColumn = "last_user_seek"
						},
						new OleDbParameter("@LastUserScan", OleDbType.Date)
						{
							SourceColumn = "last_user_scan"
						},
						new OleDbParameter("@LastUserLookup", OleDbType.Date)
						{
							SourceColumn = "last_user_lookup"
						}
					};
					result = base.SaveToAccess(oleDBString, data, cmdText, parameters);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, int timeOut)
		{
			string cmdText = "\r\nSET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED\r\nSELECT  DB_NAME(DB_ID()) [db_name] ,\r\n        di.[object_id] ,\r\n        i.name ,\r\n        di.user_seeks ,\r\n        di.user_scans ,\r\n        di.user_lookups ,\r\n        di.last_user_seek ,\r\n        di.last_user_scan ,\r\n        di.last_user_lookup\r\nFROM    sys.dm_db_index_usage_stats di ,\r\n        sys.indexes i\r\nWHERE   di.[object_id] = i.[object_id]\r\n        AND di.index_id = i.index_id\r\n        AND database_id = DB_ID()\r\n        AND i.name IS NOT NULL\r\nOPTION (MAXDOP 2)\r\n";
			return base.FillDataTable(sqlString, cmdText, timeOut);
		}
	}
}
