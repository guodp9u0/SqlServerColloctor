using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text;

namespace ZhuanCloud.Collector
{
	public class UnReasonablyTablesCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, int timeOut, string condition)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(sqlString, timeOut, condition);
				if (data.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblUnReasonablyTable\r\n        ( DBName ,\r\n          SchemaName ,\r\n          TableName ,\r\n          [RowCount] ,\r\n          HasClustIndex ,\r\n          HasTextImage ,\r\n          HasUniqueidentifier\r\n        )\r\nVALUES  ( @DBName ,\r\n          @SchemaName ,\r\n          @TableName ,\r\n          @RowCount ,\r\n          @HasClustIndex ,\r\n          @HasTextImage ,\r\n          @HasUniqueidentifier\r\n        )\r\n";
					SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder(sqlString);
					string initialCatalog = sqlConnectionStringBuilder.InitialCatalog;
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@DBName", OleDbType.VarChar)
						{
							Value = initialCatalog
						},
						new OleDbParameter("@SchemaName", OleDbType.VarChar)
						{
							SourceColumn = "schema_name"
						},
						new OleDbParameter("@TableName", OleDbType.VarChar)
						{
							SourceColumn = "table_name"
						},
						new OleDbParameter("@RowCount", OleDbType.Double)
						{
							SourceColumn = "row_count"
						},
						new OleDbParameter("@HasClustIndex", OleDbType.Integer)
						{
							SourceColumn = "hasClustIndex"
						},
						new OleDbParameter("@HasTextImage", OleDbType.Integer)
						{
							SourceColumn = "hasTextImage"
						},
						new OleDbParameter("@HasUniqueidentifier", OleDbType.Integer)
						{
							SourceColumn = "hasGuid"
						}
					};
					result = base.SaveToAccess(oleDBString, data, cmdText, parameters);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, int timeOut, string condition)
		{
			if (!string.IsNullOrEmpty(condition))
			{
				string[] array = condition.Split(new string[]
				{
					";"
				}, StringSplitOptions.RemoveEmptyEntries);
				if (array.Length > 0)
				{
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.Append(" AND ");
					int num = 1;
					string[] array2 = array;
					for (int i = 0; i < array2.Length; i++)
					{
						string text = array2[i];
						string[] array3 = text.Split(new string[]
						{
							"."
						}, StringSplitOptions.RemoveEmptyEntries);
						if (array3.Length == 1)
						{
							stringBuilder.AppendFormat("  t.name NOT LIKE '{0}' ", array3[0]);
						}
						else if (array3.Length == 2)
						{
							stringBuilder.AppendFormat(" s.name NOT LIKE '{0}' OR t.name NOT LIKE '{1}' ", array3[0], array3[1]);
						}
						if (num < array.Length)
						{
							stringBuilder.Append(" AND ");
						}
						num++;
					}
					condition = stringBuilder.ToString();
				}
			}
			string cmdText = string.Format("\r\nSELECT DISTINCT\r\n        s.name AS [schema_name] ,\r\n        t.name AS [table_name] ,\r\n        ps.row_count ,\r\n        OBJECTPROPERTY(t.[object_id], 'TableHasClustIndex') AS hasClustIndex ,\r\n        OBJECTPROPERTY(t.[object_id], 'TableHasTextImage ') AS hasTextImage ,\r\n        CASE WHEN d.tbl_count >= 1 THEN 1\r\n             ELSE 0\r\n        END AS hasGuid\r\nFROM    sys.schemas s\r\n        LEFT JOIN sys.tables t ON t.[schema_id] = s.[schema_id] {0} \r\n        LEFT JOIN ( SELECT  object_id ,\r\n                            SUM(row_count) AS row_count\r\n                    FROM    sys.dm_db_partition_stats\r\n                    WHERE   index_id < 2\r\n                    GROUP BY object_id ,\r\n                            index_id\r\n                  ) ps ON t.[object_id] = ps.[object_id]\r\n                          AND OBJECTPROPERTY(t.[object_id], 'IsMSShipped') = 0\r\n        LEFT JOIN ( SELECT  c.[object_id] ,\r\n                            COUNT(c.[object_id]) AS tbl_count\r\n                    FROM    sys.columns c\r\n                            JOIN sys.indexes i ON i.[object_id] = c.[object_id]\r\n                                                  AND i.[type] = 1\r\n                                                  AND c.system_type_id = 36\r\n                                                  AND OBJECTPROPERTY(i.[object_id],\r\n                                                              'IsMSShipped') = 0\r\n                            JOIN sys.index_columns ic ON i.[object_id] = ic.[object_id]\r\n                                                         AND i.[object_id] = c.[object_id]\r\n                                                         AND i.index_id = ic.index_id\r\n                                                         AND c.column_id = ic.column_id\r\n                            LEFT JOIN sys.default_constraints d ON d.parent_object_id = i.[object_id]\r\n                                                              AND d.parent_column_id = c.column_id\r\n                    WHERE   d.[definition] IS NULL\r\n                            OR UPPER(d.[definition]) NOT LIKE '%NEWSEQUENTIALID()%'\r\n                    GROUP BY c.[object_id]\r\n                  ) d ON t.[object_id] = d.[object_id]\r\nWHERE   OBJECTPROPERTY(t.[object_id], 'TableHasClustIndex') = 0\r\n        OR OBJECTPROPERTY(t.[object_id], 'TableHasTextImage ') = 1\r\n        OR d.tbl_count >= 1\r\n", condition);
			return base.FillDataTable(sqlString, cmdText, timeOut);
		}
	}
}
