using System;

namespace ZhuanCloud.Collector
{
	public class LogItem
	{
		public DateTime Date
		{
			get;
			set;
		}

		public string ProcessType
		{
			get;
			set;
		}

		public string ErrorInfo
		{
			get;
			set;
		}
	}
}
