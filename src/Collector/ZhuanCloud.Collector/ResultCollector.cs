using System;
using System.Data.OleDb;

namespace ZhuanCloud.Collector
{
	public class ResultCollector : OleDbCollector
	{
		public bool SaveCheckResult(string oleDBString, string itemName, bool result, int timeOut)
		{
			try
			{
				string cmdText = "INSERT INTO tblItemResult(ItemName,CollectResult) VALUES(@ItemName,@CollectResult)";
				OleDbParameter[] parameters = new OleDbParameter[]
				{
					new OleDbParameter("@ItemName", OleDbType.VarChar)
					{
						Value = itemName
					},
					new OleDbParameter("@CollectResult", OleDbType.Boolean)
					{
						Value = result
					}
				};
				if (!base.SaveToAccess(oleDBString, cmdText, timeOut, parameters))
				{
					bool result2 = false;
					return result2;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				bool result2 = false;
				return result2;
			}
			return true;
		}

		public bool Completed(string oleDBString, int timeOut)
		{
			try
			{
				string cmdText = "UPDATE tblItemResult SET CollectResult = true WHERE ItemName = 'Completed'";
				if (!base.SaveToAccess(oleDBString, cmdText, timeOut, new OleDbParameter[0]))
				{
					bool result = false;
					return result;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				bool result = false;
				return result;
			}
			return true;
		}
	}
}
