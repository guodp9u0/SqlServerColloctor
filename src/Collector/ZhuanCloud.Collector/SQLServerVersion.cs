using System;

namespace ZhuanCloud.Collector
{
	public enum SQLServerVersion
	{
		SQLServer2000,
		SQLServer2005,
		SQLServer2008,
		SQLServer2008R2,
		SQLServer2012,
		SQLServer2014,
		SQLServer2016
	}
}
