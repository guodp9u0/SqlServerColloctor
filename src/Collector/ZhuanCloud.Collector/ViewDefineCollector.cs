using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Text;

namespace ZhuanCloud.Collector
{
	public class ViewDefineCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut, string condition, bool isDecrypt)
		{
			bool result;
			try
			{
				int majorVersion = base.GetMajorVersion(sqlVersion);
				DataTable data = this.GetData(sqlString, timeOut, condition);
				if (data.Rows.Count > 0)
				{
					DataTable dataTable = new DataTable();
					dataTable.Columns.Add(new DataColumn("db_name"));
					dataTable.Columns.Add(new DataColumn("schema_name"));
					dataTable.Columns.Add(new DataColumn("object_name"));
					dataTable.Columns.Add(new DataColumn("define"));
					dataTable.Columns.Add(new DataColumn("create_date"));
					dataTable.Columns.Add(new DataColumn("modify_date"));
					SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder(sqlString);
					string initialCatalog = sqlConnectionStringBuilder.InitialCatalog;
					foreach (DataRow dataRow in data.Rows)
					{
						StringBuilder stringBuilder = new StringBuilder();
						string text = dataRow.ItemArray[1].ToString();
						string text2 = dataRow.ItemArray[2].ToString();
						stringBuilder.AppendLine(string.Format("USE [{0}]", initialCatalog));
						stringBuilder.AppendLine("GO");
						if (dataRow.ItemArray[4] != DBNull.Value)
						{
							bool flag = Convert.ToBoolean(dataRow.ItemArray[4]);
							stringBuilder.AppendLine(string.Format("SET ANSI_NULLS {0}", flag ? "ON" : "OFF"));
							stringBuilder.AppendLine("GO");
						}
						if (dataRow.ItemArray[5] != DBNull.Value)
						{
							bool flag2 = Convert.ToBoolean(dataRow.ItemArray[5]);
							stringBuilder.AppendLine(string.Format("SET QUOTED_IDENTIFIER {0}", flag2 ? "ON" : "OFF"));
							stringBuilder.AppendLine("GO");
						}
						string objectID = dataRow.ItemArray[0].ToString();
						string value = dataRow.ItemArray[3].ToString();
						if (string.IsNullOrEmpty(value))
						{
							if (isDecrypt)
							{
								try
								{
									value = Engine.Decrypt(sqlString, initialCatalog, objectID, majorVersion);
									goto IL_22B;
								}
								catch (Exception innerException)
								{
									value = "-- 对象无法解密";
									ErrorLog.Write(new Exception(string.Format("db_name:{0},view_name:{1}.{2}", initialCatalog, text, text2), innerException));
									continue;
								}
							}
							value = "-- 对象不允许解密";
						}
						IL_22B:
						stringBuilder.AppendLine(value);
						stringBuilder.AppendLine("GO");
						DateTime dateTime = DateTime.Parse(dataRow.ItemArray[6].ToString());
						DateTime dateTime2 = DateTime.Parse(dataRow.ItemArray[7].ToString());
						string text3 = TextManager.Compress(stringBuilder.ToString());
						dataTable.Rows.Add(new object[]
						{
							initialCatalog,
							text,
							text2,
							text3,
							dateTime,
							dateTime2
						});
					}
					if (dataTable.Rows.Count > 0)
					{
						if (!Util.startFlag)
						{
							result = false;
							return result;
						}
						string cmdText = "\r\nINSERT  INTO tblDBView\r\n        ( DBName ,\r\n          SchemaName ,\r\n          ObjectName ,\r\n          Define ,\r\n          CreateDate ,\r\n          ModifyDate \r\n        )\r\nVALUES  ( @DBName ,\r\n          @SchemaName ,\r\n          @ObjectName ,\r\n          @Define ,\r\n          @CreateDate ,\r\n          @ModifyDate \r\n        )\r\n";
						OleDbParameter[] parameters = new OleDbParameter[]
						{
							new OleDbParameter("@DBName", OleDbType.VarChar)
							{
								SourceColumn = "db_name"
							},
							new OleDbParameter("@SchemaName", OleDbType.VarChar)
							{
								SourceColumn = "schema_name"
							},
							new OleDbParameter("@ObjectName", OleDbType.VarChar)
							{
								SourceColumn = "object_name"
							},
							new OleDbParameter("@Define", OleDbType.VarChar)
							{
								SourceColumn = "define"
							},
							new OleDbParameter("@CreateDate", OleDbType.Date)
							{
								SourceColumn = "create_date"
							},
							new OleDbParameter("@ModifyDate", OleDbType.Date)
							{
								SourceColumn = "modify_date"
							}
						};
						result = base.SaveToAccess(oleDBString, dataTable, cmdText, parameters);
						return result;
					}
				}
				result = true;
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, int timeOut, string condition)
		{
			string text = "\r\nSET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED\r\nSELECT  v.object_id AS objId ,\r\n        s.name AS [schema_name],\r\n        v.name AS view_name ,\r\n        m.definition ,\r\n        m.uses_ansi_nulls AS ANSI ,\r\n        m.uses_quoted_identifier AS Quoted ,\r\n        v.create_date ,\r\n        v.modify_date\r\nFROM    sys.views v ,\r\n        sys.schemas s ,\r\n        sys.sql_modules m\r\nWHERE   v.object_id = m.object_id\r\n        AND v.schema_id = s.schema_id\r\n        AND OBJECTPROPERTY(m.object_id, 'IsMSShipped') = 0\r\n";
			if (!string.IsNullOrEmpty(condition))
			{
				string[] array = condition.Split(new string[]
				{
					";"
				}, StringSplitOptions.RemoveEmptyEntries);
				if (array.Length > 0)
				{
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.Append(" AND ");
					int num = 1;
					string[] array2 = array;
					for (int i = 0; i < array2.Length; i++)
					{
						string text2 = array2[i];
						string[] array3 = text2.Split(new string[]
						{
							"."
						}, StringSplitOptions.RemoveEmptyEntries);
						if (array3.Length == 1)
						{
							stringBuilder.AppendFormat("  v.name NOT LIKE '{0}' ", array3[0]);
						}
						else if (array3.Length == 2)
						{
							stringBuilder.AppendFormat(" (s.name NOT LIKE '{0}' OR v.name NOT LIKE '{1}') ", array3[0], array3[1]);
						}
						if (num < array.Length)
						{
							stringBuilder.Append(" AND ");
						}
						num++;
					}
					text += stringBuilder.ToString();
				}
			}
			return base.FillDataTable(sqlString, text, timeOut);
		}
	}
}
