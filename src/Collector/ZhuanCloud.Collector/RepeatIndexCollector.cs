using System;
using System.Data;
using System.Data.OleDb;

namespace ZhuanCloud.Collector
{
	public class RepeatIndexCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(sqlString, sqlVersion, timeOut);
				if (data.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblRepeatIndex\r\n        ( DBName ,\r\n          ObjectId ,\r\n          SchemaName ,\r\n          TableName ,\r\n          IndexName ,\r\n          IndexColumnId ,\r\n          ColumnName ,\r\n          IsIncludedColumn ,\r\n          [RowCount] ,\r\n          IsDisabled ,\r\n          [FillFactor] ,\r\n          HasFilter ,\r\n          FilterDefinition\r\n        )\r\nVALUES  ( @DBName ,\r\n          @ObjectId ,\r\n          @SchemaName ,\r\n          @TableName ,\r\n          @IndexName ,\r\n          @IndexColumnId ,\r\n          @ColumnName ,\r\n          @IsIncludedColumn ,\r\n          @RowCount ,\r\n          @IsDisabled ,\r\n          @FillFactor ,\r\n          @HasFilter ,\r\n          @FilterDefinition\r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@DBName", OleDbType.VarChar)
						{
							SourceColumn = "db_name"
						},
						new OleDbParameter("@ObjectId", OleDbType.Integer)
						{
							SourceColumn = "object_id"
						},
						new OleDbParameter("@SchemaName", OleDbType.VarChar)
						{
							SourceColumn = "schema_name"
						},
						new OleDbParameter("@TableName", OleDbType.VarChar)
						{
							SourceColumn = "table_name"
						},
						new OleDbParameter("@IndexName", OleDbType.VarChar)
						{
							SourceColumn = "index_name"
						},
						new OleDbParameter("@IndexColumnId", OleDbType.Integer)
						{
							SourceColumn = "index_column_id"
						},
						new OleDbParameter("@ColumnName", OleDbType.VarChar)
						{
							SourceColumn = "column_name"
						},
						new OleDbParameter("@IsIncludedColumn", OleDbType.Boolean)
						{
							SourceColumn = "is_included_column"
						},
						new OleDbParameter("@RowCount", OleDbType.Double)
						{
							SourceColumn = "row_count"
						},
						new OleDbParameter("@IsDisabled", OleDbType.Boolean)
						{
							SourceColumn = "is_disabled"
						},
						new OleDbParameter("@FillFactor", OleDbType.Integer)
						{
							SourceColumn = "fill_factor"
						},
						new OleDbParameter("@HasFilter", OleDbType.Boolean)
						{
							SourceColumn = "has_filter"
						},
						new OleDbParameter("@FilterDefinition", OleDbType.VarChar)
						{
							SourceColumn = "filter_definition"
						}
					};
					result = base.SaveToAccess(oleDBString, data, cmdText, parameters);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			string cmdText = string.Empty;
			if (sqlVersion >= SQLServerVersion.SQLServer2008)
			{
				cmdText = "\r\nSET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED\r\nSELECT  DB_NAME(DB_ID()) [db_name] ,\r\n        i.[object_id] ,\r\n        s.name AS [schema_name] ,\r\n        t.name AS table_name ,\r\n        i.name AS index_name ,\r\n        ic.index_column_id ,\r\n        c.name AS column_name ,\r\n        ic.is_included_column ,\r\n        dps.row_count ,\r\n        i.is_disabled ,\r\n        i.fill_factor ,\r\n        i.has_filter ,\r\n        i.filter_definition\r\nFROM    sys.indexes i\r\n        JOIN sys.index_columns ic ON i.[object_id] = ic.[object_id]\r\n                                     AND i.index_id = ic.index_id\r\n        JOIN sys.columns c ON i.[object_id] = c.[object_id]\r\n                              AND ic.column_id = c.column_id\r\n        JOIN sys.tables t ON i.[object_id] = t.[object_id]\r\n        JOIN sys.schemas s ON t.[schema_id] = s.[schema_id]\r\n        JOIN ( SELECT   object_id ,\r\n                        SUM(row_count) AS row_count\r\n               FROM     sys.dm_db_partition_stats\r\n               WHERE    index_id < 2\r\n               GROUP BY object_id ,\r\n                        index_id\r\n             ) dps ON t.[object_id] = dps.[object_id]\r\nWHERE   i.[type] > 1\r\n        AND OBJECTPROPERTY(t.[object_id], 'IsMSShipped') = 0\r\nORDER BY dps.row_count DESC ,\r\n        i.[object_id] ,\r\n        i.index_id ,\r\n        ic.index_column_id\r\nOPTION (MAXDOP 2)\r\n";
			}
			else
			{
				cmdText = "\r\nSET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED\r\nSELECT  DB_NAME(DB_ID()) [db_name] ,\r\n        i.[object_id] ,\r\n        s.name AS [schema_name] ,\r\n        t.name AS table_name ,\r\n        i.name AS index_name ,\r\n        ic.index_column_id ,\r\n        c.name AS column_name ,\r\n        ic.is_included_column ,\r\n        dps.row_count ,\r\n        i.is_disabled ,\r\n        i.fill_factor ,\r\n        0 AS has_filter ,\r\n        NULL AS filter_definition\r\nFROM    sys.indexes i\r\n        JOIN sys.index_columns ic ON i.[object_id] = ic.[object_id]\r\n                                     AND i.index_id = ic.index_id\r\n        JOIN sys.columns c ON i.[object_id] = c.[object_id]\r\n                              AND ic.column_id = c.column_id\r\n        JOIN sys.tables t ON i.[object_id] = t.[object_id]\r\n        JOIN sys.schemas s ON t.[schema_id] = s.[schema_id]\r\n        JOIN ( SELECT   object_id ,\r\n                        SUM(row_count) AS row_count\r\n               FROM     sys.dm_db_partition_stats\r\n               WHERE    index_id < 2\r\n               GROUP BY object_id ,\r\n                        index_id\r\n             ) dps ON t.[object_id] = dps.[object_id]\r\nWHERE   i.[type] > 1\r\n        AND OBJECTPROPERTY(t.[object_id], 'IsMSShipped') = 0\r\nORDER BY dps.row_count DESC ,\r\n        i.[object_id] ,\r\n        i.index_id ,\r\n        ic.index_column_id\r\nOPTION (MAXDOP 2)\r\n";
			}
			return base.FillDataTable(sqlString, cmdText, timeOut);
		}
	}
}
