using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace ZhuanCloud.Collector
{
	public class StatisticsCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder(sqlString);
			string initialCatalog = sqlConnectionStringBuilder.InitialCatalog;
			bool result;
			try
			{
				DataTable data = this.GetData(sqlString, sqlVersion, timeOut);
				if (data.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblStatistics\r\n        ( DBName ,\r\n          ObjectId ,\r\n          SchemaName ,\r\n          TableName ,\r\n          IndexName ,\r\n          IndexType ,\r\n          IsDisabled ,\r\n          KeyOrdinal ,\r\n          ColumnName ,\r\n          IsIncluded ,\r\n          [FillFactor] ,\r\n          HasFilter ,\r\n          FilterDefinition ,\r\n          LastUpdate ,\r\n          FilterRows ,\r\n          RowsSimple ,\r\n          UnfilterRows ,\r\n          NowTotalRows\r\n        )\r\nVALUES  ( @DBName ,\r\n          @ObjectId ,\r\n          @SchemaName ,\r\n          @TableName ,\r\n          @IndexName ,\r\n          @IndexType ,\r\n          @IsDisabled ,\r\n          @KeyOrdinal ,\r\n          @ColumnName ,\r\n          @IsIncluded ,\r\n          @FillFactor ,\r\n          @HasFilter ,\r\n          @FilterDefinition ,\r\n          @LastUpdate ,\r\n          @FilterRows ,\r\n          @RowsSimple ,\r\n          @UnfilterRows ,\r\n          @NowTotalRows\r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@DBName", OleDbType.VarChar)
						{
							Value = initialCatalog
						},
						new OleDbParameter("@ObjectId", OleDbType.Integer)
						{
							SourceColumn = "obj_id"
						},
						new OleDbParameter("@SchemaName", OleDbType.VarChar)
						{
							SourceColumn = "schema_name"
						},
						new OleDbParameter("@TableName", OleDbType.VarChar)
						{
							SourceColumn = "table_name"
						},
						new OleDbParameter("@IndexName", OleDbType.VarChar)
						{
							SourceColumn = "index_name"
						},
						new OleDbParameter("@IndexType", OleDbType.Integer)
						{
							SourceColumn = "index_type"
						},
						new OleDbParameter("@IsDisabled", OleDbType.Boolean)
						{
							SourceColumn = "is_disabled"
						},
						new OleDbParameter("@KeyOrdinal", OleDbType.Integer)
						{
							SourceColumn = "key_ordinal"
						},
						new OleDbParameter("@ColumnName", OleDbType.VarChar)
						{
							SourceColumn = "column_name"
						},
						new OleDbParameter("@IsIncluded", OleDbType.Boolean)
						{
							SourceColumn = "is_included_column"
						},
						new OleDbParameter("@FillFactor", OleDbType.Integer)
						{
							SourceColumn = "fill_factor"
						},
						new OleDbParameter("@HasFilter", OleDbType.Boolean)
						{
							SourceColumn = "has_filter"
						},
						new OleDbParameter("@FilterDefinition", OleDbType.LongVarWChar)
						{
							SourceColumn = "filter_definition"
						},
						new OleDbParameter("@LastUpdate", OleDbType.Date)
						{
							SourceColumn = "last_update"
						},
						new OleDbParameter("@FilterRows", OleDbType.Double)
						{
							SourceColumn = "filter_rows"
						},
						new OleDbParameter("@RowsSimple", OleDbType.Double)
						{
							SourceColumn = "rows_simple"
						},
						new OleDbParameter("@UnfilterRows", OleDbType.Double)
						{
							SourceColumn = "unfilter_rows"
						},
						new OleDbParameter("@NowTotalRows", OleDbType.Double)
						{
							SourceColumn = "now_total_rows"
						}
					};
					result = base.SaveToAccess(oleDBString, data, cmdText, parameters);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			DataTable result = new DataTable();
			string cmdText = "SELECT collation_name FROM sys.databases WHERE database_id=DB_ID()";
			object obj = base.SQLExecuteScalar(sqlString, cmdText, timeOut);
			if (obj != null)
			{
				string arg = obj.ToString();
				cmdText = string.Empty;
				if (sqlVersion >= SQLServerVersion.SQLServer2008)
				{
					cmdText = string.Format("\r\nCREATE TABLE #EFS_Statics\r\n(\r\n\t[obj_id] int,\r\n\t[schema_name] sysname,\r\n\ttable_name sysname,\r\n\tindex_name sysname,\r\n\tindex_type tinyint,\r\n\tis_disabled bit,\r\n\tkey_ordinal tinyint,\r\n\tcolumn_name sysname,\r\n\tis_included_column bit,\r\n\tfill_factor tinyint,\r\n\thas_filter bit,\r\n\tfilter_definition nvarchar(max) ,\r\n\tlast_update datetime,\r\n\tfilter_rows bigint,\t\r\n\trows_simple bigint,\r\n\tunfilter_rows bigint,\r\n\tnow_total_rows bigint,\r\n)\r\nCREATE TABLE #EFS_Statics_Rows\r\n(\r\n\tName sysname COLLATE {0} ,\r\n\tLastUpdate datetime ,\r\n\tFilteredRows bigint ,\r\n\tRowsSampled bigint ,\r\n\tSteps int ,\r\n\tDensity int ,\r\n\tAverageKeyLength float ,\r\n\tStringIndex nvarchar(max) ,\r\n\tFilterExpression nvarchar(MAX),\r\n\tUnfilteredRows bigint\r\n)\r\nDECLARE cur_stats CURSOR READ_ONLY FORWARD_ONLY\r\nFOR\r\n    SELECT  o.[object_id] ,\r\n            s.name AS [schema_name] ,\r\n            o.name AS [table_name] ,\r\n            i.name AS index_name\r\n    FROM    sys.objects o\r\n            JOIN sys.schemas s ON o.[schema_id] = s.[schema_id]\r\n                                  AND o.is_ms_shipped = 0\r\n                                  AND o.[type] = 'U'\r\n            JOIN sys.indexes i ON i.[object_id] = o.[object_id]\r\n            JOIN ( SELECT   object_id ,\r\n                            SUM(row_count) AS row_count\r\n                   FROM     sys.dm_db_partition_stats\r\n                   WHERE    index_id < 2\r\n                            AND row_count > 200000\r\n                   GROUP BY object_id ,\r\n                            index_id\r\n                 ) dps ON o.[object_id] = dps.[object_id]\r\n    WHERE   i.name IS NOT NULL            \r\nDECLARE @obj_id int\r\nDECLARE @schema_name sysname\r\nDECLARE @table_name sysname \r\nDECLARE @index_name sysname\r\nDECLARE @obj_name nvarchar(256)\r\nDECLARE @sql nvarchar(500)\r\n \r\nOPEN cur_stats\r\nFETCH NEXT FROM cur_stats INTO @obj_id, @schema_name, @table_name, @index_name \r\nWHILE @@FETCH_STATUS = 0 \r\n    BEGIN\r\n        SET @obj_name = '[' + @schema_name + '].[' + @table_name + ']'\r\n        SET @sql = 'DBCC SHOW_STATISTICS(''' + @obj_name + ''',N'''\r\n            + @index_name + ''') WITH STAT_HEADER '\r\n        \r\n        INSERT  INTO #EFS_Statics_Rows  EXEC (@sql)\r\n        INSERT  INTO #EFS_Statics\r\n                SELECT  @obj_id AS obj_id ,\r\n                        @schema_name AS [schema_name] ,\r\n                        @table_name AS table_name ,\r\n                        i.name AS index_name ,\r\n                        i.[type] AS index_type ,\r\n                        i.is_disabled ,\r\n                        ic.key_ordinal ,\r\n                        c.name AS column_name ,\r\n                        ic.is_included_column ,\r\n                        i.fill_factor ,\r\n                        i.has_filter ,\r\n                        i.filter_definition ,\r\n                        sr.LastUpdate AS last_update ,\r\n                        sr.FilteredRows AS filter_rows ,\r\n                        sr.RowsSampled AS rows_simple ,\r\n                        sr.UnfilteredRows AS unfilter_rows ,\r\n                        dps.row_count AS now_total_rows\r\n                FROM    sys.indexes i \r\n                        JOIN sys.index_columns ic ON i.[object_id] = ic.[object_id]\r\n\t\t                AND i.index_id = ic.index_id\r\n                        JOIN sys.columns c ON i.[object_id] = c.[object_id]\r\n\t\t                AND c.[object_id] = ic.[object_id]\r\n\t\t                AND ic.column_id = c.column_id\r\n                        JOIN \r\n\t\t                (\r\n\t\t\t                SELECT  object_id ,\r\n\t\t\t\t\t                SUM(row_count) AS row_count\r\n\t\t\t                FROM    sys.dm_db_partition_stats\r\n\t\t\t                WHERE   index_id < 2\r\n\t\t\t                GROUP BY object_id ,\r\n\t\t\t\t\t                index_id\r\n\t\t                ) dps ON i.[object_id] = dps.[object_id] AND c.[object_id] = dps.[object_id]\r\n                        JOIN  #EFS_Statics_Rows sr ON i.name = sr.Name\r\n                WHERE \r\n                        i.[object_id] = @obj_id\r\n                        AND DATEADD(dd, -7, GETDATE()) >= sr.LastUpdate\r\n                        AND dps.row_count - 100000 >= sr.UnfilteredRows\r\n\r\n        TRUNCATE TABLE #EFS_Statics_Rows\r\n        FETCH NEXT FROM cur_stats INTO @obj_id, @schema_name, @table_name, @index_name \r\n    END\r\nCLOSE cur_stats\r\nDEALLOCATE cur_stats\r\nDROP TABLE #EFS_Statics_Rows\r\nSELECT * FROM    #EFS_Statics\r\nDROP TABLE #EFS_Statics\r\n", arg);
				}
				else
				{
					cmdText = string.Format("\r\nCREATE TABLE #EFS_Statics\r\n(\r\n\t[obj_id] int,\r\n\t[schema_name] sysname,\r\n\ttable_name sysname,\r\n\tindex_name sysname,\r\n\tindex_type tinyint,\r\n\tis_disabled bit,\r\n\tkey_ordinal tinyint,\r\n\tcolumn_name sysname,\r\n\tis_included_column bit,\r\n\tfill_factor tinyint,\r\n\thas_filter bit,\r\n\tfilter_definition nvarchar(max) ,\r\n\tlast_update datetime,\r\n\tfilter_rows bigint,\t\r\n\trows_simple bigint,\r\n\tunfilter_rows bigint,\r\n\tnow_total_rows bigint,\r\n)\r\nCREATE TABLE #EFS_Statics_Rows\r\n(\r\n\tName sysname COLLATE {0} ,\r\n\tLastUpdate datetime ,\r\n\tFilteredRows bigint ,\r\n\tRowsSampled bigint ,\r\n\tSteps int ,\r\n\tDensity int ,\r\n\tAverageKeyLength float ,\r\n\tStringIndex nvarchar(max) \r\n)\r\nDECLARE cur_stats CURSOR READ_ONLY FORWARD_ONLY\r\nFOR\r\n    SELECT  o.[object_id] ,\r\n            s.name AS [schema_name] ,\r\n            o.name AS [table_name] ,\r\n            i.name AS index_name\r\n    FROM    sys.objects o\r\n            JOIN sys.schemas s ON o.[schema_id] = s.[schema_id]\r\n            JOIN sys.indexes i ON i.[object_id] = o.[object_id]\r\n            JOIN ( SELECT   object_id ,\r\n                            SUM(row_count) AS row_count\r\n                   FROM     sys.dm_db_partition_stats\r\n                   WHERE    index_id < 2\r\n                            AND row_count > 200000\r\n                   GROUP BY object_id ,\r\n                            index_id\r\n                 ) dps ON o.[object_id] = dps.[object_id]\r\n    WHERE   i.name IS NOT NULL\r\n            AND o.is_ms_shipped = 0\r\n            AND o.[type] = 'U'           \r\nDECLARE @obj_id int\r\nDECLARE @schema_name sysname\r\nDECLARE @table_name sysname \r\nDECLARE @index_name sysname\r\nDECLARE @obj_name nvarchar(256)\r\nDECLARE @sql nvarchar(500)\r\n \r\nOPEN cur_stats\r\nFETCH NEXT FROM cur_stats INTO @obj_id, @schema_name, @table_name, @index_name \r\nWHILE @@FETCH_STATUS = 0 \r\n    BEGIN\r\n        SET @obj_name = '[' + @schema_name + '].[' + @table_name + ']'\r\n        SET @sql = 'DBCC SHOW_STATISTICS(''' + @obj_name + ''',N'''\r\n            + @index_name + ''') WITH STAT_HEADER '\r\n        \r\n        INSERT  INTO #EFS_Statics_Rows  EXEC (@sql)\r\n        INSERT  INTO #EFS_Statics\r\n                SELECT  @obj_id AS obj_id ,\r\n                        @schema_name AS [schema_name] ,\r\n                        @table_name AS table_name ,\r\n                        i.name AS index_name ,\r\n                        i.[type] AS index_type ,\r\n                        i.is_disabled ,\r\n                        ic.key_ordinal ,\r\n                        c.name AS column_name ,\r\n                        ic.is_included_column ,\r\n                        i.fill_factor ,\r\n                        0 AS has_filter ,\r\n                        NULL AS filter_definition ,\r\n                        sr.LastUpdate AS last_update ,\r\n                        sr.FilteredRows AS filter_rows ,\r\n                        sr.RowsSampled AS rows_simple ,\r\n                        sr.FilteredRows AS unfilter_rows ,\r\n                        dps.row_count AS now_total_rows\r\n                FROM    sys.indexes i \r\n                        JOIN sys.index_columns ic ON i.[object_id] = ic.[object_id]\r\n\t\t                AND i.index_id = ic.index_id\r\n                        JOIN sys.columns c ON i.[object_id] = c.[object_id]\r\n\t\t                AND c.[object_id] = ic.[object_id]\r\n\t\t                AND ic.column_id = c.column_id\r\n                        JOIN \r\n\t\t                (\r\n\t\t\t                SELECT  object_id ,\r\n\t\t\t\t\t                SUM(row_count) AS row_count\r\n\t\t\t                FROM    sys.dm_db_partition_stats\r\n\t\t\t                WHERE   index_id < 2\r\n\t\t\t                GROUP BY object_id ,\r\n\t\t\t\t\t                index_id\r\n\t\t                ) dps ON i.[object_id] = dps.[object_id] AND c.[object_id] = dps.[object_id]\r\n                        JOIN  #EFS_Statics_Rows sr ON i.name = sr.Name\r\n                WHERE \r\n                        i.[object_id] = @obj_id\r\n                        AND DATEADD(dd, -7, GETDATE()) >= sr.LastUpdate\r\n                        AND dps.row_count - 100000 >= sr.FilteredRows\r\n\r\n\r\n        TRUNCATE TABLE #EFS_Statics_Rows\r\n        FETCH NEXT FROM cur_stats INTO @obj_id, @schema_name, @table_name, @index_name \r\n    END\r\nCLOSE cur_stats\r\nDEALLOCATE cur_stats\r\nDROP TABLE #EFS_Statics_Rows\r\nSELECT * FROM    #EFS_Statics\r\nDROP TABLE #EFS_Statics\r\n", arg);
				}
				return base.FillDataTable(sqlString, cmdText, timeOut);
			}
			return result;
		}
	}
}
