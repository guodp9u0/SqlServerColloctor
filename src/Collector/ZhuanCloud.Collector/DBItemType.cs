using System;
using System.ComponentModel;

namespace ZhuanCloud.Collector
{
	public class DBItemType
	{
		[DefaultValue(true), Description("是否收集表结构"), DisplayName("\t\t\t表")]
		public bool HasTable
		{
			get;
			set;
		}

		[DefaultValue(true), Description("是否收集视图定义"), DisplayName("\t\t视图")]
		public bool HasView
		{
			get;
			set;
		}

		[DefaultValue(true), Description("是否收集存储过程定义"), DisplayName("\t存储过程")]
		public bool HasStoredProc
		{
			get;
			set;
		}

		[DefaultValue(true), Description("是否收集函数定义"), DisplayName("函数")]
		public bool HasFuction
		{
			get;
			set;
		}
	}
}
