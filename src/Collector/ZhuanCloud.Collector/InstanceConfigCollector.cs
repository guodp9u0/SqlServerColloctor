using System;
using System.Data;
using System.Data.OleDb;

namespace ZhuanCloud.Collector
{
	public class InstanceConfigCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, int timeOut)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(sqlString, timeOut);
				if (data.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblInstanceConfig\r\n        ( ConfigName ,\r\n          Minimum ,\r\n          Maximum ,\r\n          ConfigValue ,\r\n          RunValue ,\r\n          ReStartRequried ,\r\n          IsAdvanced\r\n        )\r\nVALUES  ( @ConfigName ,\r\n          @Minimum ,\r\n          @Maximum ,\r\n          @ConfigValue ,\r\n          @RunValue ,\r\n          @ReStartRequried ,\r\n          @IsAdvanced\r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@ConfigName", OleDbType.VarChar)
						{
							SourceColumn = "name"
						},
						new OleDbParameter("@Minimum", OleDbType.Integer)
						{
							SourceColumn = "minimum"
						},
						new OleDbParameter("@Maximum", OleDbType.Integer)
						{
							SourceColumn = "maximum"
						},
						new OleDbParameter("@ConfigValue", OleDbType.Integer)
						{
							SourceColumn = "config_value"
						},
						new OleDbParameter("@RunValue", OleDbType.Integer)
						{
							SourceColumn = "run_value"
						},
						new OleDbParameter("@RunValue", OleDbType.Boolean)
						{
							SourceColumn = "is_dynamic"
						},
						new OleDbParameter("@IsAdvanced", OleDbType.Boolean)
						{
							SourceColumn = "is_advanced"
						}
					};
					result = base.SaveToAccess(oleDBString, data, cmdText, parameters);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, int timeOut)
		{
			string cmdText = "\r\nSELECT  name ,\r\n        minimum ,\r\n        maximum ,\r\n        value AS config_value ,\r\n        value_in_use AS run_value ,\r\n        is_dynamic,\r\n        is_advanced\r\nFROM    sys.configurations\r\nORDER BY name\r\n";
			return base.FillDataTable(sqlString, cmdText, timeOut);
		}
	}
}
