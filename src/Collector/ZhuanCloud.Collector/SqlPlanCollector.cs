using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace ZhuanCloud.Collector
{
	public class SqlPlanCollector : OleDbCollector
	{
		private int rowID;

		public int RowID
		{
			get
			{
				return this.rowID;
			}
			set
			{
				this.rowID = value;
			}
		}

		public bool SaveData(string oleDBString, string sqlString, int timeOut, int duration, DateTime lastExecutionTime, ref DateTime newExecutionTime)
		{
			new List<SqlPlan>();
			string commandText = string.Format("\r\nSELECT\r\n\tindividual_query = SUBSTRING (text, (statement_start_offset/2)+1, ((CASE WHEN statement_end_offset = -1 THEN LEN(CONVERT(NVARCHAR(MAX), text)) * 2 ELSE statement_end_offset END - statement_start_offset)/2)+1),\r\n\tparent_query = text,\r\n\tCONVERT(VARCHAR(100),creation_time,120) creation_time,\r\n\tCONVERT(VARCHAR(100),last_execution_time,120) last_execution_time,\r\n\texecution_count,\r\n\ttotal_elapsed_time,\r\n\tavg_elapsed_time = total_elapsed_time / execution_count,\r\n\tmin_elapsed_time,\r\n\tmax_elapsed_time,\r\n\ttotal_worker_time,\r\n\tavg_worker_time = total_worker_time / execution_count,\r\n\tmin_worker_time,\r\n\tmax_worker_time,\t\r\n\ttotal_logical_reads,\r\n\tavg_logical_reads = total_logical_reads / execution_count,\r\n\tmin_logical_reads,\r\n\tmax_logical_reads,\t\r\n\ttotal_logical_writes,\r\n\tavg_logical_writes = total_logical_writes / execution_count,\r\n\tmin_logical_writes,\r\n\tmax_logical_writes,\r\n\ttotal_physical_reads,\r\n\tavg_physical_reads = total_physical_reads / execution_count,\r\n\tmin_physical_reads,\r\n\tmax_physical_reads,\r\n\tplan_handle,\r\n\tquery_plan\r\nINTO #EFS_SqlPlan\t\r\nFROM \r\n\tsys.dm_exec_query_stats qs\r\nCROSS APPLY \r\n\tsys.dm_exec_sql_text(qs.sql_handle) AS st\r\nCROSS APPLY\r\n\tsys.dm_exec_query_plan(plan_handle) qy\r\nWHERE \r\n    last_execution_time > '{0}'\r\n\tAND total_elapsed_time / execution_count / 1000 >= {1}\r\nORDER BY execution_count \r\nOPTION (MAXDOP 2)\r\nSELECT * FROM #EFS_SqlPlan OPTION (MAXDOP 2)\r\nDROP TABLE #EFS_SqlPlan", lastExecutionTime, duration);
			newExecutionTime = DateTime.Parse("1900/01/01 00:00:00");
			OleDbConnection oleDbConnection = null;
			if (!Util.TryConnect(oleDBString, out oleDbConnection))
			{
				return false;
			}
			try
			{
				List<string> planHanlderList = this.GetPlanHanlderList(oleDBString, timeOut);
				oleDbConnection.Open();
				SqlConnection sqlConnection = new SqlConnection(sqlString);
				SqlCommand sqlCommand = sqlConnection.CreateCommand();
				sqlCommand.CommandTimeout = timeOut;
				sqlCommand.CommandText = commandText;
				sqlConnection.Open();
				IDataReader dataReader = sqlCommand.ExecuteReader();
				MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
				while (dataReader.Read())
				{
					SqlPlan sqlPlan = new SqlPlan();
					sqlPlan.RowID = this.rowID;
					sqlPlan.IndividualQuery = ((dataReader["individual_query"] == null) ? string.Empty : dataReader["individual_query"].ToString());
					if (!string.IsNullOrEmpty(sqlPlan.IndividualQuery))
					{
						byte[] bytes = TextManager.Encoding.GetBytes(sqlPlan.IndividualQuery);
						byte[] inArray = mD5CryptoServiceProvider.ComputeHash(bytes);
						sqlPlan.TextHashCode = Convert.ToBase64String(inArray);
						TextManager.GetRegexString(sqlPlan.IndividualQuery);
						bytes = TextManager.Encoding.GetBytes(sqlPlan.IndividualQuery);
						inArray = mD5CryptoServiceProvider.ComputeHash(bytes);
						sqlPlan.ParameterHash = Convert.ToBase64String(inArray);
					}
					else
					{
						sqlPlan.TextHashCode = string.Empty;
						sqlPlan.ParameterHash = string.Empty;
					}
					sqlPlan.ParentQuery = ((dataReader["parent_query"] == null) ? string.Empty : dataReader["parent_query"].ToString());
					sqlPlan.CreationTime = Util.ConvertToDate(dataReader["creation_time"]);
					sqlPlan.LastExecutionTime = Util.ConvertToDate(dataReader["last_execution_time"]);
					sqlPlan.ExecutionCount = ((dataReader["execution_count"] == null) ? 0.0 : Convert.ToDouble(dataReader["execution_count"]));
					sqlPlan.TotalElapsedTime = ((dataReader["total_elapsed_time"] == null) ? 0.0 : Convert.ToDouble(dataReader["total_elapsed_time"]));
					sqlPlan.AvgElapsedTime = ((dataReader["avg_elapsed_time"] == null) ? 0.0 : Convert.ToDouble(dataReader["avg_elapsed_time"]));
					sqlPlan.MinElapsedTime = ((dataReader["min_elapsed_time"] == null) ? 0.0 : Convert.ToDouble(dataReader["min_elapsed_time"]));
					sqlPlan.MaxElapsedTime = ((dataReader["max_elapsed_time"] == null) ? 0.0 : Convert.ToDouble(dataReader["max_elapsed_time"]));
					sqlPlan.TotalWorkerTime = ((dataReader["total_worker_time"] == null) ? 0.0 : Convert.ToDouble(dataReader["total_worker_time"]));
					sqlPlan.AvgWorkerTime = ((dataReader["avg_worker_time"] == null) ? 0.0 : Convert.ToDouble(dataReader["avg_worker_time"]));
					sqlPlan.MinWorkerTime = ((dataReader["min_worker_time"] == null) ? 0.0 : Convert.ToDouble(dataReader["min_worker_time"]));
					sqlPlan.MaxWorkerTime = ((dataReader["max_worker_time"] == null) ? 0.0 : Convert.ToDouble(dataReader["max_worker_time"]));
					sqlPlan.TotalLogicalReads = ((dataReader["total_logical_reads"] == null) ? 0.0 : Convert.ToDouble(dataReader["total_logical_reads"]));
					sqlPlan.AvgLogicalReads = ((dataReader["avg_logical_reads"] == null) ? 0.0 : Convert.ToDouble(dataReader["avg_logical_reads"]));
					sqlPlan.MinLogicalReads = ((dataReader["min_logical_reads"] == null) ? 0.0 : Convert.ToDouble(dataReader["min_logical_reads"]));
					sqlPlan.MaxLogicalReads = ((dataReader["max_logical_reads"] == null) ? 0.0 : Convert.ToDouble(dataReader["max_logical_reads"]));
					sqlPlan.TotalLogicalWrites = ((dataReader["total_logical_writes"] == null) ? 0.0 : Convert.ToDouble(dataReader["total_logical_writes"]));
					sqlPlan.AvgLogicalWrites = ((dataReader["avg_logical_writes"] == null) ? 0.0 : Convert.ToDouble(dataReader["avg_logical_writes"]));
					sqlPlan.MinLogicalWrites = ((dataReader["min_logical_writes"] == null) ? 0.0 : Convert.ToDouble(dataReader["min_logical_writes"]));
					sqlPlan.MaxLogicalWrites = ((dataReader["max_logical_writes"] == null) ? 0.0 : Convert.ToDouble(dataReader["max_logical_writes"]));
					sqlPlan.TotalPhysicalReads = ((dataReader["total_physical_reads"] == null) ? 0.0 : Convert.ToDouble(dataReader["total_physical_reads"]));
					sqlPlan.AvgPhysicalReads = ((dataReader["avg_physical_reads"] == null) ? 0.0 : Convert.ToDouble(dataReader["avg_physical_reads"]));
					sqlPlan.MinPhysicalReads = ((dataReader["min_physical_reads"] == null) ? 0.0 : Convert.ToDouble(dataReader["min_physical_reads"]));
					sqlPlan.MaxPhysicalReads = ((dataReader["max_physical_reads"] == null) ? 0.0 : Convert.ToDouble(dataReader["max_physical_reads"]));
					sqlPlan.PlanHandle = (Util.ValueIsNull(dataReader["plan_handle"]) ? null : ((byte[])dataReader["plan_handle"]));
					string text = (dataReader["query_plan"] == null) ? string.Empty : dataReader["query_plan"].ToString();
					sqlPlan.PlanText = TextManager.Compress(text);
					sqlPlan.HasSqlPlan = false;
					if (sqlPlan.LastExecutionTime > newExecutionTime)
					{
						newExecutionTime = sqlPlan.LastExecutionTime;
					}
					if (!string.IsNullOrEmpty(sqlPlan.ParameterHash))
					{
						if (!Util.planExecDic.ContainsKey(sqlPlan.ParameterHash))
						{
							SqlPlanExec sqlPlanExec = new SqlPlanExec();
							sqlPlanExec.ExecCount = sqlPlan.ExecutionCount;
							sqlPlanExec.TypeCount = 1;
							Util.planExecDic.Add(sqlPlan.ParameterHash, sqlPlanExec);
						}
						else
						{
							Util.planExecDic[sqlPlan.ParameterHash].ExecCount += sqlPlan.ExecutionCount;
							Util.planExecDic[sqlPlan.ParameterHash].TypeCount++;
						}
						string item = (sqlPlan.PlanHandle == null) ? string.Empty : ("0x" + BitConverter.ToString(sqlPlan.PlanHandle).Replace("-", ""));
						if (planHanlderList.Contains(item))
						{
							this.UpdateData(oleDbConnection, timeOut, sqlPlan);
						}
						else
						{
							if (Util.planExecDic[sqlPlan.ParameterHash].ExecCount > 10.0)
							{
								string childPlanText = this.GetChildPlanText(sqlPlan.IndividualQuery, text);
								if (string.IsNullOrEmpty(childPlanText) && !string.IsNullOrEmpty(text))
								{
									sqlPlan.HasMissIndex = text.Contains("<MissingIndexes>");
									sqlPlan.HasImplicit = text.Contains("CONVERT_IMPLICIT");
								}
								else
								{
									sqlPlan.HasMissIndex = childPlanText.Contains("<MissingIndexes>");
									sqlPlan.HasImplicit = childPlanText.Contains("CONVERT_IMPLICIT");
								}
								if (Util.planExecDic[sqlPlan.ParameterHash].TypeCount <= 10)
								{
									sqlPlan.HasSqlPlan = true;
									this.InsertDataWithPlan(oleDbConnection, timeOut, this.rowID, sqlPlan);
									if (!string.IsNullOrEmpty(childPlanText))
									{
										List<MissIndexItem> missIndexItems = this.GetMissIndexItems(childPlanText);
										if (missIndexItems != null && missIndexItems.Count > 0)
										{
											this.InsertMissIndex(oleDbConnection, timeOut, this.rowID, missIndexItems);
										}
									}
								}
								else
								{
									this.InsertData(oleDbConnection, timeOut, this.rowID, sqlPlan);
								}
							}
							planHanlderList.Add(item);
							this.rowID++;
						}
					}
					if (!Util.startFlag)
					{
						bool result = false;
						return result;
					}
				}
				sqlConnection.Close();
				oleDbConnection.Close();
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				bool result = false;
				return result;
			}
			finally
			{
				if (oleDbConnection.State != ConnectionState.Closed)
				{
					oleDbConnection.Close();
				}
				oleDbConnection.Dispose();
			}
			return true;
		}

		public List<string> GetPlanHanlderList(string oleDBString, int timeOut)
		{
			List<string> list = new List<string>();
			OleDbConnection oleDbConnection = null;
			if (!Util.TryConnect(oleDBString, out oleDbConnection))
			{
				return list;
			}
			try
			{
				OleDbCommand oleDbCommand = oleDbConnection.CreateCommand();
				oleDbCommand.CommandTimeout = timeOut;
				oleDbCommand.CommandText = "SELECT PlanHandle FROM tblSqlPlan";
				oleDbConnection.Open();
				OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader(CommandBehavior.CloseConnection);
				while (oleDbDataReader.Read())
				{
					string text = (oleDbDataReader[0] == DBNull.Value) ? string.Empty : ("0x" + BitConverter.ToString((byte[])oleDbDataReader[0]).Replace("-", ""));
					if (!string.IsNullOrEmpty(text))
					{
						list.Add(text);
					}
				}
				oleDbConnection.Close();
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
			}
			finally
			{
				if (oleDbConnection.State != ConnectionState.Closed)
				{
					oleDbConnection.Close();
				}
				oleDbConnection.Dispose();
			}
			return list;
		}

		private bool InsertData(OleDbConnection con, int timeOut, int id, SqlPlan sqlPlan)
		{
			string commandText = "\r\nINSERT  INTO tblSqlPlan\r\n        ( ID ,\r\n          CreationTime ,\r\n          LastExecutionTime ,\r\n          ExecutionCount ,\r\n          TotalElapsedTime ,\r\n          AvgElapsedTime ,\r\n          MinElapsedTime ,\r\n          MaxElapsedTime ,\r\n          TotalWorkerTime ,\r\n          AvgWorkerTime ,\r\n          MinWorkerTime ,\r\n          MaxWorkerTime ,\r\n          TotalLogicalReads ,\r\n          AvgLogicalReads ,\r\n          MinLogicalReads ,\r\n          MaxLogicalReads ,\r\n          TotalLogicalWrites ,\r\n          AvgLogicalWrites ,\r\n          MinLogicalWrites ,\r\n          MaxLogicalWrites ,\r\n          TotalPhysicalReads ,\r\n          AvgPhysicalReads ,\r\n          MinPhysicalReads ,\r\n          MaxPhysicalReads , \r\n          PlanHandle ,        \r\n          TextHashCode ,\r\n          ParameterHash ,\r\n          HasMissIndex ,\r\n          HasImplicit ,\r\n          HasSqlPlan \r\n        )\r\nVALUES  ( @ID ,\r\n          @CreationTime ,\r\n          @LastExecutionTime ,\r\n          @ExecutionCount ,\r\n          @TotalElapsedTime ,\r\n          @AvgElapsedTime ,\r\n          @MinElapsedTime ,\r\n          @MaxElapsedTime ,\r\n          @TotalWorkerTime ,\r\n          @AvgWorkerTime ,\r\n          @MinWorkerTime ,\r\n          @MaxWorkerTime ,\r\n          @TotalLogicalReads ,\r\n          @AvgLogicalReads ,\r\n          @MinLogicalReads ,\r\n          @MaxLogicalReads ,\r\n          @TotalLogicalWrites ,\r\n          @AvgLogicalWrites ,\r\n          @MinLogicalWrites ,\r\n          @MaxLogicalWrites ,\r\n          @TotalPhysicalReads ,\r\n          @AvgPhysicalReads ,\r\n          @MinPhysicalReads ,\r\n          @MaxPhysicalReads ,\r\n          @PlanHandle ,\r\n          @TextHashCode ,\r\n          @ParameterHash ,\r\n          @HasMissIndex ,\r\n          @HasImplicit ,\r\n          @HasSqlPlan \r\n        )\r\n";
			OleDbParameter[] values = new OleDbParameter[]
			{
				new OleDbParameter("@ID", OleDbType.Integer)
				{
					Value = id
				},
				new OleDbParameter("@CreationTime", OleDbType.Date)
				{
					Value = sqlPlan.CreationTime
				},
				new OleDbParameter("@LastExecutionTime", OleDbType.Date)
				{
					Value = sqlPlan.LastExecutionTime
				},
				new OleDbParameter("@ExecutionCount", OleDbType.Double)
				{
					Value = sqlPlan.ExecutionCount
				},
				new OleDbParameter("@TotalElapsedTime", OleDbType.Double)
				{
					Value = sqlPlan.TotalElapsedTime
				},
				new OleDbParameter("@AvgElapsedTime", OleDbType.Double)
				{
					Value = sqlPlan.AvgElapsedTime
				},
				new OleDbParameter("@MinElapsedTime", OleDbType.Double)
				{
					Value = sqlPlan.MinElapsedTime
				},
				new OleDbParameter("@MaxElapsedTime", OleDbType.Double)
				{
					Value = sqlPlan.MaxElapsedTime
				},
				new OleDbParameter("@TotalWorkerTime", OleDbType.Double)
				{
					Value = sqlPlan.TotalWorkerTime
				},
				new OleDbParameter("@AvgWorkerTime", OleDbType.Double)
				{
					Value = sqlPlan.AvgWorkerTime
				},
				new OleDbParameter("@MinWorkerTime", OleDbType.Double)
				{
					Value = sqlPlan.MinWorkerTime
				},
				new OleDbParameter("@MaxWorkerTime", OleDbType.Double)
				{
					Value = sqlPlan.MaxWorkerTime
				},
				new OleDbParameter("@TotalLogicalReads", OleDbType.Double)
				{
					Value = sqlPlan.TotalLogicalReads
				},
				new OleDbParameter("@AvgLogicalReads", OleDbType.Double)
				{
					Value = sqlPlan.AvgLogicalReads
				},
				new OleDbParameter("@MinLogicalReads", OleDbType.Double)
				{
					Value = sqlPlan.MinLogicalReads
				},
				new OleDbParameter("@MaxLogicalReads", OleDbType.Double)
				{
					Value = sqlPlan.MaxLogicalReads
				},
				new OleDbParameter("@TotalLogicalWrites", OleDbType.Double)
				{
					Value = sqlPlan.TotalLogicalWrites
				},
				new OleDbParameter("@AvgLogicalWrites", OleDbType.Double)
				{
					Value = sqlPlan.AvgLogicalWrites
				},
				new OleDbParameter("@MinLogicalWrites", OleDbType.Double)
				{
					Value = sqlPlan.MinLogicalWrites
				},
				new OleDbParameter("@MaxLogicalWrites", OleDbType.Double)
				{
					Value = sqlPlan.MaxLogicalWrites
				},
				new OleDbParameter("@TotalPhysicalReads", OleDbType.Double)
				{
					Value = sqlPlan.TotalPhysicalReads
				},
				new OleDbParameter("@AvgPhysicalReads", OleDbType.Double)
				{
					Value = sqlPlan.AvgPhysicalReads
				},
				new OleDbParameter("@MinPhysicalReads", OleDbType.Double)
				{
					Value = sqlPlan.MinPhysicalReads
				},
				new OleDbParameter("@MaxPhysicalReads", OleDbType.Double)
				{
					Value = sqlPlan.MaxPhysicalReads
				},
				new OleDbParameter("@PlanHandle", OleDbType.VarBinary)
				{
					Value = sqlPlan.PlanHandle
				},
				new OleDbParameter("@TextHashCode", OleDbType.VarChar)
				{
					Value = sqlPlan.TextHashCode
				},
				new OleDbParameter("@ParameterHash", OleDbType.VarChar)
				{
					Value = sqlPlan.ParameterHash
				},
				new OleDbParameter("@HasMissIndex", OleDbType.Boolean)
				{
					Value = sqlPlan.HasMissIndex
				},
				new OleDbParameter("@HasImplicit", OleDbType.Boolean)
				{
					Value = sqlPlan.HasImplicit
				},
				new OleDbParameter("@HasSqlPlan", OleDbType.Boolean)
				{
					Value = sqlPlan.HasSqlPlan
				}
			};
			bool result;
			try
			{
				OleDbCommand oleDbCommand = con.CreateCommand();
				oleDbCommand.CommandTimeout = timeOut;
				oleDbCommand.CommandText = commandText;
				oleDbCommand.Parameters.Clear();
				oleDbCommand.Parameters.AddRange(values);
				int num = oleDbCommand.ExecuteNonQuery();
				result = (num > 0);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private bool InsertDataWithPlan(OleDbConnection con, int timeOut, int id, SqlPlan sqlPlan)
		{
			string commandText = "\r\nINSERT  INTO tblSqlPlan\r\n        ( ID ,\r\n          IndividualQuery ,\r\n          ParentQuery ,\r\n          CreationTime ,\r\n          LastExecutionTime ,\r\n          ExecutionCount ,\r\n          TotalElapsedTime ,\r\n          AvgElapsedTime ,\r\n          MinElapsedTime ,\r\n          MaxElapsedTime ,\r\n          TotalWorkerTime ,\r\n          AvgWorkerTime ,\r\n          MinWorkerTime ,\r\n          MaxWorkerTime ,\r\n          TotalLogicalReads ,\r\n          AvgLogicalReads ,\r\n          MinLogicalReads ,\r\n          MaxLogicalReads ,\r\n          TotalLogicalWrites ,\r\n          AvgLogicalWrites ,\r\n          MinLogicalWrites ,\r\n          MaxLogicalWrites ,\r\n          TotalPhysicalReads ,\r\n          AvgPhysicalReads ,\r\n          MinPhysicalReads ,\r\n          MaxPhysicalReads ,\r\n          PlanHandle ,\r\n          PlanText ,\r\n          TextHashCode ,\r\n          ParameterHash ,\r\n          HasMissIndex ,\r\n          HasImplicit ,\r\n          HasSqlPlan\r\n        )\r\nVALUES  ( @ID ,\r\n          @IndividualQuery ,\r\n          @ParentQuery ,\r\n          @CreationTime ,\r\n          @LastExecutionTime ,\r\n          @ExecutionCount ,\r\n          @TotalElapsedTime ,\r\n          @AvgElapsedTime ,\r\n          @MinElapsedTime ,\r\n          @MaxElapsedTime ,\r\n          @TotalWorkerTime ,\r\n          @AvgWorkerTime ,\r\n          @MinWorkerTime ,\r\n          @MaxWorkerTime ,\r\n          @TotalLogicalReads ,\r\n          @AvgLogicalReads ,\r\n          @MinLogicalReads ,\r\n          @MaxLogicalReads ,\r\n          @TotalLogicalWrites ,\r\n          @AvgLogicalWrites ,\r\n          @MinLogicalWrites ,\r\n          @MaxLogicalWrites ,\r\n          @TotalPhysicalReads ,\r\n          @AvgPhysicalReads ,\r\n          @MinPhysicalReads ,\r\n          @MaxPhysicalReads ,\r\n          @PlanHandle ,\r\n          @PlanText ,\r\n          @TextHashCode ,\r\n          @ParameterHash ,\r\n          @HasMissIndex ,\r\n          @HasImplicit ,\r\n          HasSqlPlan\r\n        )\r\n";
			OleDbParameter[] values = new OleDbParameter[]
			{
				new OleDbParameter("@ID", OleDbType.Integer)
				{
					Value = id
				},
				new OleDbParameter("@IndividualQuery", OleDbType.LongVarWChar)
				{
					Value = sqlPlan.IndividualQuery
				},
				new OleDbParameter("@ParentQuery", OleDbType.LongVarWChar)
				{
					Value = sqlPlan.ParentQuery
				},
				new OleDbParameter("@CreationTime", OleDbType.Date)
				{
					Value = sqlPlan.CreationTime
				},
				new OleDbParameter("@LastExecutionTime", OleDbType.Date)
				{
					Value = sqlPlan.LastExecutionTime
				},
				new OleDbParameter("@ExecutionCount", OleDbType.Double)
				{
					Value = sqlPlan.ExecutionCount
				},
				new OleDbParameter("@TotalElapsedTime", OleDbType.Double)
				{
					Value = sqlPlan.TotalElapsedTime
				},
				new OleDbParameter("@AvgElapsedTime", OleDbType.Double)
				{
					Value = sqlPlan.AvgElapsedTime
				},
				new OleDbParameter("@MinElapsedTime", OleDbType.Double)
				{
					Value = sqlPlan.MinElapsedTime
				},
				new OleDbParameter("@MaxElapsedTime", OleDbType.Double)
				{
					Value = sqlPlan.MaxElapsedTime
				},
				new OleDbParameter("@TotalWorkerTime", OleDbType.Double)
				{
					Value = sqlPlan.TotalWorkerTime
				},
				new OleDbParameter("@AvgWorkerTime", OleDbType.Double)
				{
					Value = sqlPlan.AvgWorkerTime
				},
				new OleDbParameter("@MinWorkerTime", OleDbType.Double)
				{
					Value = sqlPlan.MinWorkerTime
				},
				new OleDbParameter("@MaxWorkerTime", OleDbType.Double)
				{
					Value = sqlPlan.MaxWorkerTime
				},
				new OleDbParameter("@TotalLogicalReads", OleDbType.Double)
				{
					Value = sqlPlan.TotalLogicalReads
				},
				new OleDbParameter("@AvgLogicalReads", OleDbType.Double)
				{
					Value = sqlPlan.AvgLogicalReads
				},
				new OleDbParameter("@MinLogicalReads", OleDbType.Double)
				{
					Value = sqlPlan.MinLogicalReads
				},
				new OleDbParameter("@MaxLogicalReads", OleDbType.Double)
				{
					Value = sqlPlan.MaxLogicalReads
				},
				new OleDbParameter("@TotalLogicalWrites", OleDbType.Double)
				{
					Value = sqlPlan.TotalLogicalWrites
				},
				new OleDbParameter("@AvgLogicalWrites", OleDbType.Double)
				{
					Value = sqlPlan.AvgLogicalWrites
				},
				new OleDbParameter("@MinLogicalWrites", OleDbType.Double)
				{
					Value = sqlPlan.MinLogicalWrites
				},
				new OleDbParameter("@MaxLogicalWrites", OleDbType.Double)
				{
					Value = sqlPlan.MaxLogicalWrites
				},
				new OleDbParameter("@TotalPhysicalReads", OleDbType.Double)
				{
					Value = sqlPlan.TotalPhysicalReads
				},
				new OleDbParameter("@AvgPhysicalReads", OleDbType.Double)
				{
					Value = sqlPlan.AvgPhysicalReads
				},
				new OleDbParameter("@MinPhysicalReads", OleDbType.Double)
				{
					Value = sqlPlan.MinPhysicalReads
				},
				new OleDbParameter("@MaxPhysicalReads", OleDbType.Double)
				{
					Value = sqlPlan.MaxPhysicalReads
				},
				new OleDbParameter("@PlanHandle", OleDbType.VarBinary)
				{
					Value = sqlPlan.PlanHandle
				},
				new OleDbParameter("@PlanText", OleDbType.LongVarWChar)
				{
					Value = sqlPlan.PlanText
				},
				new OleDbParameter("@TextHashCode", OleDbType.VarChar)
				{
					Value = sqlPlan.TextHashCode
				},
				new OleDbParameter("@ParameterHash", OleDbType.VarChar)
				{
					Value = sqlPlan.ParameterHash
				},
				new OleDbParameter("@HasMissIndex", OleDbType.Boolean)
				{
					Value = sqlPlan.HasMissIndex
				},
				new OleDbParameter("@HasImplicit", OleDbType.Boolean)
				{
					Value = sqlPlan.HasImplicit
				},
				new OleDbParameter("@HasSqlPlan", OleDbType.Boolean)
				{
					Value = sqlPlan.HasSqlPlan
				}
			};
			bool result;
			try
			{
				OleDbCommand oleDbCommand = con.CreateCommand();
				oleDbCommand.CommandTimeout = timeOut;
				oleDbCommand.CommandText = commandText;
				oleDbCommand.Parameters.Clear();
				oleDbCommand.Parameters.AddRange(values);
				int num = oleDbCommand.ExecuteNonQuery();
				result = (num > 0);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private bool UpdateData(OleDbConnection con, int timeOut, SqlPlan sqlPlan)
		{
			string commandText = "\r\nUPDATE  tblSqlPlan\r\nSET     CreationTime = @CreationTime ,\r\n        LastExecutionTime = @LastExecutionTime ,\r\n        ExecutionCount = @ExecutionCount ,\r\n        TotalElapsedTime = @TotalElapsedTime ,\r\n        AvgElapsedTime = @AvgElapsedTime ,\r\n        MinElapsedTime = @MinElapsedTime ,\r\n        MaxElapsedTime = @MaxElapsedTime ,\r\n        TotalWorkerTime = @TotalWorkerTime ,\r\n        AvgWorkerTime = @AvgWorkerTime ,\r\n        MinWorkerTime = @MinWorkerTime ,\r\n        MaxWorkerTime = @MaxWorkerTime ,\r\n        TotalLogicalReads = @TotalLogicalReads ,\r\n        AvgLogicalReads = @AvgLogicalReads ,\r\n        MinLogicalReads = @MinLogicalReads ,\r\n        MaxLogicalReads = @MaxLogicalReads ,\r\n        TotalLogicalWrites = @TotalLogicalWrites ,\r\n        AvgLogicalWrites = @AvgLogicalWrites ,\r\n        MinLogicalWrites = @MinLogicalWrites ,\r\n        MaxLogicalWrites = @MaxLogicalWrites ,\r\n        TotalPhysicalReads = @TotalPhysicalReads ,\r\n        AvgPhysicalReads = @AvgPhysicalReads ,\r\n        MinPhysicalReads = @MinPhysicalReads ,\r\n        MaxPhysicalReads = @MaxPhysicalReads         \r\nWHERE   PlanHandle = @PlanHandle\r\n";
			OleDbParameter[] values = new OleDbParameter[]
			{
				new OleDbParameter("@CreationTime", OleDbType.Date)
				{
					Value = sqlPlan.CreationTime
				},
				new OleDbParameter("@LastExecutionTime", OleDbType.Date)
				{
					Value = sqlPlan.LastExecutionTime
				},
				new OleDbParameter("@ExecutionCount", OleDbType.Double)
				{
					Value = sqlPlan.ExecutionCount
				},
				new OleDbParameter("@TotalElapsedTime", OleDbType.Double)
				{
					Value = sqlPlan.TotalElapsedTime
				},
				new OleDbParameter("@AvgElapsedTime", OleDbType.Double)
				{
					Value = sqlPlan.AvgElapsedTime
				},
				new OleDbParameter("@MinElapsedTime", OleDbType.Double)
				{
					Value = sqlPlan.MinElapsedTime
				},
				new OleDbParameter("@MaxElapsedTime", OleDbType.Double)
				{
					Value = sqlPlan.MaxElapsedTime
				},
				new OleDbParameter("@TotalWorkerTime", OleDbType.Double)
				{
					Value = sqlPlan.TotalWorkerTime
				},
				new OleDbParameter("@AvgWorkerTime", OleDbType.Double)
				{
					Value = sqlPlan.AvgWorkerTime
				},
				new OleDbParameter("@MinWorkerTime", OleDbType.Double)
				{
					Value = sqlPlan.MinWorkerTime
				},
				new OleDbParameter("@MaxWorkerTime", OleDbType.Double)
				{
					Value = sqlPlan.MaxWorkerTime
				},
				new OleDbParameter("@TotalLogicalReads", OleDbType.Double)
				{
					Value = sqlPlan.TotalLogicalReads
				},
				new OleDbParameter("@AvgLogicalReads", OleDbType.Double)
				{
					Value = sqlPlan.AvgLogicalReads
				},
				new OleDbParameter("@MinLogicalReads", OleDbType.Double)
				{
					Value = sqlPlan.MinLogicalReads
				},
				new OleDbParameter("@MaxLogicalReads", OleDbType.Double)
				{
					Value = sqlPlan.MaxLogicalReads
				},
				new OleDbParameter("@TotalLogicalWrites", OleDbType.Double)
				{
					Value = sqlPlan.TotalLogicalWrites
				},
				new OleDbParameter("@AvgLogicalWrites", OleDbType.Double)
				{
					Value = sqlPlan.AvgLogicalWrites
				},
				new OleDbParameter("@MinLogicalWrites", OleDbType.Double)
				{
					Value = sqlPlan.MinLogicalWrites
				},
				new OleDbParameter("@MaxLogicalWrites", OleDbType.Double)
				{
					Value = sqlPlan.MaxLogicalWrites
				},
				new OleDbParameter("@TotalPhysicalReads", OleDbType.Double)
				{
					Value = sqlPlan.TotalPhysicalReads
				},
				new OleDbParameter("@AvgPhysicalReads", OleDbType.Double)
				{
					Value = sqlPlan.AvgPhysicalReads
				},
				new OleDbParameter("@MinPhysicalReads", OleDbType.Double)
				{
					Value = sqlPlan.MinPhysicalReads
				},
				new OleDbParameter("@MaxPhysicalReads", OleDbType.Double)
				{
					Value = sqlPlan.MaxPhysicalReads
				},
				new OleDbParameter("@PlanHandle", OleDbType.VarBinary)
				{
					Value = sqlPlan.PlanHandle
				}
			};
			bool result;
			try
			{
				OleDbCommand oleDbCommand = con.CreateCommand();
				oleDbCommand.CommandTimeout = timeOut;
				oleDbCommand.CommandText = commandText;
				oleDbCommand.Parameters.Clear();
				oleDbCommand.Parameters.AddRange(values);
				int num = oleDbCommand.ExecuteNonQuery();
				result = (num > 0);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private bool InsertMissIndex(OleDbConnection con, int timeOut, int rowID, List<MissIndexItem> missList)
		{
			if (missList.Count > 0)
			{
				foreach (MissIndexItem current in missList)
				{
					string commandText = "INSERT INTO tblSqlPlanMissIndex(ROWID,DBName,SchemaName,TableName,EqualityColumns,InequalityColumns,IncludedColumns,Impact) VALUES(@ROWID,@DBName,@SchemaName,@TableName,@EqualityColumns,@InequalityColumns,@IncludedColumns,@Impact)";
					OleDbParameter[] values = new OleDbParameter[]
					{
						new OleDbParameter("@ROWID", OleDbType.Integer)
						{
							Value = rowID
						},
						new OleDbParameter("@DBName", OleDbType.VarChar)
						{
							Value = (current.DBName ?? "")
						},
						new OleDbParameter("@SchemaName", OleDbType.VarChar)
						{
							Value = (current.SchemaName ?? "")
						},
						new OleDbParameter("@TableName", OleDbType.VarChar)
						{
							Value = (current.TableName ?? "")
						},
						new OleDbParameter("@EqualityColumns", OleDbType.LongVarChar)
						{
							Value = (current.EqualityColumns ?? "")
						},
						new OleDbParameter("@InequalityColumns", OleDbType.LongVarChar)
						{
							Value = (current.InequalityColumns ?? "")
						},
						new OleDbParameter("@IncludedColumns", OleDbType.LongVarChar)
						{
							Value = (current.IncludedColumns ?? "")
						},
						new OleDbParameter("@Impact", OleDbType.Double)
						{
							Value = current.Impact
						}
					};
					try
					{
						OleDbCommand oleDbCommand = con.CreateCommand();
						oleDbCommand.CommandTimeout = timeOut;
						oleDbCommand.CommandText = commandText;
						oleDbCommand.Parameters.Clear();
						oleDbCommand.Parameters.AddRange(values);
						int num = oleDbCommand.ExecuteNonQuery();
						bool result = num > 0;
						return result;
					}
					catch (Exception ex)
					{
						ErrorLog.Write(ex);
						bool result = false;
						return result;
					}
				}
				return true;
			}
			return true;
		}

		private string GetChildPlanText(string individualQuery, string parentPlanText)
		{
			if (string.IsNullOrEmpty(individualQuery) || string.IsNullOrEmpty(parentPlanText))
			{
				return parentPlanText;
			}
			bool flag = false;
			string text = Regex.Replace(individualQuery, "[\\r\\n\\t ]", "");
			if (text.EndsWith(";"))
			{
				text = text.Remove(text.Length - 1);
			}
			XmlDocument xmlDocument = new XmlDocument();
			try
			{
				xmlDocument.LoadXml(parentPlanText);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				string result = string.Empty;
				return result;
			}
			if (xmlDocument == null)
			{
				return string.Empty;
			}
			XmlNodeList elementsByTagName = xmlDocument.GetElementsByTagName("StmtSimple");
			if (elementsByTagName != null && elementsByTagName.Count > 0)
			{
				foreach (XmlNode xmlNode in elementsByTagName)
				{
					if (xmlNode.Attributes["StatementText"] != null)
					{
						string text2 = xmlNode.Attributes["StatementText"].Value;
						if (!string.IsNullOrEmpty(text2))
						{
							text2 = Regex.Replace(text2, "[\\r\\n\\t ]", "");
							if (text2.EndsWith(";"))
							{
								text2 = text2.Remove(text2.Length - 1);
							}
							if (text2.EndsWith(text))
							{
								string text3 = string.Format("<ShowPlanXML xmlns='http://schemas.microsoft.com/sqlserver/2004/07/showplan' Version='1.1' Build='10.0.2531.0'>\r\n  <BatchSequence>\r\n    <Batch>\r\n      <Statements>\r\n        {0}\r\n\t  </Statements>\r\n    </Batch>\r\n  </BatchSequence>\r\n</ShowPlanXML>", xmlNode.OuterXml);
								flag = true;
								string result = text3;
								return result;
							}
						}
					}
				}
			}
			if (!flag && individualQuery.Length >= 4000)
			{
				return this.GetLongChildPlanText(text, parentPlanText, individualQuery);
			}
			return parentPlanText;
		}

		private string GetLongChildPlanText(string repIndividualQuery, string parentPlanText, string individualQuery)
		{
			if (repIndividualQuery.Length >= 2000)
			{
				repIndividualQuery = repIndividualQuery.Substring(0, 2000);
			}
			XmlDocument xmlDocument = new XmlDocument();
			try
			{
				xmlDocument.LoadXml(parentPlanText);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				string empty = string.Empty;
				return empty;
			}
			if (xmlDocument == null)
			{
				return string.Empty;
			}
			string arg = string.Empty;
			int num = 0;
			XmlNodeList elementsByTagName = xmlDocument.GetElementsByTagName("StmtSimple");
			if (elementsByTagName != null && elementsByTagName.Count > 0)
			{
				foreach (XmlNode xmlNode in elementsByTagName)
				{
					if (xmlNode.Attributes["StatementText"] != null)
					{
						string text = xmlNode.Attributes["StatementText"].Value;
						if (!string.IsNullOrEmpty(text))
						{
							text = Regex.Replace(text, "[\\r\\n\\t ]", "");
							if (text.EndsWith(";"))
							{
								text = text.Remove(text.Length - 1);
							}
							if (text.Contains(repIndividualQuery))
							{
								arg = xmlNode.OuterXml;
								num++;
							}
						}
					}
				}
			}
			if (num == 1)
			{
				return string.Format("<ShowPlanXML xmlns='http://schemas.microsoft.com/sqlserver/2004/07/showplan' Version='1.1' Build='10.0.2531.0'>\r\n  <BatchSequence>\r\n    <Batch>\r\n      <Statements>\r\n        {0}\r\n\t  </Statements>\r\n    </Batch>\r\n  </BatchSequence>\r\n</ShowPlanXML>", arg);
			}
			return string.Empty;
		}

		private List<MissIndexItem> GetMissIndexItems(string childPlanText)
		{
			if (!string.IsNullOrEmpty(childPlanText))
			{
				XmlDataDocument xmlDataDocument = new XmlDataDocument();
				try
				{
					xmlDataDocument.LoadXml(childPlanText);
				}
				catch (Exception ex)
				{
					ErrorLog.Write(ex);
					List<MissIndexItem> result = null;
					return result;
				}
				XmlNamespaceManager xmlNamespaceManager = new XmlNamespaceManager(xmlDataDocument.NameTable);
				xmlNamespaceManager.AddNamespace(xmlDataDocument.Prefix, xmlDataDocument.NamespaceURI);
				XmlNodeList elementsByTagName = xmlDataDocument.GetElementsByTagName("MissingIndexes");
				if (elementsByTagName.Count > 0)
				{
					List<MissIndexItem> list = new List<MissIndexItem>();
					foreach (XmlNode xmlNode in elementsByTagName)
					{
						XmlNodeList childNodes = xmlNode.ChildNodes;
						if (childNodes.Count > 0)
						{
							foreach (XmlNode xmlNode2 in childNodes)
							{
								MissIndexItem missIndexItem = new MissIndexItem();
								XmlAttribute xmlAttribute = xmlNode2.Attributes["Impact"];
								if (xmlAttribute != null)
								{
									missIndexItem.Impact = Convert.ToDouble(xmlAttribute.Value);
								}
								XmlNode firstChild = xmlNode2.FirstChild;
								if (firstChild != null)
								{
									xmlAttribute = firstChild.Attributes["Database"];
									if (xmlAttribute != null)
									{
										missIndexItem.DBName = Regex.Replace(xmlAttribute.Value, "[\\[\\]]", "");
									}
									xmlAttribute = firstChild.Attributes["Schema"];
									if (xmlAttribute != null)
									{
										missIndexItem.SchemaName = Regex.Replace(xmlAttribute.Value, "[\\[\\]]", "");
									}
									xmlAttribute = firstChild.Attributes["Table"];
									if (xmlAttribute != null)
									{
										missIndexItem.TableName = Regex.Replace(xmlAttribute.Value, "[\\[\\]]", "");
									}
									XmlNodeList childNodes2 = firstChild.ChildNodes;
									if (childNodes2.Count > 0)
									{
										foreach (XmlNode xmlNode3 in childNodes2)
										{
											string a = string.Empty;
											xmlAttribute = xmlNode3.Attributes["Usage"];
											if (xmlAttribute != null)
											{
												a = xmlAttribute.Value;
											}
											XmlNodeList childNodes3 = xmlNode3.ChildNodes;
											if (childNodes3.Count > 0)
											{
												StringBuilder stringBuilder = new StringBuilder();
												foreach (XmlNode xmlNode4 in childNodes3)
												{
													xmlAttribute = xmlNode4.Attributes["Name"];
													if (xmlAttribute != null)
													{
														stringBuilder.Append(xmlAttribute.Value);
														stringBuilder.Append(",");
													}
												}
												stringBuilder.Remove(stringBuilder.Length - 1, 1);
												if (a == "EQUALITY")
												{
													missIndexItem.EqualityColumns = stringBuilder.ToString();
												}
												else if (a == "INEQUALITY")
												{
													missIndexItem.InequalityColumns = stringBuilder.ToString();
												}
												else if (a == "INCLUDE")
												{
													missIndexItem.IncludedColumns = stringBuilder.ToString();
												}
											}
										}
									}
								}
								list.Add(missIndexItem);
							}
						}
					}
					return list;
				}
			}
			return null;
		}
	}
}
