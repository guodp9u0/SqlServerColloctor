using System;

namespace ZhuanCloud.Collector
{
	public enum CollectItem
	{
		Perf,
		Tempdb,
		Wait,
		UnusedSession,
		SqlText,
		SqlPlan,
		ErrLog,
		DBFile,
		Moebius
	}
}
