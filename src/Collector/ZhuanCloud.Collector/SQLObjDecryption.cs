using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Text.RegularExpressions;

namespace ZhuanCloud.Collector
{
	internal class SQLObjDecryption
	{
		private ArrayList objFidPid = new ArrayList();

		private int objFidPidIndex;

		private int GLOBAL_STATE = -1;

		private SQLVersion sqlVersion = SQLVersion.others;

		private SQLBits sqlBits;

		private string objectId = "";

		private string dbName = "";

		private string ConnectionString = "";

		private SqlConnection sc = new SqlConnection();

		private SqlTransaction st;

		private string objidFoundFlag = "objid = ";

		private string blobIdFoundFlag = "Blob Id:";

		private int blobState;

		private ArrayList blobPagesId = new ArrayList();

		private string blobID = "";

		private int handledPosition;

		private string objFid = "";

		private string objPid = "";

		private string realDefinition = "";

		private string realEncHexValues = "";

		private string alterfakeEncHexValues = "";

		private string createFakeHexValues = "";

		private string fakeEncHexValues = "";

		private string createFakeEncPre = "";

		private string getObjInfo = "select o.name, OBJECT_NAME(o.parent_object_id), o.type, sc.name, tr.name as db_trg_name,p.type as pt from sys.sql_modules s left join sys.objects o on s.object_id = o.object_id left join sys.schemas sc on sc.schema_id = o.schema_id left join sys.triggers tr on s.object_id = tr.object_id left join sys.objects p on o.parent_object_id = p.object_id where s.object_id = @ObjId";

		private string getFileIdAndPageId = "DECLARE @DBCC_IND_sql nvarchar(1000) SET @DBCC_IND_sql = 'DBCC IND (''' + @DBName + ''', ''sys.sysobjvalues'', 1)' EXEC (@DBCC_IND_sql)";

		private string dbccInit = "DBCC TRACEON (3604); ";

		private string dbccCmd = "DBCC PAGE (@DBName, @fid, @pid, 3)";

		private string dbccMsg = "";

		public SQLObjDecryption(string ConnectionString, string dbName, string objectId, SQLVersion sv, SQLBits sb)
		{
			this.objidFoundFlag += objectId;
			this.ConnectionString = ConnectionString;
			this.sc = new SqlConnection(ConnectionString);
			this.dbName = dbName;
			this.objectId = objectId;
			this.sqlVersion = sv;
			this.sqlBits = sb;
		}

		public string GetRealDefinition()
		{
			return this.realDefinition;
		}

		public void DecryptedObject()
		{
			using (this.sc)
			{
				this.sc.InfoMessage += new SqlInfoMessageEventHandler(this.OnInfoMessage);
				this.sc.Open();
				SQLHelper.ExecuteNonQuery(this.sc, CommandType.Text, this.dbccInit, new SqlParameter[0]);
				this.getPageFIDAndPagePID(this.dbName, this.objectId);
				SQLHelper.ExecuteNonQuery(this.sc, CommandType.Text, this.dbccCmd, new SqlParameter[]
				{
					new SqlParameter("@DBName", this.dbName),
					new SqlParameter("@fid", int.Parse(this.objFidPid.ToArray()[this.objFidPidIndex].ToString().Split(new char[]
					{
						':'
					})[0])),
					new SqlParameter("@pid", int.Parse(this.objFidPid.ToArray()[this.objFidPidIndex].ToString().Split(new char[]
					{
						':'
					})[1]))
				});
				while (this.GLOBAL_STATE != 100)
				{
					this.HandleDBCCMessage();
				}
			}
		}

		private void getPageFIDAndPagePID(string dbName, string objectId)
		{
			SqlDataReader sqlDataReader = SQLHelper.ExecuteReader(this.sc, CommandType.Text, this.getFileIdAndPageId, new SqlParameter[]
			{
				new SqlParameter("@DBName", dbName)
			});
			while (sqlDataReader.Read())
			{
				this.objFidPid.Add(sqlDataReader.GetValue(0).ToString() + ":" + sqlDataReader.GetValue(1).ToString());
			}
			sqlDataReader.Close();
			this.GLOBAL_STATE = 0;
		}

		private void HandleDBCCMessage()
		{
			if (this.GLOBAL_STATE == 0)
			{
				if (!this.findItemPage(this.dbccMsg))
				{
					this.objFidPidIndex++;
					SQLHelper.ExecuteNonQuery(this.sc, CommandType.Text, this.dbccCmd, new SqlParameter[]
					{
						new SqlParameter("@DBName", this.dbName),
						new SqlParameter("@fid", int.Parse(this.objFidPid.ToArray()[this.objFidPidIndex].ToString().Split(new char[]
						{
							':'
						})[0])),
						new SqlParameter("@pid", int.Parse(this.objFidPid.ToArray()[this.objFidPidIndex].ToString().Split(new char[]
						{
							':'
						})[1]))
					});
					return;
				}
				string encContent = this.getEncContent(this.dbccMsg);
				if (this.blobState == 0)
				{
					this.realEncHexValues = this.getObjContentFromEncContent(encContent);
					this.GLOBAL_STATE = 1;
					int num = this.realEncHexValues.Length;
					while (num % 4 != 0)
					{
						num++;
					}
					this.getAlterAndCreateFakeDefinition(this.objectId, num / 2);
					this.alterObjectInTran();
					this.execDBCCPage(this.objFid, this.objPid);
					return;
				}
				this.GLOBAL_STATE = 2;
				if (this.handledPosition == this.blobPagesId.Count)
				{
					return;
				}
				string text = this.blobPagesId.ToArray()[this.handledPosition].ToString();
				this.handledPosition++;
				SQLHelper.ExecuteNonQuery(this.sc, CommandType.Text, this.dbccCmd, new SqlParameter[]
				{
					new SqlParameter("@DBName", this.dbName),
					new SqlParameter("@fid", int.Parse(text.Split(new char[]
					{
						':'
					})[0])),
					new SqlParameter("@pid", int.Parse(text.Split(new char[]
					{
						':'
					})[1]))
				});
				return;
			}
			else if (this.GLOBAL_STATE == 1)
			{
				string encContent2 = this.getEncContent(this.dbccMsg);
				if (this.blobState == 0)
				{
					this.fakeEncHexValues = this.getObjContentFromEncContent(encContent2);
					this.GLOBAL_STATE = 100;
					this.setDecryptObjByInfo();
					return;
				}
				this.GLOBAL_STATE = 12;
				if (this.handledPosition == this.blobPagesId.Count)
				{
					return;
				}
				this.blobPagesId.ToArray()[this.handledPosition].ToString();
				string text2 = this.blobPagesId[this.handledPosition].ToString().Split(new char[]
				{
					':'
				})[0];
				string text3 = this.blobPagesId[this.handledPosition].ToString().Split(new char[]
				{
					':'
				})[1];
				this.handledPosition++;
				this.execDBCCPage(text2, text3);
				return;
			}
			else if (this.GLOBAL_STATE == 2)
			{
				this.realEncHexValues += this.getContentFromBlobPage(this.dbccMsg);
				if (this.handledPosition == this.blobPagesId.Count)
				{
					int num2 = this.realEncHexValues.Length;
					while (num2 % 4 != 0)
					{
						num2++;
					}
					this.getAlterAndCreateFakeDefinition(this.objectId, num2);
					this.handledPosition = 0;
					this.alterObjectInTran();
					this.GLOBAL_STATE = 3;
					this.execDBCCPage(this.objFid, this.objPid);
					return;
				}
				this.handledPosition++;
				SQLHelper.ExecuteNonQuery(this.sc, CommandType.Text, this.dbccCmd, new SqlParameter[]
				{
					new SqlParameter("@DBName", this.dbName),
					new SqlParameter("@fid", int.Parse(this.blobPagesId[this.handledPosition - 1].ToString().Split(new char[]
					{
						':'
					})[0])),
					new SqlParameter("@pid", int.Parse(this.blobPagesId[this.handledPosition - 1].ToString().Split(new char[]
					{
						':'
					})[1]))
				});
				return;
			}
			else
			{
				if (this.GLOBAL_STATE != 3 && this.GLOBAL_STATE != 12)
				{
					if (this.GLOBAL_STATE == 4)
					{
						this.fakeEncHexValues += this.getContentFromBlobPage(this.dbccMsg);
						if (this.handledPosition == this.blobPagesId.Count)
						{
							this.GLOBAL_STATE = 100;
							this.setDecryptObjByInfo();
							return;
						}
						string text4 = this.blobPagesId[this.handledPosition].ToString().Split(new char[]
						{
							':'
						})[0];
						string text5 = this.blobPagesId[this.handledPosition].ToString().Split(new char[]
						{
							':'
						})[1];
						this.handledPosition++;
						this.execDBCCPage(text4, text5);
					}
					return;
				}
				this.handledPosition = 0;
				this.blobPagesId.Clear();
				this.getEncContent(this.dbccMsg);
				this.GLOBAL_STATE = 4;
				if (this.handledPosition == this.blobPagesId.Count)
				{
					return;
				}
				string text6 = this.blobPagesId.ToArray()[this.handledPosition].ToString();
				this.handledPosition++;
				this.execDBCCPage(text6.Split(new char[]
				{
					':'
				})[0], text6.Split(new char[]
				{
					':'
				})[1]);
				return;
			}
		}

		private bool findItemPage(string message)
		{
			string[] array = message.Split(new char[]
			{
				'\n'
			});
			int num = 0;
			string[] array2 = array;
			for (int i = 0; i < array2.Length; i++)
			{
				string text = array2[i];
				if (text.StartsWith("valclass ="))
				{
					if (text.StartsWith("valclass = 1"))
					{
						num = 1;
					}
					else
					{
						num = 0;
					}
				}
				if (text.StartsWith(this.objidFoundFlag) && num == 1)
				{
					return true;
				}
			}
			return false;
		}

		protected void OnInfoMessage(object sender, SqlInfoMessageEventArgs args)
		{
			this.dbccMsg = args.Message;
		}

		private string getObjContentFromEncContent(string encContent)
		{
			int num = Convert.ToInt32(encContent.ToString().Substring(44, 1), 16) * 16 + Convert.ToInt32(encContent.ToString().Substring(45, 1), 16) + Convert.ToInt32(encContent.ToString().Substring(46, 1), 16) * 16 * 16 * 16 + Convert.ToInt32(encContent.ToString().Substring(47, 1), 16) * 16 * 16;
			int num2 = Convert.ToInt32(encContent.ToString().Substring(48, 1), 16) * 16 + Convert.ToInt32(encContent.ToString().Substring(49, 1), 16) + Convert.ToInt32(encContent.ToString().Substring(50, 1), 16) * 16 * 16 * 16 + Convert.ToInt32(encContent.ToString().Substring(51, 1), 16) * 16 * 16;
			return encContent.ToString().Substring(num * 2, (num2 - num) * 2);
		}

		private string getEncContent(string dbccMessage)
		{
			this.blobPagesId.Clear();
			this.blobID = "";
			StringBuilder stringBuilder = new StringBuilder();
			string[] array = dbccMessage.Split(new char[]
			{
				'\n'
			});
			int num = 0;
			string[] array2 = array;
			for (int i = 0; i < array2.Length; i++)
			{
				string text = array2[i];
				if (num == 0)
				{
					if (text.StartsWith(this.objidFoundFlag))
					{
						num = 2;
					}
					else
					{
						if (text.StartsWith("PAGE:"))
						{
							string text2 = text.Substring(text.IndexOf('(') + 1, text.IndexOf(')') - text.IndexOf('(') - 1);
							this.objFid = text2.Split(new char[]
							{
								':'
							})[0];
							this.objPid = text2.Split(new char[]
							{
								':'
							})[1];
						}
						if (text.Contains(":"))
						{
							string s = text.Split(new char[]
							{
								':'
							})[0];
							if (this.sqlBits == SQLBits.x64)
							{
								if (this.matchedBase16(s).Length == 16)
								{
									num = 1;
									stringBuilder = new StringBuilder();
									string text3 = text.Split(new char[]
									{
										':'
									})[1];
									if (this.sqlVersion != SQLVersion._2012)
									{
										stringBuilder.Append(this.matchedBase16(text3.Substring(3, 8))).Append(this.matchedBase16(text3.Substring(12, 8))).Append(this.matchedBase16(text3.Substring(21, 8))).Append(this.matchedBase16(text3.Substring(30, 8)));
									}
									else
									{
										stringBuilder.Append(this.matchedBase16(text3.Substring(3, 8))).Append(this.matchedBase16(text3.Substring(12, 8))).Append(this.matchedBase16(text3.Substring(21, 8))).Append(this.matchedBase16(text3.Substring(30, 8))).Append(this.matchedBase16(text3.Substring(39, 8)));
									}
								}
							}
							else if (this.matchedBase16(s).Length == 8)
							{
								num = 1;
								stringBuilder = new StringBuilder();
								string text4 = text.Split(new char[]
								{
									':'
								})[1];
								if (this.sqlVersion != SQLVersion._2012)
								{
									stringBuilder.Append(this.matchedBase16(text4.Substring(3, 8))).Append(this.matchedBase16(text4.Substring(12, 8))).Append(this.matchedBase16(text4.Substring(21, 8))).Append(this.matchedBase16(text4.Substring(30, 8)));
								}
								else
								{
									stringBuilder.Append(this.matchedBase16(text4.Substring(3, 8))).Append(this.matchedBase16(text4.Substring(12, 8))).Append(this.matchedBase16(text4.Substring(21, 8))).Append(this.matchedBase16(text4.Substring(30, 8))).Append(this.matchedBase16(text4.Substring(39, 8)));
								}
							}
						}
					}
				}
				else if (num == 1)
				{
					if (text.Contains(":"))
					{
						string text5 = text.Split(new char[]
						{
							':'
						})[1];
						if (this.sqlVersion != SQLVersion._2012)
						{
							stringBuilder.Append(this.matchedBase16(text5.Substring(3, 8))).Append(this.matchedBase16(text5.Substring(12, 8))).Append(this.matchedBase16(text5.Substring(21, 8))).Append(this.matchedBase16(text5.Substring(30, 8)));
						}
						else
						{
							stringBuilder.Append(this.matchedBase16(text5.Substring(3, 8))).Append(this.matchedBase16(text5.Substring(12, 8))).Append(this.matchedBase16(text5.Substring(21, 8))).Append(this.matchedBase16(text5.Substring(30, 8))).Append(this.matchedBase16(text5.Substring(39, 8)));
						}
					}
					else if (text.Length != 1)
					{
						num = 0;
					}
				}
				else if (num == 2)
				{
					if (text.StartsWith("imageval = "))
					{
						if (!text.StartsWith("imageval = [BLOB Inline Root]"))
						{
							break;
						}
						this.blobState = 1;
						num = 3;
					}
				}
				else if (num == 3)
				{
					if (text.StartsWith("TimeStamp = "))
					{
						this.blobID = (text.Split(new char[]
						{
							'='
						})[1].Trim() + " ").Split(new char[]
						{
							' '
						})[0];
						num = 4;
					}
				}
				else if (num == 4)
				{
					if (text.Contains("RowId = "))
					{
						string text6 = text.Substring(text.IndexOf("RowId = ")).Split(new char[]
						{
							'('
						})[1];
						this.blobPagesId.Add(text6.Split(new char[]
						{
							':'
						})[0] + ":" + text6.Split(new char[]
						{
							':'
						})[1]);
					}
					else if (text.StartsWith("Slot"))
					{
						break;
					}
				}
			}
			return stringBuilder.ToString();
		}

		private string getContentFromBlobPage(string blobPageContent)
		{
			StringBuilder stringBuilder = new StringBuilder();
			string[] array = blobPageContent.Split(new char[]
			{
				'\n'
			});
			int num = 0;
			string[] array2 = array;
			for (int i = 0; i < array2.Length; i++)
			{
				string text = array2[i];
				if (num == 0)
				{
					if (text.Contains(this.blobIdFoundFlag + this.blobID))
					{
						num = 1;
					}
					else if (text.Contains(this.blobIdFoundFlag + " " + this.blobID))
					{
						num = 2;
					}
				}
				else if (num == 1)
				{
					string text2 = text.Split(new char[]
					{
						':'
					})[0];
					if (this.sqlBits == SQLBits.x64)
					{
						if (text2.Length == 16)
						{
							string text3 = text.Split(new char[]
							{
								':'
							})[1];
							stringBuilder.Append(this.reorgBlobOrder(this.matchedBase16(text3.Substring(2, 8)))).Append(this.reorgBlobOrder(this.matchedBase16(text3.Substring(12, 8)))).Append(this.reorgBlobOrder(this.matchedBase16(text3.Substring(22, 8)))).Append(this.reorgBlobOrder(this.matchedBase16(text3.Substring(32, 8))));
						}
						else if (text2.Length > 1)
						{
							break;
						}
					}
					else if (this.sqlBits == SQLBits.x86)
					{
						if (text2.Length == 8)
						{
							string text4 = text.Split(new char[]
							{
								':'
							})[1];
							stringBuilder.Append(this.reorgBlobOrder(this.matchedBase16(text4.Substring(2, 8)))).Append(this.reorgBlobOrder(this.matchedBase16(text4.Substring(12, 8)))).Append(this.reorgBlobOrder(this.matchedBase16(text4.Substring(22, 8)))).Append(this.reorgBlobOrder(this.matchedBase16(text4.Substring(32, 8))));
						}
						else if (text2.Length > 1)
						{
							break;
						}
					}
				}
				else if (num == 2 && text.Contains("Child") && text.Contains("at Page"))
				{
					this.blobPagesId.Add(text.Split(new char[]
					{
						'('
					})[1].Split(new char[]
					{
						')'
					})[0]);
				}
			}
			return stringBuilder.ToString();
		}

		private string reorgBlobOrder(string origStr)
		{
			if (origStr.Length == 8)
			{
				return (origStr.Substring(6, 2) + origStr.Substring(4, 2) + origStr.Substring(2, 2) + origStr.Substring(0, 2)).Trim();
			}
			return origStr.Trim();
		}

		private string matchedBase16(string s)
		{
			Regex regex = new Regex("^[0-9a-fA-F ]+$");
			Match match = regex.Match(s);
			if (match.Success)
			{
				return s;
			}
			for (int i = 0; i < s.Length; i++)
			{
				char c = s[i];
				if (c < '0' || (c > '9' && c < 'A') || (c > 'F' && c < 'a') || c > 'f')
				{
					return s.Substring(0, s.IndexOf(c));
				}
			}
			return "";
		}

		private void alterObjectInTran()
		{
			this.st = this.sc.BeginTransaction();
			SQLHelper.ExecuteNonQuery(this.st, CommandType.Text, "IF(Object_ID('SourceSafe_Master.dbo.uspDecryptRequire') IS NOT NULL) BEGIN EXEC SourceSafe_Master.dbo.uspDecryptRequire END", new SqlParameter[0]);
			SQLHelper.ExecuteNonQuery(this.st, CommandType.Text, this.alterfakeEncHexValues, new SqlParameter[0]);
		}

		private void execDBCCPage(string objFid, string objPid)
		{
			new SqlCommand
			{
				Transaction = this.st,
				CommandText = this.dbccCmd,
				Connection = this.sc,
				Parameters = 
				{
					new SqlParameter("@DBName", this.dbName),
					new SqlParameter("@fid", int.Parse(objFid)),
					new SqlParameter("@pid", int.Parse(objPid))
				}
			}.ExecuteNonQuery();
		}

		private void getAlterAndCreateFakeDefinition(string objectId, int hexValuesLen)
		{
			string text = "";
			string text2 = "";
			string text3 = "";
			string text4 = "";
			string text5 = "";
			string text6 = "";
			SqlDataReader sqlDataReader = SQLHelper.ExecuteReader(this.sc, CommandType.Text, this.getObjInfo, new SqlParameter[]
			{
				new SqlParameter("@ObjId", objectId)
			});
			while (sqlDataReader.Read())
			{
				text = sqlDataReader.GetValue(0).ToString();
				text2 = sqlDataReader.GetValue(1).ToString();
				text3 = sqlDataReader.GetValue(2).ToString();
				text4 = sqlDataReader.GetValue(3).ToString();
				text5 = sqlDataReader.GetValue(4).ToString();
				text6 = sqlDataReader.GetValue(5).ToString();
			}
			sqlDataReader.Close();
			string text7 = "";
			string text8 = "";
			string str = "";
			string str2 = "";
			string str3 = "";
			if (text3.Trim().Equals("P"))
			{
				text7 = string.Concat(new string[]
				{
					"ALTER PROCEDURE [",
					text4,
					"].[",
					text,
					"]"
				});
				text8 = " WITH ENCRYPTION AS SELECT 1/*";
				str = new string('*', hexValuesLen - text7.Length - text8.Length - 2) + "*/";
				this.createFakeEncPre = string.Concat(new string[]
				{
					"CREATE PROCEDURE [",
					text4,
					"].[",
					text,
					"]"
				});
				str2 = " WITH ENCRYPTION AS SELECT 1/*";
				str3 = new string('*', hexValuesLen - text7.Length - text8.Length - 3) + "*/";
			}
			else if (text3.Trim().Equals("TR") && text6.Trim().Equals("U"))
			{
				text7 = string.Concat(new string[]
				{
					"ALTER TRIGGER [",
					text4,
					"].[",
					text,
					"] ON [",
					text4,
					"].[",
					text2,
					"]"
				});
				text8 = " WITH ENCRYPTION AFTER INSERT AS SELECT 1/*";
				str = new string('A', hexValuesLen - text7.Length - text8.Length - 2) + "*/";
				this.createFakeEncPre = string.Concat(new string[]
				{
					"CREATE TRIGGER [",
					text4,
					"].[",
					text,
					"] ON [",
					text4,
					"].[",
					text2,
					"]"
				});
				str2 = " WITH ENCRYPTION AFTER INSERT AS SELECT 1/*";
				str3 = new string('A', hexValuesLen - text7.Length - text8.Length - 3) + "*/";
			}
			else if (text3.Trim().Equals("TR") && text6.Trim().Equals("V"))
			{
				text7 = string.Concat(new string[]
				{
					"ALTER TRIGGER [",
					text4,
					"].[",
					text,
					"] ON [",
					text4,
					"].[",
					text2,
					"]"
				});
				text8 = " WITH ENCRYPTION INSTEAD OF INSERT AS SELECT 1/*";
				str = new string('A', hexValuesLen - text7.Length - text8.Length - 2) + "*/";
				this.createFakeEncPre = string.Concat(new string[]
				{
					"CREATE TRIGGER [",
					text4,
					"].[",
					text,
					"] ON [",
					text4,
					"].[",
					text2,
					"]"
				});
				str2 = " WITH ENCRYPTION INSTEAD OF INSERT AS SELECT 1/*";
				str3 = new string('A', hexValuesLen - text7.Length - text8.Length - 3) + "*/";
			}
			else if (text3.Trim().Equals("FN"))
			{
				text7 = string.Concat(new string[]
				{
					"ALTER FUNCTION [",
					text4,
					"].[",
					text,
					"]()"
				});
				text8 = " RETURNS int WITH ENCRYPTION AS BEGIN RETURN 1/*";
				str = new string('A', hexValuesLen - text7.Length - text8.Length - 3) + "*/END";
				this.createFakeEncPre = string.Concat(new string[]
				{
					"CREATE FUNCTION [",
					text4,
					"].[",
					text,
					"]()"
				});
				str2 = " RETURNS int WITH ENCRYPTION AS BEGIN RETURN 1/*";
				str3 = new string('A', hexValuesLen - text7.Length - text8.Length - 4) + "*/END";
			}
			else if (text3.Trim().Equals("IF"))
			{
				text7 = string.Concat(new string[]
				{
					"ALTER FUNCTION [",
					text4,
					"].[",
					text,
					"]()"
				});
				text8 = " RETURNS TABLE WITH ENCRYPTION AS RETURN (select 1 a/*";
				str = new string('A', hexValuesLen - text7.Length - text8.Length - 1) + "*/)";
				this.createFakeEncPre = string.Concat(new string[]
				{
					"CREATE FUNCTION [",
					text4,
					"].[",
					text,
					"]()"
				});
				str2 = " RETURNS TABLE WITH ENCRYPTION AS RETURN (select 1 a/*";
				str3 = new string('A', hexValuesLen - text7.Length - text8.Length - 2) + "*/)";
			}
			else if (text3.Trim().Equals("TF"))
			{
				text7 = string.Concat(new string[]
				{
					"ALTER FUNCTION [",
					text4,
					"].[",
					text,
					"]()"
				});
				text8 = " RETURNS @t TABLE (i int) WITH ENCRYPTION AS BEGIN return/*";
				str = new string('A', hexValuesLen - text7.Length - text8.Length - 3) + "*/END";
				this.createFakeEncPre = string.Concat(new string[]
				{
					"CREATE FUNCTION [",
					text4,
					"].[",
					text,
					"]()"
				});
				str2 = " RETURNS @t TABLE (i int) WITH ENCRYPTION AS BEGIN return/*";
				str3 = new string('A', hexValuesLen - text7.Length - text8.Length - 4) + "*/END";
			}
			else if (text3.Trim().Equals("V"))
			{
				text7 = string.Concat(new string[]
				{
					"ALTER VIEW [",
					text4,
					"].[",
					text,
					"]"
				});
				text8 = " WITH ENCRYPTION AS/*";
				str = new string('A', hexValuesLen - text7.Length - text8.Length - 11) + "*/SELECT 1 a";
				this.createFakeEncPre = string.Concat(new string[]
				{
					"CREATE VIEW [",
					text4,
					"].[",
					text,
					"]"
				});
				str2 = " WITH ENCRYPTION AS/*";
				str3 = new string('A', hexValuesLen - text7.Length - text8.Length - 12) + "*/SELECT 1 a";
			}
			else if (!text5.ToUpper().Equals("NULL"))
			{
				text7 = "ALTER TRIGGER [" + text5 + "] ON DATABASE";
				text8 = " WITH ENCRYPTION FOR DROP_TABLE AS/*";
				str = new string('A', hexValuesLen - text7.Length - text8.Length - 11) + "*/SELECT 1 a";
				this.createFakeEncPre = "CREATE TRIGGER [" + text5 + "] ON DATABASE";
				str2 = " WITH ENCRYPTION FOR DROP_TABLE AS/*";
				str3 = new string('A', hexValuesLen - text7.Length - text8.Length - 12) + "*/SELECT 1 a";
			}
			this.alterfakeEncHexValues = text7 + text8 + str;
			this.createFakeHexValues = this.createFakeEncPre + str2 + str3;
		}

		private void setDecryptObjByInfo()
		{
			StringBuilder stringBuilder = new StringBuilder();
			while (this.realEncHexValues.Length % 4 != 0)
			{
				this.realEncHexValues += "F";
			}
			while (this.fakeEncHexValues.Length < this.realEncHexValues.Length)
			{
				this.fakeEncHexValues += "F";
			}
			int num = this.realEncHexValues.Length / 4;
			char[] array = this.createFakeHexValues.ToCharArray();
			int[] array2 = new int[num];
			for (int i = 0; i < array2.Length; i++)
			{
				array2[i] = Convert.ToInt32(array[i]);
			}
			int[] array3 = new int[num];
			for (int j = 0; j < array3.Length; j++)
			{
				try
				{
					array3[j] = Convert.ToInt32(this.realEncHexValues.Substring(j * 4, 2), 16) + Convert.ToInt32(this.realEncHexValues.Substring(j * 4 + 2, 2), 16) * 256;
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.Message);
					ErrorLog.Write(ex);
				}
			}
			int[] array4 = new int[num];
			for (int k = 0; k < array4.Length; k++)
			{
				try
				{
					array4[k] = Convert.ToInt32(this.fakeEncHexValues.Substring(k * 4, 2), 16) + Convert.ToInt32(this.fakeEncHexValues.Substring(k * 4 + 2, 2), 16) * 256;
				}
				catch (Exception ex2)
				{
					Console.WriteLine(ex2.Message);
					ErrorLog.Write(ex2);
				}
			}
			int[] array5 = new int[num];
			for (int l = 0; l < array5.Length; l++)
			{
				array5[l] = (array2[l] ^ array4[l] ^ array3[l]);
				stringBuilder.Append(Convert.ToChar(array5[l]));
			}
			this.realDefinition = stringBuilder.ToString();
			Regex regex = new Regex("\\s+with\\s+", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
			Match match = regex.Match(this.realDefinition);
			if (match.Index <= 0)
			{
				return;
			}
			this.realDefinition = this.createFakeEncPre + this.realDefinition.Substring(match.Index, this.realDefinition.Length - match.Index - 1);
		}
	}
}
