using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace ZhuanCloud.Collector
{
	public class PerfConvertor : UITypeEditor
	{
		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			return UITypeEditorEditStyle.Modal;
		}

		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			try
			{
				IWindowsFormsEditorService windowsFormsEditorService = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
				if (windowsFormsEditorService != null)
				{
					CollectConfig collectConfig = context.Instance as CollectConfig;
					if (collectConfig != null)
					{
						FrmPerfmon frmPerfmon = new FrmPerfmon(collectConfig);
						if (windowsFormsEditorService.ShowDialog(frmPerfmon) == DialogResult.OK)
						{
							return frmPerfmon.PerfList;
						}
					}
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
			}
			return value;
		}
	}
}
