using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ZhuanCloud.Collector
{
	internal class FrmDBFilter : Form
	{
		private string instance = string.Empty;

		private List<DBFilter> filterList;

		private IContainer components;

		private Button btnOk;

		private DataGridView dgvFilter;

		private Button btnCancel;

		private DataGridViewComboBoxColumn colDBName;

		private DataGridViewComboBoxColumn colDBType;

		private DataGridViewTextBoxColumn colFilterString;

		public List<DBFilter> FilterList
		{
			get
			{
				return this.filterList;
			}
		}

		public FrmDBFilter(string instance, List<string> dbList, List<DBFilter> filterList)
		{
			this.InitializeComponent();
			this.instance = instance;
			if (dbList != null && dbList.Count > 0)
			{
				this.colDBName.Items.Add("全部");
				foreach (string current in dbList)
				{
					this.colDBName.Items.AddRange(new object[]
					{
						current
					});
				}
			}
			if (filterList != null && filterList.Count > 0)
			{
				this.filterList = filterList;
				foreach (DBFilter current2 in filterList)
				{
					this.dgvFilter.Rows.Add(new object[]
					{
						current2.DBName,
						this.GetDBType(current2.DBType),
						current2.FilterString
					});
				}
			}
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			this.filterList = new List<DBFilter>();
			this.dgvFilter.EndEdit();
			if (this.dgvFilter.Rows.Count > 0)
			{
				foreach (DataGridViewRow dataGridViewRow in ((IEnumerable)this.dgvFilter.Rows))
				{
					DBFilter dBFilter = new DBFilter();
					dBFilter.DBName = dataGridViewRow.Cells[0].FormattedValue.ToString();
					dBFilter.DBType = this.GetDBType(dataGridViewRow.Cells[1].FormattedValue.ToString());
					dBFilter.FilterString = dataGridViewRow.Cells[2].FormattedValue.ToString();
					dBFilter.InstanceName = this.instance;
					if (dBFilter.DBType > 0 && !string.IsNullOrEmpty(dBFilter.FilterString))
					{
						this.filterList.Add(dBFilter);
					}
				}
			}
			base.DialogResult = DialogResult.OK;
		}

		private int GetDBType(string typeName)
		{
			if (typeName != null)
			{
				if (typeName == "表")
				{
					return 1;
				}
				if (typeName == "视图")
				{
					return 2;
				}
				if (typeName == "存储过程")
				{
					return 3;
				}
				if (typeName == "函数")
				{
					return 4;
				}
			}
			return -1;
		}

		private string GetDBType(int typeId)
		{
			switch (typeId)
			{
			case 1:
				return "表";
			case 2:
				return "视图";
			case 3:
				return "存储过程";
			case 4:
				return "函数";
			default:
				return string.Empty;
			}
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.filterList = null;
			base.DialogResult = DialogResult.Cancel;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(FrmDBFilter));
			this.btnOk = new Button();
			this.dgvFilter = new DataGridView();
			this.colDBName = new DataGridViewComboBoxColumn();
			this.colDBType = new DataGridViewComboBoxColumn();
			this.colFilterString = new DataGridViewTextBoxColumn();
			this.btnCancel = new Button();
			((ISupportInitialize)this.dgvFilter).BeginInit();
			base.SuspendLayout();
			this.btnOk.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.btnOk.Location = new Point(408, 304);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new Size(75, 23);
			this.btnOk.TabIndex = 4;
			this.btnOk.Text = "确定";
			this.btnOk.UseVisualStyleBackColor = true;
			this.btnOk.Click += new EventHandler(this.btnOk_Click);
			this.dgvFilter.Anchor = (AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
			this.dgvFilter.BackgroundColor = Color.White;
			this.dgvFilter.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvFilter.Columns.AddRange(new DataGridViewColumn[]
			{
				this.colDBName,
				this.colDBType,
				this.colFilterString
			});
			this.dgvFilter.Location = new Point(12, 12);
			this.dgvFilter.Name = "dgvFilter";
			this.dgvFilter.RowHeadersVisible = false;
			this.dgvFilter.RowTemplate.Height = 23;
			this.dgvFilter.Size = new Size(555, 273);
			this.dgvFilter.TabIndex = 3;
			this.colDBName.HeaderText = "数据库名称";
			this.colDBName.Name = "colDBName";
			this.colDBName.Width = 160;
			this.colDBType.HeaderText = "数据库类型";
			this.colDBType.Items.AddRange(new object[]
			{
				"表",
				"视图",
				"存储过程",
				"函数"
			});
			this.colDBType.Name = "colDBType";
			this.colDBType.Resizable = DataGridViewTriState.True;
			this.colDBType.SortMode = DataGridViewColumnSortMode.Automatic;
			this.colFilterString.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
			this.colFilterString.HeaderText = "过滤条件";
			this.colFilterString.Name = "colFilterString";
			this.btnCancel.Anchor = (AnchorStyles.Bottom | AnchorStyles.Right);
			this.btnCancel.Location = new Point(491, 304);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new Size(75, 23);
			this.btnCancel.TabIndex = 5;
			this.btnCancel.Text = "取消";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
			base.AutoScaleDimensions = new SizeF(6f, 12f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(578, 339);
			base.Controls.Add(this.btnOk);
			base.Controls.Add(this.dgvFilter);
			base.Controls.Add(this.btnCancel);
			base.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "FrmDBFilter";
			base.ShowInTaskbar = false;
			base.StartPosition = FormStartPosition.CenterScreen;
			this.Text = "数据库过滤条件";
			((ISupportInitialize)this.dgvFilter).EndInit();
			base.ResumeLayout(false);
		}
	}
}
