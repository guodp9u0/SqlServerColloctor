using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

namespace ZhuanCloud.Collector
{
	public class WaitCollector : OleDbCollector
	{
		public override bool SaveData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			bool result;
			try
			{
				DataSet data = this.GetData(sqlString, oleDBString, timeOut);
				DataTable dataTable = data.Tables[0];
				DataTable dataTable2 = data.Tables[1];
				if (dataTable.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblWait\r\n        ( SessionId ,\r\n          RequestId ,\r\n          BLKId ,\r\n          DBName ,\r\n          StartTime ,\r\n          CollectTime ,\r\n          CPUTime ,\r\n          Reads ,\r\n          Writes ,\r\n          LogicalReads ,\r\n          InternalObjectsAlloc_MB ,\r\n          InternalObjectsDealloc_MB ,\r\n          UserObjectsAlloc_MB ,\r\n          UserObjectsDealloc_MB ,\r\n          LoginName ,\r\n          Status ,\r\n          WaitType ,\r\n          WaitResource ,\r\n          WaitTime ,\r\n          TaskCount ,\r\n          ChildTextHashCode ,\r\n          ParentTextHashCode ,\r\n          ProgramName ,\r\n          HostName ,\r\n          OpenTransactionCount ,\r\n          TransactionLevel ,\r\n          PlanHandle ,\r\n          ParameterHash\r\n        )\r\nVALUES  ( @SessionId ,\r\n          @RequestId ,\r\n          @BLKId ,\r\n          @DBName ,\r\n          @StartTime ,\r\n          @CollectTime ,\r\n          @CPUTime ,\r\n          @Reads ,\r\n          @Writes ,\r\n          @LogicalReads ,\r\n          @InternalObjectsAlloc_MB ,\r\n          @InternalObjectsDealloc_MB ,\r\n          @UserObjectsAlloc_MB ,\r\n          @UserObjectsDealloc_MB ,\r\n          @LoginName ,\r\n          @Status ,\r\n          @WaitType ,\r\n          @WaitResource ,\r\n          @WaitTime ,\r\n          @TaskCount ,\r\n          @ChildTextHashCode ,\r\n          @ParentTextHashCode ,\r\n          @ProgramName ,\r\n          @HostName ,\r\n          @OpenTransactionCount ,\r\n          @TransactionLevel ,\r\n          @PlanHandle ,\r\n          @ParameterHash\r\n        )\r\n";
					DataTable dataTable3 = this.AddHashCode(dataTable, dataTable2, oleDBString, timeOut);
					if (dataTable3.Rows.Count == 0)
					{
						result = true;
						return result;
					}
					OleDbParameter[] parameters = this.GetParameters();
					if (!base.SaveToAccess(oleDBString, dataTable3, cmdText, parameters))
					{
						result = false;
						return result;
					}
					if (dataTable2.Rows.Count > 0)
					{
						foreach (DataRow dataRow in dataTable2.Rows)
						{
							dataRow.SetAdded();
						}
						string cmdText2 = "\r\nINSERT  INTO tblWaitingTask\r\n        ( SessionId ,\r\n          RequestId ,\r\n          WaitType ,\r\n          WaitDuration ,\r\n          BLKId ,\r\n          CollectTime \r\n        )\r\nVALUES  ( @SessionId ,\r\n          @RequestId ,\r\n          @WaitType ,\r\n          @WaitDuration ,\r\n          @BLKId ,\r\n          @CollectTime\r\n        )\r\n";
						OleDbParameter[] parameters2 = new OleDbParameter[]
						{
							new OleDbParameter("@SessionId", OleDbType.Integer)
							{
								SourceColumn = "session_id"
							},
							new OleDbParameter("@RequestId", OleDbType.Integer)
							{
								SourceColumn = "request_id"
							},
							new OleDbParameter("@WaitType", OleDbType.VarChar)
							{
								SourceColumn = "wait_type"
							},
							new OleDbParameter("@WaitDuration", OleDbType.Double)
							{
								SourceColumn = "wait_duration_ms"
							},
							new OleDbParameter("@BLKId", OleDbType.Integer)
							{
								SourceColumn = "blocking_session_id"
							},
							new OleDbParameter("@CollectTime", OleDbType.Date)
							{
								SourceColumn = "now_time"
							}
						};
						if (!base.SaveToAccess(oleDBString, dataTable2, cmdText2, parameters2))
						{
							result = false;
							return result;
						}
					}
					List<int> list = new List<int>();
					List<int> list2 = new List<int>();
					string text = string.Empty;
					foreach (DataRow dataRow2 in dataTable3.Rows)
					{
						if (string.IsNullOrEmpty(text))
						{
							DateTime dateTime = new DateTime(1900, 1, 1);
							text = (Util.DateIsNull(dataRow2["now_time"], out dateTime) ? string.Empty : dataRow2["now_time"].ToString());
						}
						int item = Convert.ToInt32(dataRow2["session_id"]);
						int num = (dataRow2["blocking_session_id"] == DBNull.Value) ? 0 : Convert.ToInt32(dataRow2["blocking_session_id"]);
						if (!list.Contains(item))
						{
							list.Add(item);
						}
						if (num > 0 && !list2.Contains(num))
						{
							list2.Add(num);
						}
						if (!Util.startFlag)
						{
							result = false;
							return result;
						}
					}
					if (list2.Count > 0)
					{
						List<int> list3 = new List<int>();
						foreach (int current in list2)
						{
							if (!list.Contains(current))
							{
								list3.Add(current);
							}
						}
						if (list3.Count > 0)
						{
							if (!Util.startFlag)
							{
								result = false;
								return result;
							}
							DataTable lostData = this.GetLostData(sqlString, oleDBString, sqlVersion, timeOut, text, list3, dataTable3);
							if (lostData.Rows.Count > 0)
							{
								OleDbParameter[] parameters3 = this.GetParameters();
								result = base.SaveToAccess(oleDBString, lostData, cmdText, parameters3);
								return result;
							}
						}
					}
				}
				result = true;
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataSet GetData(string sqlString, string oleDBString, int timeOut)
		{
			string commandText = "\r\nDECLARE @collect_time VARCHAR(100)\r\nSET @collect_time = CONVERT(VARCHAR(100), GETDATE(), 120)\r\nSELECT  a.session_id ,\r\n        ISNULL(e.request_id,0) AS request_id ,\r\n        ISNULL(a.wait_type,'') AS wait_type ,\r\n        ISNULL(wait_duration_ms,0) AS wait_duration_ms ,\r\n        ISNULL(blocking_session_id,0) AS blocking_session_id ,\r\n        @collect_time AS now_time\r\nINTO    #WaitTask\r\nFROM    sys.dm_os_waiting_tasks a\r\n        LEFT JOIN sys.dm_os_tasks e ON a.waiting_task_address = e.task_address\r\n                                       AND a.exec_context_id = e.exec_context_id\r\nWHERE   a.session_id > 50\r\n        AND a.wait_type <> 'CXPACKET'\r\nORDER BY wait_duration_ms DESC;\r\nSELECT  es.session_id ,\r\n        er.request_id ,\r\n        blocking_session_id ,\r\n        database_name = DB_NAME(er.database_id) ,\r\n        CONVERT(VARCHAR(100), start_time, 120) start_time ,\r\n        @collect_time AS now_time ,\r\n        er.cpu_time ,\r\n        er.reads ,\r\n        er.writes ,\r\n        er.logical_reads ,\r\n        ISNULL(internal_objects_alloc_mb, 0) internal_objects_alloc_mb ,\r\n        ISNULL(internal_objects_dealloc_mb, 0) internal_objects_dealloc_mb ,\r\n        ISNULL(user_objects_alloc_mb, 0) user_objects_alloc_mb ,\r\n        ISNULL(user_objects_dealloc_mb, 0) user_objects_dealloc_mb ,\r\n        login_name ,\r\n        er.status ,\r\n        wait_type ,\r\n        wait_resource ,\r\n        wait_time ,\r\n        taskCount ,\r\n        individual_query = SUBSTRING(ISNULL(qt.text, ''),\r\n                                     ( er.statement_start_offset / 2 ) + 1,\r\n                                     ( ( CASE WHEN er.statement_end_offset = -1\r\n                                              THEN LEN(CONVERT(NVARCHAR(MAX), ISNULL(qt.text,\r\n                                                              ''))) * 2\r\n                                              ELSE er.statement_end_offset\r\n                                         END - er.statement_start_offset ) / 2 )\r\n                                     + 1) ,\r\n        parent_query = ISNULL(qt.text, '') ,\r\n        program_name ,\r\n        host_name ,\r\n        er.open_transaction_count ,\r\n        er.transaction_isolation_level ,\r\n        plan_handle\r\nFROM    sys.dm_exec_requests (NOLOCK) er\r\n        INNER JOIN sys.dm_exec_sessions (NOLOCK) es ON er.session_id = es.session_id\r\n        LEFT JOIN ( SELECT  session_id ,\r\n                            SUM(internal_objects_alloc_page_count * 8 / 1024) AS internal_objects_alloc_mb ,\r\n                            SUM(internal_objects_dealloc_page_count * 8 / 1024) AS internal_objects_dealloc_mb ,\r\n                            SUM(user_objects_alloc_page_count * 8 / 1024) AS user_objects_alloc_mb ,\r\n                            SUM(user_objects_dealloc_page_count * 8 / 1024) AS user_objects_dealloc_mb\r\n                    FROM    sys.dm_db_task_space_usage\r\n                    GROUP BY session_id\r\n                  ) AS tempdb ON er.session_id = tempdb.session_id\r\n        LEFT JOIN ( SELECT  session_id ,\r\n                            taskCount = COUNT(session_id)\r\n                    FROM    #WaitTask\r\n                    GROUP BY session_id\r\n                  ) AS task_info ON task_info.session_id = er.session_id\r\n        OUTER APPLY sys.dm_exec_sql_text(er.sql_handle) AS qt\r\nWHERE   es.session_id NOT IN ( @@SPID )\r\n        AND [program_name] <> 'ZhuanCloud'\r\n        AND wait_type NOT IN ( N'TRACEWRITE', N'SP_SERVER_DIAGNOSTICS_SLEEP', N'BROKER_RECEIVE_WAITFOR')\r\n        AND ( er.blocking_session_id > 0\r\n              OR er.wait_type IS NOT NULL\r\n              OR er.session_id IN ( SELECT DISTINCT\r\n                                            blocking_session_id\r\n                                    FROM    sys.dm_exec_requests )\r\n            )\r\nORDER BY 1 ,2\r\nOPTION (MAXDOP 2);\r\nSELECT TOP 5\r\n        session_id ,\r\n        request_id ,\r\n        wait_type ,\r\n        wait_duration_ms ,\r\n        blocking_session_id ,\r\n        now_time\r\nFROM    #WaitTask\r\nOPTION (MAXDOP 2)\r\nDROP TABLE #WaitTask\r\n";
			DataSet dataSet = new DataSet();
			try
			{
				SqlConnection sqlConnection = new SqlConnection(sqlString);
				SqlCommand sqlCommand = sqlConnection.CreateCommand();
				sqlCommand.CommandTimeout = timeOut;
				sqlCommand.CommandText = commandText;
				SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
				sqlDataAdapter.Fill(dataSet);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				throw new Exception(ex.Message);
			}
			return dataSet;
		}

		private DataTable GetLostData(string sqlString, string oleDBString, SQLServerVersion sqlVersion, int timeOut, string collectime, List<int> sessionList, DataTable tblWaitTask)
		{
			if (sessionList.Count > 0)
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append("(");
				foreach (int current in sessionList)
				{
					stringBuilder.Append(current);
					stringBuilder.Append(",");
				}
				stringBuilder.Remove(stringBuilder.Length - 1, 1);
				stringBuilder.Append(")");
				string commandText = string.Empty;
				if (sqlVersion >= SQLServerVersion.SQLServer2012)
				{
					commandText = string.Format("\r\nSELECT \r\n\tec.session_id,\r\n\t0 request_id,\r\n    0 blocking_session_id,\r\n\t'' database_name,\t\r\n\tse.last_request_start_time start_time,\r\n\t'{0}' now_time,\r\n\t0 cpu_time,\r\n\t0 reads,\r\n\t0 writes,\r\n\t0 logical_reads,\r\n    ISNULL(internal_objects_alloc_mb,0) internal_objects_alloc_mb,\r\n\tISNULL(internal_objects_dealloc_mb,0) internal_objects_dealloc_mb,\r\n\tISNULL(user_objects_alloc_mb,0) user_objects_alloc_mb,\r\n\tISNULL(user_objects_dealloc_mb,0) user_objects_dealloc_mb,\r\n\tse.login_name,\r\n\tse.status,\t\r\n\t'' wait_type,\r\n\t'' wait_resource,\r\n\t0 wait_time,\r\n    0 taskCount ,\r\n\tindividual_query = ISNULL(qt.text,''),\r\n\tparent_query = ISNULL(qt.text,''),\r\n\tse.program_name,\r\n\tse.host_name,\r\n\tse.open_transaction_count,\r\n\tse.transaction_isolation_level,\r\n\tNULL plan_handle\r\nFROM \r\n\tsys.dm_exec_connections(NOLOCK) ec \r\n\tINNER JOIN sys.dm_exec_sessions(NOLOCK) se\r\n\tON ec.session_id=se.session_id\t\r\n    LEFT JOIN (\r\n\t\t\t\t\tSELECT\r\n\t\t\t\t\t\tsession_id,\r\n\t\t\t\t\t\tSUM(internal_objects_alloc_page_count * 8 / 1024) as internal_objects_alloc_mb,\r\n\t\t\t\t\t\tSUM(internal_objects_dealloc_page_count * 8 / 1024) as internal_objects_dealloc_mb,\r\n\t\t\t\t\t\tSUM(user_objects_alloc_page_count * 8 / 1024) as user_objects_alloc_mb,\r\n\t\t\t\t\t\tSUM(user_objects_dealloc_page_count * 8 / 1024) as user_objects_dealloc_mb\r\n\t\t\t\t\tFROM\r\n\t\t\t\t\t\tsys.dm_db_task_space_usage\r\n\t\t\t\t\tGROUP BY\r\n\t\t\t\t\t\tsession_id\r\n\t\t\t\t) AS tempdb on se.session_id = tempdb.session_id\r\n\tOUTER APPLY sys.dm_exec_sql_text(ec.most_recent_sql_handle)as qt\r\nWHERE \r\n\t[program_name] <> 'ZhuanCloud'\r\n\tAND ec.session_id in {1}\r\n", collectime, stringBuilder.ToString());
				}
				else
				{
					commandText = string.Format("\r\nSELECT \r\n\tec.session_id,\r\n\t0 request_id,\r\n    0 blocking_session_id,\r\n\t'' database_name,\t\r\n\tse.last_request_start_time start_time,\r\n\t'{0}' now_time,\r\n\t0 cpu_time,\r\n\t0 reads,\r\n\t0 writes,\r\n\t0 logical_reads,\r\n    ISNULL(internal_objects_alloc_mb,0) internal_objects_alloc_mb,\r\n\tISNULL(internal_objects_dealloc_mb,0) internal_objects_dealloc_mb,\r\n\tISNULL(user_objects_alloc_mb,0) user_objects_alloc_mb,\r\n\tISNULL(user_objects_dealloc_mb,0) user_objects_dealloc_mb,\r\n\tse.login_name,\r\n\tse.status,\t\r\n\t'' wait_type,\r\n\t'' wait_resource,\r\n\t0 wait_time,\r\n    0 taskCount ,\r\n\tindividual_query = ISNULL(qt.text,''),\r\n\tparent_query = ISNULL(qt.text,''),\r\n\tse.program_name,\r\n\tse.host_name,\r\n\tISNULL(t.open_transaction_count,0) open_transaction_count,\r\n\tse.transaction_isolation_level,\r\n\tNULL plan_handle\r\nFROM \r\n\tsys.dm_exec_connections(NOLOCK) ec \r\n\tINNER JOIN sys.dm_exec_sessions(NOLOCK) se\r\n\tON ec.session_id=se.session_id\r\n\tLEFT JOIN (\r\n\t\t\t\t\tSELECT  session_id ,\r\n\t\t\t\t\t\t\tCOUNT(transaction_id) AS open_transaction_count\r\n\t\t\t\t\tFROM    sys.dm_tran_session_transactions (NOLOCK)\r\n\t\t\t\t\tGROUP BY session_id\r\n\t\t\t\t) AS t ON t.session_id = se.session_id\r\n    LEFT JOIN (\r\n\t\t\t\t\tSELECT\r\n\t\t\t\t\t\tsession_id,\r\n\t\t\t\t\t\tSUM(internal_objects_alloc_page_count * 8 / 1024) as internal_objects_alloc_mb,\r\n\t\t\t\t\t\tSUM(internal_objects_dealloc_page_count * 8 / 1024) as internal_objects_dealloc_mb,\r\n\t\t\t\t\t\tSUM(user_objects_alloc_page_count * 8 / 1024) as user_objects_alloc_mb,\r\n\t\t\t\t\t\tSUM(user_objects_dealloc_page_count * 8 / 1024) as user_objects_dealloc_mb\r\n\t\t\t\t\tFROM\r\n\t\t\t\t\t\tsys.dm_db_task_space_usage\r\n\t\t\t\t\tGROUP BY\r\n\t\t\t\t\t\tsession_id\r\n\t\t\t\t) AS tempdb on se.session_id = tempdb.session_id\r\n\tOUTER APPLY sys.dm_exec_sql_text(ec.most_recent_sql_handle)as qt\r\nWHERE \r\n\t[program_name] <> 'ZhuanCloud'\r\n\tAND ec.session_id in {1}\r\n", collectime, stringBuilder.ToString());
				}
				DataTable dataTable = new DataTable();
				try
				{
					SqlConnection sqlConnection = new SqlConnection(sqlString);
					SqlCommand sqlCommand = sqlConnection.CreateCommand();
					sqlCommand.CommandTimeout = timeOut;
					sqlCommand.CommandText = commandText;
					SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
					sqlDataAdapter.Fill(dataTable);
				}
				catch (Exception ex)
				{
					ErrorLog.Write(ex);
					throw new Exception(ex.Message);
				}
				return this.AddHashCode(dataTable, tblWaitTask, oleDBString, timeOut);
			}
			return null;
		}

		public DataTable AddHashCode(DataTable tblSource, DataTable tblWaitTask, string oleDBString, int timeOut)
		{
			try
			{
				if (tblSource.Rows.Count > 0)
				{
					bool flag = false;
					if (tblWaitTask != null)
					{
						flag = (tblWaitTask.Rows.Count > 0);
					}
					List<WaitHash> list = new List<WaitHash>();
					tblSource.CaseSensitive = false;
					DataRow[] array = tblSource.Select("parent_query not like 'WAITFOR%' and parent_query not like 'BACKUP%' and parent_query <> 'sp_server_diagnostics'");
					if (array.Length > 0)
					{
						tblSource.Columns.Add(new DataColumn("ParameterHash"));
						MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
						DataRow[] array2 = array;
						for (int i = 0; i < array2.Length; i++)
						{
							DataRow dataRow = array2[i];
							if (flag && Convert.ToInt32(dataRow["blocking_session_id"]) == 0)
							{
								int num = Convert.ToInt32(dataRow["session_id"]);
								DataRow[] array3 = tblWaitTask.Select(string.Format("session_id = {0} and blocking_session_id > 0  and session_id <> blocking_session_id", num));
								if (array3.Length > 0)
								{
									dataRow["blocking_session_id"] = Convert.ToInt32(array3[0]["blocking_session_id"]);
								}
							}
							string text = (dataRow["individual_query"] == DBNull.Value) ? string.Empty : dataRow["individual_query"].ToString();
							if (!string.IsNullOrEmpty(text))
							{
								byte[] bytes = TextManager.Encoding.GetBytes(text);
								byte[] inArray = mD5CryptoServiceProvider.ComputeHash(bytes);
								string text2 = Convert.ToBase64String(inArray);
								dataRow["individual_query"] = text2;
								if (!Util.waitHashList.Contains(text2))
								{
									WaitHash waitHash = new WaitHash();
									waitHash.TextHashCode = text2;
									waitHash.SqlText = TextManager.Compress(text);
									Util.waitHashList.Add(text2);
									list.Add(waitHash);
								}
								string regexString = TextManager.GetRegexString(text);
								bytes = TextManager.Encoding.GetBytes(regexString);
								inArray = mD5CryptoServiceProvider.ComputeHash(bytes);
								text2 = Convert.ToBase64String(inArray);
								dataRow["ParameterHash"] = text2;
							}
							else
							{
								dataRow["individual_query"] = string.Empty;
								dataRow["ParameterHash"] = string.Empty;
							}
							string text3 = (dataRow["parent_query"] == DBNull.Value) ? string.Empty : dataRow["parent_query"].ToString();
							if (!string.IsNullOrEmpty(text3))
							{
								byte[] bytes2 = TextManager.Encoding.GetBytes(text3);
								byte[] inArray2 = mD5CryptoServiceProvider.ComputeHash(bytes2);
								string text4 = Convert.ToBase64String(inArray2);
								dataRow["parent_query"] = text4;
								if (!Util.waitHashList.Contains(text4))
								{
									WaitHash waitHash2 = new WaitHash();
									waitHash2.TextHashCode = text4;
									waitHash2.SqlText = TextManager.Compress(text3);
									Util.waitHashList.Add(text4);
									list.Add(waitHash2);
								}
							}
							else
							{
								dataRow["parent_query"] = string.Empty;
							}
							dataRow.AcceptChanges();
							dataRow.SetAdded();
						}
						if (list.Count > 0)
						{
							OleDbConnection oleDbConnection = null;
							if (!Util.TryConnect(oleDBString, out oleDbConnection))
							{
								return tblSource;
							}
							try
							{
								OleDbCommand oleDbCommand = oleDbConnection.CreateCommand();
								oleDbCommand.CommandText = "INSERT INTO tblWaitHash(TextHashCode,SqlText) VALUES(@TextHashCode,@SqlText)";
								oleDbConnection.Open();
								foreach (WaitHash current in list)
								{
									OleDbParameter[] values = new OleDbParameter[]
									{
										new OleDbParameter("@TextHashCode", OleDbType.VarChar)
										{
											Value = (current.TextHashCode ?? string.Empty)
										},
										new OleDbParameter("@SqlText", OleDbType.LongVarWChar)
										{
											Value = (current.SqlText ?? string.Empty)
										}
									};
									oleDbCommand.Parameters.Clear();
									oleDbCommand.Parameters.AddRange(values);
									oleDbCommand.CommandTimeout = timeOut;
									oleDbCommand.ExecuteNonQuery();
								}
								oleDbConnection.Close();
							}
							catch (Exception ex)
							{
								ErrorLog.Write(ex);
							}
							finally
							{
								if (oleDbConnection.State != ConnectionState.Closed)
								{
									oleDbConnection.Close();
								}
								oleDbConnection.Dispose();
							}
						}
					}
					if (array.Length != tblSource.Rows.Count)
					{
						tblSource = tblSource.GetChanges(DataRowState.Added);
					}
				}
			}
			catch (Exception ex2)
			{
				ErrorLog.Write(ex2);
				throw new Exception(ex2.Message);
			}
			return tblSource;
		}

		private void GetLostSqlText(string sqlString, int spid)
		{
			try
			{
				string text = string.Format("DBCC INPUTBUFFER({0})", spid);
				ErrorLog.Debug("start exec: " + text);
				using (SqlConnection sqlConnection = new SqlConnection(sqlString))
				{
					SqlCommand sqlCommand = sqlConnection.CreateCommand();
					sqlCommand.CommandTimeout = 120;
					sqlCommand.CommandText = text;
					sqlConnection.Open();
					SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
					while (sqlDataReader.Read())
					{
						string message = sqlDataReader["EventInfo"].ToString();
						ErrorLog.Debug(message);
					}
					sqlConnection.Close();
				}
				ErrorLog.Debug("end exec: " + text);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				throw new Exception(ex.Message);
			}
		}

		private OleDbParameter[] GetParameters()
		{
			return new OleDbParameter[]
			{
				new OleDbParameter("@SessionId", OleDbType.Integer)
				{
					SourceColumn = "session_id"
				},
				new OleDbParameter("@RequestId", OleDbType.Integer)
				{
					SourceColumn = "request_id"
				},
				new OleDbParameter("@BLKId", OleDbType.Integer)
				{
					SourceColumn = "blocking_session_id"
				},
				new OleDbParameter("@DBName", OleDbType.VarChar)
				{
					SourceColumn = "database_name"
				},
				new OleDbParameter("@StartTime", OleDbType.Date)
				{
					SourceColumn = "start_time"
				},
				new OleDbParameter("@CollectTime", OleDbType.Date)
				{
					SourceColumn = "now_time"
				},
				new OleDbParameter("@CPUTime", OleDbType.Double)
				{
					SourceColumn = "cpu_time"
				},
				new OleDbParameter("@Reads", OleDbType.Double)
				{
					SourceColumn = "reads"
				},
				new OleDbParameter("@Writes", OleDbType.Double)
				{
					SourceColumn = "writes"
				},
				new OleDbParameter("@LogicalReads", OleDbType.Double)
				{
					SourceColumn = "logical_reads"
				},
				new OleDbParameter("@InternalObjectsAlloc_MB", OleDbType.Double)
				{
					SourceColumn = "internal_objects_alloc_mb"
				},
				new OleDbParameter("@InternalObjectsDealloc_MB", OleDbType.Double)
				{
					SourceColumn = "internal_objects_dealloc_mb"
				},
				new OleDbParameter("@UserObjectsAlloc_MB", OleDbType.Double)
				{
					SourceColumn = "user_objects_alloc_mb"
				},
				new OleDbParameter("@UserObjectsDealloc_MB", OleDbType.Double)
				{
					SourceColumn = "user_objects_dealloc_mb"
				},
				new OleDbParameter("@LoginName", OleDbType.VarChar)
				{
					SourceColumn = "login_name"
				},
				new OleDbParameter("@Status", OleDbType.VarChar)
				{
					SourceColumn = "status"
				},
				new OleDbParameter("@WaitType", OleDbType.VarChar)
				{
					SourceColumn = "wait_type"
				},
				new OleDbParameter("@WaitResource", OleDbType.VarChar)
				{
					SourceColumn = "wait_resource"
				},
				new OleDbParameter("@WaitTime", OleDbType.Double)
				{
					SourceColumn = "wait_time"
				},
				new OleDbParameter("@TaskCount", OleDbType.Integer)
				{
					SourceColumn = "taskCount"
				},
				new OleDbParameter("@ChildQuery", OleDbType.VarChar)
				{
					SourceColumn = "individual_query"
				},
				new OleDbParameter("@ParentQuery", OleDbType.VarChar)
				{
					SourceColumn = "parent_query"
				},
				new OleDbParameter("@ProgramName", OleDbType.VarChar)
				{
					SourceColumn = "program_name"
				},
				new OleDbParameter("@HostName", OleDbType.VarChar)
				{
					SourceColumn = "host_name"
				},
				new OleDbParameter("@OpenTransactionCount", OleDbType.Integer)
				{
					SourceColumn = "open_transaction_count"
				},
				new OleDbParameter("@TransactionLevel", OleDbType.Integer)
				{
					SourceColumn = "transaction_isolation_level"
				},
				new OleDbParameter("@PlanHandle", OleDbType.VarBinary)
				{
					IsNullable = true,
					SourceColumn = "plan_handle"
				},
				new OleDbParameter("@ParameterHash", OleDbType.VarChar)
				{
					SourceColumn = "ParameterHash"
				}
			};
		}
	}
}
