using System;

namespace ZhuanCloud.Collector
{
	internal class HashIndex
	{
		public string IndexName
		{
			get;
			set;
		}

		public int IndexType
		{
			get;
			set;
		}

		public bool IsPriKey
		{
			get;
			set;
		}

		public string TypeDesc
		{
			get;
			set;
		}

		public string ColName
		{
			get;
			set;
		}

		public bool IsDescCol
		{
			get;
			set;
		}
	}
}
