using System;

namespace ZhuanCloud.Collector
{
	public class SQLService
	{
		public string InstanceName
		{
			get;
			set;
		}

		public string ServiceName
		{
			get;
			set;
		}

		public bool IsCurrentAccount
		{
			get;
			set;
		}

		public string AccountName
		{
			get;
			set;
		}
	}
}
