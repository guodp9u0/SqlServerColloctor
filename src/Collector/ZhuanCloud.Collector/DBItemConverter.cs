using System;
using System.ComponentModel;
using System.Globalization;

namespace ZhuanCloud.Collector
{
	public class DBItemConverter : ExpandableObjectConverter
	{
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return destinationType == typeof(DBItemType) || base.CanConvertTo(context, destinationType);
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == typeof(string) && value is DBItemType)
			{
				DBItemType dBItemType = (DBItemType)value;
				return string.Format("表:{0},视图:{1},函数:{2},存储过程:{3}", new object[]
				{
					dBItemType.HasTable.ToString(),
					dBItemType.HasView.ToString(),
					dBItemType.HasFuction.ToString(),
					dBItemType.HasStoredProc.ToString()
				});
			}
			return base.ConvertTo(context, culture, value, destinationType);
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is string)
			{
				try
				{
					string text = (string)value;
					string[] array = text.Split(new string[]
					{
						","
					}, StringSplitOptions.RemoveEmptyEntries);
					if (array.Length == 4)
					{
						return new DBItemType
						{
							HasTable = Convert.ToBoolean(array[0].Substring(array[0].IndexOf(":"))),
							HasView = Convert.ToBoolean(array[1].Substring(array[1].IndexOf(":"))),
							HasFuction = Convert.ToBoolean(array[2].Substring(array[2].IndexOf(":"))),
							HasStoredProc = Convert.ToBoolean(array[3].Substring(array[3].IndexOf(":")))
						};
					}
				}
				catch (Exception)
				{
					throw new ArgumentException("数据库类型转换错误");
				}
			}
			return base.ConvertFrom(context, culture, value);
		}
	}
}
