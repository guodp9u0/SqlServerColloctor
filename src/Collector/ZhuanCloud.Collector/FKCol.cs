using System;

namespace ZhuanCloud.Collector
{
	internal class FKCol
	{
		public string FKName
		{
			get;
			set;
		}

		public bool IsDisable
		{
			get;
			set;
		}

		public string FKColName
		{
			get;
			set;
		}

		public string RFSchema
		{
			get;
			set;
		}

		public string RFName
		{
			get;
			set;
		}

		public string RFColName
		{
			get;
			set;
		}
	}
}
