using System;
using System.Data;
using System.Data.OleDb;

namespace ZhuanCloud.Collector
{
	public class SqlInfoCollector : OleDbCollector
	{
		private string loginName;

		public string LoginName
		{
			get
			{
				return this.loginName;
			}
			set
			{
				this.loginName = value;
			}
		}

		public override bool SaveData(string oleDBString, string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(sqlString, sqlVersion, timeOut);
				DataRowCollection rows = data.Rows;
				if (rows.Count > 0)
				{
					DataRow dataRow = rows[0];
					string value = dataRow.ItemArray[0].ToString();
					int num = (int)dataRow.ItemArray[1];
					int num2 = (int)dataRow.ItemArray[2];
					int num3 = (int)dataRow.ItemArray[3];
					DateTime dateTime = (DateTime)dataRow.ItemArray[4];
					bool flag = dataRow.ItemArray[5].ToString() == "1";
					string value2 = dataRow.ItemArray[6].ToString();
					string value3 = dataRow.ItemArray[7].ToString();
					long num4 = Convert.ToInt64(dataRow.ItemArray[8]);
					string value4 = dataRow.ItemArray[9].ToString();
					string cmdText = "\r\nINSERT  INTO tblSQLInfo\r\n        ( ServerName ,\r\n          CpuCount ,\r\n          HyperthreadRatio ,\r\n          CpuOnlineCount ,\r\n          SqlserverStartTime ,\r\n          IsClustered ,\r\n          ProductVersion ,\r\n          SQLEdition ,\r\n          EditionID ,\r\n          ProductLevel ,\r\n          LoginName\r\n        )\r\nVALUES  ( @ServerName ,\r\n          @CpuCount ,\r\n          @HyperthreadRatio ,\r\n          @CpuOnlineCount ,\r\n          @SqlserverStartTime ,\r\n          @IsClustered ,\r\n          @ProductVersion ,\r\n          @SQLEdition ,\r\n          @EditionID ,\r\n          @ProductLevel ,\r\n          @LoginName\r\n        )";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@ServerName", OleDbType.VarChar)
						{
							Value = value
						},
						new OleDbParameter("@CpuCount", OleDbType.Integer)
						{
							Value = num
						},
						new OleDbParameter("@HyperthreadRatio", OleDbType.Integer)
						{
							Value = num2
						},
						new OleDbParameter("@CpuOnlineCount", OleDbType.Integer)
						{
							Value = num3
						},
						new OleDbParameter("@SqlserverStartTime", OleDbType.Date)
						{
							Value = dateTime
						},
						new OleDbParameter("@IsClustered", OleDbType.Boolean)
						{
							Value = flag
						},
						new OleDbParameter("@ProductVersion", OleDbType.VarChar)
						{
							Value = value2
						},
						new OleDbParameter("@SQLEdition", OleDbType.VarChar)
						{
							Value = value3
						},
						new OleDbParameter("@EditionID", OleDbType.BigInt)
						{
							Value = num4
						},
						new OleDbParameter("@ProductLevel", OleDbType.VarChar)
						{
							Value = value4
						},
						new OleDbParameter("@LoginName", OleDbType.VarChar)
						{
							Value = ((this.loginName == null) ? string.Empty : this.loginName)
						}
					};
					result = base.SaveToAccess(oleDBString, cmdText, timeOut, parameters);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, SQLServerVersion sqlVersion, int timeOut)
		{
			string cmdText = string.Empty;
			if (sqlVersion <= SQLServerVersion.SQLServer2005)
			{
				cmdText = "\r\nDECLARE @start_time DATETIME\r\nSELECT  @start_time = create_date\r\nFROM    sys.databases\r\nWHERE   name = 'tempdb'\r\nDECLARE @cpu_online_count INT\r\nSELECT  @cpu_online_count = COUNT(*)\r\nFROM    sys.dm_os_schedulers\r\nWHERE   is_online = 1\r\n        AND status = 'VISIBLE ONLINE'\r\nSELECT  SERVERPROPERTY('ServerName') AS server_name ,\r\n        cpu_count ,\r\n        hyperthread_ratio ,\r\n        @cpu_online_count AS cpu_online_count ,\r\n        @start_time AS sqlserver_start_time ,\r\n        SERVERPROPERTY('IsClustered') AS is_clustered ,\r\n        SERVERPROPERTY('ProductVersion') AS product_version ,\r\n        SERVERPROPERTY('Edition') AS sql_edition ,\r\n        SERVERPROPERTY('EditionID') AS sql_edition ,\r\n        SERVERPROPERTY('ProductLevel') AS product_level\r\nFROM    sys.dm_os_sys_info\r\n";
			}
			else
			{
				cmdText = "\r\nDECLARE @cpu_online_count INT\r\nSELECT  @cpu_online_count = COUNT(*)\r\nFROM    sys.dm_os_schedulers\r\nWHERE   is_online = 1\r\n        AND status = 'VISIBLE ONLINE'\r\nSELECT  SERVERPROPERTY('ServerName') AS server_name ,\r\n        cpu_count ,\r\n        hyperthread_ratio ,\r\n        @cpu_online_count AS cpu_online_count ,\r\n        sqlserver_start_time ,\r\n        SERVERPROPERTY('IsClustered') AS is_clustered ,\r\n        SERVERPROPERTY('ProductVersion') AS product_version ,\r\n        SERVERPROPERTY('Edition') AS sql_edition ,\r\n        SERVERPROPERTY('EditionID') AS sql_edition ,\r\n        SERVERPROPERTY('ProductLevel') AS product_level\r\nFROM    sys.dm_os_sys_info\r\n";
			}
			return base.FillDataTable(sqlString, cmdText, timeOut);
		}
	}
}
