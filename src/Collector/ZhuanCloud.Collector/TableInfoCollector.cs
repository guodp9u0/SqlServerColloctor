using System;
using System.Data;
using System.Data.OleDb;
using System.Text;

namespace ZhuanCloud.Collector
{
	public class TableInfoCollector : OleDbCollector
	{
		public bool SaveData(string oleDBString, string sqlString, int timeOut, string condition1, string condition2)
		{
			bool result;
			try
			{
				DataTable data = this.GetData(sqlString, timeOut, condition1, condition2);
				if (data.Rows.Count > 0)
				{
					string cmdText = "\r\nINSERT  INTO tblTableSummary\r\n        ( DBName ,\r\n          SchemaName ,\r\n          ObjectName ,\r\n          ObjectType ,\r\n          [Rows] ,\r\n          Reserved ,\r\n          DataSize ,\r\n          IndexSize ,\r\n          UnusedSize ,\r\n          CreateDate ,\r\n          ModifyDate \r\n        )\r\nVALUES  ( @DBName ,\r\n          @SchemaName ,\r\n          @ObjectName ,\r\n          @ObjectType ,\r\n          @Rows ,\r\n          @Reserved ,\r\n          @DataSize ,\r\n          @IndexSize ,\r\n          @UnusedSize ,\r\n          @CreateDate ,\r\n          @ModifyDate\r\n        )\r\n";
					OleDbParameter[] parameters = new OleDbParameter[]
					{
						new OleDbParameter("@DBName", OleDbType.VarChar)
						{
							SourceColumn = "db_name"
						},
						new OleDbParameter("@SchemaName", OleDbType.VarChar)
						{
							SourceColumn = "schema"
						},
						new OleDbParameter("@ObjectName", OleDbType.VarChar)
						{
							SourceColumn = "name"
						},
						new OleDbParameter("@ObjectType", OleDbType.Integer)
						{
							SourceColumn = "object_type"
						},
						new OleDbParameter("@Rows", OleDbType.Double)
						{
							SourceColumn = "rows"
						},
						new OleDbParameter("@Reserved", OleDbType.Double)
						{
							SourceColumn = "reserved_kb"
						},
						new OleDbParameter("@DataSize", OleDbType.Double)
						{
							SourceColumn = "data_kb"
						},
						new OleDbParameter("@IndexSize", OleDbType.Double)
						{
							SourceColumn = "index_size_kb"
						},
						new OleDbParameter("@UnusedSize", OleDbType.Double)
						{
							SourceColumn = "unused_kb"
						},
						new OleDbParameter("@CreateDate", OleDbType.Date)
						{
							SourceColumn = "create_date"
						},
						new OleDbParameter("@ModifyDate", OleDbType.Date)
						{
							SourceColumn = "modify_date"
						}
					};
					result = base.SaveToAccess(oleDBString, data, cmdText, parameters);
				}
				else
				{
					result = true;
				}
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				result = false;
			}
			return result;
		}

		private DataTable GetData(string sqlString, int timeOut, string condition1, string condition2)
		{
			if (!string.IsNullOrEmpty(condition1))
			{
				string[] array = condition1.Split(new string[]
				{
					";"
				}, StringSplitOptions.RemoveEmptyEntries);
				if (array.Length > 0)
				{
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.Append(" AND ");
					int num = 1;
					string[] array2 = array;
					for (int i = 0; i < array2.Length; i++)
					{
						string text = array2[i];
						string[] array3 = text.Split(new string[]
						{
							"."
						}, StringSplitOptions.RemoveEmptyEntries);
						if (array3.Length == 1)
						{
							stringBuilder.AppendFormat("  t.name NOT LIKE '{0}' ", array3[0]);
						}
						else if (array3.Length == 2)
						{
							stringBuilder.AppendFormat(" s.name NOT LIKE '{0}' OR t.name NOT LIKE '{1}' ", array3[0], array3[1]);
						}
						if (num < array.Length)
						{
							stringBuilder.Append(" AND ");
						}
						num++;
					}
					condition1 = stringBuilder.ToString();
				}
			}
			if (!string.IsNullOrEmpty(condition2))
			{
				string[] array4 = condition2.Split(new string[]
				{
					";"
				}, StringSplitOptions.RemoveEmptyEntries);
				if (array4.Length > 0)
				{
					StringBuilder stringBuilder2 = new StringBuilder();
					stringBuilder2.Append(" AND ");
					int num2 = 1;
					string[] array5 = array4;
					for (int j = 0; j < array5.Length; j++)
					{
						string text2 = array5[j];
						string[] array6 = text2.Split(new string[]
						{
							"."
						}, StringSplitOptions.RemoveEmptyEntries);
						if (array6.Length == 1)
						{
							stringBuilder2.AppendFormat("  v.name NOT LIKE '{0}' ", array6[0]);
						}
						else if (array6.Length == 2)
						{
							stringBuilder2.AppendFormat(" s.name NOT LIKE '{0}' OR v.name NOT LIKE '{1}' ", array6[0], array6[1]);
						}
						if (num2 < array4.Length)
						{
							stringBuilder2.Append(" AND ");
						}
						num2++;
					}
					condition2 = stringBuilder2.ToString();
				}
			}
			string cmdText = string.Format("\r\nCREATE TABLE #TableInfo\r\n    (\r\n      [db_name] sysname ,\r\n      [schema] VARCHAR(1000) ,\r\n      [name] VARCHAR(1000) ,\r\n      [object_type] INT,\r\n      [rows] INT ,\r\n      [reserved_kb] FLOAT ,\r\n      [data_kb] FLOAT ,\r\n      [index_size_kb] FLOAT ,\r\n      [unused_kb] FLOAT ,\r\n      [create_date] DATETIME ,\r\n      [modify_date] DATETIME ,\r\n    )\r\nDECLARE cur CURSOR\r\nFOR\r\n    SELECT t.[object_id]\r\n    FROM    sys.tables t\r\n            JOIN sys.schemas AS s ON t.schema_id = s.schema_id \r\n    WHERE   t.[type] = 'U' {0} \r\n    UNION\r\n    SELECT v.[object_id]\r\n    FROM    sys.views v\r\n            JOIN sys.schemas AS s ON v.schema_id = s.schema_id \r\n    WHERE   v.[type] = 'V' {1} \r\nDECLARE @objId INT\r\nOPEN cur\r\nFETCH NEXT FROM cur INTO @objId\r\nWHILE @@FETCH_STATUS = 0 \r\n    BEGIN\r\n        DECLARE @type character(2) -- The object type.\r\n            ,\r\n            @pages BIGINT\t\t\t-- Working variable for size calc.\r\n            ,\r\n            @dbname sysname ,\r\n            @dbsize BIGINT ,\r\n            @logsize BIGINT ,\r\n            @reservedpages BIGINT ,\r\n            @usedpages BIGINT ,\r\n            @rowCount BIGINT\r\n\t\r\n        SELECT  @reservedpages = SUM(reserved_page_count) ,\r\n                @usedpages = SUM(used_page_count) ,\r\n                @pages = SUM(CASE WHEN ( index_id < 2 )\r\n                                  THEN ( in_row_data_page_count\r\n                                         + lob_used_page_count\r\n                                         + row_overflow_used_page_count )\r\n                                  ELSE 0\r\n                             END) ,\r\n                @rowCount = SUM(CASE WHEN ( index_id < 2 ) THEN row_count\r\n                                     ELSE 0\r\n                                END)\r\n        FROM    sys.dm_db_partition_stats\r\n        WHERE   object_id = @objId;\r\n\r\n\t/*\r\n\t** Check if table has XML Indexes or Fulltext Indexes which use internal tables tied to this table\r\n\t*/\r\n        IF ( SELECT COUNT(*)\r\n             FROM   sys.internal_tables\r\n             WHERE  parent_id = @objId\r\n                    AND internal_type IN ( 202, 204, 207, 211, 212, 213, 214,\r\n                                           215, 216, 221, 222, 236 )\r\n           ) > 0 \r\n            BEGIN\r\n\t\t/*\r\n\t\t**  Now calculate the summary data. Row counts in these internal tables don't \r\n\t\t**  contribute towards row count of original table.\r\n\t\t*/\r\n                SELECT  @reservedpages = @reservedpages\r\n                        + SUM(reserved_page_count) ,\r\n                        @usedpages = @usedpages + SUM(used_page_count)\r\n                FROM    sys.dm_db_partition_stats p ,\r\n                        sys.internal_tables it\r\n                WHERE   it.parent_id = @objId\r\n                        AND it.internal_type IN ( 202, 204, 207, 211, 212, 213,\r\n                                                  214, 215, 216, 221, 222, 236 )\r\n                        AND p.object_id = it.object_id;\r\n            END\r\n        IF OBJECTPROPERTY(@objId,'IsTable')=1\r\n        BEGIN\r\n        INSERT  INTO #TableInfo        \r\n                SELECT  DB_NAME(DB_ID()) ,\r\n                        s.name ,\r\n                        t.name ,\r\n                        1,\r\n                        rows = CONVERT (CHAR(20), @rowCount) ,\r\n                        reserved_kb = CONVERT(FLOAT, STR(@reservedpages * 8,\r\n                                                         15, 0)) ,\r\n                        data_kb = CONVERT(FLOAT, STR(@pages * 8, 15, 0)) ,\r\n                        index_size_kb = CONVERT(FLOAT, STR(( CASE\r\n                                                              WHEN @usedpages > @pages\r\n                                                              THEN ( @usedpages - @pages )\r\n                                                              ELSE 0\r\n                                                             END ) * 8, 15, 0)) ,\r\n                        unused_kb = CONVERT(FLOAT, STR(( CASE WHEN @reservedpages > @usedpages\r\n                                                              THEN ( @reservedpages - @usedpages )\r\n                                                              ELSE 0\r\n                                                         END ) * 8, 15, 0)) ,\r\n                        t.create_date ,\r\n                        t.modify_date \r\n                FROM    sys.tables t ,\r\n                        sys.schemas s\r\n                WHERE   t.object_id = @objId\r\n                        AND t.schema_id = s.schema_id  \r\n        END\r\n        ELSE\r\n        BEGIN\r\n        INSERT  INTO #TableInfo        \r\n                SELECT  DB_NAME(DB_ID()) ,\r\n                        s.name ,\r\n                        v.name ,\r\n                        2,\r\n                        rows = CONVERT (CHAR(20), @rowCount) ,\r\n                        reserved_kb = CONVERT(FLOAT, STR(@reservedpages * 8,\r\n                                                         15, 0)) ,\r\n                        data_kb = CONVERT(FLOAT, STR(@pages * 8, 15, 0)) ,\r\n                        index_size_kb = CONVERT(FLOAT, STR(( CASE\r\n                                                              WHEN @usedpages > @pages\r\n                                                              THEN ( @usedpages - @pages )\r\n                                                              ELSE 0\r\n                                                             END ) * 8, 15, 0)) ,\r\n                        unused_kb = CONVERT(FLOAT, STR(( CASE WHEN @reservedpages > @usedpages\r\n                                                              THEN ( @reservedpages - @usedpages )\r\n                                                              ELSE 0\r\n                                                         END ) * 8, 15, 0)) ,\r\n                        v.create_date ,\r\n                        v.modify_date \r\n                FROM    sys.views v ,\r\n                        sys.schemas s\r\n                WHERE   v.object_id = @objId\r\n                        AND v.schema_id = s.schema_id  \r\n        END                        \r\n        FETCH NEXT FROM cur INTO @objId\r\n    END\r\nCLOSE cur\r\nDEALLOCATE cur\r\nSELECT TOP 50 *\r\nFROM    #TableInfo\r\nORDER BY reserved_kb DESC\r\nDROP TABLE  #TableInfo\r\n", condition1, condition2);
			return base.FillDataTable(sqlString, cmdText, timeOut);
		}
	}
}
