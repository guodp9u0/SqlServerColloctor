using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: Debuggable(DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints)]
[assembly: AssemblyCompany("北京格瑞趋势科技有限公司")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCopyright("Copyright © 北京格瑞趋势科技有限公司  2016")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyFileVersion("1.0.0")]
[assembly: AssemblyProduct("ZhuanCloud.Collector")]
[assembly: AssemblyTitle("ZhuanCloud.Collector")]
[assembly: AssemblyTrademark("")]
[assembly: CompilationRelaxations(8)]
[assembly: RuntimeCompatibility(WrapNonExceptionThrows = true)]
[assembly: ComVisible(false)]
[assembly: Guid("40ba46f1-23f9-4336-a5c7-4bbe7b40f153")]
