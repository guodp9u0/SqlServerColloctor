using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ZhuanCloud.Collector.UI.Properties;

namespace ZhuanCloud.Collector.UI
{
	public class FrmAgree : Form
	{
		private IContainer components;

		private WebBrowser webBrowser1;

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(FrmAgree));
			this.webBrowser1 = new WebBrowser();
			base.SuspendLayout();
			this.webBrowser1.Dock = DockStyle.Fill;
			this.webBrowser1.Location = new Point(0, 0);
			this.webBrowser1.MinimumSize = new Size(20, 20);
			this.webBrowser1.Name = "webBrowser1";
			this.webBrowser1.Size = new Size(662, 475);
			this.webBrowser1.TabIndex = 0;
			base.AutoScaleDimensions = new SizeF(6f, 12f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(662, 475);
			base.Controls.Add(this.webBrowser1);
			base.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			base.Name = "FrmAgree";
			base.StartPosition = FormStartPosition.CenterScreen;
			this.Text = "软件许可协议";
			base.WindowState = FormWindowState.Maximized;
			base.Load += new EventHandler(this.FrmAgree_Load);
			base.ResumeLayout(false);
		}

		public FrmAgree()
		{
			this.InitializeComponent();
		}

		private void FrmAgree_Load(object sender, EventArgs e)
		{
			this.webBrowser1.DocumentText = Resources.HTMLAgree;
		}
	}
}
