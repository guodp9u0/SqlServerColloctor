using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ZhuanCloud.Collector.UI
{
	public class ucDuration : UserControl
	{
		private IContainer components;

		private Label lblDuration;

		private Label label1;

		private Label lblCollectInfo;

		private Timer PerfTimer;

		private PictureBox pciLoading;

		private Timer SqlPlanTimer;

		private Timer TempdbTimer;

		private Timer ErrorLogTimer;

		private Timer WaitTimer;

		private Timer secTimer;

		private Timer UnusedSessiontimer;

		private Timer SqlTextTimer;

		private Timer DBFileTimer;

		private int planCount = 1;

		private int errCount = 1;

		private int tempdbCount = 1;

		private int perfCount = 1;

		private int waitCount = 1;

		private int unusedSessionCount = 1;

		private int dbFileCount = 1;

		private int secCount;

		public event CollectPerfHandler CollectPerf;

		public event CollectWaitHandler CollectWait;

		public event CollectUnusedSessionHandler CollectUnusedSession;

		public event CollectSqlPlanHandler CollectSqlPlan;

		public event CollectErrorLogHandler CollectErrorLog;

		public event CollectTempdbHandler CollectTempdb;

		public event CollectSqlTextHandler CollectSqlText;

		public event CollectDBFileHandler CollectDBFile;

		public int PerfElapsed
		{
			get;
			set;
		}

		public int WaitElapsed
		{
			get;
			set;
		}

		public int UnusedSessionElapsed
		{
			get;
			set;
		}

		public int SqlPlanElapsed
		{
			get;
			set;
		}

		public int ErrorLogElapsed
		{
			get;
			set;
		}

		public int TempdbElapsed
		{
			get;
			set;
		}

		public int DBFileElapsed
		{
			get;
			set;
		}

		public bool HasSqlText
		{
			get;
			set;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new Container();
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(ucDuration));
			this.lblDuration = new Label();
			this.label1 = new Label();
			this.lblCollectInfo = new Label();
			this.PerfTimer = new Timer(this.components);
			this.pciLoading = new PictureBox();
			this.SqlPlanTimer = new Timer(this.components);
			this.TempdbTimer = new Timer(this.components);
			this.ErrorLogTimer = new Timer(this.components);
			this.WaitTimer = new Timer(this.components);
			this.secTimer = new Timer(this.components);
			this.UnusedSessiontimer = new Timer(this.components);
			this.SqlTextTimer = new Timer(this.components);
			this.DBFileTimer = new Timer(this.components);
			((ISupportInitialize)this.pciLoading).BeginInit();
			base.SuspendLayout();
			this.lblDuration.AutoSize = true;
			this.lblDuration.Font = new Font("宋体", 10f, FontStyle.Regular, GraphicsUnit.Point, 134);
			this.lblDuration.ForeColor = Color.FromArgb(36, 180, 253);
			this.lblDuration.Location = new Point(385, 92);
			this.lblDuration.Name = "lblDuration";
			this.lblDuration.Size = new Size(119, 14);
			this.lblDuration.TabIndex = 7;
			this.lblDuration.Text = "00天00时00分00秒";
			this.label1.AutoSize = true;
			this.label1.Location = new Point(313, 93);
			this.label1.Name = "label1";
			this.label1.Size = new Size(65, 12);
			this.label1.TabIndex = 6;
			this.label1.Text = "收集时间：";
			this.lblCollectInfo.AutoSize = true;
			this.lblCollectInfo.Font = new Font("宋体", 12f, FontStyle.Regular, GraphicsUnit.Point, 134);
			this.lblCollectInfo.Location = new Point(312, 62);
			this.lblCollectInfo.Name = "lblCollectInfo";
			this.lblCollectInfo.Size = new Size(72, 16);
			this.lblCollectInfo.TabIndex = 5;
			this.lblCollectInfo.Text = "正在收集";
			this.PerfTimer.Interval = 1000;
			this.PerfTimer.Tag = "1秒一次";
			this.PerfTimer.Tick += new EventHandler(this.PerfTimer_Tick);
			this.pciLoading.Image = (Image)componentResourceManager.GetObject("pciLoading.Image");
			this.pciLoading.Location = new Point(132, 8);
			this.pciLoading.Name = "pciLoading";
			this.pciLoading.Size = new Size(176, 220);
			this.pciLoading.SizeMode = PictureBoxSizeMode.AutoSize;
			this.pciLoading.TabIndex = 4;
			this.pciLoading.TabStop = false;
			this.SqlPlanTimer.Interval = 3600000;
			this.SqlPlanTimer.Tag = "1小时一次";
			this.SqlPlanTimer.Tick += new EventHandler(this.SqlPlanTimer_Tick);
			this.TempdbTimer.Interval = 60000;
			this.TempdbTimer.Tag = "1分钟一次";
			this.TempdbTimer.Tick += new EventHandler(this.TempdbTimer_Tick);
			this.ErrorLogTimer.Interval = 3600000;
			this.ErrorLogTimer.Tag = "1小时一次";
			this.ErrorLogTimer.Tick += new EventHandler(this.ErrorLogTimer_Tick);
			this.WaitTimer.Interval = 1000;
			this.WaitTimer.Tag = "1秒一次";
			this.WaitTimer.Tick += new EventHandler(this.WaitTimer_Tick);
			this.secTimer.Interval = 1000;
			this.secTimer.Tag = "1秒一次";
			this.secTimer.Tick += new EventHandler(this.secTimer_Tick);
			this.UnusedSessiontimer.Interval = 60000;
			this.UnusedSessiontimer.Tag = "1分钟一次";
			this.UnusedSessiontimer.Tick += new EventHandler(this.UnusedSessiontimer_Tick);
			this.SqlTextTimer.Interval = 300000;
			this.SqlTextTimer.Tag = "检查连接5分钟一次";
			this.SqlTextTimer.Tick += new EventHandler(this.SqlTextTimer_Tick);
			this.DBFileTimer.Interval = 60000;
			this.DBFileTimer.Tag = "1分钟一次";
			this.DBFileTimer.Tick += new EventHandler(this.DBFileTimer_Tick);
			base.AutoScaleMode = AutoScaleMode.None;
			this.BackColor = Color.White;
			base.Controls.Add(this.lblDuration);
			base.Controls.Add(this.label1);
			base.Controls.Add(this.lblCollectInfo);
			base.Controls.Add(this.pciLoading);
			base.Name = "ucDuration";
			base.Size = new Size(620, 238);
			base.Load += new EventHandler(this.ucDuration_Load);
			((ISupportInitialize)this.pciLoading).EndInit();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		public ucDuration()
		{
			this.InitializeComponent();
		}

		private void ucDuration_Load(object sender, EventArgs e)
		{
		}

		public void StartTiming()
		{
			this.secTimer.Enabled = true;
			this.secTimer.Start();
		}

		public void StartCollect()
		{
			if (this.SqlPlanElapsed > 0 && this.CollectSqlPlan != null)
			{
				this.SqlPlanTimer.Enabled = true;
				this.SqlPlanTimer.Start();
			}
			if (this.ErrorLogElapsed > 0 && this.CollectErrorLog != null)
			{
				this.ErrorLogTimer.Enabled = true;
				this.ErrorLogTimer.Start();
			}
			if (this.TempdbElapsed > 0 && this.CollectTempdb != null)
			{
				this.TempdbTimer.Enabled = true;
				this.TempdbTimer.Start();
			}
			if (this.PerfElapsed > 0 && this.CollectPerf != null)
			{
				this.PerfTimer.Enabled = true;
				this.PerfTimer.Start();
			}
			if (this.WaitElapsed > 0 && this.CollectWait != null)
			{
				this.WaitTimer.Enabled = true;
				this.WaitTimer.Start();
			}
			if (this.UnusedSessionElapsed > 0 && this.CollectUnusedSession != null)
			{
				this.UnusedSessiontimer.Enabled = true;
				this.UnusedSessiontimer.Start();
			}
			if (this.HasSqlText && this.CollectSqlText != null)
			{
				this.SqlTextTimer.Enabled = true;
				this.SqlTextTimer.Start();
			}
			if (this.DBFileElapsed > 0 && this.CollectDBFile != null)
			{
				this.DBFileTimer.Enabled = true;
				this.DBFileTimer.Start();
			}
		}

		public void Stop()
		{
			this.SqlPlanTimer.Stop();
			this.ErrorLogTimer.Stop();
			this.TempdbTimer.Stop();
			this.PerfTimer.Stop();
			this.WaitTimer.Stop();
			this.UnusedSessiontimer.Stop();
			this.SqlTextTimer.Stop();
			this.DBFileTimer.Stop();
			this.secTimer.Stop();
			this.planCount = 0;
			this.errCount = 0;
			this.tempdbCount = 0;
			this.perfCount = 0;
			this.waitCount = 0;
			this.unusedSessionCount = 0;
			this.dbFileCount = 0;
			this.secCount = 0;
		}

		private void SqlPlanTimer_Tick(object sender, EventArgs e)
		{
			if (this.CollectSqlPlan != null && this.planCount % this.SqlPlanElapsed == 0)
			{
				this.CollectSqlPlan(DateTime.Now);
			}
			this.planCount++;
		}

		private void ErrorLogTimer_Tick(object sender, EventArgs e)
		{
			if (this.CollectErrorLog != null && this.errCount % this.ErrorLogElapsed == 0)
			{
				this.CollectErrorLog(DateTime.Now);
			}
			this.errCount++;
		}

		private void TempdbTimer_Tick(object sender, EventArgs e)
		{
			if (this.CollectTempdb != null && this.tempdbCount % this.TempdbElapsed == 0)
			{
				this.CollectTempdb(DateTime.Now);
			}
			this.tempdbCount++;
		}

		private void PerfTimer_Tick(object sender, EventArgs e)
		{
			if (this.CollectPerf != null && this.perfCount % this.PerfElapsed == 0)
			{
				this.CollectPerf(DateTime.Now);
			}
			this.perfCount++;
		}

		private void WaitTimer_Tick(object sender, EventArgs e)
		{
			if (this.CollectWait != null && this.waitCount % this.WaitElapsed == 0)
			{
				this.CollectWait(DateTime.Now);
			}
			this.waitCount++;
		}

		private void UnusedSessiontimer_Tick(object sender, EventArgs e)
		{
			if (this.CollectUnusedSession != null && this.unusedSessionCount % this.UnusedSessionElapsed == 0)
			{
				this.CollectUnusedSession(DateTime.Now);
			}
			this.unusedSessionCount++;
		}

		private void DBFileTimer_Tick(object sender, EventArgs e)
		{
			if (this.CollectDBFile != null && this.dbFileCount % this.DBFileElapsed == 0)
			{
				this.CollectDBFile(DateTime.Now);
			}
			this.dbFileCount++;
		}

		private void secTimer_Tick(object sender, EventArgs e)
		{
			this.lblDuration.Text = this.GetTimeStr(this.secCount);
			this.secCount++;
		}

		private void SqlTextTimer_Tick(object sender, EventArgs e)
		{
			if (this.CollectSqlText != null)
			{
				this.CollectSqlText(DateTime.Now);
			}
		}

		private string GetTimeStr(int secondCount)
		{
			string empty = string.Empty;
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			int num4;
			if (secondCount < 60)
			{
				num4 = secondCount;
			}
			else
			{
				num = secondCount / 60 / 60 / 24;
				num2 = secondCount / 60 / 60 % 24;
				num3 = secondCount / 60 % 60;
				num4 = secondCount % 60;
			}
			return string.Format("{0}天{1}时{2}分{3}秒", new object[]
			{
				num.ToString("D2"),
				num2.ToString("D2"),
				num3.ToString("D2"),
				num4.ToString("D2")
			});
		}
	}
}
