using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ZhuanCloud.Collector.UI
{
	public class FrmPerfTip : Form
	{
		private IContainer components;

		private Label label1;

		private TextBox txtPerfList;

		private Button btnContinue;

		private Button btnCancel;

		public FrmPerfTip(string message)
		{
			this.InitializeComponent();
			this.txtPerfList.Text = message;
		}

		private void btnContinue_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.OK;
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			base.DialogResult = DialogResult.Cancel;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(FrmPerfTip));
			this.label1 = new Label();
			this.txtPerfList = new TextBox();
			this.btnContinue = new Button();
			this.btnCancel = new Button();
			base.SuspendLayout();
			this.label1.AutoSize = true;
			this.label1.Location = new Point(10, 13);
			this.label1.Name = "label1";
			this.label1.Size = new Size(125, 12);
			this.label1.TabIndex = 0;
			this.label1.Text = "以下计数器无法收集：";
			this.txtPerfList.BackColor = SystemColors.ButtonHighlight;
			this.txtPerfList.Location = new Point(12, 34);
			this.txtPerfList.Multiline = true;
			this.txtPerfList.Name = "txtPerfList";
			this.txtPerfList.ReadOnly = true;
			this.txtPerfList.ScrollBars = ScrollBars.Vertical;
			this.txtPerfList.Size = new Size(409, 162);
			this.txtPerfList.TabIndex = 1;
			this.btnContinue.Location = new Point(266, 210);
			this.btnContinue.Name = "btnContinue";
			this.btnContinue.Size = new Size(75, 23);
			this.btnContinue.TabIndex = 2;
			this.btnContinue.Text = "继续收集";
			this.btnContinue.UseVisualStyleBackColor = true;
			this.btnContinue.Click += new EventHandler(this.btnContinue_Click);
			this.btnCancel.Location = new Point(348, 210);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new Size(75, 23);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "取消";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
			base.AutoScaleDimensions = new SizeF(6f, 12f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.AutoSizeMode = AutoSizeMode.GrowAndShrink;
			base.ClientSize = new Size(435, 241);
			base.Controls.Add(this.btnCancel);
			base.Controls.Add(this.btnContinue);
			base.Controls.Add(this.txtPerfList);
			base.Controls.Add(this.label1);
			base.FormBorderStyle = FormBorderStyle.FixedSingle;
			base.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "FrmPerfTip";
			base.StartPosition = FormStartPosition.CenterScreen;
			this.Text = "ZhuanCloud";
			base.ResumeLayout(false);
			base.PerformLayout();
		}
	}
}
