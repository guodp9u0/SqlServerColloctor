using System;
using System.Diagnostics;
using System.Reflection;
using System.Security.Principal;
using System.Threading;
using System.Windows.Forms;

namespace ZhuanCloud.Collector.UI
{
	internal static class Program
	{
		[STAThread]
		private static void Main()
		{
			bool flag = true;
			Mutex mutex = new Mutex(true, "ZhuanCloud", out flag);
			if (!flag)
			{
				MessageBox.Show("SQL专家云已经在运行了，可在任务栏查看", "SQL专家云");
				return;
			}
			WindowsIdentity current = WindowsIdentity.GetCurrent();
			WindowsPrincipal windowsPrincipal = new WindowsPrincipal(current);
			if (windowsPrincipal.IsInRole(WindowsBuiltInRole.Administrator))
			{
				Application.EnableVisualStyles();
				Application.SetCompatibleTextRenderingDefault(false);
				Application.Run(new FrmMain());
				mutex.ReleaseMutex();
				return;
			}
			MessageBox.Show("请以管理员身份运行程序", "SQL专家云");
		}

		public static Process RunningInstance()
		{
			Process currentProcess = Process.GetCurrentProcess();
			Process[] processesByName = Process.GetProcessesByName(currentProcess.ProcessName);
			Process[] array = processesByName;
			for (int i = 0; i < array.Length; i++)
			{
				Process process = array[i];
				if (process.Id != currentProcess.Id && Assembly.GetExecutingAssembly().Location.Replace("/", "//") == currentProcess.MainModule.FileName)
				{
					return process;
				}
			}
			return null;
		}
	}
}
