using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using ZhuanCloud.Collector.UI.Properties;

namespace ZhuanCloud.Collector.UI
{
	internal class FrmMain : Form
	{
		private CollectConfig selectConfig;

		private string sqlConString = string.Empty;

		private string oleDBString = string.Empty;

		private bool generalCompleted;

		private int planRowID = -1;

		private string sqlTextFileName = "SQLText";

		private string lastSqlTraceName = string.Empty;

		private int sqlTextTraceId = -1;

		private string moebiusTraceName = "Moebius";

		private string lastMoebiusTraceName = string.Empty;

		private int moebiusTraceId = -1;

		internal readonly string flagFileName = "Flag_Temp";

		private SQLServerVersion sqlVersion = SQLServerVersion.SQLServer2005;

		private DateTime lastExecTime = DateTime.Parse("1900/01/01 00:00:00");

		private DateTime lastLogTime = DateTime.Parse("1900/01/01 00:00:00");

		private CollectState collectState;

		private List<SQLService> sqlServiceList;

		private SQLService sqlServiceInfo;

		private readonly int fileSize = 1750;

		private IContainer components;

		private Panel pnlTitle;

		private Panel pnlBottom;

		private Panel pnlContent;

		private PictureBox btnCollector;

		private ImageList imageList1;

		private CheckBox cbAgree;

		private LinkLabel lblAgree;

		private Panel pnlDBCollector;

		private Panel pnlAuthent;

		private TextBox txtPassWord;

		private TextBox txtUserName;

		private Label label4;

		private Label label3;

		private Label label2;

		private ComboBox cbInstanceName;

		private ComboBox cbAuthent;

		private Label label1;

		private Label label5;

		private ucComboBox cbDBList;

		private Label label7;

		private TextBox txtFilePath;

		private BackgroundWorker collectionAysc;

		private FileSystemWatcher TempWatcher;

		private PictureBox btnSavePath;

		private PictureBox btnEdit;

		private ToolTip toolTip1;

		private FileSystemWatcher MoebiusWatcher;

		public CollectConfig SelectConfig
		{
			get
			{
				return this.selectConfig;
			}
		}

		public FrmMain()
		{
			this.InitializeComponent();
		}

		private void FrmCollector_Load(object sender, EventArgs e)
		{
			this.InitializeConfig();
		}

		private void InitializeConfig()
		{
			this.lblAgree.Location = new Point(this.cbAgree.Location.X + this.cbAgree.Width + 6, this.cbAgree.Location.Y + 2);
			CollectManager collectManager = new CollectManager();
			this.sqlServiceList = collectManager.GetSQLServiceList();
			if (this.sqlServiceList.Count > 0)
			{
				foreach (SQLService current in this.sqlServiceList)
				{
					if (!this.cbInstanceName.Items.Contains(current.InstanceName))
					{
						this.cbInstanceName.Items.Add(current.InstanceName);
					}
				}
				this.cbInstanceName.SelectedIndex = 0;
			}
			this.cbAuthent.SelectedIndex = 0;
			ConfigManager configManager = new ConfigManager();
			this.selectConfig = configManager.LoadConfig();
			if (this.selectConfig == null)
			{
				configManager.ClearConfig();
				this.selectConfig = configManager.GetDefaultConfig();
			}
			else if (this.selectConfig.SZCVersion != Assembly.GetEntryAssembly().GetName().Version)
			{
				configManager.ClearConfig();
				this.selectConfig = configManager.GetDefaultConfig();
			}
			this.txtFilePath.Text = Application.StartupPath;
			this.TempWatcher.EnableRaisingEvents = false;
			this.MoebiusWatcher.EnableRaisingEvents = false;
		}

		private void cbInstanceName_TextChanged(object sender, EventArgs e)
		{
			this.cbDBList.Clear();
			if (this.selectConfig != null)
			{
				this.selectConfig.PerfList = null;
			}
		}

		private void cbInstanceName_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.cbDBList.Clear();
		}

		private string GetInstanceName()
		{
			string text = this.cbInstanceName.Text;
			if (!string.IsNullOrEmpty(text))
			{
				string empty = string.Empty;
				if (text.Contains("\\"))
				{
					text = "MSSQL$" + text.Substring(text.IndexOf("\\") + 1).ToUpper();
				}
				else if (Util.TryGetInstance(this.cbInstanceName.Text.Trim(), this.cbAuthent.SelectedIndex == 0, this.txtUserName.Text.Trim(), this.txtPassWord.Text.Trim(), out empty))
				{
					if (empty.Contains("\\"))
					{
						text = "MSSQL$" + empty.Substring(empty.IndexOf("\\") + 1).ToUpper();
					}
					else
					{
						text = "SQLSERVER";
					}
				}
				else
				{
					text = "SQLSERVER";
				}
				return text;
			}
			return "SQLSERVER";
		}

		private void cbAuthent_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.cbDBList.Clear();
			this.pnlAuthent.Enabled = (this.cbAuthent.SelectedIndex == 1);
		}

		private void cbDBList_DBChecked(object sender, EventArgs e)
		{
			string empty = string.Empty;
			CollectManager collectManager = new CollectManager();
			this.sqlConString = collectManager.GetConnStr(this.cbInstanceName.Text.Trim(), this.cbAuthent.SelectedIndex == 0, this.txtUserName.Text.Trim(), this.txtPassWord.Text.Trim(), out empty);
			if (string.IsNullOrEmpty(this.sqlConString))
			{
				MessageBox.Show(empty, "SQL专家云");
				return;
			}
			if (!this.cbDBList.HasItemChecked())
			{
				List<string> dBList = collectManager.GetDBList(this.sqlConString);
				this.cbDBList.Bind(dBList);
				this.selectConfig.DBList = this.cbDBList.DBList;
			}
		}

		private void btnEdit_Click(object sender, EventArgs e)
		{
			if (this.selectConfig != null)
			{
				string instanceName = this.GetInstanceName();
				this.selectConfig.DBInstanceName = instanceName;
				if (this.cbDBList.HasItemChecked())
				{
					if (this.selectConfig.DBFilterList != null && this.selectConfig.DBFilterList.Count > 0)
					{
						this.selectConfig.DBFilterList = this.GetDBFilterList(instanceName, this.selectConfig.DBFilterList);
					}
				}
				else
				{
					this.selectConfig.DBFilterList = null;
				}
				FrmEditConfig frmEditConfig = new FrmEditConfig(this.selectConfig);
				if (frmEditConfig.ShowDialog() == DialogResult.OK && frmEditConfig.Config != null)
				{
					this.selectConfig = frmEditConfig.Config;
				}
			}
		}

		private void btnSavePath_Click(object sender, EventArgs e)
		{
			FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
			folderBrowserDialog.Description = "保存文件路径";
			folderBrowserDialog.ShowNewFolderButton = true;
			string text = this.txtFilePath.Text.Trim();
			if (!string.IsNullOrEmpty(text))
			{
				folderBrowserDialog.SelectedPath = text;
			}
			if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
			{
				this.txtFilePath.Text = folderBrowserDialog.SelectedPath;
			}
		}

		private void btnCollector_Click(object sender, EventArgs e)
		{
			if (Util.startFlag)
			{
				if (!this.generalCompleted)
				{
					if (MessageBox.Show("常规的数据没有收集完，停止收集会导致收集文件不可用，是否要停止收集？", "SQL专家云", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						this.StopCollection();
						this.CheckCollectState();
						return;
					}
				}
				else
				{
					this.StopCollection();
					this.CheckCollectState();
				}
				return;
			}
			if (!this.collectionAysc.IsBusy)
			{
				this.StartCollection();
				return;
			}
			MessageBox.Show("操作过于频繁，请稍后再试。", "SQL专家云");
		}

		private void StartCollection()
		{
			CollectManager collectManager = new CollectManager();
			string empty = string.Empty;
			Util.errDic = new Dictionary<string, int>();
			Util.collectDir = string.Empty;
			Util.oleDbString = string.Empty;
			this.sqlServiceInfo = null;
			this.lastExecTime = DateTime.Parse("1900/01/01 00:00:00");
			this.lastLogTime = DateTime.Parse("1900/01/01 00:00:00");
			string text = this.txtFilePath.Text.Trim();
			if (!collectManager.IsPassed(this.cbInstanceName.Text.Trim(), this.cbAuthent.SelectedIndex == 0, this.txtUserName.Text.Trim(), this.txtPassWord.Text.Trim(), this.cbDBList.DBList, text, out empty))
			{
				MessageBox.Show(empty, "SQL专家云", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}
			string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
			if (text.StartsWith(folderPath))
			{
				MessageBox.Show("请不要在桌面上运行程序", "SQL专家云", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}
			if (Util.IsRootPath(text) && !text.Contains("\\"))
			{
				text += "\\";
			}
			this.collectState = new CollectState();
			this.sqlConString = empty;
			this.selectConfig.SqlConString = this.sqlConString;
			if (this.selectConfig.DBFilterList != null && this.selectConfig.DBFilterList.Count > 0)
			{
				List<DBFilter> dBFilterList = this.GetDBFilterList(this.selectConfig.DBInstanceName, this.selectConfig.DBFilterList);
				this.selectConfig.DBFilterList = dBFilterList;
			}
			if (this.selectConfig.HasDBDefine)
			{
				string empty2 = string.Empty;
				if (Util.IsBigTableOrView(this.sqlConString, this.selectConfig.DBList, this.selectConfig.DBFilterList, out empty2) && MessageBox.Show(empty2, "SQL专家云", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.No)
				{
					return;
				}
			}
			if (this.selectConfig.HasDBDefine && this.selectConfig.DBItemType.HasTable && this.selectConfig.HasCheckTable)
			{
				double num = Util.CheckTableTimes(this.sqlConString, this.selectConfig.DBList, this.selectConfig.DBFilterList);
				double num2 = num / 350000.0;
				long num3 = Convert.ToInt64(num2 / 60.0);
				int num4 = Convert.ToInt32(num2 % 60.0);
				if (num3 >= 5L && MessageBox.Show(string.Format("表一致性检查至少需要 {0} 分 {1} 秒， 是否继续？", num3, num4), "SQL专家云", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.No)
				{
					return;
				}
			}
			if (this.selectConfig.HasPerfCounter)
			{
				string instanceName = this.GetInstanceName();
				if (this.selectConfig.PerfList == null || this.selectConfig.DBInstanceName != instanceName)
				{
					this.selectConfig.DBInstanceName = instanceName;
					this.btnCollector.Enabled = false;
					this.pnlDBCollector.Enabled = false;
					string empty3 = string.Empty;
					List<Perf> list = ConfigManager.LoadPerfCounter(instanceName, out empty3);
					this.pnlDBCollector.Enabled = true;
					this.btnCollector.Enabled = true;
					if (empty3.Length > 0)
					{
						FrmPerfTip frmPerfTip = new FrmPerfTip(empty3);
						if (frmPerfTip.ShowDialog() != DialogResult.OK)
						{
							return;
						}
					}
					else if (list != null)
					{
						List<Perf> list2 = new List<Perf>();
						int num5 = 1;
						foreach (Perf current in list)
						{
							current.ID = num5;
							if (current.Checked)
							{
								list2.Add(current);
							}
							num5++;
						}
						this.selectConfig.PerfList = list2;
						this.selectConfig.AllPerfList = list;
					}
				}
				else if (this.selectConfig.PerfList != null && this.selectConfig.PerfList.Count > 0)
				{
					int num6 = 1;
					foreach (Perf current2 in this.selectConfig.PerfList)
					{
						current2.ID = num6;
						num6++;
					}
				}
			}
			string text2 = "SZC" + DateTime.Now.ToString("yyyyMMdd_HHmmss");
			string text3 = Path.Combine(text, text2);
			if (!Directory.Exists(text3))
			{
				try
				{
					Directory.CreateDirectory(text3);
					this.sqlServiceInfo = this.GetSQLService(this.cbInstanceName.Text.Trim());
					if (this.sqlServiceInfo != null && this.sqlServiceInfo.IsCurrentAccount)
					{
						if (this.IsDenyWrite(this.sqlServiceInfo.AccountName, text3))
						{
							MessageBox.Show("存储路径没有可写权限。", "SQL专家云");
							return;
						}
						if (!string.IsNullOrEmpty(this.sqlServiceInfo.AccountName))
						{
							this.GrantDirAuthority(text3, this.sqlServiceInfo.AccountName);
						}
					}
					goto IL_4F2;
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message, "SQL专家云");
					return;
				}
				goto IL_4E1;
				IL_4F2:
				string text4 = Path.Combine(text3, text2 + ".szc");
				if (!this.CreateCollectFile(text4))
				{
					MessageBox.Show("收集文件创建失败", "SQL专家云", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					return;
				}
				if (this.selectConfig.HasSqlText)
				{
					string text5 = Path.Combine(text3, this.sqlTextFileName + "_Temp");
					if (!Directory.Exists(text5))
					{
						try
						{
							Directory.CreateDirectory(text5);
						}
						catch (Exception ex2)
						{
							MessageBox.Show(ex2.Message, "SQL专家云");
							return;
						}
					}
					this.selectConfig.TempDirectory = text5;
				}
				if (this.selectConfig.HasMoebius)
				{
					string text6 = Path.Combine(text3, this.moebiusTraceName + "_Temp");
					if (!Directory.Exists(text6))
					{
						try
						{
							Directory.CreateDirectory(text6);
						}
						catch (Exception ex3)
						{
							MessageBox.Show(ex3.Message, "SQL专家云");
							return;
						}
					}
					this.selectConfig.MoebiusDirectory = text6;
				}
				Util.collectDir = text3;
				this.oleDBString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + text4;
				this.selectConfig.OleDbString = this.oleDBString;
				Util.oleDbString = this.oleDBString;
				Util.filePath = text4;
				Util.waitHashList.Clear();
				Util.sqlHashList.Clear();
				Util.moebiusHashList.Clear();
				Util.unusedKeyList.Clear();
				Util.unusedHashList.Clear();
				Util.planExecDic.Clear();
				Util.startFlag = true;
				this.generalCompleted = false;
				this.cbAgree.Visible = false;
				this.lblAgree.Visible = false;
				this.btnEdit.Visible = false;
				this.lastSqlTraceName = string.Empty;
				this.lastMoebiusTraceName = string.Empty;
				if (this.pnlContent.Controls.Contains(this.pnlDBCollector))
				{
					this.pnlContent.Controls.Remove(this.pnlDBCollector);
				}
				ucDuration ucDuration = new ucDuration();
				this.pnlContent.Controls.Clear();
				this.pnlContent.Controls.Add(ucDuration);
				this.btnCollector.Enabled = true;
				ucDuration.StartTiming();
				try
				{
					this.collectionAysc.RunWorkerAsync();
				}
				catch (Exception ex4)
				{
					ErrorLog.Write(ex4);
					this.StopCollection();
					MessageBox.Show(ex4.Message, "SQL专家云");
				}
				return;
			}
			IL_4E1:
			MessageBox.Show("收集文件文件夹已存在", "SQL专家云");
		}

		private SQLService GetSQLService(string instanceName)
		{
			if (this.sqlServiceList != null && this.sqlServiceList.Count > 0)
			{
				foreach (SQLService current in this.sqlServiceList)
				{
					if (current.InstanceName == instanceName)
					{
						return current;
					}
				}
			}
			return null;
		}

		private bool IsDenyWrite(string userIdentity, string dir)
		{
			if (Directory.Exists(dir))
			{
				new StringBuilder();
				DirectorySecurity accessControl = Directory.GetAccessControl(dir, AccessControlSections.All);
				foreach (FileSystemAccessRule fileSystemAccessRule in accessControl.GetAccessRules(true, true, typeof(NTAccount)))
				{
					if (fileSystemAccessRule.IdentityReference.Value == userIdentity && fileSystemAccessRule.AccessControlType == AccessControlType.Deny && fileSystemAccessRule.FileSystemRights.ToString() == "Write")
					{
						return true;
					}
				}
				return false;
			}
			return false;
		}

		private void GrantDirAuthority(string directory, string userIdentity)
		{
			if (Directory.Exists(directory) && !string.IsNullOrEmpty(userIdentity))
			{
				try
				{
					DirectoryInfo directoryInfo = new DirectoryInfo(directory);
					DirectorySecurity accessControl = directoryInfo.GetAccessControl();
					accessControl.AddAccessRule(new FileSystemAccessRule(userIdentity, FileSystemRights.FullControl, InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow));
					directoryInfo.SetAccessControl(accessControl);
				}
				catch
				{
				}
			}
		}

		public bool CreateCollectFile(string filePath)
		{
			try
			{
				File.WriteAllBytes(filePath, Resources.ZhuanCloud);
			}
			catch (Exception ex)
			{
				ErrorLog.Write(ex);
				return false;
			}
			return true;
		}

		private void StopCollection()
		{
			Util.startFlag = false;
			base.Invoke(new EventHandler(delegate
			{
				this.btnCollector.Enabled = false;
				if (this.pnlContent.Controls.Count > 0)
				{
					ucDuration ucDuration = this.pnlContent.Controls[0] as ucDuration;
					if (ucDuration != null)
					{
						ucDuration.Stop();
					}
				}
				this.collectionAysc.CancelAsync();
				this.collectionAysc.Dispose();
			}));
			if (this.selectConfig.HasSqlText && this.sqlTextTraceId != -1)
			{
				try
				{
					SqlTextCollector sqlTextCollector = new SqlTextCollector();
					string lastTracePath = sqlTextCollector.GetLastTracePath(this.sqlTextTraceId, this.selectConfig.SqlConString, this.selectConfig.TimeOut);
					sqlTextCollector.Stop(this.sqlTextTraceId, this.sqlConString, this.selectConfig.TimeOut);
					if (!string.IsNullOrEmpty(lastTracePath))
					{
						bool flag = sqlTextCollector.SaveData(lastTracePath, this.selectConfig.OleDbString, this.sqlConString, this.selectConfig.TimeOut);
						this.collectState.SqlTextSum++;
						if (!flag)
						{
							this.collectState.SqlTextFaild++;
							flag = sqlTextCollector.SaveData(lastTracePath, this.selectConfig.OleDbString, this.sqlConString, this.selectConfig.TimeOut);
							this.collectState.SqlTextSum++;
							if (!flag)
							{
								this.collectState.SqlTextFaild++;
							}
						}
						flag = ItemCountCollector.SaveItemCount(this.selectConfig.OleDbString, this.selectConfig.TimeOut, CollectItem.SqlText, this.collectState.SqlTextSum, this.collectState.SqlTextFaild);
					}
				}
				catch (Exception ex)
				{
					ErrorLog.Write(ex);
				}
			}
			if (this.selectConfig.HasMoebius && this.moebiusTraceId != -1)
			{
				try
				{
					MoebiusTrace moebiusTrace = new MoebiusTrace();
					string lastTracePath2 = moebiusTrace.GetLastTracePath(this.moebiusTraceId, this.selectConfig.SqlConString, this.selectConfig.TimeOut);
					moebiusTrace.Stop(this.moebiusTraceId, this.sqlConString, this.selectConfig.TimeOut);
					if (!string.IsNullOrEmpty(lastTracePath2))
					{
						bool flag2 = moebiusTrace.SaveData(lastTracePath2, this.selectConfig.OleDbString, this.sqlConString, this.selectConfig.TimeOut);
						this.collectState.MoebiusTraceSum++;
						if (!flag2)
						{
							this.collectState.MoebiusTraceFaild++;
							flag2 = moebiusTrace.SaveData(lastTracePath2, this.selectConfig.OleDbString, this.sqlConString, this.selectConfig.TimeOut);
							this.collectState.SqlTextSum++;
							if (!flag2)
							{
								this.collectState.SqlTextFaild++;
							}
						}
						flag2 = ItemCountCollector.SaveItemCount(this.selectConfig.OleDbString, this.selectConfig.TimeOut, CollectItem.Moebius, this.collectState.MoebiusTraceSum, this.collectState.MoebiusTraceFaild);
					}
				}
				catch (Exception ex2)
				{
					ErrorLog.Write(ex2);
				}
			}
			this.TempWatcher.EnableRaisingEvents = false;
			if (Directory.Exists(this.selectConfig.TempDirectory))
			{
				try
				{
					Directory.Delete(this.selectConfig.TempDirectory, true);
				}
				catch (Exception ex3)
				{
					ErrorLog.Write(ex3);
				}
			}
			this.MoebiusWatcher.EnableRaisingEvents = false;
			if (Directory.Exists(this.selectConfig.MoebiusDirectory))
			{
				try
				{
					Directory.Delete(this.selectConfig.MoebiusDirectory, true);
				}
				catch (Exception ex4)
				{
					ErrorLog.Write(ex4);
				}
			}
			string path = Path.Combine(Util.collectDir, this.flagFileName);
			if (Directory.Exists(path))
			{
				try
				{
					Directory.Delete(path, true);
				}
				catch (Exception ex5)
				{
					ErrorLog.Write(ex5);
				}
			}
			VersionCollector versionCollector = new VersionCollector();
			versionCollector.SaveEndTime(this.selectConfig.OleDbString, this.selectConfig.TimeOut);
			Registion registion = new Registion();
			registion.SaveData(this.selectConfig.OleDbString, this.selectConfig.TimeOut);
			base.Invoke(new EventHandler(delegate
			{
				this.btnCollector.Enabled = true;
				this.btnCollector.Image = this.imageList1.Images[0];
			}));
			if (this.pnlContent.Controls.Count > 0)
			{
				try
				{
					base.Invoke(new EventHandler(delegate
					{
						this.pnlContent.Controls.Clear();
						this.pnlContent.Controls.Add(this.pnlDBCollector);
						this.cbAgree.Visible = true;
						this.lblAgree.Visible = true;
						this.btnEdit.Visible = true;
					}));
				}
				catch (Exception ex6)
				{
					ErrorLog.Write(ex6);
				}
			}
			OleDbConnection oleDbConnection = null;
			if (Util.TryConnect(this.selectConfig.OleDbString, out oleDbConnection))
			{
				try
				{
					oleDbConnection.Open();
				}
				finally
				{
					oleDbConnection.Close();
					oleDbConnection.Dispose();
				}
			}
			ErrorLog.Info("结束收集");
		}

		private void CheckCollectState()
		{
			if (this.collectState != null)
			{
				StringBuilder stringBuilder = new StringBuilder();
				if (!this.generalCompleted)
				{
					stringBuilder.AppendLine("常规检查项");
				}
				if (this.selectConfig.HasPerfCounter && this.collectState.PerfFaild > 0 && (double)this.collectState.PerfFaild * 1.0 / (double)this.collectState.PerfSum >= 0.1)
				{
					stringBuilder.AppendLine("性能计数器");
				}
				if (this.selectConfig.HasTempdbInfo && this.collectState.TempdbFaild > 0 && (double)this.collectState.TempdbFaild * 1.0 / (double)this.collectState.TempdbSum >= 0.1)
				{
					stringBuilder.AppendLine("Tempdb 信息");
				}
				if (this.selectConfig.HasSession)
				{
					if (this.collectState.WaitFaild > 0 && (double)this.collectState.WaitFaild * 1.0 / (double)this.collectState.WaitSum >= 0.1)
					{
						stringBuilder.AppendLine("等待");
					}
					if (this.collectState.UnusedSessionFaild > 0 && (double)this.collectState.UnusedSessionFaild * 1.0 / (double)this.collectState.UnusedSessionSum >= 0.1)
					{
						stringBuilder.AppendLine("空闲会话");
					}
				}
				if (this.selectConfig.HasSqlText && this.collectState.SqlTextFaild > 0 && (double)this.collectState.SqlTextFaild * 1.0 / (double)this.collectState.SqlTextSum >= 0.1)
				{
					stringBuilder.AppendLine("查询语句");
				}
				if (this.selectConfig.HasSqlPlan && this.collectState.SqlPlanSum - this.collectState.SqlPlanFaild < 1)
				{
					stringBuilder.AppendLine("执行计划");
				}
				if (this.selectConfig.HasErrorLog && this.collectState.ErrLogSum - this.collectState.ErrLogFaild < 1)
				{
					stringBuilder.AppendLine("日志");
				}
				if (this.selectConfig.HasMoebius && this.collectState.MoebiusTraceFaild > 0 && (double)this.collectState.MoebiusTraceFaild * 1.0 / (double)this.collectState.MoebiusTraceSum >= 0.1)
				{
					stringBuilder.AppendLine("Moebius");
				}
				if (stringBuilder.Length > 0)
				{
					ResultCollector resultCollector = new ResultCollector();
					resultCollector.SaveCheckResult(this.selectConfig.OleDbString, "timecollect", false, this.selectConfig.TimeOut);
					string text = "收集文件不完整，以下部分收集不完全：\r\n" + stringBuilder.ToString() + "\r\n建议重新收集 ";
					MessageBox.Show(text, "SQL专家云", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
					return;
				}
				ResultCollector resultCollector2 = new ResultCollector();
				resultCollector2.SaveCheckResult(this.selectConfig.OleDbString, "timecollect", true, this.selectConfig.TimeOut);
				resultCollector2.Completed(this.selectConfig.OleDbString, this.selectConfig.TimeOut);
			}
		}

		private void collectionAysc_DoWork(object sender, DoWorkEventArgs e)
		{
			string str = Assembly.GetEntryAssembly().GetName().Version.ToString();
			ErrorLog.Info("SQL专家云 v" + str);
			ErrorLog.Info("开始收集");
			this.generalCompleted = false;
			base.Invoke(new EventHandler(delegate
			{
				this.btnCollector.Image = this.imageList1.Images[1];
			}));
			DBCollector dBCollector = new DBCollector();
			try
			{
				this.sqlVersion = dBCollector.GetSQLVersion(this.sqlConString);
				ErrorLog.Info("数据库版本：" + this.sqlVersion.ToString());
				ErrorLog.Info("存储路径：" + Util.filePath);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "SQL专家云");
				return;
			}
			if (Util.startFlag)
			{
				ResultCollector resultCollector = new ResultCollector();
				bool result = resultCollector.SaveCheckResult(this.oleDBString, "Completed", false, this.selectConfig.TimeOut);
				this.CollectResult("收集数据发生错误。", result);
				if (!Util.startFlag)
				{
					return;
				}
				ErrorLog.Info("收集目标数据库列表");
				bool flag = dBCollector.SaveDBList(this.oleDBString, this.selectConfig.DBList, this.selectConfig.TimeOut);
				this.CollectResult("收集目标数据库列表发生错误。", flag);
				result = resultCollector.SaveCheckResult(this.oleDBString, "DBList", flag, this.selectConfig.TimeOut);
				this.CollectResult("收集目标数据库列表发生错误。", result);
				if (!Util.startFlag)
				{
					return;
				}
				ErrorLog.Info("收集工具配置信息");
				ConfigCollector configCollector = new ConfigCollector();
				flag = configCollector.SaveConfig(this.oleDBString, this.selectConfig);
				this.CollectResult("收集工具配置信息发生错误。", flag);
				result = resultCollector.SaveCheckResult(this.oleDBString, "Config", flag, this.selectConfig.TimeOut);
				this.CollectResult("收集工具配置信息发生错误。", result);
				if (!Util.startFlag)
				{
					return;
				}
				ErrorLog.Info("收集项目初始化");
				flag = ItemCountCollector.SaveItemCount(this.oleDBString, this.selectConfig.TimeOut);
				this.CollectResult("收集项目初始化发生错误。", flag);
				if (!Util.startFlag)
				{
					return;
				}
				ErrorLog.Info("收集SQL专家云版本信息");
				OleDbCollector oleDbCollector = new VersionCollector();
				flag = oleDbCollector.SaveData(this.oleDBString, this.selectConfig.TimeOut);
				this.CollectResult("收集SQL专家云版本信息发生错误。", flag);
				result = resultCollector.SaveCheckResult(this.oleDBString, "Version", flag, this.selectConfig.TimeOut);
				this.CollectResult("收集SQL专家云版本信息发生错误。", result);
				if (!Util.startFlag)
				{
					return;
				}
				ErrorLog.Info("收集操作系统信息");
				oleDbCollector = new SystemInfoCollector();
				flag = oleDbCollector.SaveData(this.oleDBString, this.sqlConString, this.selectConfig.TimeOut);
				this.CollectResult("收集操作系统信息发生错误。", flag);
				result = resultCollector.SaveCheckResult(this.oleDBString, "SystemInfo", flag, this.selectConfig.TimeOut);
				this.CollectResult("收集操作系统信息发生错误。", result);
				ErrorLog.Info("收集网卡信息");
				oleDbCollector = new NetworkCollector();
				flag = oleDbCollector.SaveData(this.oleDBString, this.sqlConString, this.selectConfig.TimeOut);
				this.CollectResult("收集网卡信息发生错误，请启用网卡。", flag);
				result = resultCollector.SaveCheckResult(this.oleDBString, "Network", flag, this.selectConfig.TimeOut);
				this.CollectResult("收集网卡信息发生错误。", result);
				if (!Util.startFlag)
				{
					return;
				}
				ErrorLog.Info("收集SQL Server信息");
				SqlInfoCollector sqlInfoCollector = new SqlInfoCollector();
				if (this.sqlServiceInfo != null)
				{
					sqlInfoCollector.LoginName = this.sqlServiceInfo.AccountName;
				}
				flag = sqlInfoCollector.SaveData(this.oleDBString, this.sqlConString, this.sqlVersion, this.selectConfig.TimeOut);
				this.CollectResult("收集SQL Server信息发生错误。", flag);
				result = resultCollector.SaveCheckResult(this.oleDBString, "SqlInfo", flag, this.selectConfig.TimeOut);
				this.CollectResult("收集SQL Server信息发生错误。", result);
				if (!Util.startFlag)
				{
					return;
				}
				ErrorLog.Info("收集系统磁盘信息");
				oleDbCollector = new DiskCollector();
				flag = oleDbCollector.SaveData(this.oleDBString, this.sqlConString, this.selectConfig.TimeOut);
				this.CollectResult("收集系统磁盘信息发生错误。", flag);
				result = resultCollector.SaveCheckResult(this.oleDBString, "Disk", flag, this.selectConfig.TimeOut);
				this.CollectResult("收集系统磁盘信息发生错误。", result);
				if (!Util.startFlag)
				{
					return;
				}
				ErrorLog.Info("收集实例参数信息");
				oleDbCollector = new InstanceConfigCollector();
				flag = oleDbCollector.SaveData(this.oleDBString, this.sqlConString, this.selectConfig.TimeOut);
				this.CollectResult("收集实例参数信息发生错误。", flag);
				result = resultCollector.SaveCheckResult(this.oleDBString, "InstanceConfig", flag, this.selectConfig.TimeOut);
				this.CollectResult("收集实例参数信息发生错误。", result);
				if (!Util.startFlag)
				{
					return;
				}
				ErrorLog.Info("收集Tempdb 文件信息");
				if (this.selectConfig.HasTempdbInfo)
				{
					TempDBCollector tempDBCollector = new TempDBCollector();
					flag = tempDBCollector.SaveData(this.oleDBString, this.sqlConString, this.selectConfig.TimeOut, DateTime.Now);
					this.CollectResult("收集Tempdb 文件信息发生错误。", flag);
					result = resultCollector.SaveCheckResult(this.oleDBString, "TempDB", flag, this.selectConfig.TimeOut);
					this.CollectResult("收集Tempdb 文件信息发生错误。", result);
					if (!Util.startFlag)
					{
						return;
					}
					ErrorLog.Info("收集Tempdb 空间使用情况");
					flag = tempDBCollector.SaveTempSpace(this.oleDBString, this.sqlConString, this.selectConfig.TimeOut, DateTime.Now);
					this.CollectResult("收集Tempdb 空间信息发生错误。", result);
					result = resultCollector.SaveCheckResult(this.oleDBString, "TempSpace", flag, this.selectConfig.TimeOut);
					this.CollectResult("收集Tempdb 空间信息发生错误。", result);
					if (!Util.startFlag)
					{
						return;
					}
				}
				ErrorLog.Info("收集会话概况信息");
				oleDbCollector = new SessionCollector();
				flag = oleDbCollector.SaveData(this.oleDBString, this.sqlConString, this.selectConfig.TimeOut);
				this.CollectResult("收集会话概况信息发生错误。", flag);
				result = resultCollector.SaveCheckResult(this.oleDBString, "Session", flag, this.selectConfig.TimeOut);
				this.CollectResult("收集会话概况信息发生错误。", result);
				if (!Util.startFlag)
				{
					return;
				}
				ErrorLog.Info("收集作业信息");
				oleDbCollector = new JobCollector();
				flag = oleDbCollector.SaveData(this.oleDBString, this.sqlConString, this.selectConfig.TimeOut);
				this.CollectResult("收集作业信息发生错误。", flag);
				result = resultCollector.SaveCheckResult(this.oleDBString, "Job", flag, this.selectConfig.TimeOut);
				this.CollectResult("收集作业信息发生错误。", result);
				if (!Util.startFlag)
				{
					return;
				}
				if (this.selectConfig.HasSqlText)
				{
					ErrorLog.Info("开始创建查询语句跟踪");
					SqlTextCollector sqlTextCollector = new SqlTextCollector();
					string text = Path.Combine(this.selectConfig.TempDirectory, this.sqlTextFileName);
					object obj = sqlTextCollector.Start(this.sqlConString, this.selectConfig.TimeOut, text, this.selectConfig.SqlTextValue);
					if (obj != null)
					{
						this.sqlTextTraceId = Convert.ToInt32(obj);
						this.lastSqlTraceName = sqlTextCollector.GetLastTracePath(this.sqlTextTraceId, this.sqlConString, this.selectConfig.TimeOut);
						if (text + ".trc" != this.lastSqlTraceName)
						{
							this.selectConfig.TempDirectory = Path.GetDirectoryName(this.lastSqlTraceName);
							this.sqlTextFileName = Path.GetFileNameWithoutExtension(this.lastSqlTraceName);
						}
						this.TempWatcher.Path = this.selectConfig.TempDirectory;
						this.TempWatcher.EnableRaisingEvents = true;
						ErrorLog.Info("创建查询语句跟踪成功");
						result = resultCollector.SaveCheckResult(this.oleDBString, "sqlTexttrace", flag, this.selectConfig.TimeOut);
						this.CollectResult("创建查询语句跟踪失败。", result);
					}
					else
					{
						this.sqlTextTraceId = -1;
						if (this.TempWatcher.EnableRaisingEvents)
						{
							this.TempWatcher.EnableRaisingEvents = false;
						}
						ErrorLog.Info("创建查询语句跟踪失败");
						this.CollectResult("创建查询语句跟踪失败", false);
						result = resultCollector.SaveCheckResult(this.oleDBString, "sqlTexttrace", flag, this.selectConfig.TimeOut);
						this.CollectResult("创建查询语句跟踪失败。", result);
					}
				}
				else
				{
					this.sqlTextTraceId = -1;
					if (this.TempWatcher.EnableRaisingEvents)
					{
						this.TempWatcher.EnableRaisingEvents = false;
					}
				}
				if (!Util.startFlag)
				{
					return;
				}
				ErrorLog.Info("收集数据库信息");
				ErrorLog.Info("收集数据库概览信息");
				DBSummaryCollector dBSummaryCollector = new DBSummaryCollector();
				dBSummaryCollector.SaveData(this.oleDBString, this.sqlConString, this.selectConfig.TimeOut);
				this.CollectResult("收集数据库概览信息发生错误。", flag);
				result = resultCollector.SaveCheckResult(this.oleDBString, "DBSummary", flag, this.selectConfig.TimeOut);
				this.CollectResult("收集数据库概览信息发生错误。", result);
				UnMoebiusCollector unMoebiusCollector = new UnMoebiusCollector();
				if (this.selectConfig.DBList.Count > 0)
				{
					foreach (string current in this.selectConfig.DBList)
					{
						if (!Util.startFlag)
						{
							return;
						}
						string sqlString = new SqlConnectionStringBuilder(this.sqlConString)
						{
							InitialCatalog = current
						}.ToString();
						if (!Util.startFlag)
						{
							return;
						}
						ErrorLog.Info("收集" + current + "数据库配置信息");
						oleDbCollector = new DBGeneralCollector();
						flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.sqlVersion, this.selectConfig.TimeOut);
						this.CollectResult("收集" + current + "数据库配置信息发生错误。", flag);
						result = resultCollector.SaveCheckResult(this.oleDBString, current + " DBGeneral", flag, this.selectConfig.TimeOut);
						this.CollectResult("收集" + current + "数据库配置信息发生错误。", result);
						if (!Util.startFlag)
						{
							return;
						}
						ErrorLog.Info("检查" + current + "数据库是否在Moebius集群中");
						oleDbCollector = new MoebiusCollector();
						flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.selectConfig.TimeOut);
						this.CollectResult("检查" + current + "数据库是否在Moebius集群时发生错误。", flag);
						result = resultCollector.SaveCheckResult(this.oleDBString, current + " Moebius", flag, this.selectConfig.TimeOut);
						this.CollectResult("检查" + current + "数据库是否在Moebius集群时发生错误。", result);
						if (!Util.startFlag)
						{
							return;
						}
						ErrorLog.Info("检查" + current + "数据库是否在高可用性组中");
						oleDbCollector = new AlwaysOnCollector();
						flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.sqlVersion, this.selectConfig.TimeOut);
						this.CollectResult("检查" + current + "是否在高可用性组时发生错误。", flag);
						result = resultCollector.SaveCheckResult(this.oleDBString, current + " AlwaysOn", flag, this.selectConfig.TimeOut);
						this.CollectResult("检查" + current + "是否在高可用性组时发生错误。", result);
						if (!Util.startFlag)
						{
							return;
						}
						ErrorLog.Info("检查" + current + "数据库是否有镜像");
						oleDbCollector = new MirroringCollector();
						flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.selectConfig.TimeOut);
						this.CollectResult("检查" + current + "数据库是否有镜像时发生错误。", flag);
						result = resultCollector.SaveCheckResult(this.oleDBString, current + " Mirroring", flag, this.selectConfig.TimeOut);
						this.CollectResult("检查" + current + "数据库是否有镜像时发生错误。", result);
						if (!Util.startFlag)
						{
							return;
						}
						ErrorLog.Info("收集" + current + "数据库表空间");
						TableInfoCollector tableInfoCollector = new TableInfoCollector();
						DBFilter filter = this.GetFilter(this.selectConfig.DBInstanceName, current, 1, this.selectConfig.DBFilterList);
						DBFilter filter2 = this.GetFilter(this.selectConfig.DBInstanceName, current, 2, this.selectConfig.DBFilterList);
						if (filter != null)
						{
							if (filter2 != null)
							{
								flag = tableInfoCollector.SaveData(this.oleDBString, sqlString, this.selectConfig.TimeOut, filter.FilterString, filter2.FilterString);
							}
							else
							{
								flag = tableInfoCollector.SaveData(this.oleDBString, sqlString, this.selectConfig.TimeOut, filter.FilterString, string.Empty);
							}
						}
						else if (filter2 != null)
						{
							flag = tableInfoCollector.SaveData(this.oleDBString, sqlString, this.selectConfig.TimeOut, string.Empty, filter2.FilterString);
						}
						else
						{
							flag = tableInfoCollector.SaveData(this.oleDBString, sqlString, this.selectConfig.TimeOut, string.Empty, string.Empty);
						}
						this.CollectResult("收集" + current + "数据库表空间发生错误。", flag);
						result = resultCollector.SaveCheckResult(this.oleDBString, current + " TableInfo", flag, this.selectConfig.TimeOut);
						this.CollectResult("收集" + current + "数据库表空间发生错误。", result);
						if (!Util.startFlag)
						{
							return;
						}
						ErrorLog.Info("收集" + current + "数据库设计不合理的表");
						oleDbCollector = new UnReasonablyTablesCollector();
						if (filter != null)
						{
							flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.selectConfig.TimeOut, filter.FilterString);
						}
						else
						{
							flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.selectConfig.TimeOut, string.Empty);
						}
						this.CollectResult("收集" + current + "数据库设计不合理的表发生错误。", flag);
						result = resultCollector.SaveCheckResult(this.oleDBString, current + " UnReasonably", flag, this.selectConfig.TimeOut);
						this.CollectResult("收集" + current + "数据库设计不合理的表发生错误。", result);
						if (!Util.startFlag)
						{
							return;
						}
						if (this.selectConfig.HasDBFile)
						{
							ErrorLog.Info("收集" + current + "数据库文件信息");
							DBFileCollector dBFileCollector = new DBFileCollector();
							flag = dBFileCollector.SaveData(this.oleDBString, sqlString, this.selectConfig.TimeOut, DateTime.Now);
							this.collectState.DBFileSum++;
							if (!flag)
							{
								this.collectState.DBFileFaild++;
							}
							this.CollectResult("收集" + current + "数据库文件信息发生错误。", flag);
							result = resultCollector.SaveCheckResult(this.oleDBString, current + " DBFile", flag, this.selectConfig.TimeOut);
							this.CollectResult("收集" + current + "数据库文件信息发生错误。", result);
							if (!Util.startFlag)
							{
								return;
							}
						}
						ErrorLog.Info("收集" + current + "数据库备份信息");
						oleDbCollector = new BackupCollector();
						flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.selectConfig.TimeOut);
						this.CollectResult("收集" + current + "数据库备份信息发生错误。", flag);
						result = resultCollector.SaveCheckResult(this.oleDBString, current + " Backup", flag, this.selectConfig.TimeOut);
						this.CollectResult("收集" + current + "数据库备份信息发生错误。", result);
						if (!Util.startFlag)
						{
							return;
						}
						ErrorLog.Info("收集" + current + "数据库缺失索引");
						oleDbCollector = new MissIndexCollector();
						flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.selectConfig.TimeOut);
						this.CollectResult("收集" + current + "数据库缺失索引发生错误。", flag);
						result = resultCollector.SaveCheckResult(this.oleDBString, current + " MissIndex", flag, this.selectConfig.TimeOut);
						this.CollectResult("收集" + current + "数据库缺失索引发生错误。", result);
						if (!Util.startFlag)
						{
							return;
						}
						ErrorLog.Info("收集" + current + "数据库没有索引的外键");
						oleDbCollector = new ForeignNoIndexCollector();
						flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.selectConfig.TimeOut);
						this.CollectResult("收集" + current + "数据库没有索引的外键发生错误。", flag);
						result = resultCollector.SaveCheckResult(this.oleDBString, current + " ForeignNoIndex", flag, this.selectConfig.TimeOut);
						this.CollectResult("收集" + current + "数据库没有索引的外键发生错误。", result);
						if (!Util.startFlag)
						{
							return;
						}
						ErrorLog.Info("收集" + current + "数据库没有使用的索引");
						oleDbCollector = new UnusedIndexCollector();
						flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.sqlVersion, this.selectConfig.TimeOut);
						this.CollectResult("收集" + current + "数据库没有使用的索引发生错误。", flag);
						result = resultCollector.SaveCheckResult(this.oleDBString, current + " UnusedIndex", flag, this.selectConfig.TimeOut);
						this.CollectResult("收集" + current + "数据库没有使用的索引发生错误。", result);
						if (!Util.startFlag)
						{
							return;
						}
						ErrorLog.Info("收集" + current + "数据库重复的索引");
						oleDbCollector = new RepeatIndexCollector();
						flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.sqlVersion, this.selectConfig.TimeOut);
						this.CollectResult("收集" + current + "数据库重复的索引发生错误。", flag);
						result = resultCollector.SaveCheckResult(this.oleDBString, current + " RepeatIndex", flag, this.selectConfig.TimeOut);
						this.CollectResult("收集" + current + "数据库重复的索引发生错误。", result);
						if (!Util.startFlag)
						{
							return;
						}
						ErrorLog.Info("收集" + current + "数据库索引使用情况");
						oleDbCollector = new IndexUsedCollector();
						flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.selectConfig.TimeOut);
						this.CollectResult("收集" + current + "数据库索引使用情况发生错误。", flag);
						result = resultCollector.SaveCheckResult(this.oleDBString, current + " IndexUsed", flag, this.selectConfig.TimeOut);
						this.CollectResult("收集" + current + "数据库索引使用情况发生错误。", result);
						if (!Util.startFlag)
						{
							return;
						}
						ErrorLog.Info("收集" + current + "数据库统计信息");
						oleDbCollector = new StatisticsCollector();
						flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.sqlVersion, this.selectConfig.TimeOut);
						this.CollectResult("收集" + current + "数据库统计信息发生错误。", flag);
						result = resultCollector.SaveCheckResult(this.oleDBString, current + " Statistics", flag, this.selectConfig.TimeOut);
						this.CollectResult("收集" + current + "数据库统计信息发生错误。", result);
						if (!Util.startFlag)
						{
							return;
						}
						if (this.selectConfig.HasDBDefine)
						{
							if (!Util.startFlag)
							{
								return;
							}
							if (this.selectConfig.DBItemType.HasTable)
							{
								ErrorLog.Info("收集" + current + "数据库表定义");
								oleDbCollector = new TableDefineCollector();
								if (filter != null)
								{
									flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.sqlVersion, this.selectConfig.TimeOut, filter.FilterString, this.selectConfig.IsDecrypt);
								}
								else
								{
									flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.sqlVersion, this.selectConfig.TimeOut, string.Empty, this.selectConfig.IsDecrypt);
								}
								this.CollectResult("收集" + current + "数据库表定义发生错误。", flag);
								result = resultCollector.SaveCheckResult(this.oleDBString, current + " TableDefine", flag, this.selectConfig.TimeOut);
								this.CollectResult("收集" + current + "数据库表定义发生错误。", result);
								if (this.selectConfig.HasCheckTable)
								{
									ErrorLog.Info("收集" + current + "数据库表一致性");
									oleDbCollector = new CheckTableCollector();
									if (filter != null)
									{
										flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.selectConfig.TimeOut, filter.FilterString);
									}
									else
									{
										flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.selectConfig.TimeOut, string.Empty);
									}
									this.CollectResult("收集" + current + "数据库表一致性发生错误。", flag);
									result = resultCollector.SaveCheckResult(this.oleDBString, current + " CheckTable", flag, this.selectConfig.TimeOut);
									this.CollectResult("收集" + current + "数据库表一致性发生错误。", result);
								}
							}
							if (!Util.startFlag)
							{
								return;
							}
							if (this.selectConfig.DBItemType.HasView)
							{
								ErrorLog.Info("收集" + current + "数据库视图定义");
								oleDbCollector = new ViewDefineCollector();
								DBFilter filter3 = this.GetFilter(this.selectConfig.DBInstanceName, current, 2, this.selectConfig.DBFilterList);
								if (filter3 != null)
								{
									flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.sqlVersion, this.selectConfig.TimeOut, filter.FilterString, this.selectConfig.IsDecrypt);
								}
								else
								{
									flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.sqlVersion, this.selectConfig.TimeOut, string.Empty, this.selectConfig.IsDecrypt);
								}
								this.CollectResult("收集" + current + "数据库视图定义发生错误。", flag);
								result = resultCollector.SaveCheckResult(this.oleDBString, current + " ViewDefine", flag, this.selectConfig.TimeOut);
								this.CollectResult("收集" + current + "数据库视图定义发生错误。", result);
							}
							if (!Util.startFlag)
							{
								return;
							}
							if (this.selectConfig.DBItemType.HasStoredProc)
							{
								ErrorLog.Info("收集" + current + "数据库存储过程定义");
								oleDbCollector = new StoredProcCollector();
								DBFilter filter4 = this.GetFilter(this.selectConfig.DBInstanceName, current, 3, this.selectConfig.DBFilterList);
								if (filter4 != null)
								{
									flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.sqlVersion, this.selectConfig.TimeOut, filter4.FilterString, this.selectConfig.IsDecrypt);
								}
								else
								{
									flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.sqlVersion, this.selectConfig.TimeOut, string.Empty, this.selectConfig.IsDecrypt);
								}
								this.CollectResult("收集" + current + "数据库存储过程定义发生错误。", flag);
								result = resultCollector.SaveCheckResult(this.oleDBString, current + " StoredProc", flag, this.selectConfig.TimeOut);
								this.CollectResult("收集" + current + "数据库存储过程定义发生错误。", result);
							}
							if (!Util.startFlag)
							{
								return;
							}
							if (this.selectConfig.DBItemType.HasFuction)
							{
								ErrorLog.Info("收集" + current + "数据库函数定义");
								oleDbCollector = new FunctionCollector();
								DBFilter filter5 = this.GetFilter(this.selectConfig.DBInstanceName, current, 4, this.selectConfig.DBFilterList);
								if (filter5 != null)
								{
									flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.sqlVersion, this.selectConfig.TimeOut, filter5.FilterString, this.selectConfig.IsDecrypt);
								}
								else
								{
									flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.sqlVersion, this.selectConfig.TimeOut, string.Empty, this.selectConfig.IsDecrypt);
								}
								this.CollectResult("收集" + current + "数据库函数定义发生错误。", flag);
								result = resultCollector.SaveCheckResult(this.oleDBString, current + " Function", flag, this.selectConfig.TimeOut);
								this.CollectResult("收集" + current + "数据库函数定义发生错误。", result);
							}
							ErrorLog.Info("收集" + current + "数据库自定义类型");
							oleDbCollector = new UserTypeCollector();
							flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.sqlVersion, this.selectConfig.TimeOut);
							this.CollectResult("收集" + current + "数据库自定义类型发生错误。", flag);
							result = resultCollector.SaveCheckResult(this.oleDBString, current + " UserType", flag, this.selectConfig.TimeOut);
							this.CollectResult("收集" + current + "数据库自定义类型发生错误。", result);
							if (this.sqlVersion >= SQLServerVersion.SQLServer2008)
							{
								ErrorLog.Info("收集" + current + "数据库自定义表类型");
								oleDbCollector = new UserTableTypeCollector();
								flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.sqlVersion, this.selectConfig.TimeOut);
								this.CollectResult("收集" + current + "数据库自定义表类型发生错误。", flag);
							}
							ErrorLog.Info("收集" + current + "数据库自定义数据类型");
							oleDbCollector = new UDTypeCollector();
							flag = oleDbCollector.SaveData(this.oleDBString, sqlString, this.sqlVersion, this.selectConfig.TimeOut);
							this.CollectResult("收集" + current + "数据库自定义数据类型发生错误。", flag);
							result = resultCollector.SaveCheckResult(this.oleDBString, current + " UserTableType", flag, this.selectConfig.TimeOut);
							this.CollectResult("收集" + current + "数据库自定义数据类型发生错误。", result);
							if (this.selectConfig.HasMoebius)
							{
								ErrorLog.Info("收集" + current + "Moebius相关信息");
								flag = unMoebiusCollector.SaveDBData(this.oleDBString, sqlString, this.sqlVersion, this.selectConfig.TimeOut, (filter == null) ? string.Empty : filter.FilterString);
								this.CollectResult("收集" + current + "数据库Moebius发生错误。", flag);
								result = resultCollector.SaveCheckResult(this.oleDBString, current + " Moebius", flag, this.selectConfig.TimeOut);
								this.CollectResult("收集" + current + "数据库Moebius发生错误。", result);
							}
						}
					}
				}
				if (this.selectConfig.HasMoebius)
				{
					ErrorLog.Info("收集Moebius常规信息");
					flag = unMoebiusCollector.SaveData(this.oleDBString, this.selectConfig.SqlConString, this.selectConfig.TimeOut);
					this.CollectResult("收集Moebius常规信息发生错误。", flag);
					result = resultCollector.SaveCheckResult(this.oleDBString, " Moebius", flag, this.selectConfig.TimeOut);
					this.CollectResult("收集Moebius常规信息发生错误。", result);
				}
				if (this.selectConfig.HasSession)
				{
					ErrorLog.Info("收集空闲会话");
					UnusedSessionCollector unusedSessionCollector = new UnusedSessionCollector();
					flag = unusedSessionCollector.SaveData(this.selectConfig.OleDbString, this.selectConfig.SqlConString, this.selectConfig.TimeOut, this.selectConfig.UnusedSessionValue);
					this.collectState.UnusedSessionSum++;
					if (!flag)
					{
						this.collectState.UnusedSessionFaild++;
						ErrorLog.Info("收集空闲会话 失败");
					}
					flag = ItemCountCollector.SaveItemCount(this.selectConfig.OleDbString, this.selectConfig.TimeOut, CollectItem.UnusedSession, this.collectState.UnusedSessionSum, this.collectState.UnusedSessionFaild);
				}
				if (this.selectConfig.HasSqlPlan)
				{
					ErrorLog.Info("收集执行计划");
					SqlPlanCollector sqlPlanCollector = new SqlPlanCollector();
					DateTime dateTime = DateTime.Parse("1900/01/01 00:00:00");
					this.planRowID = 1;
					sqlPlanCollector.RowID = this.planRowID;
					flag = sqlPlanCollector.SaveData(this.selectConfig.OleDbString, this.selectConfig.SqlConString, this.selectConfig.TimeOut, this.selectConfig.SqlPlanValue, this.lastExecTime, ref dateTime);
					this.planRowID = sqlPlanCollector.RowID;
					this.collectState.SqlPlanSum++;
					if (flag)
					{
						this.lastExecTime = dateTime;
					}
					else
					{
						this.collectState.SqlPlanFaild++;
						ErrorLog.Info("收集执行计划 失败");
					}
					flag = ItemCountCollector.SaveItemCount(this.selectConfig.OleDbString, this.selectConfig.TimeOut, CollectItem.SqlPlan, this.collectState.SqlPlanSum, this.collectState.SqlPlanFaild);
				}
				if (this.selectConfig.HasErrorLog)
				{
					ErrorLog.Info("收集错误日志");
					SqlLogCollector sqlLogCollector = new SqlLogCollector();
					DateTime dateTime2 = DateTime.Parse("1900/01/01 00:00:00");
					flag = sqlLogCollector.SaveData(this.selectConfig.OleDbString, this.selectConfig.SqlConString, this.sqlVersion, this.selectConfig.TimeOut, this.selectConfig.ErrorLogKeys, this.lastLogTime, ref dateTime2);
					this.collectState.ErrLogSum++;
					if (flag)
					{
						this.lastLogTime = dateTime2;
					}
					else
					{
						this.collectState.ErrLogFaild++;
						ErrorLog.Info("收集错误日志 失败");
					}
					flag = ItemCountCollector.SaveItemCount(this.selectConfig.OleDbString, this.selectConfig.TimeOut, CollectItem.ErrLog, this.collectState.ErrLogSum, this.collectState.ErrLogFaild);
				}
				if (this.selectConfig.HasMoebius)
				{
					ErrorLog.Info("开始创建Moebius跟踪");
					MoebiusTrace moebiusTrace = new MoebiusTrace();
					string text2 = Path.Combine(this.selectConfig.MoebiusDirectory, this.moebiusTraceName);
					object obj2 = moebiusTrace.Start(this.sqlConString, this.selectConfig.TimeOut, text2);
					if (obj2 != null)
					{
						this.moebiusTraceId = Convert.ToInt32(obj2);
						this.lastMoebiusTraceName = moebiusTrace.GetLastTracePath(this.moebiusTraceId, this.sqlConString, this.selectConfig.TimeOut);
						if (text2 + ".trc" != this.lastMoebiusTraceName)
						{
							this.selectConfig.MoebiusDirectory = Path.GetDirectoryName(this.lastMoebiusTraceName);
							this.moebiusTraceName = Path.GetFileNameWithoutExtension(this.lastMoebiusTraceName);
						}
						this.MoebiusWatcher.Path = this.selectConfig.MoebiusDirectory;
						this.MoebiusWatcher.EnableRaisingEvents = true;
						ErrorLog.Info("创建Moebius跟踪成功");
						result = resultCollector.SaveCheckResult(this.oleDBString, "moebiustrace", flag, this.selectConfig.TimeOut);
						this.CollectResult("创建Moebius跟踪失败。", result);
					}
					else
					{
						this.moebiusTraceId = -1;
						if (this.MoebiusWatcher.EnableRaisingEvents)
						{
							this.MoebiusWatcher.EnableRaisingEvents = false;
						}
						ErrorLog.Info("创建Moebius跟踪失败");
						this.CollectResult("创建Moebius跟踪失败", false);
						result = resultCollector.SaveCheckResult(this.oleDBString, "moebiustrace", flag, this.selectConfig.TimeOut);
						this.CollectResult("创建Moebius跟踪失败。", result);
					}
				}
				else
				{
					this.moebiusTraceId = -1;
					if (this.MoebiusWatcher.EnableRaisingEvents)
					{
						this.MoebiusWatcher.EnableRaisingEvents = false;
					}
				}
				ErrorLog.Info("常规信息收集完成");
				this.generalCompleted = true;
				ucDuration duration = null;
				if (this.pnlContent.Controls.Count > 0)
				{
					duration = (this.pnlContent.Controls[0] as ucDuration);
				}
				if (duration == null)
				{
					this.CollectResult("发生未知错误，无法继续收集。", flag);
					return;
				}
				if (this.selectConfig.PerfList != null && this.selectConfig.PerfList.Count == 0)
				{
					this.selectConfig.HasPerfCounter = false;
				}
				if (this.selectConfig.HasPerfCounter)
				{
					duration.PerfElapsed = this.selectConfig.PerfElapsed;
					duration.CollectPerf += new CollectPerfHandler(this.duration_CollectPerf);
				}
				if (this.selectConfig.HasDBFile)
				{
					duration.DBFileElapsed = this.selectConfig.DBFileElapsed;
					duration.CollectDBFile += new CollectDBFileHandler(this.duration_CollectDBFile);
				}
				if (this.selectConfig.HasSession)
				{
					duration.WaitElapsed = this.selectConfig.WaitElapsed;
					duration.CollectWait += new CollectWaitHandler(this.duration_CollectWait);
					duration.UnusedSessionElapsed = this.selectConfig.UnusedSessionElapsed;
					duration.CollectUnusedSession += new CollectUnusedSessionHandler(this.duration_CollectUnusedSession);
				}
				if (this.selectConfig.HasSqlPlan)
				{
					duration.SqlPlanElapsed = this.selectConfig.SqlPlanElapsed;
					duration.CollectSqlPlan += new CollectSqlPlanHandler(this.duration_CollectSqlPlan);
				}
				if (this.selectConfig.ErrorLogKeys != null && this.selectConfig.ErrorLogKeys.Length == 0)
				{
					this.selectConfig.HasErrorLog = false;
				}
				if (this.selectConfig.HasErrorLog)
				{
					duration.ErrorLogElapsed = this.selectConfig.ErrorLogElapsed;
					duration.CollectErrorLog += new CollectErrorLogHandler(this.duration_CollectErrorLog);
				}
				if (this.selectConfig.HasTempdbInfo)
				{
					duration.TempdbElapsed = this.selectConfig.TempdbElapsed;
					duration.CollectTempdb += new CollectTempdbHandler(this.duration_CollectTempdb);
				}
				if (this.selectConfig.HasSqlText)
				{
					duration.HasSqlText = true;
					duration.CollectSqlText += new CollectSqlTextHandler(this.duration_CollectSqlText);
				}
				base.Invoke(new EventHandler(delegate
				{
					duration.StartCollect();
				}));
				return;
			}
		}

		private void duration_CollectPerf(DateTime collectTime)
		{
			lock (this)
			{
				if (!Util.startFlag)
				{
					this.collectState.PerfSum++;
					this.collectState.PerfFaild++;
				}
				else
				{
					PerfCollector perfCollector = new PerfCollector();
					bool flag = perfCollector.SaveData(this.selectConfig.OleDbString, this.selectConfig.SqlConString, this.selectConfig.TimeOut, collectTime, this.selectConfig.PerfList);
					this.collectState.PerfSum++;
					if (!flag)
					{
						this.collectState.PerfFaild++;
					}
					flag = ItemCountCollector.SaveItemCount(this.selectConfig.OleDbString, this.selectConfig.TimeOut, CollectItem.Perf, this.collectState.PerfSum, this.collectState.PerfFaild);
					this.CheckFileSize(Util.filePath);
				}
			}
		}

		private void duration_CollectWait(DateTime collectTime)
		{
			ThreadPool.QueueUserWorkItem(new WaitCallback(this.Wait));
		}

		private void Wait(object sender)
		{
			lock (this)
			{
				if (!Util.startFlag)
				{
					this.collectState.WaitSum++;
					this.collectState.WaitFaild++;
				}
				else
				{
					WaitCollector waitCollector = new WaitCollector();
					bool flag = waitCollector.SaveData(this.selectConfig.OleDbString, this.selectConfig.SqlConString, this.sqlVersion, this.selectConfig.TimeOut);
					this.collectState.WaitSum++;
					if (!flag)
					{
						this.collectState.WaitFaild++;
					}
					flag = ItemCountCollector.SaveItemCount(this.selectConfig.OleDbString, this.selectConfig.TimeOut, CollectItem.Wait, this.collectState.WaitSum, this.collectState.WaitFaild);
					this.CheckFileSize(Util.filePath);
				}
			}
		}

		private void duration_CollectUnusedSession(DateTime collectTime)
		{
			ThreadPool.QueueUserWorkItem(new WaitCallback(this.UnusedSession));
		}

		private void UnusedSession(object sender)
		{
			lock (this)
			{
				if (!Util.startFlag)
				{
					this.collectState.UnusedSessionSum++;
					this.collectState.UnusedSessionFaild++;
				}
				else
				{
					UnusedSessionCollector unusedSessionCollector = new UnusedSessionCollector();
					bool flag = unusedSessionCollector.SaveData(this.selectConfig.OleDbString, this.selectConfig.SqlConString, this.selectConfig.TimeOut, this.selectConfig.UnusedSessionValue);
					this.collectState.UnusedSessionSum++;
					if (!flag)
					{
						this.collectState.UnusedSessionFaild++;
					}
					flag = ItemCountCollector.SaveItemCount(this.selectConfig.OleDbString, this.selectConfig.TimeOut, CollectItem.UnusedSession, this.collectState.UnusedSessionSum, this.collectState.UnusedSessionFaild);
					this.CheckFileSize(Util.filePath);
				}
			}
		}

		private void duration_CollectDBFile(DateTime collectTime)
		{
			if (this.selectConfig.DBList.Count > 0)
			{
				SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder(this.sqlConString);
				foreach (string current in this.selectConfig.DBList)
				{
					DBFileCollector dBFileCollector = new DBFileCollector();
					sqlConnectionStringBuilder.InitialCatalog = current;
					string sqlString = sqlConnectionStringBuilder.ToString();
					bool flag = dBFileCollector.SaveData(this.selectConfig.OleDbString, sqlString, this.selectConfig.TimeOut, collectTime);
					this.collectState.DBFileSum++;
					if (!flag)
					{
						this.collectState.DBFileFaild++;
					}
					flag = ItemCountCollector.SaveItemCount(this.selectConfig.OleDbString, this.selectConfig.TimeOut, CollectItem.DBFile, this.collectState.DBFileSum, this.collectState.DBFileFaild);
				}
				this.CheckFileSize(Util.filePath);
			}
		}

		private void duration_CollectSqlPlan(DateTime collectTime)
		{
			SqlPlanCollector sqlPlanCollector = new SqlPlanCollector();
			DateTime dateTime = DateTime.Parse("1900/01/01 00:00:00");
			sqlPlanCollector.RowID = this.planRowID;
			bool flag = sqlPlanCollector.SaveData(this.selectConfig.OleDbString, this.selectConfig.SqlConString, this.selectConfig.TimeOut, this.selectConfig.SqlPlanValue, this.lastExecTime, ref dateTime);
			this.planRowID = sqlPlanCollector.RowID;
			this.collectState.SqlPlanSum++;
			if (flag)
			{
				this.lastExecTime = dateTime;
			}
			else
			{
				this.collectState.SqlPlanFaild++;
			}
			flag = ItemCountCollector.SaveItemCount(this.selectConfig.OleDbString, this.selectConfig.TimeOut, CollectItem.SqlPlan, this.collectState.SqlPlanSum, this.collectState.SqlPlanFaild);
			this.CheckFileSize(Util.filePath);
		}

		private void duration_CollectErrorLog(DateTime collectTime)
		{
			SqlLogCollector sqlLogCollector = new SqlLogCollector();
			DateTime dateTime = DateTime.Parse("1900/01/01 00:00:00");
			bool flag = sqlLogCollector.SaveData(this.selectConfig.OleDbString, this.selectConfig.SqlConString, this.sqlVersion, this.selectConfig.TimeOut, this.selectConfig.ErrorLogKeys, this.lastLogTime, ref dateTime);
			this.collectState.ErrLogSum++;
			if (flag)
			{
				this.lastLogTime = dateTime;
			}
			else
			{
				this.collectState.ErrLogFaild++;
			}
			flag = ItemCountCollector.SaveItemCount(this.selectConfig.OleDbString, this.selectConfig.TimeOut, CollectItem.ErrLog, this.collectState.ErrLogSum, this.collectState.ErrLogFaild);
			this.CheckFileSize(Util.filePath);
		}

		private void duration_CollectTempdb(DateTime collectTime)
		{
			TempDBCollector tempDBCollector = new TempDBCollector();
			bool flag = tempDBCollector.SaveTempSpace(this.selectConfig.OleDbString, this.selectConfig.SqlConString, this.selectConfig.TimeOut, collectTime);
			this.collectState.TempdbSum++;
			if (!flag)
			{
				this.collectState.TempdbFaild++;
			}
			flag = ItemCountCollector.SaveItemCount(this.selectConfig.OleDbString, this.selectConfig.TimeOut, CollectItem.Tempdb, this.collectState.TempdbSum, this.collectState.TempdbFaild);
			this.CheckFileSize(Util.filePath);
		}

		private void duration_CollectSqlText(DateTime collectTime)
		{
			bool flag = false;
			try
			{
				SqlConnection sqlConnection = new SqlConnection(this.selectConfig.SqlConString);
				sqlConnection.Open();
				sqlConnection.Close();
				flag = true;
			}
			catch (SqlException)
			{
				flag = false;
			}
			catch (Exception)
			{
				flag = false;
			}
			if (flag)
			{
				if (this.selectConfig.HasSqlText)
				{
					SqlTextCollector sqlTextCollector = new SqlTextCollector();
					int num = -1;
					if (!string.IsNullOrEmpty(this.lastSqlTraceName))
					{
						num = sqlTextCollector.GetLastTraceID(this.lastSqlTraceName, this.selectConfig.SqlConString, this.selectConfig.TimeOut);
					}
					if (this.sqlTextTraceId == -1)
					{
						if (num > 0)
						{
							this.sqlTextTraceId = num;
						}
						else
						{
							this.CreateSqlTrace();
						}
					}
					else
					{
						string lastTracePath = sqlTextCollector.GetLastTracePath(this.sqlTextTraceId, this.selectConfig.SqlConString, this.selectConfig.TimeOut);
						if (!lastTracePath.StartsWith(this.selectConfig.TempDirectory))
						{
							this.CreateSqlTrace();
						}
					}
				}
				if (this.selectConfig.HasMoebius)
				{
					MoebiusTrace moebiusTrace = new MoebiusTrace();
					int num2 = -1;
					if (!string.IsNullOrEmpty(this.lastMoebiusTraceName))
					{
						num2 = moebiusTrace.GetLastTraceID(this.lastMoebiusTraceName, this.selectConfig.SqlConString, this.selectConfig.TimeOut);
					}
					if (this.moebiusTraceId == -1)
					{
						if (num2 > 0)
						{
							this.moebiusTraceId = num2;
							return;
						}
						this.CreateMoebiusTrace();
						return;
					}
					else
					{
						string lastTracePath2 = moebiusTrace.GetLastTracePath(this.moebiusTraceId, this.selectConfig.SqlConString, this.selectConfig.TimeOut);
						if (!lastTracePath2.StartsWith(this.selectConfig.MoebiusDirectory))
						{
							this.CreateMoebiusTrace();
							return;
						}
					}
				}
			}
			else
			{
				this.collectState.SqlTextSum++;
				this.collectState.SqlTextFaild++;
				ItemCountCollector.SaveItemCount(this.selectConfig.OleDbString, this.selectConfig.TimeOut, CollectItem.SqlText, this.collectState.SqlTextSum, this.collectState.SqlTextFaild);
				this.sqlTextTraceId = -1;
				if (this.TempWatcher.EnableRaisingEvents)
				{
					this.TempWatcher.EnableRaisingEvents = false;
				}
				this.collectState.MoebiusTraceSum++;
				this.collectState.MoebiusTraceFaild++;
				ItemCountCollector.SaveItemCount(this.selectConfig.OleDbString, this.selectConfig.TimeOut, CollectItem.Moebius, this.collectState.MoebiusTraceSum, this.collectState.MoebiusTraceFaild);
				this.moebiusTraceId = -1;
				if (this.MoebiusWatcher.EnableRaisingEvents)
				{
					this.MoebiusWatcher.EnableRaisingEvents = false;
				}
			}
		}

		private void CreateSqlTrace()
		{
			SqlTextCollector sqlTextCollector = new SqlTextCollector();
			if (File.Exists(this.lastSqlTraceName))
			{
				try
				{
					bool flag = sqlTextCollector.SaveData(this.lastSqlTraceName, this.selectConfig.OleDbString, this.selectConfig.SqlConString, this.selectConfig.TimeOut);
					this.collectState.SqlTextSum++;
					if (!flag)
					{
						this.collectState.SqlTextFaild++;
						flag = sqlTextCollector.SaveData(this.lastSqlTraceName, this.selectConfig.OleDbString, this.selectConfig.SqlConString, this.selectConfig.TimeOut);
					}
					File.Delete(this.lastSqlTraceName);
				}
				catch (Exception ex)
				{
					ErrorLog.Write(ex);
					return;
				}
			}
			try
			{
				if (Directory.Exists(this.selectConfig.TempDirectory))
				{
					Directory.Delete(this.selectConfig.TempDirectory, true);
				}
				Directory.CreateDirectory(this.selectConfig.TempDirectory);
			}
			catch (Exception ex2)
			{
				ErrorLog.Write(ex2);
			}
			string text = Path.Combine(this.selectConfig.TempDirectory, this.sqlTextFileName);
			object obj = sqlTextCollector.Start(this.sqlConString, this.selectConfig.TimeOut, text, this.selectConfig.SqlTextValue);
			if (obj != null)
			{
				this.sqlTextTraceId = Convert.ToInt32(obj);
				this.lastSqlTraceName = sqlTextCollector.GetLastTracePath(this.sqlTextTraceId, this.sqlConString, this.selectConfig.TimeOut);
				if (text + ".trc" != this.lastSqlTraceName)
				{
					this.selectConfig.TempDirectory = Path.GetDirectoryName(this.lastSqlTraceName);
					this.sqlTextFileName = Path.GetFileNameWithoutExtension(this.lastSqlTraceName);
				}
				this.TempWatcher.Path = this.selectConfig.TempDirectory;
				this.TempWatcher.EnableRaisingEvents = true;
				return;
			}
			this.sqlTextTraceId = -1;
			if (this.TempWatcher.EnableRaisingEvents)
			{
				this.TempWatcher.EnableRaisingEvents = false;
			}
		}

		private void CreateMoebiusTrace()
		{
			MoebiusTrace moebiusTrace = new MoebiusTrace();
			if (File.Exists(this.lastMoebiusTraceName))
			{
				try
				{
					bool flag = moebiusTrace.SaveData(this.lastMoebiusTraceName, this.selectConfig.OleDbString, this.selectConfig.SqlConString, this.selectConfig.TimeOut);
					this.collectState.MoebiusTraceSum++;
					if (!flag)
					{
						this.collectState.MoebiusTraceFaild++;
						flag = moebiusTrace.SaveData(this.lastMoebiusTraceName, this.selectConfig.OleDbString, this.selectConfig.SqlConString, this.selectConfig.TimeOut);
					}
					File.Delete(this.lastMoebiusTraceName);
				}
				catch (Exception ex)
				{
					ErrorLog.Write(ex);
				}
			}
			try
			{
				if (Directory.Exists(this.selectConfig.MoebiusDirectory))
				{
					Directory.Delete(this.selectConfig.MoebiusDirectory, true);
				}
				Directory.CreateDirectory(this.selectConfig.MoebiusDirectory);
			}
			catch (Exception ex2)
			{
				ErrorLog.Write(ex2);
			}
			string text = Path.Combine(this.selectConfig.MoebiusDirectory, this.moebiusTraceName);
			object obj = moebiusTrace.Start(this.sqlConString, this.selectConfig.TimeOut, text);
			if (obj != null)
			{
				this.moebiusTraceId = Convert.ToInt32(obj);
				this.lastMoebiusTraceName = moebiusTrace.GetLastTracePath(this.moebiusTraceId, this.sqlConString, this.selectConfig.TimeOut);
				if (text + ".trc" != this.lastMoebiusTraceName)
				{
					this.selectConfig.MoebiusDirectory = Path.GetDirectoryName(this.lastMoebiusTraceName);
					this.moebiusTraceName = Path.GetFileNameWithoutExtension(this.lastMoebiusTraceName);
				}
				this.MoebiusWatcher.Path = this.selectConfig.MoebiusDirectory;
				this.MoebiusWatcher.EnableRaisingEvents = true;
				return;
			}
			this.moebiusTraceId = -1;
			if (this.MoebiusWatcher.EnableRaisingEvents)
			{
				this.MoebiusWatcher.EnableRaisingEvents = false;
			}
		}

		private void CollectResult(string errMessage, bool result)
		{
			if (!result)
			{
				this.StopCollection();
				MessageBox.Show(errMessage, "SQL专家云", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private List<DBFilter> GetDBFilterList(string instanceName, List<DBFilter> filterList)
		{
			List<DBFilter> list = new List<DBFilter>();
			if (filterList.Count > 0)
			{
				Dictionary<string, Dictionary<int, List<string>>> dictionary = new Dictionary<string, Dictionary<int, List<string>>>();
				foreach (DBFilter current in filterList)
				{
					if (current.InstanceName == instanceName)
					{
						if (!dictionary.ContainsKey(current.DBName))
						{
							dictionary.Add(current.DBName, new Dictionary<int, List<string>>());
						}
						if (!dictionary[current.DBName].ContainsKey(current.DBType))
						{
							dictionary[current.DBName].Add(current.DBType, new List<string>());
						}
						dictionary[current.DBName][current.DBType].Add(current.FilterString);
					}
				}
				if (dictionary.Count > 0)
				{
					foreach (KeyValuePair<string, Dictionary<int, List<string>>> current2 in dictionary)
					{
						foreach (KeyValuePair<int, List<string>> current3 in current2.Value)
						{
							StringBuilder stringBuilder = new StringBuilder();
							foreach (string current4 in current3.Value)
							{
								stringBuilder.Append(current4);
								stringBuilder.Append(";");
							}
							DBFilter dBFilter = new DBFilter();
							dBFilter.InstanceName = instanceName;
							dBFilter.DBName = current2.Key;
							dBFilter.DBType = current3.Key;
							dBFilter.FilterString = stringBuilder.ToString();
							if (dBFilter.FilterString.EndsWith(";"))
							{
								dBFilter.FilterString = dBFilter.FilterString.Remove(dBFilter.FilterString.Length - 1);
							}
							list.Add(dBFilter);
						}
					}
				}
			}
			return list;
		}

		private DBFilter GetFilter(string instanceName, string dbName, int dbType, List<DBFilter> dbFilterList)
		{
			if (dbFilterList != null && dbFilterList.Count > 0)
			{
				foreach (DBFilter current in dbFilterList)
				{
					if (current.InstanceName == instanceName && current.DBName == dbName && current.DBType == dbType)
					{
						return current;
					}
				}
			}
			return null;
		}

		private void TempWatcher_Created(object sender, FileSystemEventArgs e)
		{
			if (this.selectConfig.HasSqlText && e.Name.StartsWith(this.sqlTextFileName + "_"))
			{
				int length = this.sqlTextFileName.Length;
				if (e.Name.Length > length + 5)
				{
					int num = -1;
					bool flag = int.TryParse(e.Name.Substring(length + 1, e.Name.IndexOf(".") - length - 1), out num);
					if (flag)
					{
						string filePath = string.Empty;
						if (num == 1)
						{
							filePath = Path.Combine(this.selectConfig.TempDirectory, this.sqlTextFileName + ".trc");
						}
						else
						{
							filePath = Path.Combine(this.selectConfig.TempDirectory, string.Concat(new object[]
							{
								this.sqlTextFileName,
								"_",
								num - 1,
								".trc"
							}));
						}
						SqlTextCollector sqlTextCollector = new SqlTextCollector();
						bool flag2 = sqlTextCollector.SaveData(filePath, this.selectConfig.OleDbString, this.selectConfig.SqlConString, this.selectConfig.TimeOut);
						this.collectState.SqlTextSum++;
						if (!flag2)
						{
							this.collectState.SqlTextFaild++;
							flag2 = sqlTextCollector.SaveData(filePath, this.selectConfig.OleDbString, this.selectConfig.SqlConString, this.selectConfig.TimeOut);
						}
					}
				}
			}
		}

		private void MoebiusWatcher_Created(object sender, FileSystemEventArgs e)
		{
			if (this.selectConfig.HasMoebius && e.Name.StartsWith(this.moebiusTraceName + "_"))
			{
				int length = this.moebiusTraceName.Length;
				if (e.Name.Length > length + 5)
				{
					int num = -1;
					bool flag = int.TryParse(e.Name.Substring(length + 1, e.Name.IndexOf(".") - length - 1), out num);
					if (flag)
					{
						string filePath = string.Empty;
						if (num == 1)
						{
							filePath = Path.Combine(this.selectConfig.MoebiusDirectory, this.moebiusTraceName + ".trc");
						}
						else
						{
							filePath = Path.Combine(this.selectConfig.MoebiusDirectory, string.Concat(new object[]
							{
								this.moebiusTraceName,
								"_",
								num - 1,
								".trc"
							}));
						}
						MoebiusTrace moebiusTrace = new MoebiusTrace();
						bool flag2 = moebiusTrace.SaveData(filePath, this.selectConfig.OleDbString, this.selectConfig.SqlConString, this.selectConfig.TimeOut);
						this.collectState.MoebiusTraceSum++;
						if (!flag2)
						{
							this.collectState.MoebiusTraceFaild++;
							flag2 = moebiusTrace.SaveData(filePath, this.selectConfig.OleDbString, this.selectConfig.SqlConString, this.selectConfig.TimeOut);
						}
						if (num > 100)
						{
							moebiusTrace.Stop(this.moebiusTraceId, this.sqlConString, this.selectConfig.TimeOut);
							this.moebiusTraceId = -1;
							ErrorLog.Info("moebius trace 文件数量达到100个，trace已停止");
						}
					}
				}
			}
		}

		private string GetLogDirectory(string conStr)
		{
			using (SqlConnection sqlConnection = new SqlConnection(conStr))
			{
				SqlCommand sqlCommand = sqlConnection.CreateCommand();
				sqlCommand.CommandText = "SELECT  physical_name FROM  sys.database_files WHERE  [type] = 0";
				sqlConnection.Open();
				object obj = sqlCommand.ExecuteScalar();
				if (obj != null)
				{
					string text = obj.ToString();
					if (text.Contains("DATA"))
					{
						return Path.Combine(text.Substring(0, text.LastIndexOf("DATA")), "LOG");
					}
				}
				sqlConnection.Close();
			}
			return string.Empty;
		}

		private void FrmMain_Resize(object sender, EventArgs e)
		{
			if (base.WindowState == FormWindowState.Minimized)
			{
				this.cbDBList.HideDropList();
			}
		}

		private void cbAgree_CheckedChanged(object sender, EventArgs e)
		{
			this.btnCollector.Enabled = this.cbAgree.Checked;
		}

		private void lblAgree_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			FrmAgree frmAgree = new FrmAgree();
			frmAgree.ShowDialog();
		}

		private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (Util.startFlag)
			{
				e.Cancel = true;
				MessageBox.Show("正在收集，请先停止收集。", "SQL专家云", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
			}
		}

		private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			base.ShowInTaskbar = true;
			base.WindowState = FormWindowState.Normal;
			base.Focus();
		}

		private void tsOpen_Click(object sender, EventArgs e)
		{
			this.notifyIcon1_MouseDoubleClick(null, null);
		}

		private void tsExit_Click(object sender, EventArgs e)
		{
			if (Util.startFlag)
			{
				Application.Exit();
				return;
			}
			base.Close();
			base.Dispose();
		}

		private void CheckFileSize(string filePath)
		{
			if (File.Exists(filePath))
			{
				FileInfo fileInfo = new FileInfo(filePath);
				double num = (double)fileInfo.Length / 1024.0 / 1024.0;
				if (num >= (double)this.fileSize)
				{
					this.StopCollection();
				}
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.components = new Container();
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(FrmMain));
			this.pnlTitle = new Panel();
			this.pnlBottom = new Panel();
			this.btnEdit = new PictureBox();
			this.lblAgree = new LinkLabel();
			this.cbAgree = new CheckBox();
			this.btnCollector = new PictureBox();
			this.pnlContent = new Panel();
			this.pnlDBCollector = new Panel();
			this.btnSavePath = new PictureBox();
			this.txtFilePath = new TextBox();
			this.label7 = new Label();
			this.cbDBList = new ucComboBox();
			this.label5 = new Label();
			this.pnlAuthent = new Panel();
			this.txtPassWord = new TextBox();
			this.txtUserName = new TextBox();
			this.label4 = new Label();
			this.label3 = new Label();
			this.label2 = new Label();
			this.cbInstanceName = new ComboBox();
			this.cbAuthent = new ComboBox();
			this.label1 = new Label();
			this.imageList1 = new ImageList(this.components);
			this.collectionAysc = new BackgroundWorker();
			this.TempWatcher = new FileSystemWatcher();
			this.toolTip1 = new ToolTip(this.components);
			this.MoebiusWatcher = new FileSystemWatcher();
			this.pnlBottom.SuspendLayout();
			((ISupportInitialize)this.btnEdit).BeginInit();
			((ISupportInitialize)this.btnCollector).BeginInit();
			this.pnlContent.SuspendLayout();
			this.pnlDBCollector.SuspendLayout();
			((ISupportInitialize)this.btnSavePath).BeginInit();
			this.pnlAuthent.SuspendLayout();
			((ISupportInitialize)this.TempWatcher).BeginInit();
			((ISupportInitialize)this.MoebiusWatcher).BeginInit();
			base.SuspendLayout();
			this.pnlTitle.BackColor = Color.Transparent;
			this.pnlTitle.BackgroundImage = (Image)componentResourceManager.GetObject("pnlTitle.BackgroundImage");
			this.pnlTitle.Dock = DockStyle.Top;
			this.pnlTitle.Location = new Point(0, 0);
			this.pnlTitle.Name = "pnlTitle";
			this.pnlTitle.Size = new Size(621, 86);
			this.pnlTitle.TabIndex = 0;
			this.pnlBottom.BackColor = Color.Transparent;
			this.pnlBottom.BackgroundImage = (Image)componentResourceManager.GetObject("pnlBottom.BackgroundImage");
			this.pnlBottom.Controls.Add(this.btnEdit);
			this.pnlBottom.Controls.Add(this.lblAgree);
			this.pnlBottom.Controls.Add(this.cbAgree);
			this.pnlBottom.Controls.Add(this.btnCollector);
			this.pnlBottom.Dock = DockStyle.Bottom;
			this.pnlBottom.Location = new Point(0, 324);
			this.pnlBottom.Name = "pnlBottom";
			this.pnlBottom.Size = new Size(621, 45);
			this.pnlBottom.TabIndex = 1;
			this.btnEdit.Cursor = Cursors.Hand;
			this.btnEdit.Image = (Image)componentResourceManager.GetObject("btnEdit.Image");
			this.btnEdit.Location = new Point(460, 10);
			this.btnEdit.Name = "btnEdit";
			this.btnEdit.Size = new Size(26, 26);
			this.btnEdit.SizeMode = PictureBoxSizeMode.CenterImage;
			this.btnEdit.TabIndex = 8;
			this.btnEdit.TabStop = false;
			this.toolTip1.SetToolTip(this.btnEdit, "配置");
			this.btnEdit.Click += new EventHandler(this.btnEdit_Click);
			this.lblAgree.AutoSize = true;
			this.lblAgree.LinkBehavior = LinkBehavior.NeverUnderline;
			this.lblAgree.Location = new Point(103, 19);
			this.lblAgree.Name = "lblAgree";
			this.lblAgree.Size = new Size(77, 12);
			this.lblAgree.TabIndex = 7;
			this.lblAgree.TabStop = true;
			this.lblAgree.Text = "软件许可协议";
			this.lblAgree.VisitedLinkColor = Color.Blue;
			this.lblAgree.LinkClicked += new LinkLabelLinkClickedEventHandler(this.lblAgree_LinkClicked);
			this.cbAgree.AutoSize = true;
			this.cbAgree.Checked = true;
			this.cbAgree.CheckState = CheckState.Checked;
			this.cbAgree.Location = new Point(13, 17);
			this.cbAgree.Name = "cbAgree";
			this.cbAgree.Size = new Size(84, 16);
			this.cbAgree.TabIndex = 6;
			this.cbAgree.Text = "阅读并同意";
			this.cbAgree.UseVisualStyleBackColor = false;
			this.cbAgree.CheckedChanged += new EventHandler(this.cbAgree_CheckedChanged);
			this.btnCollector.Cursor = Cursors.Hand;
			this.btnCollector.Image = (Image)componentResourceManager.GetObject("btnCollector.Image");
			this.btnCollector.Location = new Point(215, 7);
			this.btnCollector.Name = "btnCollector";
			this.btnCollector.Size = new Size(234, 34);
			this.btnCollector.SizeMode = PictureBoxSizeMode.AutoSize;
			this.btnCollector.TabIndex = 0;
			this.btnCollector.TabStop = false;
			this.btnCollector.Click += new EventHandler(this.btnCollector_Click);
			this.pnlContent.BackColor = Color.White;
			this.pnlContent.Controls.Add(this.pnlDBCollector);
			this.pnlContent.Dock = DockStyle.Fill;
			this.pnlContent.Location = new Point(0, 86);
			this.pnlContent.Name = "pnlContent";
			this.pnlContent.Size = new Size(621, 238);
			this.pnlContent.TabIndex = 2;
			this.pnlDBCollector.Controls.Add(this.btnSavePath);
			this.pnlDBCollector.Controls.Add(this.txtFilePath);
			this.pnlDBCollector.Controls.Add(this.label7);
			this.pnlDBCollector.Controls.Add(this.cbDBList);
			this.pnlDBCollector.Controls.Add(this.label5);
			this.pnlDBCollector.Controls.Add(this.pnlAuthent);
			this.pnlDBCollector.Controls.Add(this.label2);
			this.pnlDBCollector.Controls.Add(this.cbInstanceName);
			this.pnlDBCollector.Controls.Add(this.cbAuthent);
			this.pnlDBCollector.Controls.Add(this.label1);
			this.pnlDBCollector.Dock = DockStyle.Fill;
			this.pnlDBCollector.Location = new Point(0, 0);
			this.pnlDBCollector.Name = "pnlDBCollector";
			this.pnlDBCollector.Size = new Size(621, 238);
			this.pnlDBCollector.TabIndex = 0;
			this.btnSavePath.Cursor = Cursors.Hand;
			this.btnSavePath.Image = (Image)componentResourceManager.GetObject("btnSavePath.Image");
			this.btnSavePath.Location = new Point(476, 165);
			this.btnSavePath.Name = "btnSavePath";
			this.btnSavePath.Size = new Size(16, 16);
			this.btnSavePath.SizeMode = PictureBoxSizeMode.AutoSize;
			this.btnSavePath.TabIndex = 43;
			this.btnSavePath.TabStop = false;
			this.toolTip1.SetToolTip(this.btnSavePath, "修改存储路径");
			this.btnSavePath.Click += new EventHandler(this.btnSavePath_Click);
			this.txtFilePath.Location = new Point(220, 164);
			this.txtFilePath.Name = "txtFilePath";
			this.txtFilePath.Size = new Size(250, 21);
			this.txtFilePath.TabIndex = 39;
			this.label7.AutoSize = true;
			this.label7.Location = new Point(138, 169);
			this.label7.Name = "label7";
			this.label7.Size = new Size(65, 12);
			this.label7.TabIndex = 38;
			this.label7.Text = "存储路径：";
			this.cbDBList.Location = new Point(220, 135);
			this.cbDBList.Name = "cbDBList";
			this.cbDBList.Size = new Size(250, 20);
			this.cbDBList.TabIndex = 36;
			this.cbDBList.DBChecked += new EventHandler(this.cbDBList_DBChecked);
			this.label5.AutoSize = true;
			this.label5.Location = new Point(138, 138);
			this.label5.Name = "label5";
			this.label5.Size = new Size(77, 12);
			this.label5.TabIndex = 34;
			this.label5.Text = "目标数据库：";
			this.pnlAuthent.Controls.Add(this.txtPassWord);
			this.pnlAuthent.Controls.Add(this.txtUserName);
			this.pnlAuthent.Controls.Add(this.label4);
			this.pnlAuthent.Controls.Add(this.label3);
			this.pnlAuthent.Location = new Point(140, 64);
			this.pnlAuthent.Name = "pnlAuthent";
			this.pnlAuthent.Size = new Size(344, 61);
			this.pnlAuthent.TabIndex = 33;
			this.txtPassWord.Location = new Point(106, 35);
			this.txtPassWord.Name = "txtPassWord";
			this.txtPassWord.PasswordChar = '●';
			this.txtPassWord.Size = new Size(224, 21);
			this.txtPassWord.TabIndex = 3;
			this.txtUserName.Location = new Point(106, 6);
			this.txtUserName.Name = "txtUserName";
			this.txtUserName.Size = new Size(224, 21);
			this.txtUserName.TabIndex = 2;
			this.label4.AutoSize = true;
			this.label4.Location = new Point(32, 40);
			this.label4.Name = "label4";
			this.label4.Size = new Size(41, 12);
			this.label4.TabIndex = 1;
			this.label4.Text = "密码：";
			this.label3.AutoSize = true;
			this.label3.Location = new Point(32, 13);
			this.label3.Name = "label3";
			this.label3.Size = new Size(53, 12);
			this.label3.TabIndex = 0;
			this.label3.Text = "用户名：";
			this.label2.AutoSize = true;
			this.label2.Location = new Point(138, 44);
			this.label2.Name = "label2";
			this.label2.Size = new Size(65, 12);
			this.label2.TabIndex = 32;
			this.label2.Text = "身份验证：";
			this.cbInstanceName.FormattingEnabled = true;
			this.cbInstanceName.Location = new Point(220, 12);
			this.cbInstanceName.Name = "cbInstanceName";
			this.cbInstanceName.Size = new Size(250, 20);
			this.cbInstanceName.TabIndex = 0;
			this.cbInstanceName.SelectedIndexChanged += new EventHandler(this.cbInstanceName_SelectedIndexChanged);
			this.cbInstanceName.TextChanged += new EventHandler(this.cbInstanceName_TextChanged);
			this.cbAuthent.DropDownStyle = ComboBoxStyle.DropDownList;
			this.cbAuthent.FormattingEnabled = true;
			this.cbAuthent.Items.AddRange(new object[]
			{
				"Windows 身份验证",
				"SQL Server验证"
			});
			this.cbAuthent.Location = new Point(220, 40);
			this.cbAuthent.Name = "cbAuthent";
			this.cbAuthent.Size = new Size(250, 20);
			this.cbAuthent.TabIndex = 1;
			this.cbAuthent.SelectedIndexChanged += new EventHandler(this.cbAuthent_SelectedIndexChanged);
			this.label1.AutoSize = true;
			this.label1.Location = new Point(136, 18);
			this.label1.Name = "label1";
			this.label1.Size = new Size(65, 12);
			this.label1.TabIndex = 31;
			this.label1.Text = "实例名称：";
			this.imageList1.ImageStream = (ImageListStreamer)componentResourceManager.GetObject("imageList1.ImageStream");
			this.imageList1.TransparentColor = Color.Empty;
			this.imageList1.Images.SetKeyName(0, "开始收集btn.png");
			this.imageList1.Images.SetKeyName(1, "停止收集btn.png");
			this.collectionAysc.WorkerSupportsCancellation = true;
			this.collectionAysc.DoWork += new DoWorkEventHandler(this.collectionAysc_DoWork);
			this.TempWatcher.EnableRaisingEvents = true;
			this.TempWatcher.NotifyFilter = NotifyFilters.FileName;
			this.TempWatcher.SynchronizingObject = this;
			this.TempWatcher.Created += new FileSystemEventHandler(this.TempWatcher_Created);
			this.toolTip1.AutoPopDelay = 5000;
			this.toolTip1.InitialDelay = 800;
			this.toolTip1.ReshowDelay = 100;
			this.MoebiusWatcher.EnableRaisingEvents = true;
			this.MoebiusWatcher.NotifyFilter = NotifyFilters.FileName;
			this.MoebiusWatcher.SynchronizingObject = this;
			this.MoebiusWatcher.Created += new FileSystemEventHandler(this.MoebiusWatcher_Created);
			base.AutoScaleMode = AutoScaleMode.None;
			base.AutoSizeMode = AutoSizeMode.GrowAndShrink;
			base.ClientSize = new Size(621, 369);
			base.Controls.Add(this.pnlContent);
			base.Controls.Add(this.pnlBottom);
			base.Controls.Add(this.pnlTitle);
			this.Cursor = Cursors.Default;
			base.FormBorderStyle = FormBorderStyle.FixedSingle;
			base.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			base.MaximizeBox = false;
			base.Name = "FrmMain";
			base.StartPosition = FormStartPosition.CenterScreen;
			this.Text = "SQL专家云收集工具";
			base.FormClosing += new FormClosingEventHandler(this.FrmMain_FormClosing);
			base.Load += new EventHandler(this.FrmCollector_Load);
			base.Resize += new EventHandler(this.FrmMain_Resize);
			this.pnlBottom.ResumeLayout(false);
			this.pnlBottom.PerformLayout();
			((ISupportInitialize)this.btnEdit).EndInit();
			((ISupportInitialize)this.btnCollector).EndInit();
			this.pnlContent.ResumeLayout(false);
			this.pnlDBCollector.ResumeLayout(false);
			this.pnlDBCollector.PerformLayout();
			((ISupportInitialize)this.btnSavePath).EndInit();
			this.pnlAuthent.ResumeLayout(false);
			this.pnlAuthent.PerformLayout();
			((ISupportInitialize)this.TempWatcher).EndInit();
			((ISupportInitialize)this.MoebiusWatcher).EndInit();
			base.ResumeLayout(false);
		}
	}
}
