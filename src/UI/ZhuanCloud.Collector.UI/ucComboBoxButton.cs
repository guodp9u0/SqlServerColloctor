using System;
using System.Windows.Forms;

namespace ZhuanCloud.Collector.UI
{
	internal class ucComboBoxButton : Button
	{
		private ButtonState state;

		public ucComboBoxButton()
		{
			base.SetStyle(ControlStyles.UserPaint, true);
			base.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
			base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
		}

		protected override void OnMouseUp(MouseEventArgs mevent)
		{
			this.state = ButtonState.Normal;
			base.OnMouseUp(mevent);
		}

		protected override void OnPaint(PaintEventArgs pevent)
		{
			base.OnPaint(pevent);
			ControlPaint.DrawComboButton(pevent.Graphics, 0, 0, base.Width, base.Height, this.state);
		}
	}
}
