using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ZhuanCloud.Collector.UI
{
	internal class FrmCreateFlag : Form
	{
		private IContainer components;

		private Label label1;

		private Label label2;

		private TextBox txtFlagName;

		private TextBox txtDescription;

		private Button btnOk;

		private Button btnCancel;

		private List<CollectFlag> flagList;

		private CollectFlag collectFlag;

		public CollectFlag CollectFlag
		{
			get
			{
				return this.collectFlag;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(FrmCreateFlag));
			this.label1 = new Label();
			this.label2 = new Label();
			this.txtFlagName = new TextBox();
			this.txtDescription = new TextBox();
			this.btnOk = new Button();
			this.btnCancel = new Button();
			base.SuspendLayout();
			this.label1.AutoSize = true;
			this.label1.Location = new Point(15, 17);
			this.label1.Name = "label1";
			this.label1.Size = new Size(65, 12);
			this.label1.TabIndex = 0;
			this.label1.Text = "标签名称：";
			this.label2.AutoSize = true;
			this.label2.Location = new Point(17, 68);
			this.label2.Name = "label2";
			this.label2.Size = new Size(41, 12);
			this.label2.TabIndex = 1;
			this.label2.Text = "描述：";
			this.txtFlagName.Location = new Point(19, 35);
			this.txtFlagName.Name = "txtFlagName";
			this.txtFlagName.Size = new Size(331, 21);
			this.txtFlagName.TabIndex = 2;
			this.txtDescription.Location = new Point(19, 84);
			this.txtDescription.Multiline = true;
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new Size(331, 94);
			this.txtDescription.TabIndex = 3;
			this.btnOk.Location = new Point(200, 199);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new Size(70, 23);
			this.btnOk.TabIndex = 4;
			this.btnOk.Text = "确定";
			this.btnOk.UseVisualStyleBackColor = true;
			this.btnOk.Click += new EventHandler(this.btnOk_Click);
			this.btnCancel.Location = new Point(280, 199);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new Size(70, 23);
			this.btnCancel.TabIndex = 5;
			this.btnCancel.Text = "取消";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
			base.AutoScaleMode = AutoScaleMode.None;
			base.AutoSizeMode = AutoSizeMode.GrowAndShrink;
			base.ClientSize = new Size(368, 231);
			base.Controls.Add(this.btnCancel);
			base.Controls.Add(this.btnOk);
			base.Controls.Add(this.txtDescription);
			base.Controls.Add(this.txtFlagName);
			base.Controls.Add(this.label2);
			base.Controls.Add(this.label1);
			base.FormBorderStyle = FormBorderStyle.FixedSingle;
			base.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "FrmCreateFlag";
			base.StartPosition = FormStartPosition.CenterScreen;
			this.Text = "创建标签";
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		public FrmCreateFlag(List<CollectFlag> flagList)
		{
			this.InitializeComponent();
			this.flagList = flagList;
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			string text = this.txtFlagName.Text.Trim();
			if (string.IsNullOrEmpty(text))
			{
				MessageBox.Show("标签名称不能为空", "SQL专家云", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}
			if (this.Exists(text))
			{
				MessageBox.Show("标签名称已存在", "SQL专家云", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}
			if (text.Length > 200)
			{
				MessageBox.Show("标签名称长度不能大于200", "SQL专家云", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}
			if (!this.IsPassed(text))
			{
				MessageBox.Show("标签名称不能包含' \" “” ~ ` ^ * &  / \\ : * ? <> | 空格 特殊字符", "SQL专家云", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}
			if (!this.NameValid(text))
			{
				MessageBox.Show("标签名称不能以_Num 结尾", "SQL专家云", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}
			this.collectFlag = new CollectFlag(text, DateTime.Now, this.txtDescription.Text.Trim());
			base.DialogResult = DialogResult.OK;
		}

		private bool Exists(string name)
		{
			if (this.flagList != null && this.flagList.Count > 0)
			{
				foreach (CollectFlag current in this.flagList)
				{
					if (current.Name == name)
					{
						return true;
					}
				}
				return false;
			}
			return false;
		}

		private bool IsPassed(string flagName)
		{
			return !Regex.IsMatch(flagName, "['~`“”\\^\\*\\&\\\"\\/\\\\:\\*\\?<> |]");
		}

		private bool NameValid(string flagName)
		{
			return !Regex.IsMatch(flagName, "[\\w\\W]*_\\d{1,}");
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.collectFlag = null;
			base.DialogResult = DialogResult.Cancel;
		}
	}
}
