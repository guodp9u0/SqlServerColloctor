using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ZhuanCloud.Collector.UI
{
	public class ucComboBox : UserControl
	{
		private IContainer components;

		private TextBox txtView;

		private ucComboBoxButton btnSelect;

		private Form frmCheck;

		private CheckedListBox checkList;

		private List<string> dbList = new List<string>();

		private bool frmIsVisibled;

		public event EventHandler DBChecked;

		public List<string> DBList
		{
			get
			{
				return this.dbList;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			this.txtView = new TextBox();
			this.btnSelect = new ucComboBoxButton();
			base.SuspendLayout();
			this.txtView.BackColor = Color.White;
			this.txtView.Location = new Point(0, 0);
			this.txtView.Name = "txtView";
			this.txtView.ReadOnly = true;
			this.txtView.Size = new Size(250, 21);
			this.txtView.TabIndex = 0;
			this.txtView.Text = "请选择数据库";
			this.txtView.Click += new EventHandler(this.txtView_Click);
			this.txtView.Leave += new EventHandler(this.txtView_Leave);
			this.btnSelect.Location = new Point(231, 2);
			this.btnSelect.Name = "btnSelect";
			this.btnSelect.Size = new Size(17, 17);
			this.btnSelect.TabIndex = 1;
			this.btnSelect.Text = "ucComboBoxButton1";
			this.btnSelect.UseVisualStyleBackColor = true;
			this.btnSelect.Click += new EventHandler(this.btnSelect_Click);
			base.AutoScaleMode = AutoScaleMode.None;
			base.Controls.Add(this.btnSelect);
			base.Controls.Add(this.txtView);
			base.Name = "ucComboBox";
			base.Size = new Size(250, 140);
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		public ucComboBox()
		{
			this.InitializeComponent();
			base.Height = this.txtView.Height;
			this.checkList = new CheckedListBox();
			this.checkList.CheckOnClick = true;
			this.checkList.BorderStyle = BorderStyle.FixedSingle;
			this.checkList.SelectedIndexChanged += new EventHandler(this.checkList_SelectedIndexChanged);
			this.frmCheck = new Form();
			this.frmCheck.FormBorderStyle = FormBorderStyle.None;
			this.frmCheck.StartPosition = FormStartPosition.Manual;
			this.frmCheck.BackColor = Color.White;
			this.frmCheck.ShowInTaskbar = false;
			this.frmCheck.Size = new Size(this.txtView.Width, 150);
			this.frmCheck.Location = new Point(this.txtView.Location.X, this.txtView.Location.Y + this.txtView.Height);
			this.frmCheck.Controls.Add(this.checkList);
			this.checkList.Location = new Point(0, 0);
			this.checkList.Size = new Size(this.frmCheck.Width, this.frmCheck.Height);
			this.frmCheck.LostFocus += new EventHandler(this.frmCheck_LostFocus);
			this.frmCheck.Deactivate += new EventHandler(this.frmCheck_Deactivate);
		}

		private void frmCheck_Deactivate(object sender, EventArgs e)
		{
			this.frmCheck.Hide();
		}

		private void checkList_SelectedIndexChanged(object sender, EventArgs e)
		{
			int count = this.checkList.CheckedItems.Count;
			if (count > 0)
			{
				this.txtView.Text = "已选择" + count + "个数据库";
				this.dbList.Clear();
			    IEnumerator enumerator = this.checkList.CheckedItems.GetEnumerator();
				{
					while (enumerator.MoveNext())
					{
						string item = (string)enumerator.Current;
						this.dbList.Add(item);
					}
					return;
				}
			}
			this.txtView.Text = "请选择数据库";
		}

		private void btnSelect_Click(object sender, EventArgs e)
		{
			if (!this.frmIsVisibled)
			{
				Rectangle rectangle = base.RectangleToScreen(base.ClientRectangle);
				this.frmCheck.Location = new Point(rectangle.X, rectangle.Y + this.txtView.Height);
				this.frmCheck.Show();
				this.frmCheck.BringToFront();
				this.checkList.Select();
				this.checkList.Focus();
				this.frmIsVisibled = true;
			}
			else
			{
				this.txtView.Select();
				this.frmIsVisibled = false;
				this.dbList.Clear();
				if (this.checkList.CheckedItems.Count > 0)
				{
					foreach (string item in this.checkList.CheckedItems)
					{
						this.dbList.Add(item);
					}
				}
			}
			if (this.DBChecked != null)
			{
				this.DBChecked(sender, e);
			}
		}

		private void txtView_Click(object sender, EventArgs e)
		{
			this.btnSelect_Click(this.btnSelect, e);
		}

		private void frmCheck_LostFocus(object sender, EventArgs e)
		{
			this.frmCheck.Hide();
		}

		public void Bind(List<string> dbList)
		{
			this.checkList.Items.Clear();
			if (dbList != null && dbList.Count > 0)
			{
				foreach (string current in dbList)
				{
					this.checkList.Items.Add(current);
				}
				this.checkList.SelectedIndex = -1;
			}
		}

		public void Clear()
		{
			this.checkList.Items.Clear();
			this.dbList.Clear();
			this.txtView.Text = "请选择数据库";
		}

		public bool HasItemChecked()
		{
			return this.checkList.CheckedItems.Count > 0;
		}

		private void txtView_Leave(object sender, EventArgs e)
		{
			this.frmCheck.Hide();
		}

		public void HideDropList()
		{
			this.frmCheck.Hide();
		}
	}
}
