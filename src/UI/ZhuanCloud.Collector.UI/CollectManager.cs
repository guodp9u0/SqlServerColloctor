using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Management;
using System.Net;
using System.ServiceProcess;

namespace ZhuanCloud.Collector.UI
{
	public class CollectManager
	{
		public bool IsPassed(string instanceName, bool isWindowsAuthent, string userName, string passWord, List<string> dbList, string directory, out string msg)
		{
			msg = string.Empty;
			if (string.IsNullOrEmpty(instanceName))
			{
				msg = "请输入正确的实例名称";
				return false;
			}
			if (dbList == null)
			{
				msg = "请选择一个目标数据库";
				return false;
			}
			if (dbList.Count == 0)
			{
				msg = "请选择一个目标数据库";
				return false;
			}
			if (string.IsNullOrEmpty(directory))
			{
				msg = "请选择存储路径";
				return false;
			}
			if (!directory.Contains("\\") && !Directory.Exists(directory))
			{
				msg = "存储路径不正确，请更换目录";
				return false;
			}
			SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder();
			sqlConnectionStringBuilder.DataSource = instanceName;
			sqlConnectionStringBuilder.InitialCatalog = "master";
			sqlConnectionStringBuilder.IntegratedSecurity = isWindowsAuthent;
			sqlConnectionStringBuilder.ApplicationName = "ZhuanCloud";
			if (!sqlConnectionStringBuilder.IntegratedSecurity)
			{
				sqlConnectionStringBuilder.UserID = userName;
				sqlConnectionStringBuilder.Password = passWord;
			}
			string connStr = this.GetConnStr(instanceName, isWindowsAuthent, userName, passWord, out msg);
			if (string.IsNullOrEmpty(connStr))
			{
				return false;
			}
			bool flag = false;
			if (Util.IsClustered(connStr))
			{
				string text = string.Empty;
				if (instanceName.Contains("\\"))
				{
					string[] array = instanceName.Split(new char[]
					{
						'\\'
					});
					if (array.Length > 0)
					{
						text = array[0];
					}
				}
				else if (instanceName.Contains(","))
				{
					text = instanceName.Substring(0, instanceName.IndexOf(','));
				}
				else
				{
					text = instanceName;
				}
				if (string.IsNullOrEmpty(text))
				{
					flag = false;
					msg = "无法获取IP地址";
					return false;
				}
				IPAddress iPAddress = null;
				if (!IPAddress.TryParse(text, out iPAddress))
				{
					flag = false;
					msg = "集群化数据库请使用IP地址连接";
					return false;
				}
				if (Util.IsLocalIP(iPAddress.ToString()))
				{
					flag = true;
				}
			}
			else
			{
				try
				{
					string text2 = string.Empty;
					using (SqlConnection sqlConnection = new SqlConnection(connStr))
					{
						SqlCommand sqlCommand = sqlConnection.CreateCommand();
						sqlCommand.CommandText = "SELECT  physical_name FROM  sys.master_files WHERE  name = 'master'";
						sqlConnection.Open();
						text2 = sqlCommand.ExecuteScalar().ToString().ToUpper();
						sqlConnection.Close();
					}
					if (string.IsNullOrEmpty(text2))
					{
						msg = "执行命令发生错误";
						bool result = false;
						return result;
					}
					string directoryName = Path.GetDirectoryName(text2);
					if (!Directory.Exists(directoryName))
					{
						msg = "请连接本地数据库";
						bool result = false;
						return result;
					}
					string path = Guid.NewGuid().ToString("N");
					string text3 = Path.Combine(directoryName, path);
					while (File.Exists(text3))
					{
						path = Guid.NewGuid().ToString("N");
						text3 = Path.Combine(directoryName, path);
					}
					FileStream fileStream = File.Create(text3);
					fileStream.Close();
					fileStream.Dispose();
					int num = -1;
					using (SqlConnection sqlConnection2 = new SqlConnection(connStr))
					{
						SqlCommand sqlCommand2 = sqlConnection2.CreateCommand();
						sqlCommand2.CommandText = string.Format("EXEC xp_fileexist '{0}'", text3);
						sqlConnection2.Open();
						num = Convert.ToInt32(sqlCommand2.ExecuteScalar());
						sqlConnection2.Close();
					}
					if (num == 1)
					{
						flag = true;
					}
					try
					{
						File.Delete(text3);
					}
					catch
					{
					}
				}
				catch (Exception ex)
				{
					msg = ex.Message;
					bool result = false;
					return result;
				}
			}
			if (!flag)
			{
				msg = "请连接本地数据库";
				return false;
			}
			msg = connStr;
			return true;
		}

		public string GetConnStr(string instanceName, bool isWindowsAuthent, string userName, string passWord, out string message)
		{
			message = string.Empty;
			SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder();
			sqlConnectionStringBuilder.DataSource = instanceName;
			sqlConnectionStringBuilder.InitialCatalog = "master";
			sqlConnectionStringBuilder.IntegratedSecurity = isWindowsAuthent;
			sqlConnectionStringBuilder.ApplicationName = "ZhuanCloud";
			if (!sqlConnectionStringBuilder.IntegratedSecurity)
			{
				sqlConnectionStringBuilder.UserID = userName;
				sqlConnectionStringBuilder.Password = passWord;
			}
			string text = sqlConnectionStringBuilder.ToString();
			string result;
			try
			{
				if (!Util.IsSysadmin(text))
				{
					message = "登录帐户没有足够的权限，请使用sysadmin权限的帐户连接";
					result = string.Empty;
				}
				else
				{
					result = text;
				}
			}
			catch (Exception ex)
			{
				message = ex.Message;
				result = string.Empty;
			}
			return result;
		}

		public List<string> GetDBList(string conStr)
		{
			List<string> list = new List<string>();
			List<string> list2 = new List<string>();
			list2.Add("master");
			list2.Add("tempdb");
			list2.Add("model");
			list2.Add("msdb");
			using (SqlConnection sqlConnection = new SqlConnection(conStr))
			{
				SqlCommand sqlCommand = sqlConnection.CreateCommand();
				sqlCommand.CommandText = "SELECT name FROM sys.databases Where [state] = 0 AND user_access = 0 ORDER BY name";
				sqlConnection.Open();
				SqlDataReader sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
				while (sqlDataReader.Read())
				{
					string item = sqlDataReader[0].ToString();
					if (!list2.Contains(item))
					{
						list.Add(item);
					}
				}
				sqlConnection.Close();
			}
			return list;
		}

		public List<SQLService> GetSQLServiceList()
		{
			List<SQLService> list = new List<SQLService>();
			string hostName = Dns.GetHostName();
			ServiceController[] services = ServiceController.GetServices();
			ServiceController[] array = services;
			for (int i = 0; i < array.Length; i++)
			{
				ServiceController serviceController = array[i];
				if (serviceController.ServiceName.ToUpper().IndexOf("MSSQL$") != -1)
				{
					SQLService sQLService = new SQLService();
					sQLService.ServiceName = serviceController.ServiceName;
					sQLService.InstanceName = hostName + '\\' + serviceController.ServiceName.Substring(serviceController.ServiceName.IndexOf("$") + 1);
					string serviceAccount = this.GetServiceAccount(serviceController.ServiceName);
					if (string.IsNullOrEmpty(serviceAccount))
					{
						sQLService.IsCurrentAccount = false;
					}
					else
					{
						sQLService.IsCurrentAccount = true;
						sQLService.AccountName = serviceAccount;
					}
					list.Add(sQLService);
				}
				else if (serviceController.ServiceName.ToUpper() == "MSSQLSERVER")
				{
					SQLService sQLService2 = new SQLService();
					sQLService2.ServiceName = serviceController.ServiceName;
					sQLService2.InstanceName = hostName;
					string serviceAccount2 = this.GetServiceAccount(serviceController.ServiceName);
					if (string.IsNullOrEmpty(serviceAccount2))
					{
						sQLService2.IsCurrentAccount = false;
					}
					else
					{
						sQLService2.IsCurrentAccount = true;
						sQLService2.AccountName = serviceAccount2;
					}
					list.Add(sQLService2);
				}
			}
			if (list.Count > 0)
			{
				list.Sort(new SQLServiceCompare());
			}
			return list;
		}

		private string GetServiceAccount(string serviceName)
		{
			string result;
			try
			{
				string text = string.Empty;
				using (ManagementObject managementObject = new ManagementObject(new ManagementPath(string.Format("Win32_Service.Name='{0}'", serviceName))))
				{
					string text2 = managementObject.GetPropertyValue("StartName").ToString();
					if (text2.StartsWith("."))
					{
						text = Environment.MachineName + text2.Substring(1);
					}
					else
					{
						text = text2;
					}
				}
				result = text;
			}
			catch (Exception)
			{
				result = string.Empty;
			}
			return result;
		}

		private int DBTypeCount(string cmdText, string conStr)
		{
			object obj = SQLHelper.ExecuteScalar(conStr, CommandType.Text, cmdText, new SqlParameter[0]);
			if (obj != null)
			{
				return Convert.ToInt32(obj);
			}
			return 0;
		}
	}
}
