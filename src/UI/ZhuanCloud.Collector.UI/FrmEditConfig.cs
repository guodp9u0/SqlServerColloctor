using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace ZhuanCloud.Collector.UI
{
	internal class FrmEditConfig : Form
	{
		private IContainer components;

		private Button btnApply;

		private Button btnCancel;

		private PropertyGrid gridCollection;

		private CollectConfig initConfig;

		private Dictionary<string, string[]> propertyDic;

		private CollectConfig config;

		public CollectConfig Config
		{
			get
			{
				return this.config;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(FrmEditConfig));
			this.btnApply = new Button();
			this.btnCancel = new Button();
			this.gridCollection = new PropertyGrid();
			base.SuspendLayout();
			this.btnApply.Location = new Point(290, 497);
			this.btnApply.Name = "btnApply";
			this.btnApply.Size = new Size(80, 25);
			this.btnApply.TabIndex = 16;
			this.btnApply.Text = "应用";
			this.btnApply.UseVisualStyleBackColor = true;
			this.btnApply.Click += new EventHandler(this.btnApply_Click);
			this.btnCancel.Location = new Point(376, 497);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new Size(80, 25);
			this.btnCancel.TabIndex = 17;
			this.btnCancel.Text = "取消";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
			this.gridCollection.Location = new Point(14, 12);
			this.gridCollection.Name = "gridCollection";
			this.gridCollection.Size = new Size(442, 456);
			this.gridCollection.TabIndex = 0;
			this.gridCollection.ToolbarVisible = false;
			this.gridCollection.PropertyValueChanged += new PropertyValueChangedEventHandler(this.gridCollection_PropertyValueChanged);
			base.AutoScaleDimensions = new SizeF(6f, 12f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(473, 534);
			base.Controls.Add(this.gridCollection);
			base.Controls.Add(this.btnCancel);
			base.Controls.Add(this.btnApply);
			base.FormBorderStyle = FormBorderStyle.FixedSingle;
			base.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			base.MaximizeBox = false;
			base.MinimizeBox = false;
			base.Name = "FrmEditConfig";
			base.StartPosition = FormStartPosition.CenterScreen;
			this.Text = "ZhuanCloud 配置";
			base.Load += new EventHandler(this.FrmEditConfig_Load);
			base.ResumeLayout(false);
		}

		public FrmEditConfig(CollectConfig config)
		{
			this.InitializeComponent();
			this.initConfig = config;
			this.gridCollection.SelectedObject = config;
			this.gridCollection.Refresh();
		}

		private void FrmEditConfig_Load(object sender, EventArgs e)
		{
			this.propertyDic = new Dictionary<string, string[]>();
			this.propertyDic.Add("查询语句", new string[]
			{
				"SqlTextElapsed",
				"SqlTextValue"
			});
			this.propertyDic.Add("会话", new string[]
			{
				"SessionElapsed"
			});
			this.propertyDic.Add("性能计数器", new string[]
			{
				"PerfElapsed",
				"PerfList"
			});
			this.propertyDic.Add("对象定义", new string[]
			{
				"DBItemType",
				"IsDecrypt",
				"DBFilterList"
			});
			this.propertyDic.Add("执行计划", new string[]
			{
				"SqlPlanElapsed",
				"SqlPlanValue"
			});
			this.propertyDic.Add("错误日志", new string[]
			{
				"ErrorLogElapsed",
				"ErrorLogKeys"
			});
		}

		private void btnApply_Click(object sender, EventArgs e)
		{
			this.config = (this.gridCollection.SelectedObject as CollectConfig);
			ConfigManager configManager = new ConfigManager();
			configManager.SaveConfig(this.config);
			base.DialogResult = DialogResult.OK;
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.config = this.initConfig;
			base.DialogResult = DialogResult.Cancel;
		}

		private void gridCollection_PropertyValueChanged(object s, PropertyValueChangedEventArgs e)
		{
			CollectConfig collectConfig = this.gridCollection.SelectedObject as CollectConfig;
			if (collectConfig != null)
			{
				string a = e.ChangedItem.Label.Replace("\t", "");
				if (a == "是否收集")
				{
					string propertyName = e.ChangedItem.Parent.Label.Replace("\t", "");
					this.SetPropertyReadOnly(collectConfig, propertyName, !(bool)e.ChangedItem.Value);
				}
			}
		}

		private void SetPropertyReadOnly(CollectConfig config, string propertyName, bool enabled)
		{
			if (this.propertyDic.ContainsKey(propertyName))
			{
				string[] array = this.propertyDic[propertyName];
				string[] array2 = array;
				for (int i = 0; i < array2.Length; i++)
				{
					string name = array2[i];
					Type typeFromHandle = typeof(ReadOnlyAttribute);
					PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(config);
					AttributeCollection attributes = properties[name].Attributes;
					FieldInfo field = typeFromHandle.GetField("isReadOnly", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.CreateInstance);
					field.SetValue(attributes[typeFromHandle], enabled);
				}
			}
		}
	}
}
