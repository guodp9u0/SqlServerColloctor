using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace ZhuanCloud.Collector.UI
{
	internal class FrmAddPerfCounter : Form
	{
		private IContainer components;

		private GroupBox groupBox1;

		private GroupBox groupBox2;

		private TreeView tvPerfList;

		private Label label1;

		private ListBox lbInstance;

		private Button btnAdd;

		private Button btnDel;

		private Button btnOk;

		private Button btnCancel;

		private ListView lvPerfmon;

		private ColumnHeader columnHeader1;

		private ColumnHeader columnHeader2;

		private ColumnHeader columnHeader3;

		private List<Perf> perfList;

		public List<Perf> PerfList
		{
			get
			{
				return this.perfList;
			}
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private void InitializeComponent()
		{
			ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(FrmAddPerfCounter));
			this.groupBox1 = new GroupBox();
			this.btnAdd = new Button();
			this.lbInstance = new ListBox();
			this.label1 = new Label();
			this.tvPerfList = new TreeView();
			this.groupBox2 = new GroupBox();
			this.lvPerfmon = new ListView();
			this.columnHeader1 = new ColumnHeader();
			this.columnHeader2 = new ColumnHeader();
			this.columnHeader3 = new ColumnHeader();
			this.btnDel = new Button();
			this.btnOk = new Button();
			this.btnCancel = new Button();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			base.SuspendLayout();
			this.groupBox1.Controls.Add(this.btnAdd);
			this.groupBox1.Controls.Add(this.lbInstance);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.tvPerfList);
			this.groupBox1.Location = new Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new Size(307, 399);
			this.groupBox1.TabIndex = 12;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "可用计数器";
			this.btnAdd.Location = new Point(220, 367);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new Size(75, 23);
			this.btnAdd.TabIndex = 3;
			this.btnAdd.Text = "添加(&D) >>";
			this.btnAdd.UseVisualStyleBackColor = true;
			this.btnAdd.Click += new EventHandler(this.btnAdd_Click);
			this.lbInstance.FormattingEnabled = true;
			this.lbInstance.ItemHeight = 12;
			this.lbInstance.Location = new Point(10, 254);
			this.lbInstance.Name = "lbInstance";
			this.lbInstance.SelectionMode = SelectionMode.MultiSimple;
			this.lbInstance.Size = new Size(285, 100);
			this.lbInstance.TabIndex = 2;
			this.lbInstance.DoubleClick += new EventHandler(this.lbInstance_DoubleClick);
			this.label1.AutoSize = true;
			this.label1.Location = new Point(8, 236);
			this.label1.Name = "label1";
			this.label1.Size = new Size(101, 12);
			this.label1.TabIndex = 1;
			this.label1.Text = "选定对象的实例：";
			this.tvPerfList.HideSelection = false;
			this.tvPerfList.ItemHeight = 14;
			this.tvPerfList.Location = new Point(10, 19);
			this.tvPerfList.Name = "tvPerfList";
			this.tvPerfList.ShowLines = false;
			this.tvPerfList.Size = new Size(285, 205);
			this.tvPerfList.TabIndex = 0;
			this.tvPerfList.AfterSelect += new TreeViewEventHandler(this.tvPerfList_AfterSelect);
			this.tvPerfList.DoubleClick += new EventHandler(this.tvPerfList_DoubleClick);
			this.groupBox2.Controls.Add(this.lvPerfmon);
			this.groupBox2.Controls.Add(this.btnDel);
			this.groupBox2.Location = new Point(326, 12);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new Size(357, 399);
			this.groupBox2.TabIndex = 14;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "添加计数器";
			this.lvPerfmon.Columns.AddRange(new ColumnHeader[]
			{
				this.columnHeader1,
				this.columnHeader2,
				this.columnHeader3
			});
			this.lvPerfmon.FullRowSelect = true;
			this.lvPerfmon.Location = new Point(11, 19);
			this.lvPerfmon.Name = "lvPerfmon";
			this.lvPerfmon.Size = new Size(340, 335);
			this.lvPerfmon.TabIndex = 4;
			this.lvPerfmon.UseCompatibleStateImageBehavior = false;
			this.lvPerfmon.View = View.Details;
			this.lvPerfmon.SelectedIndexChanged += new EventHandler(this.lvPerfmon_SelectedIndexChanged);
			this.columnHeader1.Text = "对象";
			this.columnHeader1.Width = 150;
			this.columnHeader2.Text = "计数器";
			this.columnHeader2.Width = 100;
			this.columnHeader3.Text = "实例";
			this.columnHeader3.Width = 80;
			this.btnDel.Enabled = false;
			this.btnDel.Location = new Point(11, 367);
			this.btnDel.Name = "btnDel";
			this.btnDel.Size = new Size(75, 23);
			this.btnDel.TabIndex = 1;
			this.btnDel.Text = "删除(&R) <<";
			this.btnDel.UseVisualStyleBackColor = true;
			this.btnDel.Click += new EventHandler(this.btnDel_Click);
			this.btnOk.Enabled = false;
			this.btnOk.Location = new Point(524, 426);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new Size(75, 23);
			this.btnOk.TabIndex = 15;
			this.btnOk.Text = "确定";
			this.btnOk.UseVisualStyleBackColor = true;
			this.btnOk.Click += new EventHandler(this.btnOk_Click);
			this.btnCancel.Location = new Point(608, 426);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new Size(75, 23);
			this.btnCancel.TabIndex = 16;
			this.btnCancel.Text = "取消";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new EventHandler(this.btnCancel_Click);
			base.AutoScaleDimensions = new SizeF(6f, 12f);
			base.AutoScaleMode = AutoScaleMode.Font;
			base.ClientSize = new Size(695, 455);
			base.Controls.Add(this.btnCancel);
			base.Controls.Add(this.btnOk);
			base.Controls.Add(this.groupBox2);
			base.Controls.Add(this.groupBox1);
			base.FormBorderStyle = FormBorderStyle.FixedSingle;
			base.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
			base.Name = "FrmAddPerfCounter";
			base.StartPosition = FormStartPosition.CenterScreen;
			this.Text = "添加性能计数器";
			base.Load += new EventHandler(this.FrmAddPerfCounter_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			base.ResumeLayout(false);
		}

		public FrmAddPerfCounter()
		{
			this.InitializeComponent();
		}

		private void FrmAddPerfCounter_Load(object sender, EventArgs e)
		{
			this.LoadPerfCounter();
		}

		private void LoadPerfCounter()
		{
			TreeNode netNode = this.GetNetNode("Network Interface");
			this.tvPerfList.Nodes.Add(netNode);
		}

		private TreeNode GetNode(string categoryName)
		{
			PerformanceCounterCategory performanceCounterCategory = new PerformanceCounterCategory(categoryName);
			TreeNode treeNode = new TreeNode(categoryName);
			string[] instanceNames = performanceCounterCategory.GetInstanceNames();
			PerformanceCounter[] counters;
			if (instanceNames.Length > 0)
			{
				treeNode.Tag = instanceNames;
				counters = performanceCounterCategory.GetCounters("_Total");
			}
			else
			{
				counters = performanceCounterCategory.GetCounters();
			}
			if (counters.Length > 0)
			{
				PerformanceCounter[] array = counters;
				for (int i = 0; i < array.Length; i++)
				{
					PerformanceCounter performanceCounter = array[i];
					TreeNode node = new TreeNode(performanceCounter.CounterName);
					treeNode.Nodes.Add(node);
				}
			}
			return treeNode;
		}

		private TreeNode GetNetNode(string categoryName)
		{
			List<string> list = new List<string>
			{
				"Bytes Total/sec",
				"Output Queue Length"
			};
			PerformanceCounterCategory performanceCounterCategory = new PerformanceCounterCategory(categoryName);
			TreeNode treeNode = new TreeNode(categoryName);
			string[] instanceNames = performanceCounterCategory.GetInstanceNames();
			if (instanceNames.Length > 0)
			{
				treeNode.Tag = instanceNames;
				foreach (string current in list)
				{
					TreeNode node = new TreeNode(current);
					treeNode.Nodes.Add(node);
				}
			}
			return treeNode;
		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			if (this.tvPerfList.SelectedNode != null)
			{
				string[] array = null;
				if (this.lbInstance.SelectedItems.Count > 0)
				{
					array = new string[this.lbInstance.SelectedItems.Count];
					int num = 0;
				    IEnumerator enumerator = this.lbInstance.SelectedItems.GetEnumerator();
					{
						while (enumerator.MoveNext())
						{
							string text = (string)enumerator.Current;
							array[num] = text;
							num++;
						}
						goto IL_CB;
					}
				}
				if (this.tvPerfList.SelectedNode.Parent == null)
				{
					array = (this.tvPerfList.SelectedNode.Tag as string[]);
				}
				else
				{
					array = (this.tvPerfList.SelectedNode.Parent.Tag as string[]);
				}
				IL_CB:
				if (array != null)
				{
					string[] array2 = array;
					int i = 0;
					while (i < array2.Length)
					{
						string instance = array2[i];
						if (this.tvPerfList.SelectedNode.Parent != null)
						{
							goto IL_177;
						}
						if (this.tvPerfList.SelectedNode.Nodes.Count > 0) {
						    IEnumerator enumerator2 = this.tvPerfList.SelectedNode.Nodes.GetEnumerator();
							{
								while (enumerator2.MoveNext())
								{
									TreeNode treeNode = (TreeNode)enumerator2.Current;
									this.AddPerfCounter(this.tvPerfList.SelectedNode.Text, treeNode.Text, instance);
								}
								goto IL_1A3;
							}
							goto IL_177;
						}
						IL_1A3:
						i++;
						continue;
						IL_177:
						this.AddPerfCounter(this.tvPerfList.SelectedNode.Parent.Text, this.tvPerfList.SelectedNode.Text, instance);
						goto IL_1A3;
					}
				}
				else
				{
					if (this.tvPerfList.SelectedNode.Parent == null)
					{
						if (this.tvPerfList.SelectedNode.Nodes.Count <= 0)
						{
							goto IL_282;
						}
					    IEnumerator enumerator3 = this.tvPerfList.SelectedNode.Nodes.GetEnumerator();
						{
							while (enumerator3.MoveNext())
							{
								TreeNode treeNode2 = (TreeNode)enumerator3.Current;
								this.AddPerfCounter(this.tvPerfList.SelectedNode.Text, treeNode2.Text, "");
							}
							goto IL_282;
						}
					}
					this.AddPerfCounter(this.tvPerfList.SelectedNode.Parent.Text, this.tvPerfList.SelectedNode.Text, "");
				}
			}
			IL_282:
			this.btnOk.Enabled = (this.lvPerfmon.Items.Count > 0);
		}

		private void AddPerfCounter(string objectName, string counterName, string instance)
		{
			Perf perf = new Perf(objectName, counterName, instance);
			if (!perf.HasError && !this.Contrains(perf))
			{
				this.lvPerfmon.Items.Add(new ListViewItem(new string[]
				{
					perf.ObjectName,
					perf.CounterName,
					perf.InstanceName
				}));
			}
		}

		private bool Contrains(Perf perfCounter)
		{
			if (this.lvPerfmon.Items.Count > 0)
			{
				foreach (ListViewItem listViewItem in this.lvPerfmon.Items)
				{
					if (listViewItem.SubItems[0].Text == perfCounter.ObjectName && listViewItem.SubItems[1].Text == perfCounter.CounterName && listViewItem.SubItems[2].Text == perfCounter.InstanceName)
					{
						return true;
					}
				}
				return false;
			}
			return false;
		}

		private void lvPerfmon_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.btnDel.Enabled = (this.lvPerfmon.SelectedItems.Count > 0);
		}

		private void btnDel_Click(object sender, EventArgs e)
		{
			if (this.lvPerfmon.SelectedItems.Count > 0)
			{
				ListViewItem listViewItem = this.lvPerfmon.SelectedItems[0];
				int index = listViewItem.Index;
				this.lvPerfmon.Items.Remove(listViewItem);
				if (index >= 1)
				{
					this.lvPerfmon.Items[index - 1].Selected = true;
				}
				else if (this.lvPerfmon.Items.Count > 0)
				{
					this.lvPerfmon.Items[this.lvPerfmon.Items.Count - 1].Selected = true;
				}
				this.btnOk.Enabled = (this.lvPerfmon.Items.Count > 0);
			}
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			if (this.lvPerfmon.Items.Count > 0)
			{
				this.perfList = new List<Perf>();
				foreach (ListViewItem listViewItem in this.lvPerfmon.Items)
				{
					Perf perf = new Perf(listViewItem.SubItems[0].Text, listViewItem.SubItems[1].Text, listViewItem.SubItems[2].Text);
					if (!perf.HasError)
					{
						this.perfList.Add(perf);
					}
				}
			}
			base.DialogResult = DialogResult.OK;
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.perfList = null;
			base.DialogResult = DialogResult.Cancel;
		}

		private void tvPerfList_AfterSelect(object sender, TreeViewEventArgs e)
		{
			this.lbInstance.Items.Clear();
			TreeNode treeNode;
			if (e.Node.Parent != null)
			{
				treeNode = e.Node.Parent;
			}
			else
			{
				treeNode = e.Node;
			}
			if (treeNode.Tag != null)
			{
				string[] array = treeNode.Tag as string[];
				if (array != null)
				{
					this.lbInstance.Items.Add("_Total");
					string[] array2 = array;
					for (int i = 0; i < array2.Length; i++)
					{
						string text = array2[i];
						if (!(text == "_Total"))
						{
							this.lbInstance.Items.Add(text);
						}
					}
				}
			}
		}

		private void tvPerfList_DoubleClick(object sender, EventArgs e)
		{
			if (this.tvPerfList.SelectedNode.Parent != null)
			{
				this.btnAdd.PerformClick();
			}
		}

		private void lbInstance_DoubleClick(object sender, EventArgs e)
		{
			this.btnAdd.PerformClick();
		}
	}
}
